# rubocop:disable Style/FrozenStringLiteralComment

# jekyll_config.rb
# Maintains _config.yml in a String
#
# Usage:
#   config = new JekyllConfig(page)
#   config[:key] ||= value
#   config.save!(committer)
#
# Manipulates yaml text using only simple/naive regexp matches
# a. to preserve user comments, style etc.
# b. for simplicity and safety.
#
# The tradeoff is that this will break the config for users whose _config.yml
# contains weird encodings, line endings, or YAML with anchors, !!str etc.
#
# TODO: - support for clearing values (remove entire line)
#       - test and figure out what to do with windows files.
#
module GitHub
  module Pages
    class JekyllConfig

      def initialize(page)
        @page = page
        @str = config_from_blob || ""
      end

      # Look for `key: value` on a line.
      #
      # Returns string value or nil
      def [](key)
        match = regexp(key).match(@str)
        match ? from_yaml_string(match[1]) : nil
      end

      # Set `key: value` adding a new line if necessary.
      #
      # Returns self.
      def []=(key, val)
        val = to_yaml_string(val)
        match = regexp(key).match(@str)
        if match
          @str[match.begin(1), match[1].length] = val
        else
          newline = @str == "" ? "" : "\n"
          @str << "#{newline}#{key}: #{val}"
        end
        self
      end

      def to_s
        @str
      end

      # Write _config.yml in a single commit
      # Does not require that the pages branch exist
      #
      # Returns self.
      def save!(committer, message = "Save _config.yml")
        repository.heads.read(@page.source_branch).append_commit(
          {message: message, committer: committer},
          committer,
        ) do |files|
          files.add(file_path, @str)
        end
        self
      end

      def file_path
        @page.source_file_path("_config.yml")
      end

      private

      def repository
        @page.repository
      end

      # YAML string decode value
      def from_yaml_string(val)
        YAML.safe_load("x: #{val}")["x"]
      rescue Psych::BadAlias, Psych::DisallowedClass, Psych::SyntaxError
        val
      end

      # YAML string encode value
      def to_yaml_string(val)
        if val.blank?
          ""
        elsif try_unquoted_yaml?(val)
          val
        else
          val.to_s.inspect
        end
      end

      def try_unquoted_yaml?(val)
        YAML.safe_load("x: #{val}")["x"] == val
      rescue Psych::BadAlias, Psych::DisallowedClass, Psych::SyntaxError
        false
      end

      # Regexp to match a key: value
      # Does not catch mismatched quotes or other YAML syntax errors.
      # After matching, the matched value will be in match[1].
      def regexp(key)
        Regexp.new("^#{key}: +(([\"']?).*?\\2) *$")
      end

      # Read _config.yml, if one exists.
      #
      # Returns a String or nil
      def config_from_blob
        ref = @page.pages_ref
        return nil if ref.nil?
        blob = repository.blob(ref.commit.oid, file_path)
        return nil if blob.nil?
        blob.data
      rescue GitRPC::InvalidObject
        nil
      end
    end
  end
end
