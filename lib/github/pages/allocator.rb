# frozen_string_literal: true

require "github/sql"
require "github/partition_usage"

module GitHub::Pages::Allocator
  extend self

  class AllocationFailed < StandardError
    def areas_of_responsibility
      [:pages]
    end
  end

  # Finds the hosts with the greatest amount of free space on their storage disks.
  # Calculates based on percentage of free space.
  # If site_size_kb is given, then hosts returned will have a guaranteed size of
  #
  # is_voting -- the Boolean value of whether the host is "voting" or not.
  # min_replicas -- the Integer number of hosts to select
  # data_center -- (optional) the String value of the datacenter to limit results on (e.g. "cp1", "va3", "us-east-1")
  # site_size_kb -- (optional) the Integer kilobyte size of the site being written to the storage tier.
  #
  # Returns an Array of hosts which have the greatest amount of free disk space.
  def least_loaded_hosts(is_voting:, min_replicas: nil, data_center: nil, site_size_kb: nil)
    return [] if min_replicas.is_a?(Integer) && min_replicas <= 0

    if is_voting
      min_replicas ||= GitHub.pages_replica_count
    else
      min_replicas ||= GitHub.pages_non_voting_replica_count
    end

    return [] if min_replicas.nil?

    hosts = hosts_query(
      limit: min_replicas,
      non_voting: is_voting ? 0 : 1,
      min_size_kb: site_size_kb || 0,
      data_center: data_center,
    ).values

    if hosts.length != min_replicas
      raise AllocationFailed, "could not find #{min_replicas} online #{is_voting ? "voting" : "non-voting"} fileservers"
    end

    Array(hosts)
  end

  # Fetch a list of voting and non-voting hosts and create a Hash
  # encapsulating each.
  #
  # voting -- the Integer number of voting hosts to return.
  # non_voting -- the Integer number of non-voting hosts to return.
  # site_size_kb -- (optional) the Integer kilobyte size of the site being written to the storage tier.
  #
  # Returns a Hash with 2 keys: :voting, and :non_voting. Each key has a corresponding value of an Array of host names.
  def hosts_for_new_replicas(voting:, non_voting:, site_size_kb: nil, replication_strategy: nil)
    replication_strategy ||= GitHub::Pages::ReplicationStrategy.new(replica_counts: voting)
    voting_hosts = replication_strategy.distribute do |data_center, replica_count|
      GitHub.dogstats.count("pages.allocate", replica_count, tags: ["voting:true", "datacenter:#{data_center}", "strategy:#{replication_strategy}"])
      least_loaded_hosts(is_voting: true, min_replicas: replica_count, site_size_kb: site_size_kb, data_center: data_center)
    end

    GitHub.dogstats.count("pages.allocate", non_voting || 0, tags: ["voting:false", "strategy:none"])
    non_voting_hosts = least_loaded_hosts(is_voting: false, min_replicas: non_voting, site_size_kb: site_size_kb)

    { voting: voting_hosts, non_voting: non_voting_hosts }
  end

  def get_hosts
    ApplicationRecord::Domain::Repositories.github_sql.values <<-SQL
      SELECT host FROM pages_fileservers
        WHERE online = 1
          AND host != 'localhost'
    SQL
  end

  def update_disk_usage!(hostname:)
    info = GitHub::PartitionUsage.df([GitHub.pages_dir]).first
    inodes_info = GitHub::PartitionUsage.df_i([GitHub.pages_dir]).first

    sql_context = {
      hostname: hostname,
      disk_free: info.free,
      disk_used: info.used,
      inodes_free: inodes_info.free,
      inodes_used: inodes_info.used,
    }

    ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, sql_context)
      UPDATE
        pages_fileservers
      SET
        disk_free  = :disk_free,
        disk_used  = :disk_used,
        inodes_free = :inodes_free,
        inodes_used = :inodes_used,
        updated_at = NOW()
      WHERE host = :hostname
    SQL
  end

  private

  def hosts_query(non_voting:, min_size_kb:, limit:, data_center: nil)
    binds = {
      non_voting: non_voting,
      min_size_kb: min_size_kb,
      data_center: data_center,
      limit: limit,
    }

    query = ApplicationRecord::Domain::Repositories.github_sql.new(<<-SQL, binds)
      SELECT host FROM pages_fileservers
      WHERE online = 1 AND embargoed = 0 AND non_voting = :non_voting AND disk_free > :min_size_kb
    SQL
    query.add("AND datacenter = :data_center") if !GitHub.enterprise? && data_center.present? && data_center != GitHub.default_datacenter
    query.add("ORDER BY disk_free DESC, host ASC LIMIT :limit")

    query
  end
end
