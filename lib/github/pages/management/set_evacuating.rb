# frozen_string_literal: true

require "logger"
require "github/sql"

module GitHub::Pages::Management
  class SetEvacuating



    attr_reader :host, :evacuating, :delegate

    def initialize(host:, evacuating:, delegate:)
      @host = host
      @evacuating = !!evacuating
      @delegate = delegate
      nil
    end

    def perform
      sql = ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, host: host, evacuating: evacuating)
        UPDATE pages_fileservers SET evacuating = :evacuating WHERE host = :host
      SQL

      if sql.affected_rows == 0
        delegate.log "No such host=#{host}"
        false
      else
        delegate.log "Set host=#{host} evacuating=#{evacuating}."
        true
      end
    end
  end
end
