# frozen_string_literal: true

require "logger"
require "github/sql"

module GitHub::Pages::Management
  class SetFileserverAttribute

    ALLOWED_ATTRIBUTES = [
      :datacenter, :ip, :online, :rack
    ].freeze

    attr_reader :host, :attribute, :value, :delegate

    def initialize(host:, attribute:, value:, delegate:)
      @host = host
      @attribute = attribute
      @delegate = delegate
      @value = value
      nil
    end

    def perform
      unless ALLOWED_ATTRIBUTES.include?(attribute)
        delegate.log "Unable to set attribute '#{attribute}' for host=#{host}"
        return false
      end

      sql = ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, host: host, attribute: GitHub::SQL::Literal.new(attribute), value: value || GitHub::SQL::NULL)
        UPDATE pages_fileservers SET :attribute = :value WHERE host = :host
      SQL

      if sql.affected_rows == 0
        delegate.log "No such host=#{host}"
        return false
      else
        delegate.log "Set host=#{host} #{attribute}=#{value.inspect}"
        return true
      end
    end
  end
end
