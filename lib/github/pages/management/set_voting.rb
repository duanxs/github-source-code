# frozen_string_literal: true

require "logger"
require "github/sql"

module GitHub::Pages::Management
  class SetVoting



    attr_reader :host, :voting, :delegate

    def initialize(host:, voting:, delegate:)
      @host = host
      @voting = !!voting
      @delegate = delegate
      nil
    end

    def perform
      sql = ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, host: host, non_voting: !voting)
        UPDATE pages_fileservers SET non_voting = :non_voting WHERE host = :host
      SQL

      if sql.affected_rows == 0
        delegate.log "No such host=#{host}"
        return false
      else
        delegate.log "Set host=#{host} voting=#{voting}"
        return true
      end
    end
  end
end
