# frozen_string_literal: true

require "logger"
require "github/sql"

module GitHub::Pages::Management
  class SetEmbargoed



    attr_reader :host, :embargoed, :delegate

    def initialize(host:, embargoed:, delegate:)
      @host = host
      @embargoed = !!embargoed
      @delegate = delegate
      nil
    end

    def perform
      success = false

      ApplicationRecord::Domain::Repositories.github_sql.transaction do
        # If trying to unembargo a host but it's evacuating, then fail.
        evacuating = ApplicationRecord::Domain::Repositories.github_sql.value(<<-SQL, host: host)
          SELECT evacuating FROM pages_fileservers WHERE host = :host FOR UPDATE
        SQL
        if evacuating.to_i != 0 && embargoed == false
          delegate.log "Cannot set embargoed=#{embargoed} for host=#{host} when evacuating=#{evacuating}"
          raise ActiveRecord::Rollback
        end

        sql = ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, host: host, embargoed: embargoed)
          UPDATE pages_fileservers SET embargoed = :embargoed WHERE host = :host
        SQL

        if sql.affected_rows == 0
          delegate.log "No such host=#{host}"
        else
          delegate.log "Set host=#{host} embargoed=#{embargoed}"
          success = true
        end
      end

      success
    end
  end
end
