# frozen_string_literal: true

require "github-launch"
require "github/launch_client"

module GitHub
  module LaunchClient
    module RunnerGroups
      include Instrumentation::Model

      GROUP_VISIBILITY_ALL = :ALL
      GROUP_VISIBILITY_SELECTED = :SELECTED
      GROUP_VISIBILITY_PRIVATE = :PRIVATE

      VALID_GROUP_VISIBILITIES = [
        GROUP_VISIBILITY_ALL,
        GROUP_VISIBILITY_SELECTED,
        GROUP_VISIBILITY_PRIVATE
      ].to_set.freeze

      TO_VISIBILITY_MAP = {
        GROUP_VISIBILITY_ALL => "all",
        GROUP_VISIBILITY_SELECTED => "selected",
        GROUP_VISIBILITY_PRIVATE => "private",
      }.freeze

      FROM_VISIBILITY_MAP = {
        "all" => GROUP_VISIBILITY_ALL,
        "selected" => GROUP_VISIBILITY_SELECTED,
        "private" => GROUP_VISIBILITY_PRIVATE,
      }.freeze

      TO_UPDATE_VISIBILITY_MAP = {
        GROUP_VISIBILITY_ALL => :UPDATE_ALL,
        GROUP_VISIBILITY_SELECTED => :UPDATE_SELECTED,
        GROUP_VISIBILITY_PRIVATE => :UPDATE_PRIVATE,
      }.freeze

      def self.list_groups(owner:, include_runners: false)
        enforce_service_available!

        GitHub.launch_runnergroups.list_groups GitHub::Launch::Services::Runnergroups::ListGroupsRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          plan_owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: plan_owner_for(owner).global_relay_id),
          include_runners: include_runners,
          is_enterprise_owner: !owner.organization?,
        )
      end

      def self.get_group(owner:, group_id:, include_runners: false)
        enforce_service_available!
        GitHub.launch_runnergroups.get_group GitHub::Launch::Services::Runnergroups::GetGroupRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          plan_owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: plan_owner_for(owner).global_relay_id),
          group_id: group_id,
          include_runners: include_runners,
          is_enterprise_owner: !owner.organization?,
        )
      end

      def self.create_group(actor:, owner:, name:, runner_ids: [], visibility: GROUP_VISIBILITY_SELECTED, selected_targets: [])
        enforce_service_available!

        selected_targets = selected_targets_to_global_ids(selected_targets)

        request = GitHub::Launch::Services::Runnergroups::CreateGroupRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          name: name,
          runner_ids: runner_ids,
          visibility: visibility,
          selected_targets: selected_targets,
        )

        response = GitHub.launch_runnergroups.create_group(request)

        instrument_event(owner: owner, actor: actor, runner_group_id: response.runner_group.id, event: "runner_group_created")

        response
      end

      def self.delete_group(actor:, owner:, group_id:)
        enforce_service_available!

        response = GitHub.launch_runnergroups.delete_group GitHub::Launch::Services::Runnergroups::DeleteGroupRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          group_id: group_id,
        )

        instrument_event(owner: owner, actor: actor, runner_group_id: group_id, event: "runner_group_removed")

        response
      end

      def self.update_group(actor:, owner:, group_id:, name:, visibility:, selected_targets: nil)
        enforce_service_available!

        if selected_targets.present?
          selected_targets = selected_targets_to_global_ids(selected_targets)
        end

        request = GitHub::Launch::Services::Runnergroups::UpdateGroupRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          plan_owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: plan_owner_for(owner).global_relay_id),
          group_id: group_id,
          name: name,
          update_visibility: TO_UPDATE_VISIBILITY_MAP[visibility],
          selected_targets: selected_targets,
        )

        response = GitHub.launch_runnergroups.update_group(request)

        if name.present?
          instrument_event(owner: owner, actor: actor, runner_group_id: group_id, event: "runner_group_renamed")
        end
        if visibility.present?
          instrument_event(owner: owner, actor: actor, runner_group_id: group_id, visibility: visibility, event: "runner_group_visiblity_updated")
        end

        response
      end

      def self.update_runners(actor:, owner:, group_id:, runner_ids:)
        enforce_service_available!

        request = GitHub::Launch::Services::Runnergroups::UpdateRunnersRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          group_id: group_id,
          runner_ids: runner_ids,
        )

        response = GitHub.launch_runnergroups.update_runners(request)

        instrument_event(owner: owner, actor: actor, runner_group_id: group_id, runner_ids: runner_ids, event: "runner_group_runners_updated")

        response
      end

      def self.add_runners(actor:, owner:, group_id:, runner_ids:)
        enforce_service_available!
        response = GitHub.launch_runnergroups.add_runners GitHub::Launch::Services::Runnergroups::AddRunnersRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          group_id: group_id,
          runner_ids: runner_ids,
        )

        instrument_event(owner: owner, actor: actor, runner_group_id: group_id, runner_ids: runner_ids, event: "runner_group_runners_added")

        response
      end

      def self.remove_runner(actor:, owner:, group_id:, runner_id:)
        enforce_service_available!
        response = GitHub.launch_runnergroups.remove_runner GitHub::Launch::Services::Runnergroups::RemoveRunnerRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          group_id: group_id,
          runner_id: runner_id,
        )

        instrument_event(owner: owner, actor: actor, runner_group_id: group_id, runner_id: runner_id, event: "runner_group_runner_removed")

        response
      end

      def self.update_targets(owner:, group_id:, selected_targets:)
        enforce_service_available!

        request = GitHub::Launch::Services::Runnergroups::UpdateTargetsRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          group_id: group_id,
          target_ids: selected_targets_to_global_ids(selected_targets),
        )

        GitHub.launch_runnergroups.update_targets(request)
      end

      def self.add_target(owner:, group_id:, selected_target:)
        enforce_service_available!
        GitHub.launch_runnergroups.add_target GitHub::Launch::Services::Runnergroups::AddTargetRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          group_id: group_id,
          target_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: selected_target),
        )
      end

      def self.remove_target(owner:, group_id:, selected_target:)
        enforce_service_available!
        GitHub.launch_runnergroups.remove_target GitHub::Launch::Services::Runnergroups::RemoveTargetRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          group_id: group_id,
          target_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: selected_target),
        )
      end

      def self.enforce_service_available!
        return if GitHub.launch_runnergroups.present?
        raise LaunchClient::ServiceUnavailable, "runner groups service is not configured"
      end
      private_class_method :enforce_service_available!

      def self.plan_owner_for(owner)
        return owner unless owner.organization?
        return owner.business if owner.business.present?

        owner
      end
      private_class_method :plan_owner_for

      def self.selected_targets_to_global_ids(selected_targets)
        selected_targets.map do |target|
          GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: target)
        end
      end
      private_class_method :selected_targets_to_global_ids

      def self.instrument_event(owner:, event:, actor:, **args)
        payload = args.merge(
          actor: actor,
        )

        case owner
        when Organization
          payload[:org] = owner
          GitHub.instrument "#{owner.event_prefix}.#{event}", payload
        when Business
          payload[:business] = owner
          GitHub.instrument "enterprise.#{event}", payload
        end
      end
      private_class_method :instrument_event
    end
  end
end
