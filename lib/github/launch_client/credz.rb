# frozen_string_literal: true

require "github-launch"
require "github/launch_client"

module GitHub
  module LaunchClient
    module Credz
      include Instrumentation::Model

      class Validation
        attr_reader :succeeded, :error

        def initialize(succeded, error)
          @succeeded = succeded
          @error = error
        end

        def succeeded?
          succeeded
        end

        def ==(other)
          other.succeeded == succeeded &&
            other.error == error
        end
      end

      SECRET_VALUE_MAX_SIZE = 64_000
      SECRET_KEY_MAX_SIZE = 300
      SECRET_KEY_RESERVED_PREFIX = "GITHUB_"
      SECRET_KEY_VALID_CHAR_REGEX = /\A[A-Za-z_][A-Za-z0-9_]*\z/
      SECRET_ORG_MAX = 1_000
      SECRET_REPO_MAX = 100

      CREDENTIAL_VISIBILITY_OWNER = :VISIBILITY_OWNER
      CREDENTIAL_VISIBILITY_ALL_REPOS = :VISIBILITY_ALL_REPOSITORIES
      CREDENTIAL_VISIBILITY_PRIVATE_REPOS = :VISIBILITY_PRIVATE_REPOSITORIES
      CREDENTIAL_VISIBILITY_SELECTED_REPOS = :VISIBILITY_SELECTED_REPOSITORIES

      VALID_CREDENTIAL_VISIBILITIES = [
        CREDENTIAL_VISIBILITY_ALL_REPOS,
        CREDENTIAL_VISIBILITY_PRIVATE_REPOS,
        CREDENTIAL_VISIBILITY_SELECTED_REPOS
      ].to_set.freeze

      TO_VISIBILITY_MAP = {
        CREDENTIAL_VISIBILITY_ALL_REPOS => "all",
        CREDENTIAL_VISIBILITY_PRIVATE_REPOS => "private",
        CREDENTIAL_VISIBILITY_SELECTED_REPOS => "selected"
      }.freeze

      FROM_VISIBILITY_MAP = {
        "all" => CREDENTIAL_VISIBILITY_ALL_REPOS,
        "private" => CREDENTIAL_VISIBILITY_PRIVATE_REPOS,
        "selected" => CREDENTIAL_VISIBILITY_SELECTED_REPOS
      }.freeze

      def self.list_repository_secrets(app:, repository:, actor:)
        enforce_service_available!
        enforce_parameters_present!(app, repository, actor, "bogus_key_to_force_arg_presence_requirement")
        GitHub.launch_credz.list_secrets_for_repository \
          GitHub::Launch::Services::Credz::ListSecretsForRepositoryRequest.new(
            repository: GitHub::Launch::Services::Credz::RepositoryWithOwner.new(
              repository: GitHub::Launch::Services::Credz::Repository.new(
                global_id: repository.global_relay_id,
              ),
              owner: credential_owner_for(repository.owner),
            ),
            integration: app.global_relay_id,
            is_private: repository.private?,
          ),
          metadata: GitHub::LaunchClient.request_metadata
      end

      def self.list_credentials(app:, owner:, actor:)
        enforce_service_available!
        enforce_parameters_present!(app, owner, actor, "bogus_key_to_force_arg_presence_requirement")

        GitHub.launch_credz.list \
          GitHub::Launch::Services::Credz::ListRequest.new(
            owner: credential_owner_for(owner),
            integration: app.global_relay_id,
          ),
          metadata: GitHub::LaunchClient.request_metadata
      end

      def self.fetch_credential(app:, owner:, actor:, key:)
        enforce_service_available!
        enforce_parameters_present!(app, owner, actor, key)

        GitHub.launch_credz.fetch \
          GitHub::Launch::Services::Credz::FetchRequest.new(
            repository: GitHub::Launch::Services::Credz::Repository.new(
              global_id: owner.global_relay_id,
            ),
            credential: GitHub::Launch::Services::Credz::Credential.new(
              owner: credential_owner_for(owner),
              integration: app.global_relay_id,
              name: key,
            ),
          ),
          metadata: GitHub::LaunchClient.request_metadata
      end

      def self.store_credential(app:, owner:, actor:, key:, value:, visibility: CREDENTIAL_VISIBILITY_OWNER, selected_repositories: [])
        enforce_service_available!
        enforce_parameters_present!(app, owner, actor, key)
        raise ArgumentError, "value parameter required" unless value.present?

        response = GitHub.launch_credz.store \
          GitHub::Launch::Services::Credz::StoreRequest.new(
            credential: GitHub::Launch::Services::Credz::Credential.new(
              owner: credential_owner_for(owner),
              integration: app.global_relay_id,
              name: key,
              value: value.to_s,
              visibility: visibility,
              selected_repositories: selected_repositories.map { |repository_id| GitHub::Launch::Services::Credz::Repository.new(global_id: repository_id) },
            ),
          ),
          metadata: GitHub::LaunchClient.request_metadata

        event = response.credential.created_at == response.credential.updated_at ? "create_actions_secret" : "update_actions_secret"
        instrument_event(owner: owner, actor: actor, key: key, event: event, visibility: visibility, updated_value: true)

        response
      end

      def self.update_credential(app:, owner:, actor:, key:, value: "", visibility: CREDENTIAL_VISIBILITY_OWNER, selected_repositories: [])
        enforce_service_available!
        enforce_parameters_present!(app, owner, actor, key)

        response = GitHub.launch_credz.store \
          GitHub::Launch::Services::Credz::StoreRequest.new(
            credential: GitHub::Launch::Services::Credz::Credential.new(
              owner: credential_owner_for(owner),
              integration: app.global_relay_id,
              name: key,
              value: value.to_s,
              visibility: visibility || CREDENTIAL_VISIBILITY_OWNER,
              selected_repositories: selected_repositories.map { |repository_id| GitHub::Launch::Services::Credz::Repository.new(global_id: repository_id) },
            ),
          ),
          metadata: GitHub::LaunchClient.request_metadata

        event = "update_actions_secret"
        instrument_event(event: event, owner: owner, actor: actor, key: key, visibility: visibility, updated_value: value.present?)

        response
      end

      def self.delete_credential(app:, owner:, actor:, key:)
        enforce_service_available!
        enforce_parameters_present!(app, owner, actor, key)

        response = GitHub.launch_credz.delete \
          GitHub::Launch::Services::Credz::DeleteRequest.new(
            repository: GitHub::Launch::Services::Credz::Repository.new(
              global_id: owner.global_relay_id,
            ),
            credential: GitHub::Launch::Services::Credz::Credential.new(
              owner: credential_owner_for(owner),
              integration: app.global_relay_id,
              name: key,
            ),
          ),
          metadata: GitHub::LaunchClient.request_metadata

        instrument_event(owner: owner, actor: actor, key: key, event: "remove_actions_secret")

        response
      end

      def self.enforce_service_available!
        return if GitHub.launch_credz.present?
        raise LaunchClient::ServiceUnavailable, "credz service is not configured"
      end
      private_class_method :enforce_service_available!

      def self.enforce_parameters_present!(app, owner, actor, key)
        raise ArgumentError, "actor parameter required" unless actor.present?
        raise ArgumentError, "app parameter required" unless app.present?
        raise ArgumentError, "owner parameter required" unless owner.present?
        raise ArgumentError, "key parameter required" unless key.present?
      end
      private_class_method :enforce_parameters_present!

      def self.instrument_event(owner:, actor:, key:, event:, visibility: nil, updated_value: false)
        payload = {
          actor: actor,
          key: key,
          updated_value: updated_value,
        }.tap do |p|
          p[:visibility] = TO_VISIBILITY_MAP[visibility] unless visibility.nil?
        end

        if owner.is_a? Repository
          repo = owner
          payload[:repo] = repo
          payload[:org] = repo.owner if repo.owner.is_a? Organization
        elsif owner.is_a? Organization
          payload[:org] = owner
        end

        GitHub.instrument "#{owner.event_prefix}.#{event}", payload
      end
      private_class_method :instrument_event

      def self.validate_secret(key, encrypted_value)
        unless key =~ SECRET_KEY_VALID_CHAR_REGEX && !key.upcase.start_with?(SECRET_KEY_RESERVED_PREFIX) && key.length <= SECRET_KEY_MAX_SIZE
          return Validation.new(false, "Failed to add secret. Name is invalid")
        end
        if encrypted_value.bytesize > Credz::SECRET_VALUE_MAX_SIZE
          return Validation.new(false, "Failed to add secret. Value is too large.")
        end
        Validation.new(true, "")
      end

      def self.validate_org_secret(key, encrypted_value, visibility)
        unless VALID_CREDENTIAL_VISIBILITIES.include?(visibility)
          return Validation.new(false, "Failed to add secret. Unknown visibility.")
        end

        self.validate_secret(key, encrypted_value)
      end

      def self.credential_owner_for(owner)
        if owner.is_a? Repository
          GitHub::Launch::Services::Credz::CredentialOwner.new(
            repository: GitHub::Launch::Services::Credz::Repository.new(
              global_id: owner.global_relay_id,
            ),
          )
        elsif owner.is_a? Organization
          GitHub::Launch::Services::Credz::CredentialOwner.new(
            organization: GitHub::Launch::Services::Credz::Organization.new(
              global_id: owner.global_relay_id,
            ),
          )
        elsif owner.is_a? User
          GitHub::Launch::Services::Credz::CredentialOwner.new(
            user: GitHub::Launch::Services::Credz::User.new(
              global_id: owner.global_relay_id,
            ),
          )
        end
      end
      private_class_method :credential_owner_for
    end
  end
end
