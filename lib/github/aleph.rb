# frozen_string_literal: true

require "aleph"

module GitHub
  class Aleph
    class ReportableError < StandardError; end

    SERVICE_URL         = "#{GitHub.aleph_url}/twirp"
    SERVICE_NAME        = "aleph"
    HAYSTACK_APP        = "github-aleph-client"
    IS_PUBLIC_HEADER    = "X-GitHub-Is-Public"
    FEATURE_FLAG_HEADER = "X-GitHub-Feature-Flags"

    # Some characters are not valid for GitHub.flipper names.
    # This regex is used to remove invalid characters from Linguist language names
    # when converting a Linguist language name to the derived format used with GitHub.flipper.
    INVALID_CHARS = /[-+\s\.]/

    # For these errors, stats will be recorded (see GitHub::FaradayMiddleware::Datadog)
    # but callers of aleph should gracefully degrade to render content without aleph data.
    ERRORS_TO_IGNORE = [
      Timeout::Error,
      Faraday::TimeoutError,
      Faraday::SSLError,
      Faraday::ClientError,
      Faraday::ConnectionFailed,
    ]

    # Public: Find the location of the definition for a given symbol. `textDocument/definition`
    def self.text_document_definition(repo:, commit_oid:, path:, row:, col:, backend: :AUTO)
      with_error_reporting do
        client.text_document_definition(
          repo_id: repo.id,
          commit_oid: commit_oid,
          path: path,
          row: row,
          col: col,
          backend: backend,
          opt_headers: { IS_PUBLIC_HEADER => repo.public?.to_s },
        )
      end
    end

    # Public: Find the locations of references of a given symbol. `textDocument/references`
    def self.text_document_references(repo:, commit_oid:, path:, row:, col:, backend: :AUTO)
      with_error_reporting do
        client.text_document_references(
          repo_id: repo.id,
          commit_oid: commit_oid,
          path: path,
          row: row,
          col: col,
          backend: backend,
          opt_headers: { IS_PUBLIC_HEADER => repo.public?.to_s },
        )
      end
    end

    # Public: Find the symbols matching the given symbol at a specific repo and SHA.
    #
    # repo_id  - Repo ID to look up.
    # sha      - The sha to look for tags.
    # symbol   - The symbol to match.
    # language - The language of the repo.
    #
    # Returns a FindSymbolsResponse containing an Array of Definitions (see the Aleph proto file).
    def self.find_symbols(repo_id:, sha:, query:, language:, is_public:, features: [])
      with_error_reporting do
        client.find_symbols(repo_id: repo_id, sha: sha, query: query, language: language, opt_headers: { IS_PUBLIC_HEADER => is_public.to_s, FEATURE_FLAG_HEADER => features })
      end
    end

    # Public: Find references matching the given symbol at a specific repo and SHA.
    #
    # repo_id  - Repo ID to look up.
    # sha      - The sha to look for tags.
    # symbol   - The symbol to match.
    # language - The language of the repo.
    #
    # Returns a FindSymbolsResponse containing an Array of References (see the Aleph proto file) or nil on error.
    def self.find_symbol_references(repo_id:, sha:, query:, language:, is_public:, features: [])
      with_error_reporting do
        client.find_symbol_references(repo_id: repo_id, sha: sha, query: query, language: language, opt_headers: { IS_PUBLIC_HEADER => is_public.to_s, FEATURE_FLAG_HEADER => features })
      end
    end

    # Public: Find the symbols for a specific repo, SHA, and path.
    #
    # repo_id  - Repo ID to look up.
    # sha      - The sha to look for tags.
    # path     - The filepath to look for tags.
    #
    # Returns a FindSymbolsResponse containing an Array of Definitions (see the Aleph proto file).
    def self.find_symbols_for_path(repo_id:, sha:, path:, is_public:, features: [])
      with_error_reporting do
        client.find_symbols_for_path(repo_id: repo_id, sha: sha, path: path, opt_headers: { IS_PUBLIC_HEADER => is_public.to_s, FEATURE_FLAG_HEADER => features })
      end
    end

    def self.with_error_reporting
      response = yield
      if response.nil?
        Failbot.report(ReportableError.new("Nil response from aleph"), {app: HAYSTACK_APP})
      elsif response.error && response.error.code != :not_found
        Failbot.report(ReportableError.new(response.error.msg), {app: HAYSTACK_APP}.merge(response.error.to_h))
      end
      response
    rescue *ERRORS_TO_IGNORE
      nil
    rescue => e
      # These should be fixed, report to haystack
      Failbot.report(e, {app: HAYSTACK_APP})
      nil
    end

    def self.client
      @client ||= begin
        connection = Faraday.new(url: SERVICE_URL) do |conn|
          conn.use GitHub::FaradayMiddleware::RequestID
          conn.use GitHub::FaradayMiddleware::Datadog, stats: GitHub.dogstats, service_name: SERVICE_NAME
          conn.use Faraday::Resilient, name: SERVICE_NAME, options: {
            instrumenter: GitHub,
            sleep_window_seconds: 10,
            error_threshold_percentage: 5,
            window_size_in_seconds: 30,
            bucket_size_in_seconds: 5,
          }
          conn.options[:open_timeout] = 0.1 # connection open timeout in seconds.
          conn.options[:timeout] = 1.0      # read timeout in seconds.
          conn.adapter :persistent_excon
        end
        ::Aleph::Client.new(connection, hmac_key: GitHub.aleph_api_hmac_key)
      end
    end

    # Public: Converts a language name to the format used by Aleph's code nav feature flipper format.
    #
    # language_name - language name to convert.
    #
    # Returns a symbol representing a feature flag (e.g. GitHub.flipper[:aleph_language_ruby]).
    def self.convert_language_name(language_name)
      "aleph_language_#{clean_language_name(language_name)}".to_sym
    end

    # Public: Converts a language name to the format used by Aleph's darkship code nav feature flipper format.
    #
    # language_name - language name to convert.
    #
    # Returns a symbol representing a feature flag (e.g. GitHub.flipper[:aleph_darkship_language_ruby]).
    def self.convert_darkship_language_name(language_name)
      "aleph_darkship_language_#{clean_language_name(language_name)}".to_sym
    end

    # Public: Scrubs a Linguist language name to remove special symbols.
    #
    # language_name - language name to sanitize.
    #
    # Returns a sanitized string (e.g. Given "HTML+ERB" this method returns "html_erb").
    def self.clean_language_name(language_name)
      language_name&.gsub(INVALID_CHARS, "_")&.downcase
    end

    # Tests are stubbed so that calls to aleph hit this FakeServer instead.
    class FakeServer
      def self.call(env)
        # For now, just return an empty response. Eventually this could be fleshed
        # out to return sensible dummy data.
        [200, {"Content-Type" => "application/protobuf"}, [""]] # Empty response from aleph
      end
    end
  end
end
