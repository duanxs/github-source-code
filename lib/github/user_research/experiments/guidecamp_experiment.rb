# frozen_string_literal: true

module GitHub
  module UserResearch
    # UserExperiment to see the impact of an improved guidecamp vs bootcamp for users
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::GUIDECAMP_EXPERIMENT do
      experiment_name UserExperimentCohort::GUIDECAMP_EXPERIMENT.to_s.humanize

      # We want new users who aren't marked spammy.
      qualify do |user|
        !user.spammy? &&
        (Time.now - user.created_at) <= 24.hours
      end

      # This experiment has 4 variants; here's a great screenshot that should tell you everything:
      # https://github.com/github/github/pull/29414#issuecomment-50212784
      #
      # Each group has an equal share (25%) of the total experiment enrollment.
      groups do
        group :new_bootcamp, 25
        group :guidecamp, 25
        group :nothing, 25
        group :control, :rest
      end
    end
  end
end
