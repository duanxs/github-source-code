# frozen_string_literal: true

module GitHub
  module UserResearch
    # Name: Enterprise page CTA buttons Experiment
    # Contributors: @mattjohnlee
    # URL: https://frequency.githubapp.com/experiments/enterprise_page_cta_buttons
    #
    # We learned in prior Enterprise experiments that we had the best engagement when
    # using the single dropdown button currently seen on /pricing. This experiment is
    # testing whether or not the same pattern is more effective in the header and foot
    # er of the new /enterprise design.
    #
    # Experiment groups:
    #   * Control - The existing design of /enterprise. Three different CTA buttons are
    # down in the header and footer: Sign up for Enterprise Cloud, Try Enterprise Server
    #  for free, and Contact Sales.
    #   * Variation - The alternate design uses a single CTA button reading "Choose Ente
    # rprise", which contains a dropdown list with four options: Start a free trial, Get
    #  started in the cloud, Contact Sales, and Learn more about Enterprise. (This same
    # button pattern is being used currently on /pricing.)

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::ENTERPRISE_PAGE_CTA_BUTTONS do
      experiment_name "enterprise_page_cta_buttons"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 100

      # Add qualifying conditions for users. If this block evaluates falsey,
      # the subject will be excluded from the experiment.
      qualify do |user|
        !user.spammy?
      end

      # Determine what percentage of qualifying users should be enrolled in each
      # group.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :variation, 50
        group :control, :rest
      end
    end
  end
end
