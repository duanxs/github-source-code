# frozen_string_literal: true

module GitHub
  module UserResearch
    MINIMUM_ELIGIBLE_SEATS = 10

    # Name: Enterprise link in user menu Experiment
    # Contributors: @mattjohnlee
    # URL: https://frequency.githubapp.com/experiments/enterprise_link_in_user_menu
    #
    # The overall goal is to increase GitHub Enterprise sign-ups.

    # Data and experience both tell us that it is quite difficult to get to our Enterprise page when lo
    # gged into GitHub. We suspect that adding a CTA link in the user dropdown will in
    # crease visibility, reduce the amount of clicks to get to the Enterprise page, an
    # d help increase overall sign-ups.
    #
    # Experiment groups:
    #   * Control - The existing user menu
    #   * Variation - The existing user menu, plus a new "Try Enterprise" link that take
    # s users to the Enterprise page

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::ENTERPRISE_LINK_IN_USER_MENU do
      experiment_name "enterprise_link_in_user_menu"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 5

      # Add qualifying conditions for users. If this block evaluates falsey,
      # the subject will be excluded from the experiment.
      qualify do |user|
        !user.spammy? &&
          !GitHub.single_business_environment? &&
          !GitHub.enterprise? &&
          user.billing_manageable_non_business_plus_orgs.any? do |org|
            (org.seats >= MINIMUM_ELIGIBLE_SEATS) && org.business.nil?
          end
      end

      # Determine what percentage of qualifying users should be enrolled in each
      # group.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :variation, 50
        group :control, :rest
      end
    end
  end
end
