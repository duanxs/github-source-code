# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module UserResearch
    # Name: Welcome Series Combo Experiment
    # Contributors: shayfrendt, jglovier
    # URL: https://frequency.githubapp.com/experiments/welcome-series-combo
    #
    # We'd like to determine how onboarding KPIs are affected when users receive all
    # emails in the welcome series, versus just some of the emails in the welcome series.
    # This experiment intends to test different combinations of welcome series emails.
    #
    # Experiment arms:
    #   * Control - The control group will receive the entire onboarding series.
    #   * welcome-and-explore - This group will receive the "Welcome to GitHub" email and
    #     the "Explore GitHub" email.
    #   * welcome-and-github-flow - This group will receive the "Welcome to GitHub" and
    #     the "Learn GitHub Flow" email.
    #   * welcome-email-only - This group will only receive the "Welcome to GitHub" email

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::WELCOME_SERIES_COMBO do
      experiment_name UserExperimentCohort::WELCOME_SERIES_COMBO.to_s.humanize

      # Add qualifying conditions for users.  For example, don't include spammy  users
      qualify do |user|
        true
      end

      # Determine what percentage of qualifying users should be enrolled in each
      # arm.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :welcome_and_explore, 25
        group :welcome_and_github_flow, 25
        group :welcome_email_only, 25
        group :control, :rest
      end
    end
  end
end
