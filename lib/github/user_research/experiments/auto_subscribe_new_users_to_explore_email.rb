# frozen_string_literal: true
module GitHub
  module UserResearch
    # Name: Auto Subscribe New Users to Explore Email Experiment
    # Contributors: iancanderson, andrewbredow, annafil
    # URL: https://frequency.githubapp.com/experiments/auto_subscribe_new_users_to_explore_email
    #
    # https://github.com/github/product/issues/885

    # We currently have a rather hidden email newsletter where users can opt-in to new Explore content.
    # From a MEU perspective, Explore is an important path for discovering repos (and someday people) to follow.
    # **We hypothesize that opted-in new users will have higher engagement (Stars, Following, Cloning),
    # and more likely to be MEU than non-opted in new users**
    #
    # Experiment groups:
    #   * Control - These users will not be auto-subscribed to the explore email upon Gi
    # tHub.com signup.
    #   * Auto Subscribe - These users will be auto-subscribed to the explore email upon
    #  GitHub.com signup.

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::AUTO_SUBSCRIBE_NEW_USERS_TO_EXPLORE_EMAIL do
      experiment_name "auto_subscribe_new_users_to_explore_email"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 10

      # Add qualifying conditions for users. If this block evaluates falsey,
      # the subject will be excluded from the experiment.
      qualify do |user|
        # We should only enroll newly signed up users to this experiment.
        # This double checks that we only enroll very new users.
        user.created_at > 1.day.ago
      end

      # Determine what percentage of qualifying users should be enrolled in each
      # group.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :auto_subscribe, 50
        group :control, :rest
      end
    end
  end
end
