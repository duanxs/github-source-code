# frozen_string_literal: true

module GitHub
  module UserResearch
    # UserExperiment to test how users respond to an updated global navigation bar.
    # See: https://github.com/github/measurement/pull/57
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::NEW_HEADER_NAV do
      experiment_name UserExperimentCohort::NEW_HEADER_NAV.to_s.humanize

      # We want new users who aren't marked spammy.
      qualify do |user|
        !user.spammy?
      end

      groups do
        group :new_header_nav, 5
        group :control, :rest
      end
    end
  end
end
