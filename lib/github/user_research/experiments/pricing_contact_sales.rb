# frozen_string_literal: true

module GitHub
  module UserResearch
    # Name: Pricing Contact Sales Experiment
    # Contributors: @brandonrosage, @sandhub
    # URL: https://frequency.githubapp.com/experiments/pricing_contact_sales
    #
    # We seldom sell GHE at $21/user. Using "Contact Sales" as the call-to-action will
    #  hopefully encourage people to submit more contact requests which have a very hi
    # gh conversion to closed won deals.
    #
    # Experiment groups:
    #   * Control -
    #   * Contact Sales - This group sees a link to "Contact Sales," with no price.

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::PRICING_CONTACT_SALES do
      experiment_name "pricing_contact_sales"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 50

      # Determine what percentage of qualifying users should be enrolled in each
      # group.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :contact_sales, 50
        group :control, :rest
      end
    end
  end
end
