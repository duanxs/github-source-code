# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module UserResearch
    # Name: Welcome Email Series Experiment
    # Contributors: zawaideh
    # URL: https://frequency.githubapp.com/experiments/welcome_email_series
    #
    # The experiment compares sending users only one welcome email vs sending them Wel
    # come, Learn GitHub Flow, and Explore GitHub emails
    #
    # Experiment arms:
    #   * Control - Welcome email only
    #   * All emails - Welcome, Learn GitHub Flow, and Explore GitHub emails

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::WELCOME_EMAIL_SERIES do
      experiment_name "welcome_email_series"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 100

      # Add qualifying conditions for users.  For example, don't include spammy  users
      qualify do |user|
        true
      end

      # Determine what percentage of qualifying users should be enrolled in each
      # arm.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :all_emails, 50
        group :control, :rest
      end
    end
  end
end
