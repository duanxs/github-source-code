# frozen_string_literal: true

module GitHub
  module Spokes
    autoload :Client,      "github/spokes/client.rb"
    autoload :ClientError, "github/spokes/client.rb"

    def self.client
      @spokes_client ||= GitHub::Spokes::Client.new
    end
  end
end
