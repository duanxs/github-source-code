# frozen_string_literal: true

module GitHub
  module FaradayMiddleware
    autoload :RequestID,          "github/faraday_middleware/request_id"
    autoload :Tracer,             "github/faraday_middleware/tracer"
    autoload :Datadog,            "github/faraday_middleware/datadog"
    autoload :HMACAuth,           "github/faraday_middleware/hmac_auth"
    autoload :IncreasingTimeout,  "github/faraday_middleware/increasing_timeout"
    autoload :RaiseError,         "github/faraday_middleware/raise_error"
  end
end
