# frozen_string_literal: true

module GitHub
  class PrivateInstanceBootstrapper
    class EnterpriseProfileUpdate < BootstrapConfiguration
      def self.key
        "enterprise-profile-updated"
      end

      def title
        "Set enterprise account name"
      end

      def completed?
        completed == "true"
      end

      def complete!
        set_completed
        true
      end

      def required?
        true
      end

      def perform(business, actor, attributes = nil)
        return false unless attributes.present?
        return false unless business.update(attributes)
        return false unless business.rename_slug(
          attributes[:name].parameterize.presence || GitHub.default_business_base_slug,
          actor: business.owners.first,
        )

        complete!
      end
    end
  end
end
