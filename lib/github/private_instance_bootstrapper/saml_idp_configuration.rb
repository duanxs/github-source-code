# frozen_string_literal: true

module GitHub
  class PrivateInstanceBootstrapper
    class SAMLIdPConfiguration < BootstrapConfiguration
      def self.key
        "saml-idp"
      end

      def title
        "Set up your identity provider"
      end

      def description
        if completed?
          "You have enabled SAML authentication for your enterprise."
        else
          "Enable SAML authentication for your enterprise through an identity provider like Azure, Okta,
          Onelogin, Ping Identity or your custom SAML 2.0 provider."
        end
      end

      def completed?
        completed == "true"
      end

      def required?
        GitHub.private_instance_idp_configuration_required?
      end

      def complete!
        set_completed
        true
      end

      def destroy!
        GitHub.global_business&.saml_provider&.destroy!
        GitHub.kv.del(key)
      end
    end
  end
end
