# frozen_string_literal: true

module GitHub
  class PrivateInstanceBootstrapper
    class PoliciesConfiguration < BootstrapConfiguration
      def self.key
        "enterprise-policies"
      end

      def title
        "Set your enterprise policies"
      end

      def description
        if completed?
          "You have reviewed and configured policies for your enterprise."
        else
          "Review and set critical policies around repository and organization management
          for your enterprise."
        end
      end

      def step
        "policies"
      end

      def octicon
        "law"
      end

      def completed?
        completed == "true"
      end

      def perform(business, actor, attributes = nil)
        apply_attributes(business, actor, attributes)
        complete!
      end

      def set_defaults(business, actor)
        apply_attributes(business, actor,
          {
            default_repository_permissions: "read",
            repository_creation: "allowed",
            repository_creation_allowed_public: "false",
            repository_creation_allowed_private: "true",
            repository_creation_allowed_internal: "true",
            repository_forking: "disabled",
            repository_invitations: "disabled",
            default_repository_visibility: "internal",
            users_can_create_orgs: "false",
            force_pushes: "all",
            git_ssh_access: "true",
          }
        )
      end

      def complete!
        set_completed
        true
      end

      private

      def apply_attributes(business, actor, attributes)
        case attributes[:default_repository_permissions]
        when "no_policy"
          business.clear_default_repository_permission(actor: actor)
        when "none", "read", "write", "admin"
          business.update_default_repository_permission(
            attributes[:default_repository_permissions],
            force: true,
            actor: actor,
          )
        end

        case attributes[:repository_creation]
        when "no_policy"
          business.clear_members_can_create_repositories(actor: actor)
        when "allowed"
          business.allow_members_can_create_repositories_with_visibilities(
            force: true,
            actor: actor,
            public_visibility: attributes[:repository_creation_allowed_public] == "true",
            private_visibility: attributes[:repository_creation_allowed_private] == "true",
            internal_visibility: attributes[:repository_creation_allowed_internal] == "true",
          )
        else # "disabled"
          business.allow_members_can_create_repositories_with_visibilities(
            force: true,
            actor: actor,
            public_visibility: false,
            private_visibility: false,
            internal_visibility: false,
          )
        end

        case attributes[:repository_forking]
        when "no_policy"
          business.clear_private_repository_forking_setting(actor: actor)
        when "enabled"
          business.allow_private_repository_forking(force: true, actor: actor)
        else # "disabled"
          business.block_private_repository_forking(actor: actor)
        end

        case attributes[:repository_invitations]
        when "no_policy"
          business.clear_members_can_invite_outside_collaborators(actor: actor)
        when "enabled"
          business.allow_members_can_invite_outside_collaborators(force: true, actor: actor)
        else # "disabled"
          business.disallow_members_can_invite_outside_collaborators(force: true, actor: actor)
        end

        # "public", "private" or "internal", validated by model
        GitHub.set_default_repo_visibility(attributes[:default_repository_visibility] || "public", actor)

        if attributes[:users_can_create_orgs] == "false"
          GitHub.disable_org_creation(actor)
        else # "true"
          GitHub.enable_org_creation(actor)
        end

        if attributes[:force_pushes] == "_clear"
          GitHub.clear_force_push_rejection(actor)
        else # "all", "default" or "false"
          GitHub.set_force_push_rejection(
            attributes[:force_pushes] || "false",
            actor,
            true
          )
        end

        case attributes[:git_ssh_access]
        when "_clear"
          GitHub.clear_ssh(actor)
        when "true"
          GitHub.enable_ssh(actor, true)
        else # "false"
          GitHub.disable_ssh(actor, true)
        end
      end
    end
  end
end
