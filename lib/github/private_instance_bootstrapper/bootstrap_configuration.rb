# frozen_string_literal: true

module GitHub
  class PrivateInstanceBootstrapper
    class BootstrapConfiguration
      class IncompleteConfigurationError < StandardError
      end

      def self.key
        ""
      end

      def self.key_prefix
        "private-instance-bootstrap"
      end

      def title
        ""
      end

      def description
        ""
      end

      def step
        key_suffix
      end

      def octicon
        "key"
      end

      def learn_more_link
        nil
      end

      def configuration
        {}
      end

      def completed(key = self.key)
        GitHub.kv.get(key).value { "false" }
      end

      def set_completed(key = self.key)
        GitHub.kv.set(key, "true")
      end

      def completed?
        raise IncompleteConfigurationError.new
      end

      def complete!
        true
      end

      def valid?
        true
      end

      def required?
        false
      end

      def perform(business, actor, attributes = nil)
        complete!
      end

      def key(postfix = nil)
        [self.class.key_prefix, self.class.key, postfix].compact.join("-")
      end

      def key_suffix
        self.class.key
      end

      def destroy!
        GitHub.kv.del(key)
      end
    end
  end
end
