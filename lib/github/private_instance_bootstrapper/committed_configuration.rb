# frozen_string_literal: true

module GitHub
  class PrivateInstanceBootstrapper
    class CommittedConfiguration < BootstrapConfiguration
      def self.key
        "committed"
      end

      def completed?
        completed == "true"
      end

      def complete!
        persist_enterprise_configuration
        set_completed
        true
      end

      def required?
        true
      end

      private

      # Update any configuration data required in the enterprise configuration
      # system and trigger a configuration run in the background.
      def persist_enterprise_configuration
        # Currently the only configuration data we need to update is the support
        # link if it has been set.
        if GitHub.private_instance_bootstrapper.internal_support_contact.completed?
          link = GitHub.private_instance_bootstrapper.internal_support_contact.configuration.support_address
          type = GitHub.private_instance_bootstrapper.internal_support_contact.configuration.type

          settings = {
            kv_hash: {
              GitHub::Config::SupportLink::SUPPORT_LINK_KEY => link,
              GitHub::Config::SupportLink::SUPPORT_LINK_TYPE_KEY => type
            },
            enterprise_hash: {
              "enterprise": {
                "smtp": {
                  "support_address": link,
                  "support_address_type": type
                }
              }
            }
          }
          GitHub.enterprise_configuration_updater.update_configuration!(settings: settings)
        end
      end
    end
  end
end
