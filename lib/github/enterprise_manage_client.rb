# frozen_string_literal: true

module GitHub
  class EnterpriseManageClient
    TRUSTED_PORT = 1337

    # Raised when an API call responds in an unexpected way.
    class Error < StandardError; end

    def self.create
      if Rails.development?
        DevelopmentClient.new
      else
        self.new
      end
    end

    # Public: Sets the given configuration data in the enterprise configuration
    # system on the GHES installation.
    #
    # The update can be partial and does not require sending the entire previous
    # configuration.
    #
    # See the documentation for this API endpoint here:
    #
    # https://docs.github.com/en/enterprise/2.21/user/rest/reference/enterprise-admin#set-settings
    #
    # data - A Hash representing the enterprise configuration settings.
    #
    # Returns Boolean indicating success or failure.
    def set_config(data)
      conn = manage_api_connection
      response = conn.put do |req|
        req.params["settings"] = data.to_json
        manage_api_request(req, "/setup/api/settings")
      end

      success = response.status == 204
      unless success
        err = Error.new "PUT /setup/api/settings expected 204, but got #{response.status}"
        Failbot.report err, response_body: response.body
      end

      success
    end

    # Public: Triggers a configuration run on the GHES installation. This will
    # update configuration for and restart any necessary services.
    #
    # See the documentation for this API endpoint here:
    #
    # https://docs.github.com/en/enterprise/2.21/user/rest/reference/enterprise-admin#start-a-configuration-process
    #
    # Returns Boolean indicating success or failure.
    def start_config_apply
      conn = manage_api_connection
      response = conn.post do |req|
        manage_api_request(req, "/setup/api/configure")
      end

      success = response.status == 202
      unless success
        err = Error.new "POST /setup/api/configure expected 202, but got #{response.status}"
        Failbot.report err, response_body: response.body
      end

      success
    end

    # Public: Gets the current configuration status of the enterprise configuration
    # system on the GHES installation.
    #
    # The status is returned as a Hash of the following form:
    #
    # {
    #   status: "success",
    #   progress: [
    #     {
    #       key: "Preparing storage device",
    #       status: "DONE"
    #     },
    #     {
    #       key: "Updating configuration",
    #       status: "DONE"
    #     },
    #     {
    #       key: "Reloading system services",
    #       status: "DONE"
    #     },
    #     {
    #       key: "Running migrations",
    #       status: "DONE"
    #     },
    #     {
    #       key: "Reloading application services",
    #       status: "DONE"
    #     }
    #   ]
    # }
    #
    # Possible overall status values are:
    # - "success": The previous configuration run completed successfully.
    # - "failed": The previous configuration run completed unexpectedly.
    # - "running": There is a configuration run currently in progress.
    #
    # Possible progress status values for individual configuration steps are:
    # - "PENDING": The step has not started yet.
    # - "CONFIGURING": The step is running.
    # - "DONE": The step completed successfully.
    # - "FAILED": The step completed unexpectedly.
    #
    # See the documentation for this API endpoint here:
    #
    # https://docs.github.com/en/enterprise/2.21/user/rest/reference/enterprise-admin#get-the-configuration-status
    #
    # Returns Hash.
    def get_config_status
      conn = manage_api_connection
      response = conn.get do |req|
        manage_api_request(req, "/setup/api/configcheck")
      end

      if response.status == 200
        begin
          result = GitHub::JSON.parse(response.body, symbolize_names: true)
        rescue Yajl::ParseError => err
          result = {}
          Failbot.report err
        end
      else
        result = {}
        err = Error.new "POST /setup/api/configure expected 202, but got #{response.status}"
        Failbot.report err, response_body: response.body
      end

      result
    end

    private

    # Private: Configure a Faraday client for connecting to the local enterprise
    # manage server.
    #
    # Return Faraday::Client
    def manage_api_connection
      options = {
        url: "http://127.0.0.1:#{manage_api_port}",
      }
      Failbot.push enterprise_manage_url: options[:url]
      Faraday.new(options) { |f|
        f.adapter Faraday.default_adapter
        f.headers["User-Agent"] = "GitHub::PrivateInstanceBootstrapper/#{GitHub.version_number}"
      }
    end

    # Private: Port for the enterprise-manage service. Uses the trusted port
    # 1337 except in development.
    #
    # Return Int
    def manage_api_port
      Rails.development? ? 3654 : TRUSTED_PORT
    end

    # Private: Configure a Faraday request for connecting to the local
    # enterprise manage server.
    def manage_api_request(req, path, headers = {})
      req.url path
      req.params["password_hash"] = GitHub.management_console_password_hash
      req.options.timeout = 15
      req.options.open_timeout = 7
      headers.each { |header, value| req.headers[header] = value }
      Failbot.push enterprise_manage_request_method: req.method
    end
  end

  # This client is used in development. It "just works" because we do not expect
  # there to be a Management Console API running.
  class DevelopmentClient
    def set_config(data)
      true
    end

    def start_config_apply
      true
    end

    def get_config_status
      {
        status: "success",
        progress: [
          {
            key: "Preparing storage device",
            status: "DONE"
          },
          {
            key: "Updating configuration",
            status: "DONE"
          },
          {
            key: "Reloading system services",
            status: "DONE"
          },
          {
            key: "Running migrations",
            status: "DONE"
          },
          {
            key: "Reloading application services",
            status: "DONE"
          }
        ]
      }
    end
  end
end
