# frozen_string_literal: true

module GitHub::Earthsmoke
  extend self

  autoload :AcmeJwk,            "github/earthsmoke/acme_jwk"
  autoload :EncryptedAttribute, "github/earthsmoke/encrypted_attribute"
  autoload :Middleware,         "github/earthsmoke/middleware"

  def ignore_errors
    raise ArgumentError unless block_given?
    yield
  rescue ::Earthsmoke::Error => e
    nil
  end
end
