# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Prioritizable
    include ActiveRecord::ConnectionAdapters::Quoting

    GAP_SIZE = 100
    MAXIMUM_PRIORITIZABLE_ITEM_COUNT = 2_500
    MAX_MOVES = 200
    MOVES_THRESHOLD_FOR_REBALANCE = MAX_MOVES / 2
    MAX_PRIORITY_VALUE = 18446744073709551615 # MySQL BIGINT (unsigned) max value
    MAX_TRANSACTION_NESTING_LEVELS = 0
    START_VALUE = MAX_PRIORITY_VALUE / 2

    def siblings(include_self: false)
      siblings = context.send(self.class.context_reflection)
      if !include_self && persisted?
        siblings = siblings.where("#{self.class.quoted_table_name}.id <> ?", id)
      end
      siblings.where.not(self.class.priority_column => nil)
    end

    def prioritized?
      persisted? && priority? && priority != MAX_PRIORITY_VALUE
    end

    def highest_priority?
      priority = read_attribute(priority_column)
      !siblings.where("#{priority_column} > ?", priority).exists?
    end

    def reprioritize(before: nil, after: nil, position: nil, in_context: context)
      # Disallow moving a record that's in a locked context.
      if context&.locked_for_rebalance?
        raise GitHub::Prioritizable::Context::LockedForRebalance
      end

      if [before, after, position].compact.length > 1
        raise ConflictingPrioritizationArgumentError, "Must provide maximum one prioritization parameter"
      end

      if in_context != context
        # Disallow moving a record into a locked context.
        if in_context.locked_for_rebalance?
          raise GitHub::Prioritizable::Context::LockedForRebalance
        end

        reparent(in_context)
      end

      if before
        insert_before(before)
      elsif after
        insert_after(after)
      elsif position == :bottom
        insert_at_bottom
      else
        insert_at_top
      end
    rescue ActiveRecord::RecordNotUnique
      self.errors.add(:prioritization, "The priority you are attempting to set for this record is not unique")
      GitHub.dogstats.increment("#{kind}.prioritization.error.record_not_unique")
      self
    rescue ActiveModel::RangeError
      # We should not end up in this state, as priorities should have been effectively rebalanced
      # But adding this as a safeguard to prevent 500s
      self.errors.add(:prioritization, "The priority you are attempting to set for this record is not valid")
    end

    # Internal: The logical priority (1, 2, 3, ...) value for this record.
    #
    # Returns an Integer
    def canonical_priority
      return unless persisted?
      siblings(include_self: true)
        .where("#{self.class.quoted_table_name}.#{safe_column(self.class.priority_column)} >= ?", priority)
        .count
    end

    def raw_priority
      read_attribute(priority_column)
    end

    def context_id
      read_attribute(context_column)
    end

    private

    def subject_is_polymorphic?
      !!association(priority_subject).reflection.options[:polymorphic]
    end

    def subject_type_column
      association(priority_subject).reflection.foreign_type
    end

    def subject_type
      read_attribute(subject_type_column)
    end

    def subject_id_column
      association(priority_subject).reflection.foreign_key
    end

    def subject_id
      read_attribute(subject_id_column)
    end

    def subject
      @subject ||= send(priority_subject)
    end

    def context_column
      association(priority_context).reflection.foreign_key
    end

    def context
      send(self.class.priority_context)
    end

    def kind
      self.class.name.underscore
    end

    def binary_column?(column_name)
      self.class.columns_hash[column_name.to_s]&.type == :binary
    end

    def reparent(new_context)
      write_attribute(context_column, new_context.id)
      write_attribute(priority_column, nil)
    end

    # Private: The single point of reprioritization. Sets this record's
    # priority to the given `to` value, shifting siblings around only if
    # required to make room for the reprioritization.
    #
    #   from         - (Optional) Integer value of the old priority
    #   to           - (Required) Integer value of the new priority
    #
    # Returns nothing
    def change_priority(from: nil, to:)
      old_priority = from || self.raw_priority
      new_priority = to
      return if old_priority == new_priority

      shift_is_required = false
      moves = 0

      if siblings(include_self: true).where(priority: new_priority).exists?
        shift_is_required = true

        if guard_against_nested_transactions?
          GitHub.dogstats.increment("#{kind}.prioritization.error.no_nested_transactions")
          raise CannotReprioritizeInNestedTransactionError, open_transaction_count
        end
      end

      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        self.class.transaction do
          if shift_is_required
            # Get this one out of the way while we shift things around
            update_attribute(self.class.priority_column, MAX_PRIORITY_VALUE) if prioritized?

            moves = if old_priority && old_priority > new_priority
              self.class.shift_priorities_down(new_priority, context_column: context_column, context_id: context_id)
            else
              self.class.shift_priorities_up(new_priority, context_column: context_column, context_id: context_id)
            end
          end

          update_attribute(priority_column.to_sym, new_priority)
        end
      end

      context.rebalance if moves > MOVES_THRESHOLD_FOR_REBALANCE

      [self, context, subject].each do |entity|
        entity.notify_subscribers if entity.respond_to?(:notify_subscribers)
      end
    end

    def safe_column(column_name)
      self.class.connection.quote_column_name(column_name)
    end

    # Private: Moves this IssuePriority to the logical first position in the
    # list. Does not update any other IssuePriority records.
    #
    # Returns self.
    def move_to_top
      return insert_at_top if new_record?
      return self if priority.present? && !siblings.exists?

      current_max = siblings.maximum(priority_column)

      if current_max
        change_priority(to: current_max + GAP_SIZE)
      else
        self[priority_column] = START_VALUE
        save!
      end

      self
    end

    # Private: Inserts this record with the highest priority for the context.
    # Uses GitHub::SQL to determine the top priority to use, and saves the
    # record using ActiveRecord. Rescues on unique index priority collisions
    # in the event of race conditions.
    #
    # NOTE: If this record has already been persisted, falls back to `move_to_top`.
    #
    # Returns the current priority record.
    def insert_at_top
      return move_to_top if persisted?

      # Exit before insertion if there are errors on any attrs other than
      # priority (since we're going to let MySQL determine priority)
      if GitHub.rails_6_0?
        if !valid? && errors.keys != [priority_column]
          return self
        end
      else
        if !valid? && errors.attribute_names != [priority_column]
          return self
        end
      end

      priority_value_sql = github_sql.new(<<-SQL, context_id: context_id)
        SELECT IFNULL(MAX(#{safe_column(priority_column)}),#{START_VALUE}) + #{GAP_SIZE}
        FROM #{self.class.quoted_table_name}
        WHERE #{safe_column(context_column)} = :context_id FOR UPDATE
      SQL

      self.priority = priority_value_sql.value

      begin
        self.save(validate: false)
        return self
      rescue ActiveRecord::RecordNotUnique => error
        if has_unique_subject_key? && error.message.match?(/key .#{unique_subject_key}./)
          # if there's been a race condition due to prioritization within the subject
          # this can happen if the issue is already assigned to the milestone
          self.class.find_by(issue_id: self.issue_id, milestone_id: self.milestone_id)
        else
          raise
        end
      end
    end

    # Private: Moves this object to the logical last position in the
    # list.
    #
    # Returns self.
    def move_to_bottom
      return insert_at_bottom if new_record?
      return self if priority.present? && !siblings.exists?

      current_min = siblings.minimum(priority_column)

      if current_min
        new_priority = current_min - GAP_SIZE

        if new_priority < 0
          # rebalance the priorities so there is more room for records at the
          # bottom and in between siblings
          GitHub.dogstats.increment("#{kind}.prioritization.rebalance.priority_out_of_range")
          context.rebalance!
          new_priority = siblings.minimum(priority_column) - GAP_SIZE
        end
        change_priority(to: new_priority)
      else
        self[priority_column] = START_VALUE
        save!
      end

      self
    end

    # Private: Inserts this record with the lowest priority for the context.
    # Uses GitHub::SQL to determine the current lowest available priority and
    # saves the card using ActiveRecord. Rescues on unique index priority
    # collisions in the event of race conditions.
    #
    # NOTE: If this record has already been persisted, falls back to `move_to_bottom`.
    #
    # Returns the current priority record.
    def insert_at_bottom
      return move_to_bottom if persisted?

      # Exit before insertion if there are errors on any attrs other than
      # priority (since we're going to let MySQL determine priority)
      if GitHub.rails_6_0?
        if !valid? && errors.keys != [priority_column]
          return self
        end
      else
        if !valid? && errors.attribute_names != [priority_column]
          return self
        end
      end

      priority = minimum_priority_value

      if priority < 0
        # rebalance the priorities so objects can safely be prioritized at the bottom
        GitHub.dogstats.increment("#{kind}.prioritization.rebalance.priority_out_of_range")
        context.rebalance!
        priority = minimum_priority_value
      end

      self.priority = priority

      begin
        self.save(validate: false)
        return self
      rescue ActiveRecord::RecordNotUnique => error
        if has_unique_subject_key? && error.message.match?(/key .#{unique_subject_key}./)
          # if there's been a race condition specifically due to prioritization during save,
          # we know the record was safely saved and can return it here.
          self.class.find_by(issue_id: self.issue_id, milestone_id: self.milestone_id)
        else
          raise
        end
      end
    end

    # Private: The unique (database) index enforcing uniqueness for subjects
    # within a context.
    #
    # Example: index_issue_priorities_on_issue_id_and_milestone_id
    #
    # Returns a String index name or nil.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def unique_subject_key
      @unique_subject_key ||= begin
        unique_pair = [subject_id_column, context_column]
        index = self.class.connection.indexes(self.class.table_name)&.detect do |index|
          # find indicies matching the suject and context, and allow for polymorphic subject relationships
          index.unique && (index.columns & unique_pair) == unique_pair
        end
        index&.name
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def has_unique_subject_key?
      !!unique_subject_key
    end

    # Private: Inserts or moves this record to a higher logical priority
    # than the given subject. Updates any priority records in between. Can move
    # no more than MAX_MOVES away from the current logical position to prevent
    # expensive mass updates.
    #
    #   subject - the subject (e.g. Issue) we want to shift this priority
    #             directly above
    #
    # Returns nothing.
    def insert_before(subject)
      return insert_at_top unless persisted? && prioritized?
      return insert_after(siblings.last) if !subject

      if subject.class == self.class
        priority_below = subject
      else
        conditions = {subject_id_column => subject.id}
        conditions.merge(subject_type: subject.type) if subject_is_polymorphic?
        return self unless priority_below = siblings.where(conditions).first
      end

      return insert_at_top if priority_below.highest_priority?

      old_priority = priority

      if old_priority > priority_below.raw_priority
        new_priority = priority_below.raw_priority + 1
      else
        new_priority = priority_below.raw_priority
      end

      change_priority(from: old_priority, to: new_priority)

      self
    end

    # Private: Inserts or moves this record to a lower logical priority than
    # the given subject. Updates any priority records in between. Can move no
    # more than MAX_MOVES away from the current logical position to prevent
    # expensive mass updates.
    #
    #   subject - the record (e.g. Issue) we want to shift this priority
    #             directly underneath
    #
    # Returns nothing.
    def insert_after(subject)

      return insert_at_bottom unless context.prioritized?

      if subject.class == self.class
        priority_above = subject
      else
        return self unless priority_above = siblings.where(subject_id_column.to_sym => subject.id).first
      end

      old_priority = raw_priority
      cursor = priority_above.raw_priority

      if !prioritized? || old_priority < cursor
        new_priority = cursor - 1
      else
        new_priority = cursor
      end

      change_priority(from: old_priority, to: new_priority)

      self
    end

    def open_transaction_count
      self.class.connection.open_transactions
    end

    def guard_against_nested_transactions?
      return false if GitHub.enterprise?
      open_transaction_count > MAX_TRANSACTION_NESTING_LEVELS
    end

    # Internal
    # Returns the lowest possible priority for insertion at the bottom
    def minimum_priority_value
      priority_value_sql = github_sql.new(<<-SQL, context_id: context_id)
        SELECT IFNULL(MIN(#{safe_column(priority_column)}),#{START_VALUE}) - #{GAP_SIZE}
        FROM #{self.class.quoted_table_name}
        WHERE #{safe_column(context_column)} = :context_id FOR UPDATE
      SQL

      priority_value_sql.value
    end

    module ClassMethods
      def shift_priorities_up(new_priority, context_column:, context_id:)
        sql = github_sql.new(
          table_name: GitHub::SQL.LITERAL(quoted_table_name),
          priority_column: GitHub::SQL.LITERAL(priority_column),
          context_column: GitHub::SQL.LITERAL(context_column),
          context_id: context_id,
          new_priority: new_priority,
        )

        # Decrementing down from new_priority, find the first priority that has
        # a gap after it.
        #
        # Note: do not try to move this into a subquery of the UPDATE query, as
        # MySQL doesn't allow subqueries on the table being updated.
        sql.add <<-SQL
          SELECT t1.:priority_column
          FROM :table_name t1
          LEFT OUTER JOIN :table_name t2 ON t2.:context_column = t1.:context_column AND t2.:priority_column = (t1.:priority_column - 1)
          WHERE t1.:context_column = :context_id
          AND t1.:priority_column <= :new_priority
          AND t2.id IS NULL
          ORDER BY t1.:priority_column DESC
          LIMIT 1
        SQL

        transaction do
          highest_priority_before_gap = sql.value
          window_size = new_priority - highest_priority_before_gap + 1

          if window_size > MAX_MOVES
            raise MovedTooFarError, window_size
          else
            measure_shift do
              records_in_window = where("
                #{context_column} = ? AND
                #{priority_column} <= ? AND
                #{priority_column} >= ?",
                context_id, new_priority, highest_priority_before_gap
              )
              records_in_window.order("#{priority_column} ASC").update_all("#{priority_column} = #{priority_column} - 1")
            end
          end
        end
      end

      def shift_priorities_down(new_priority, context_column:, context_id:)
        sql = github_sql.new(
          table_name: GitHub::SQL.LITERAL(quoted_table_name),
          priority_column: GitHub::SQL.LITERAL(priority_column),
          context_column: GitHub::SQL.LITERAL(context_column),
          context_id: context_id,
          new_priority: new_priority,
        )

        # Incrementing up from new_priority, find the first priority that has
        # a gap after it.
        #
        # Note: do not try to move this into a subquery of the UPDATE query, as
        # MySQL doesn't allow subqueries on the table being updated.
        sql.add <<-SQL
          SELECT t1.:priority_column
          FROM :table_name t1
          LEFT OUTER JOIN :table_name t2 ON t2.:context_column = t1.:context_column AND t2.:priority_column = (t1.:priority_column + 1)
          WHERE t1.:context_column = :context_id
          AND t1.:priority_column >= :new_priority
          AND t2.id IS NULL
          ORDER BY t1.:priority_column ASC
          LIMIT 1
        SQL

        transaction do
          lowest_priority_before_gap = sql.value
          window_size = lowest_priority_before_gap - new_priority + 1

          if window_size > MAX_MOVES
            raise MovedTooFarError, window_size
          else
            measure_shift do
              records_in_window = where("
                #{context_column} = ? AND
                #{priority_column} <= ? AND
                #{priority_column} >= ?",
                context_id, lowest_priority_before_gap, new_priority
              )
              records_in_window.order("#{priority_column} DESC").update_all("#{priority_column} = #{priority_column} + 1")
            end
          end
        end
      end

      def context_reflection
        @context_reflection ||= reflect_on_association(priority_context).inverse_of.name
      end

      private

      def prioritizable_by(column = :priority, subject:, context:)
        define_singleton_method(:priority_column)  { column }
        define_singleton_method(:priority_subject) { subject }
        define_singleton_method(:priority_context) { context }

        define_method(:priority_column)  { column }
        define_method(:priority_subject) { subject }
        define_method(:priority_context) { context }

        class_eval <<-RUBY, __FILE__, __LINE__ + 1
          scope :by_priority, -> { order("#{column} DESC") }
        RUBY
      end

      # Private: Wrapper for shifting a range of records up or down in
      # priority. Measures the time it takes the shift query to complete and
      # increments a Datadog counter responsible for keeping track of how many
      # records we're shifting (between 1 and MAX_MOVES), bucketed in range
      # groups of 5.
      #
      # Requires a block, which should execute the shift query and return the
      # number of records modified (as .update_all does).
      #
      # Returns nothing.
      def measure_shift(&block)
        count = GitHub.dogstats.time("#{name.underscore}.shift_priorities") do
          block.call
        end

        shift_groups = (1..MAX_MOVES).to_a.in_groups_of(25, false)
        shift_buckets = ({}).tap do |buckets|
          shift_groups.map do |group|
            buckets["#{group.first}-#{group.last}"] = group
          end
        end

        if bucket = shift_buckets.detect { |k, v| v.include?(count) }.first
          GitHub.dogstats.increment "#{name.underscore}.shift_priorities_count.#{bucket}"
        else
          # This should never happen.
          GitHub.dogstats.increment "#{name.underscore}.shift_priorities_count.out-of-range"
        end

        count
      end
    end

    class CannotReprioritizeInNestedTransactionError < StandardError
      def initialize(value)
        super "Cannot shift inside of nested transaction (current nesting depth is #{value})"
      end
    end

    class ConflictingPrioritizationArgumentError < ArgumentError; end

    class MovedTooFarError < StandardError
      def initialize(value)
        super "Cannot shift #{value.inspect} positions - max is #{MAX_MOVES}"
      end
    end

    def self.included(model) # rubocop:disable Lint/IneffectiveAccessModifier
      model.class_eval do
        include Instrumentation::Model
      end

      model.extend ClassMethods
    end
  end
end
