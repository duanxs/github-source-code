# frozen_string_literal: true
require "treelights"
require "treelights/gzip_request"
require "sinatra"
require "set"

module GitHub
  class Treelights
    SERVICE_URL = "#{GitHub.treelights_url}/twirp".freeze
    SERVICE_NAME = "treelights".freeze

    # Time in seconds for which the server info should be locally cached
    SERVER_INFO_TTL = 5 * 60

    class RPCError < StandardError; end

    # Private: The maximum amount of text that should be sent to Treelights,
    # per second of of timeout. It can take significant time to serialize,
    # transmit, and deserialize documents from Rails to Treelights. And
    # Treelights has a limited throughput. So we avoid sending documents that
    # are unlikely to be successfully highlighted within the time limit.
    MAXIMUM_BYTES_PER_SECOND = 6 * 1024 * 1024

    class << self
      # Public: Cache key prefix for caching syntax highlighted markup.
      def cache_key
        refresh_server_info
        @cache_key
      end

      # Public: Cache key prefix for caching syntax highlighted markup in
      # darkship mode.
      def darkship_cache_key
        refresh_server_info
        @darkship_cache_key
      end

      # Public: Syntax highlight some text with treelights.
      #
      # scope   - Textmate scope string identifying the language of the text.
      # text    - String containing source code to highlight
      # timeout - Time limit in seconds for highlighting the document
      # mode    - Symbol indicating which highlighting algorithms are allowed.
      #           By default, Tree-sitter is used for supported languages. Pass
      #           :TEXTMATE_ONLY to disable the Tree-sitter highlighter.
      #
      # Returns an Array of Strings (html safe String lines).
      def highlight(scope, text, timeout: nil, mode: :DEFAULT)
        GitHub.tracer.with_span("GitHub::Treelights.highlight") do |span|
          highlight_many([scope], [text], timeout: timeout, mode: mode).first
        end
      end

      # Public: Syntax highlight multiple documents.
      #
      # scopes  - Array of textmate scope strings identifying the language of each text.
      # texts   - Array of strings containing source code to highlight.
      # timeout - Time limit in seconds for highlighting each document
      # mode    - Symbol indicating which highlighting algorithms are allowed.
      #           By default, Tree-sitter is used for supported languages. Pass
      #           :TEXTMATE_ONLY to disable the Tree-sitter highlighter.
      #
      # Returns an Array of arrays of strings (html safe String lines).
      def highlight_many(scopes, texts, timeout: nil, mode: :DEFAULT)
        GitHub.tracer.with_span("GitHub::Treelights.highlight_many") do |span|
          span.set_tag("mode", mode)

          # Limit the total amount of text that is sent to Treelights,
          # so that we avoid sending text that is unlikely to be highlighted
          # within the timeout.
          if timeout
            timeout_ms = timeout ? (timeout * 1000).to_i : nil
            total_bytes = 0
            maximum_bytes = MAXIMUM_BYTES_PER_SECOND * timeout
            allowed_doc_count = texts.count do |text|
              total_bytes += text.bytesize
              total_bytes <= maximum_bytes
            end
          else
            timeout_ms = nil
            allowed_doc_count = texts.length
          end

          span.log(
            event: "highlighting many",
            scopes_length: scopes.length,
            texts_length: texts.length,
            allowed_texts_length: allowed_doc_count,
          )

          highlighted = client.highlight(
            scopes.first(allowed_doc_count),
            texts.first(allowed_doc_count),
            timeout: timeout_ms,
            mode: mode,
          ).map do |lines|
            lines&.map(&:html_safe)
          end

          # Ensure that the returned array has the same length as the input
          # arrays, in case any inputs were omitted from the treelights request
          # due to the total size limit.
          highlighted.fill(nil, highlighted.length...texts.length)
        rescue Faraday::TimeoutError, Timeout::Error => error
          GitHub::Logger.log(at: "treelights.timeout_error", error: error)
          GitHub.dogstats.increment("rpc.treelights.timeouts")
          span.set_tag("error", true)
          span.log(event: "error", 'error.object': error)
          raise RPCError
        rescue Faraday::SSLError, Faraday::ClientError, Faraday::ConnectionFailed => error
          GitHub::Logger.log(at: "treelights.connection_error", error: error)
          GitHub.dogstats.increment("rpc.treelights.connection_errors")
          span.set_tag("error", true)
          span.log(event: "error", 'error.object': error)
          raise RPCError
        rescue ::Treelights::Client::ResponseError
          GitHub::Logger.log(at: "treelights.response_error", error: error)
          GitHub.dogstats.increment("rpc.treelights.response_errors")
          span.set_tag("error", true)
          span.log(event: "error", 'error.object': error)
          raise RPCError
        end
      end

      # Public: Get a `Set` of all of the scope strings recognized by the server.
      def valid_scopes
        refresh_server_info
        @valid_scopes
      end

      # Public: Forget everything. Useful for tests.
      def reset
        @server_info_fetch_time = nil
        @cache_key = nil
        @darkship_cache_key = nil
        @valid_scopes = nil

        # Sometimes pre-prep stages (before Treelights has been stubbed!) can
        # cause the circuit breaker to trip, failing requests against a
        # non-existent local Treelights instance, meaning requests are failed
        # later even though we've stubbed out the responses by then.
        Resilient::CircuitBreaker.get(SERVICE_NAME).reset
      end

      private

      INFO_ERRORS = [
        Faraday::ClientError,
        Faraday::ConnectionFailed,
        Faraday::TimeoutError,
        Faraday::SSLError,
        Timeout::Error,
        ::Treelights::Client::ResponseError,
      ]

      def refresh_server_info
        time = Time.now
        unless @server_info_fetch_time && time - @server_info_fetch_time < SERVER_INFO_TTL
          info = GitHub.tracer.with_span("GitHub::Treelights.info") do |span|
            info = client.info
            info
          rescue *INFO_ERRORS => error
            GitHub::Logger.log(at: "treelights.info.error", error: error)
            span.set_tag("error", true)
            span.log(event: "error", 'error.object': error)
            {version: "unknown", scopes: []}
          end

          @cache_key = "treelights.#{::Treelights::VERSION}.#{info[:version]}".freeze
          @darkship_cache_key = "treelights-darkship.#{::Treelights::VERSION}.#{info[:version]}".freeze
          @valid_scopes = Set.new(info[:scopes]).freeze
          @server_info_fetch_time = time
        end
      end

      def client
        @client ||= begin
          connection = Faraday.new(url: SERVICE_URL) do |conn|
            conn.request(:retry,
              max: 3,
              interval: 0.1,
              backoff_factor: 2,
              exceptions: [Faraday::ConnectionFailed],
              methods: [:post],
            )
            conn.use ::Treelights::GzipRequest
            conn.use GitHub::FaradayMiddleware::RequestID
            conn.use GitHub::FaradayMiddleware::Tracer,
              service_name: SERVICE_NAME,
              parent_span: proc { GitHub.tracer.last_span },
              tracer: GitHub.tracer,
              # If we don't specify the operation, it defaults to "POST", in
              # which case LightStep produces an operation that filters the last
              # component of the URL.  Since in Twirp, that's the RPC method
              # name, which we want to keep, we set the operation explicitly
              # ourselves.
              operation: proc { |env| "#{env[:method].to_s.upcase} #{URI(env[:url]).path}" }
            conn.use GitHub::FaradayMiddleware::Datadog, stats: GitHub.dogstats, service_name: SERVICE_NAME
            conn.use Faraday::Resilient, name: SERVICE_NAME, options: {
              instrumenter: GitHub,
              error_threshold_percentage: 5,
            }
            conn.adapter :typhoeus
            conn.options[:open_timeout] = 0.1 # connection open timeout in seconds.
            conn.options[:timeout] = 5.5      # read timeout in seconds.
          end
          ::Treelights::Client.new(connection)
        end
      end
    end
  end
end
