# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Limiters
    # Limit anonymous requests by client IP and path
    class ElapsedAnonymousByIP < MemcachedWindow
      include GitHub::Middleware::Constants
      include GitHub::Middleware::AnonymousRequest

      REQUEST_STARTED_AT = "github.limiters.anon-ip-elapsed.started"

      # Limit is in milliseconds.
      def initialize(limit:, ttl: 60)
        super "anon-ip-elapsed", limit: limit, ttl: ttl
      end

      def start(request)
        return OK unless anonymous_request?(request)
        request.env[REQUEST_STARTED_AT] = Time.now
        super
      end

      def record_finish(request)
        increment_counter(request) if request.env[REQUEST_STARTED_AT]
      end

      # Number of milliseconds this request took.
      def cost(request)
        Integer((Time.now - request.env[REQUEST_STARTED_AT]) * 1_000)
      end

      def key(request)
        request.ip
      end
    end
  end
end
