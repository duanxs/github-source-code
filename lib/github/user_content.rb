# rubocop:disable Style/FrozenStringLiteralComment
# rubocop:disable GitHub/DontPassBlockToAsyncMethod

require "set"

module GitHub
  # Mixin adds HTML filtering related methods to models and other stuff
  # that has a #body attribute.
  #
  # Classes that include UserContent MUST have a `new_record?` method
  # and a `body` method
  module UserContent
    BODY_CACHE_VERSION = 57

    module ClassMethods
      def setup_attachments
        has_many :attachments, as: :attachable
        after_save :attach_matching_assets, if: :attach_matching_assets?
      end
    end

    def self.included(model)
      model.extend ClassMethods
      model.send(:include, GitHub::Validations)
    end

    # Run the pipeline on the body if necessary and create a DocumentFragment.
    def async_body_document
      async_body_content.then(&:async_document)
    end

    def async_body_content(context: {})
      if context.any?
        return async_body_content!(context)
      end

      async_body_cache_key(:content).then do |cache_key|
        if @body_content && @body_content_cache_key && @body_content_cache_key == cache_key
          @body_content
        else
          async_body_context.then do |body_context|
            @body_content_cache_key = cache_key
            @body_content = async_body_content!({})
          end
        end
      end
    end

    def async_body_content!(context)
      async_body_context.then do |body_context|
        HTML::BodyContent.new(body, body_context.merge(context), body_pipeline)
      end
    end

    def body_result
      async_body_result.sync
    end

    def async_body_result
      async_body_content.then(&:result)
    end

    # The body HTML after all filters have been performed.
    #
    # Returns the body HTML as a String
    def body_html
      async_body_html.sync
    end

    def async_body_html
      return Promise.resolve(nil) if body.nil?
      async_body_cache(:html) { async_body_html! }
    end

    def async_body_html_refresh
      return Promise.resolve(nil) if body.nil?
      async_body_refresh_cache(:html) { async_body_html! }
    end

    def async_body_html!(context: {})
      async_body_content(context: context).then do |body_content|
        body_content.async_result.then do |result|
          output = result[:output]

          html = if output.is_a?(String)
            output
          else
            output.to_html
          end

          result[:html_safe] ? html.html_safe : html # rubocop:disable Rails/OutputSafety
        end
      end
    end

    # A text only version of the rendered body HTML. This should be used when
    # showing titles and truncated previews.
    #
    # Returns the textual version of the body HTML.
    def body_text
      async_body_text.sync
    end

    def async_body_text
      return Promise.resolve(nil) if body.nil?
      async_body_cache(:text) { async_body_text! }
    end

    def async_body_text!
      async_body_document.then do |doc|
        doc.inner_text.strip
      end
    end

    # The body HTML truncated to the given number of visual characters.
    #
    # Returns the truncated body HTML as a String.
    def async_truncated_body_html(max)
      async_body_cache("trunc#{max}") { async_truncated_body_html!(max) }
    end

    def async_truncated_body_html!(max)
      async_body_document.then do |body_document|
        html = HTMLTruncator.new(body_document, max).to_html
        html_safe? ? html.html_safe : html # rubocop:disable Rails/OutputSafety
      end
    end

    # HTML rendering pipeline context information. Filters have no knowledge of
    # the outside world other than the values provided here.
    #
    # Subclasses should override this method to provide different defaults.
    # Filters can put extracted information into the context which would be
    # available after the Promise returned by async_body_document is resolved.
    def async_body_context
      if respond_to?(:entity) && !respond_to?(:async_entity)
        raise "Responds to #entity but not to #async_entity!"
      end

      Promise.all([
        respond_to?(:async_organization) ? async_organization : Promise.resolve(nil),
        respond_to?(:async_entity) ? async_entity : Promise.resolve(nil),
        async_body_context_user,
        async_body_context_location,
      ]).then do |organization, entity, body_context_user, location|
        {
          subject: self,
          entity: entity,
          organization: organization,
          current_user: body_context_user,
          base_url: GitHub.url,
          asset_root: "#{GitHub.asset_host_url}/images/icons",
          asset_proxy: GitHub.image_proxy_url,
          disable_asset_proxy: !GitHub.image_proxy_enabled?,
          location: location,
        }
      end
    end

    def async_body_context_user
      if respond_to?(:async_latest_user_content_edit)
        async_latest_user_content_edit.then do |latest_user_content_edit|
          if latest_user_content_edit
            latest_user_content_edit.async_editor
          elsif respond_to?(:async_user)
            async_user
          end
        end
      elsif respond_to?(:async_user)
        async_user
      end
    end

    # The location where this Markdown is being rendered affects whether commands like "Closes
    # #1"/"Duplicate of #1" do anything.
    def async_body_context_location
      if is_a?(Issue)
        async_pull_request.then do |pull_request|
          pull_request? ? pull_request.class.name : self.class.name
        end
      elsif is_a?(IssueComment)
        async_issue.then do |issue|
          issue.async_pull_request.then do
            if issue.pull_request?
              "PullRequestComment"
            else
              self.class.name
            end
          end
        end
      else
        Promise.resolve(nil)
      end
    end

    # Cutover date for comments moved to Markdown.  Hitler's birthday.
    MARKDOWN_CUTOVER = Time.utc(2009, 4, 20, 19, 0, 0)

    # The HTML pipeline used to render the body to HTML. This may be
    # overridden by subclasses to specify an different pipeline setup.
    #
    # By default, the pipeline varies based on the object's #formatter
    # attribute and creation time. GFM, Email replies, and old Textile bodies
    # are all supported.
    #
    # Returns the GitHub::HTML::Pipeline object that the body text should be
    # passed through.
    def body_pipeline
      case try(:formatter)
        when :email then GitHub::HTML::EmailPipeline
        else
          if legacy_textile_formatter?
            GitHub::HTML::TextilePipeline
          else
            GitHub::Goomba::MarkdownPipeline
          end
      end
    end

    # Body HTML used when sending email notifications. This can be slightly
    # different from the HTML used to display messages on the site.
    #
    # The only case that's different currently is messages sent via email reply.
    # They're stored as plain text and the default body_html pipeline strips
    # quoting, line ends, and applies github specific styles. When sending email
    # notifications for messages generated by email replies we want to use much
    # more conservative HTML formatting rules.
    #
    # Returns a string of HTML.
    def body_html_for_email
      if try(:formatter) == :email
        async_body_cache(:html_for_email) {
          html = EscapeUtils.escape_html(body)
          html.gsub("\n", "<br>\n")
        }.sync
      else
        async_body_html!(context: {for_email: true}).sync
      end
    end

    # If the body contains html elements that markdown could produce, it
    # considers the body to contain markdown. It also makes sure an anchor tag's
    # href does not equal its content to distinguish pasted urls from markdown
    # urls.
    #
    # Returns a Boolean
    def body_contains_markdown?
      regex = %r{
        </i>|
        </li>|
        </strong>|
        <a\s+href="([^"]+)">(?!\1)[^<]+</a>|
        </em>|
        </h\d>|
        </blockquote>
      }x

      regex =~ body_html
    end

    CONTAINS_SUGGESTION_REGEX = /js-suggested-changes-blob/
    MAY_CONTAIN_SUGGESTION_REGEX = /```suggestion/i

    # If the body contains html elements that only the SuggestedChangesFilter
    # could produce, it considers the body to contain a suggested change.
    #
    # Returns a Boolean
    def body_contains_suggestion?
      !!(CONTAINS_SUGGESTION_REGEX =~ body_html)
    end

    # If the body contains text that implies a suggestion, it considers
    # that the body _may_ contain a suggested change. This could produce false positives
    # which is OK in some cases.
    #
    # Returns a Boolean
    def body_may_contain_suggestion?
      !!(MAY_CONTAIN_SUGGESTION_REGEX =~ body)
    end

    def legacy_textile_formatter?
      created_before_markdown_cutover? && id_low_enough_for_textile?
    end

    # Internal: is `created_at` before 20-Apr-2009?
    def created_before_markdown_cutover?
      respond_to?(:created_at) && (created_at || Time.now) < MARKDOWN_CUTOVER
    end

    # Internal: is this record already in the prod database before 14-Mar-2016?
    def id_low_enough_for_textile?
      if respond_to?(:max_textile_id, true) && max_textile_id
        respond_to?(:id) && (id.nil? || id < max_textile_id)
      else
        false
      end
    end

    # Users mentioned in the body. The GitHub::HTML::MentionFilter must
    # be part of the body pipeline in order for this information to be
    # extracted.
    #
    # Returns the Array of User objects.
    def mentioned_users
      Array body_result.mentioned_users
    end

    # User names mentioned in the body. The GitHub::HTML::MentionFilter must
    # be part of the body pipeline in order for this information to be
    # extracted.
    #
    # Returns the Array of user names.
    def mentioned_usernames
      async_body_cache(:mentioned_usernames) {
        Set.new(Array(body_result.mentioned_usernames))
      }.sync
    end

    # Teams mentioned in the body. The GitHub::HTML::MentionFilter must
    # be part of the body pipeline in order for this information to be
    # extracted.
    #
    # Returns the Array of Team objects.
    def mentioned_teams
      Array body_result.mentioned_teams
    end

    # Issues mentioned in the body. The GitHub::HTML::IssueMentionFilter must
    # be part of the body pipeline in order for this information to be
    # extracted.
    #
    # Returns the Array of Issue objects.
    def mentioned_issues
      Array(body_result.issues).map { |ref| ref.issue }
    end

    # Discussions mentioned in the body. The GitHub::HTML::IssueMentionFilter must
    # be part of the body pipeline in order for this information to be
    # extracted.
    #
    # Returns the Array of Discussion objects.
    def mentioned_discussions
      Array(body_result.discussions).map { |ref| ref.discussion }
    end

    # Public: Returns the cached TaskList::Summary object.
    def task_list_summary
      async_task_list_summary.sync
    end

    def async_task_list_summary
      async_body_cache(:task_list_summary) do
        async_body_result.then do |body_result|
          TaskList::Summary.new(Array(body_result.task_list_items))
        end
      end
    end

    # Public: Counts task list items (with optional filter by status)
    #
    # statuses - One or more symbols that, if present, limit the count to matching items.
    #            Default is to count all of them (:complete and :incomplete).
    #
    # Returns the count (a positive integer number)
    def task_list_item_count(*statuses)
      async_task_list_item_count(*statuses).sync
    end

    # Task list summary that don't call full pipeline to avoid unnecessary overhead
    #
    # Returns a Promise resolving to a TaskList::Summary object.
    def async_lightweight_task_list_summary
      async_body_cache(:lightweight_task_list_summary) do
        GitHub::Goomba::LightweightTaskListPipeline.async_call(body).then do |body_result|
          TaskList::Summary.new(Array(body_result.task_list_items))
        end
      end
    end

    def async_task_list_item_count(*statuses)
      return Promise.resolve(0) unless body =~ TaskList::Filter::ItemPatternChecker

      async_lightweight_task_list_summary.then do |task_list_summary|
        next task_list_summary.item_count if statuses.compact.empty?

        result = 0
        result += task_list_summary.complete_count if :complete.in?(statuses)
        result += task_list_summary.incomplete_count if :incomplete.in?(statuses)
        result
      end
    end

    # Public: Returns true if there are any task list items.
    def task_list?
      async_task_list?.sync
    end

    def async_task_list?
      # Use a quick check to see if we have anything here
      # that looks like a task item. If not, we don't try
      # to render the real task list. This saves a lot of
      # time for the common case of no task list.
      return Promise.resolve(false) unless body =~ TaskList::Filter::ItemPatternChecker

      async_lightweight_task_list_summary.then(&:items?)
    end

    def body_asset_matches
      @body_asset_matches ||= AssetScanner.scan(async_body_document.sync)
    end

    # Internal: Should we try to attach matching assets to the record?
    # Determines if the attach_matching_assets after_save callback is invoked.
    #
    # Returns a Boolean.
    def attach_matching_assets?
      true
    end

    def attach_matching_assets
      @body_asset_matches = @body_content = nil
      Attachment.attach(self, body_asset_matches)
    end

    def async_body_cache_key(key)
      Promise.all([
        respond_to?(:async_repository) && async_repository.then { |repository|
          repository&.async_math_filter_enabled?
        },
        async_body_context_version,
      ]).then do |math_filter_enabled, body_context_version|
        # Preload the `owner` association for Repository#math_filter_enabled?
        [
          self.class.name,
          body_cache_key_id || "",
          "body_#{key}",
          "v#{BODY_CACHE_VERSION}",
          ("math.v2" if math_filter_enabled),
          body_version,
          "reoderable.task.list.v1",
          body_context_version,

          # Changes to our syntax highlighting code affect the syntax-highlighted
          # output.
          GitHub::Colorize.cache_key_prefix,

          # Changes to task lists affect blob rendering
          GitHub.task_list_cache_version,
        ].compact.join(":")
      end
    end

    # Internal: return a unique cache key id for this user content body
    def body_cache_key_id
      if respond_to?(:new_record?) && respond_to?(:id) && !new_record?
        id
      elsif respond_to?(:sha)
        sha
      end
    end

    def async_body_cache(key)
      async_body_cache_key(key).then do |cache_key|
        Platform::Loaders::Cache.load(cache_key).then do |value|
          next value unless value.nil?
          next unless block_given?

          Promise.resolve(yield).then do |fresh_value|
            GitHub.cache.set(cache_key, fresh_value) if fresh_value
            fresh_value
          end
        end
      end
    end

    def async_body_refresh_cache(key)
      async_body_cache_key(key).then do |cache_key|
        Promise.resolve(yield).then do |fresh_value|
          GitHub.cache.set(cache_key, fresh_value) if fresh_value
          fresh_value
        end
      end
    end

    def body_version
      body.to_s.to_md5
    end

    def async_body_context_version
      async_body_context.then do |body_context|
        body_context.values.map { |value|
          if value.is_a?(ActiveRecord::Integration)
            "#{value.model_name.cache_key}/#{value.id}"
          else
            value
          end
        }.join(":").to_md5
      end
    end

    def html_safe?
      body_result[:html_safe] == true
    end
  end
end
