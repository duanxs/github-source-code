# frozen_string_literal: true

class GitHub::Earthsmoke::Middleware::EnableTracing < ::Earthsmoke::Middleware::Base
  def call(env)
    GitHub.tracer.with_enabled(true) do
      return @app.call(env)
    end
  end
end
