# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class ProxyDetection
    # Choose an arbitrary and meaningless parameter name.
    # See app/assets/javascripts/github/security/proxy-site-reporting.coffee
    PARAM = "$__"

    APPROVED_HOSTS_THAT_PROXY_GITHUB = Set.new(YAML.safe_load(File.read(File.join(Rails.root, "config/ignored_proxy_sites.yml")))["hosts"])
    ACTIONABLE_PROXY_SCHEMES = ["http", "https"].freeze
    FRESH_UNTIL = 10.minutes
    PROXY_BANNED_MESSAGE = "Proxy Site Detected (%s): https://haystack.githubapp.com/github-sec/rollups/%s"

    class << self
      def hmac(data)
        return "" if GitHub.enterprise?
        OpenSSL::HMAC.hexdigest("sha256", GitHub.proxy_site_detection_secret, data)
      end

      def encode(data)
        Base64.strict_encode64([hmac(data), data].join("|"))
      end

      def decode(data)
        Base64.strict_decode64(data).split("|", 2)
      end

      def report_proxy_site(encoded_error)
        proxy = new(encoded_error)

        unless proxy.valid_hmac?
          GitHub.dogstats.increment("banned_ip.report", tags: ["report_error:invalid_hmac", "operation:report"])
          return
        end

        unless proxy.fresh_violation?
          GitHub.dogstats.increment("banned_ip.report", tags: ["report_error:stale", "operation:report"])
          return
        end

        if proxy.take_action?
          context = failbot_context(proxy)
          GitHub.context.push(context) do
            Audit.context.push(context) do
              GitHub.dogstats.increment("banned_ip.report", tags: ["report:success", "operation:report"])
              Failbot.report!(Api::SecurityViolation.new(proxy.error), context)
            end
          end
        else
          GitHub.dogstats.increment("banned_ip.report", tags: ["report:ignored", "operation:report"])
        end
      rescue URI::InvalidURIError, URI::InvalidComponentError, JSON::ParseError, ArgumentError
        # nbd, this endpoint could be full of garbage.
      end

      private

      def rollup(proxy)
        Digest::MD5.hexdigest("#{proxy.proxy_host}")
      end

      def failbot_context(proxy)
        context = {
          "message" => "Potential proxy site detected: #{proxy.proxy_host}",
          "app" => "github-sec",
          "proxy_request_id" => proxy.request_id,
          "rollup" => rollup(proxy),
          "created_at" => Time.now,
        }.reverse_merge(proxy.error)
      end
    end

    attr_reader :error

    def initialize(encoded_error)
      @error = JSON.parse(Base64.decode64(encoded_error)) rescue JSON::ParserError
      @hmac, @raw_payload = self.class.decode(@error["proxyPayload"]) if @error
    end

    def valid_hmac?
      return false if @raw_payload.blank? || @hmac.blank?
      SecurityUtils.secure_compare(@hmac, self.class.hmac(@raw_payload))
    end

    def fresh_violation?
      parsed_payload["timestamp"] && parsed_payload["timestamp"].to_i >= FRESH_UNTIL.ago.to_i
    end

    # Public: an ambiguosly named method that checks various conditions to see
    # if we will report/block the proxy site. This allows:
    #
    # 1. Whitelisted proxies
    # 2. GitHub IPs
    # 3. Fastly IPs
    # 4. Private IPs
    # 5. Non-http protocols
    # 6. GitHub hosts
    def take_action?
      !APPROVED_HOSTS_THAT_PROXY_GITHUB.include?(parsed_proxy_url.host) &&
        !BannedIpAddress.permanently_allowed_ip?(proxy_address) &&
        ACTIONABLE_PROXY_SCHEMES.include?(parsed_proxy_url.scheme) &&
        !our_host?
    end

    def proxy_host
      [parsed_proxy_url.host, parsed_proxy_url.port].join(":")
    end

    def proxy_address
      @proxy_address ||= parsed_payload["remote_address"]
    end

    def request_id
      parsed_payload["request_id"]
    end

    private
    # Private: do not block proxy IPs because of whacky bugs in our applications
    # e.g. https://github.com/github/github/pull/60932#pullrequestreview-9847
    # Yes, the optional `.` is intentional because the DNS root zone is
    # required but is assumed to be `.`.
    def our_host?
      !!(parsed_proxy_url.host.to_s =~ /(#{GitHub.host_name}|#{GitHub.gist_host_name})\.?\z/)
    end

    def parsed_payload
      @parsed_payload ||= begin
        JSON.parse(@raw_payload)
      rescue JSON::ParserError
        {}
      end
    end

    def parsed_proxy_url
      @parsed_proxy_url ||= begin
        Addressable::URI.parse(@error["url"])
      rescue Addressable::InvalidURIError
        Addressable::URI.new
      end
    end
  end
end
