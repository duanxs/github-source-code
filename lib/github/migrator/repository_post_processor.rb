# frozen_string_literal: true

module GitHub
  class Migrator
    class RepositoryPostProcessor < BasePostProcessor

      def joins
        %i(issues commit_comments)
      end

      # Public: Post process a repository rewrites reference urls and mentions
      # in commit comment bodies.
      def process(repo, attributes)
        if sequence_number = repo.issues.maximum(:number)
          Sequence.set(repo, sequence_number)
        end

        repo.commit_comments.each do |comment|
          rewritten_body = user_content_rewriter.process(comment)
          next if comment.body == rewritten_body
          comment.update_column(:body, rewritten_body)
        end
      end
    end
  end
end
