# frozen_string_literal: true

module GitHub
  class Migrator
    class PullRequestPostProcessor < BasePostProcessor

      def joins
        %i(issue review_comments)
      end

      # Public: Post processing a pull request does the following:
      #
      #   1. Rewrites urls referencing other issues in review comment bodies.
      #   2. Rewrites @mentions in review comment bodies.
      #   3. Post processes the associated issue.
      #   4. Update the comment count
      def process(pull_request, attributes)
        # fix for referred_at
        pull_request.review_comments.update_all("pull_request_review_comments.updated_at = pull_request_review_comments.created_at")
        pull_request.review_comments.each do |comment|
          comment.body = user_content_rewriter.process(comment)
          next unless comment.changed?
          update_body_with_references(comment)
        end

        issue_post_processor.process(pull_request.issue, attributes)
        pull_request.issue.update_issue_comments_count
      end

    private

      # Internal: Memoized GitHub::Migrator::IssuePostProcessor instance for
      # processing the Issue associated with a PullRequest.
      #
      # Returns a GitHub::Migrator::IssuePostProcessor.
      def issue_post_processor
        @issue_post_processor ||= \
          GitHub::Migrator::IssuePostProcessor.new(post_processor_options)
      end
    end
  end
end
