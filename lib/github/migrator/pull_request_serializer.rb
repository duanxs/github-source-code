# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class PullRequestSerializer < IssueSerializer
      def scope
        PullRequest.includes(:user, :review_requests, issue: included_issue_associations)
      end

      def as_json(options = {})
        {
          type:             "pull_request",
          url:              url,
          user:             user,
          repository:       repository,
          title:            title,
          body:             body,
          base:             base,
          head:             head,
          assignee:         assignee,
          assignees:        assignees,
          milestone:        milestone,
          labels:           labels,
          review_requests:  review_requests,
          work_in_progress: work_in_progress,
          merged_at:        merged_at,
          closed_at:        closed_at,
          created_at:       created_at,
        }
      end

    private

      def issue
        model.issue
      end

      def base
        {
          ref: model.display_base_ref_name,
          sha: model.base_sha,
          user: url_for_association(model, :base_user),
          repo: url_for_association(model, :base_repository),
        }
      end

      def head
        {
          ref: model.display_head_ref_name,
          sha: model.head_sha,
          user: url_for_association(model, :head_user),
          repo: url_for_association(model, :head_repository),
        }
      end

      def merged_at
        time(model.merged_at)
      end

      def review_requests
        model.review_requests.map do |review_request|
          {
            reviewer: url_for_model(review_request.reviewer),
            reviewer_type: review_request.reviewer_type,
            created_at: time(review_request.created_at),
            updated_at: time(review_request.updated_at),
            dismissed_at: time(review_request.dismissed_at),
          }
        end
      end

      def work_in_progress
        model.work_in_progress
      end
    end
  end
end
