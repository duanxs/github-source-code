# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class ProjectSerializer < BaseSerializer
      def scope
        Project.includes(included_associations)
      end

      def as_json(options = {})
        {
          type:       "project",
          url:        url,
          owner:      owner,
          creator:    creator,
          name:       name,
          body:       body,
          number:     number,
          public:     public?,
          columns:    columns,
          created_at: created_at,
          closed_at:  closed_at,
          deleted_at: deleted_at,
        }
      end

    private

      def included_associations
        [:owner, :creator, :columns, :cards]
      end

      def project
        model
      end

      def columns
        [].tap do |project_columns|
          project.columns.map do |column|
            project_columns << {
              name: column.name,
              position: column.position,
              hidden_at: time(column.hidden_at),
              cards: cards_for_column(column),
            }
          end
        end
      end

      def cards_for_column(column)
        [].tap do |project_cards|
          column.cards.each do |card|
            project_cards << {
              creator: url_for_model(card.creator),
              content: (url_for_model(card.content) if card.content_id.present?),
              note: card.note,
              priority: card.priority,
              hidden_at: time(card.hidden_at),
            }
          end
        end
      end

      def owner
        url_for_model(project.owner)
      end

      def creator
        url_for_model(project.creator)
      end

      def name
        project.name
      end

      def body
        project.body
      end

      def number
        project.number
      end

      def public?
        project.public?
      end

      def closed_at
        time(project.closed_at)
      end

      def created_at
        time(project.created_at)
      end

      def deleted_at
        time(project.deleted_at)
      end
    end
  end
end
