# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class MilestoneImporter < GitHub::Migrator::Importer

      def import(attributes, options = {})
        Milestone.create! do |milestone|
          milestone.repository = model_from_source_url!(attributes["repository"], attributes)
          milestone.created_by = model_from_source_url!(attributes["user"], attributes) || milestone.repository.owner
          milestone.title = attributes["title"]
          milestone.description = attributes["description"]
          milestone.state = attributes["state"]
          milestone.due_on = attributes["due_on"]
          milestone.created_at = attributes["created_at"]
        end
      end
    end
  end
end
