# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class UserImporter < GitHub::Migrator::Importer

      def import(attributes, options = {})
        user = User.new do |user|
          user.login = last_part_of_url(options[:target_url]) || attributes["login"]
          user.password = SecureRandom.hex
          user.encrypt_password # sets user.bcrypt_auth_token
          user.created_at = attributes["created_at"]

          if primary_email = attributes["emails"].find { |email| !!email["primary"] }
            user.email = primary_email["address"]
          else
            user.email = "#{user.login}@migrations.noreply.#{GitHub.host_name}"
          end

          user.build_profile({
            name: attributes["name"],
            company: attributes["company"],
            blog: attributes["website"],
            location: attributes["location"],
          })
        end

        unless User.private_method_defined?(:skip_seat_limit_enforcement?)
          def user.enforce_seat_limit; end
        end

        user.save!
        user.emails.first.verify! if GitHub.email_verification_enabled?

        user
      rescue ActiveRecord::RecordInvalid => error
        if error.message =~ /Emails is invalid/
          user.emails = []
          user.email = "#{user.login}-#{SecureRandom.hex}@migrations.noreply.#{GitHub.host_name}"
          user.save!

          GitHub::Logger.log({
            fn: "GitHub::Migrator::UserImporter#import",
            class: error.class.name,
            model_name: "user",
            source_url: attributes["url"],
            resolution: "Updated email to #{user.email}",
          })

          user
        else
          raise error
        end
      end
    end
  end
end
