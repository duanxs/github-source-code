# rubocop:disable Style/FrozenStringLiteralComment

require "github/migrator/migratable_models"

module GitHub
  class Migrator

    class MigrationReporter
      include AreasOfResponsibility

      areas_of_responsibility :migration

      attr_reader :migratable_resource_scope
      attr_reader :extra

      def initialize(migratable_resource_scope, extra = {})
        @migratable_resource_scope = migratable_resource_scope
        @extra = extra
      end

      def self.migrator_result(migratable_resource_scope, extra = {})
        MigrationReporter.new(migratable_resource_scope, extra).migrator_result
      end

      def self.record_result_for(migration)
        MigrationReporter.new(migration.migratable_resources).record_result_for(migration)
      end

      # Internal: This method will build a hash of results by iterating through
      # MIGRATABLE_MODELS and calling count on the passed in migratable_resource_scope.
      #
      # Adds migratable_resource_scope #guid and optional extra Hash to result Hash.
      #
      # migratable_resource_scope - MigratableResource migration to count on.
      # extra - (optional) Hash with other results.
      def migrator_result
        result = GitHub::Migrator::MIGRATABLE_MODELS.inject({guid: migratable_resource_scope.guid}) do |result, migratable_model|
          model_type = model_type_key(migratable_model)
          result[model_type] = total_count.fetch(migratable_model.model_type, 0)
          result
        end.merge(extra)

        result
      end

      def record_result_for(migration)
        GitHub::Migrator::MIGRATABLE_MODELS.each do |migratable_model|
          MigratableResourceReport.create!(
            migration: migration,
            model_type: model_type_key(migratable_model),
            total_count: total_count[migratable_model.model_type].to_i,
            success_count: success_count[migratable_model.model_type].to_i,
            failure_count: failure_count[migratable_model.model_type].to_i,
          )
        end
      end

      private

      def success_count
        @_success_count ||= MigratableResource.uncached do
          migratable_resource_scope.succeeded.group("model_type").count
        end
      end

      def failure_count
        @_failure_count ||= MigratableResource.uncached do
          migratable_resource_scope.failed.group("model_type").count
        end
      end

      def total_count
        @_total_count ||= MigratableResource.uncached do
          migratable_resource_scope.group("model_type").count
        end
      end

      def model_type_key(migratable_model)
        migratable_model.model_type.pluralize.to_sym
      end
    end
  end
end
