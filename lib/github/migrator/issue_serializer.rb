# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class IssueSerializer < BaseSerializer
      def scope
        Issue.includes(included_issue_associations)
      end

      def as_json(options = {})
        {
          type: "issue",
          url: url,
          repository: repository,
          user: user,
          title: title,
          body: body,
          assignee: assignee,
          assignees: assignees,
          milestone: milestone,
          labels: labels,
          closed_at: closed_at,
          created_at: created_at,
          updated_at: updated_at,
        }
      end

    private

      def included_issue_associations
        [:repository, :user, :assignee, :assignees, :milestone, :labels]
      end

      def issue
        model
      end

      def repository
        url_for_model(issue.repository)
      end

      def user
        url_for_model(issue.user)
      end

      def title
        issue.title
      end

      def body
        issue.body
      end

      def assignee
        assignees.first
      end

      def assignees
        issue.assignees.map do |assignee|
          url_for_model(assignee)
        end
      end

      def milestone
        if milestone = issue.milestone
          url_for_model(milestone)
        end
      end

      def labels
        issue.labels.map do |label|
          url_for_model(label)
        end
      end

      def closed_at
        time(issue.closed_at)
      end

      def updated_at
        time(issue.updated_at)
      end

    end
  end
end
