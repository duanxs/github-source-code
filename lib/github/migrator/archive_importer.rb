# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class ArchiveImporter
      include MigratorHelper
      include AreasOfResponsibility

      areas_of_responsibility :migration

      attr_reader :guid, :archive_path, :migration_path, :actor, :event_handler, :per_second

      def initialize(guid:, archive_path:, migration_path:, actor:, event_handler: Events::Null.new, per_second: nil)
        @guid = guid
        @archive_path = archive_path
        @migration_path = migration_path
        @actor = actor
        @event_handler = event_handler
        @per_second = per_second
      end

      def call
        raise EmptyMigration unless current_migration.count > 0
        extract_archive(archive_path, migration_path)

        event_handler.fire(:sample_resource_usage)
        importing_mode do
          import_models
          post_process_models

          # Suspend users that don't belong to repository owning orgs or are
          # repository collaborators.
          UserSuspender.process(current_migration)
          event_handler.fire(:progress_with_count, 1)
        end

        # Index models that are imported using the direct insert approach.
        ModelIndexer.process(current_migration)
        event_handler.fire(:progress_with_count, 1)

        event_handler.fire(:sample_resource_usage)
        MigrationReporter.migrator_result(current_migration.migrated)
      ensure
        cleanup_after_migrator
        cache.clear
      end

      private

      def import_models
        # Import (and optionally rename), map, and merge each model that has
        # a migratable_model.importer.
        MIGRATABLE_MODELS.each do |migratable_model|
          next unless migratable_model && migratable_model.importer

          importer = migratable_model.importer.new(
            actor:             actor,
            current_migration: current_migration,
            migration_path:    migration_path,
            model_url_service: model_url_service,
            cache:             cache,
          )

          get_serialized_models_from_archive(migratable_model.model_type) do |serialized_models|
            event_handler.fire(:sample_resource_usage)
            migratable_resources_by_url = migratable_resources_for_serialized_models_by_url(serialized_models)
            models_by_id = models_for_migratable_resources_by_id(migratable_resources_by_url.values)

            serialized_models.each do |attributes|

              # This throttler is used on GitHub Enterprise and can be set
              # manually from the gh-migrator command line utility.
              per_second_throttler.throttle do
                begin
                    migratable_resource = migratable_resources_by_url[attributes["url"]]
                    migratable_resource.model_type = attributes["type"]
                    MigratableResource.throttle_with_retry do
                      migratable_resource.save
                    end

                  GitHub.dogstats.time("migrator.import.import_model.time", tags: ["model_type:#{migratable_resource.model_type}", "guid:#{guid}"]) do
                    # This is a model specific throttler that is used while importing records
                    # on our internal infrastucture (currently used for SaaS migrations).
                    #
                    # On Enterprise this throttle will just yield immediately.
                    model = migratable_model.model_class.throttle_with_retry(max_retry_count: 8) do
                      import_map_or_merge(
                        models_by_id[migratable_resource.model_id],
                        migratable_resource,
                        importer,
                        attributes,
                      )
                    end

                    next unless model

                    if model.present? && model.persisted?
                      migratable_resource.model_id = model.id
                      migratable_resource.target_url = url_for_model(model)
                    end

                    MigratableResource.throttle_with_retry do
                      migratable_resource.save
                    end

                    event_handler.fire(:progress_with_count, 1)
                  end
                rescue AssociationFailed => error
                  migratable_resource.warning = error.message
                  migratable_resource.failed_import!
                rescue => error
                  log_error_and_fail(error, migratable_resource, attributes)

                  raise error
                end
              end
            end
          end
        end
      end

      def post_process_models
        # Post process each model that has a migratable_model.post_processor.
        MIGRATABLE_MODELS.each do |migratable_model|
          next unless migratable_model && migratable_model.post_processor

          model_type = migratable_model.model_type
          post_processor = migratable_model.post_processor.new(
            current_migration:     current_migration,
            user_content_rewriter: user_content_rewriter,
          )

          get_serialized_models_from_archive(model_type) do |serialized_models|
            migratable_resources_by_url = migratable_resources_for_serialized_models_by_url(serialized_models)
            models_by_id = models_for_migratable_resources_by_id(migratable_resources_by_url.values, joins: post_processor.joins)

            serialized_models.each do |attributes|
              # This throttler is used on GitHub Enterprise and can be set
              # manually from the gh-migrator command line utility.
              per_second_throttler.throttle do
                migratable_resource = migratable_resources_by_url[attributes["url"]]

                begin
                  GitHub.dogstats.time("migrator.import.post_process_models.time", tags: ["model_type:#{migratable_resource.model_type}", "guid:#{guid}"]) do
                    # This is a model specific throttler that is used while importing records
                    # on our internal infrastucture (currently used for SaaS migrations).
                    #
                    # On Enterprise this throttle will just yield immediately.
                    migratable_model.model_class.throttle_with_retry do
                      post_processor.process(models_by_id[migratable_resource.model_id], attributes)
                    end
                  end
                rescue NoMethodError => error
                  GitHub::Logger.log_exception({
                    at:         "raise",
                    fn:         "GitHub::Migrator::ArchiveImporter#post_process_models",
                    class:      error.class.name,
                    model_name: "attachment",
                    source_url: attributes["url"],
                    resolution: "Skipped import",
                  }, error)
                end

                event_handler.fire(:progress_with_count, 1)
              end
            end
          end
        end
      end

      def import_map_or_merge(model_by_id, migratable_resource, importer, attributes)
        model_methods = {
          import_migratable_resource: [
            model_by_id,
            migratable_resource,
            importer,
            attributes,
          ],
          merge_migratable_resource: [
            model_by_id,
            migratable_resource,
            importer,
            attributes,
          ],
          map_migratable_resource: [
            model_by_id,
            migratable_resource,
          ],
          skip_migratable_resource: [
            migratable_resource,
            attributes,
          ],
          merge_owners_team: [
            model_by_id,
            migratable_resource,
            importer,
            attributes,
          ],
        }
        # We don't want to continue calling methods once one returns a model, so
        # this will abort once one is returned.
        model_methods.each do |method, args|
          model = send(method, *args)
          return model if model
        end
        nil
      end

      def import_migratable_resource(model_by_id, migratable_resource, importer, attributes)
        return unless migratable_resource.should_import?(model_by_id)
        model = importer.import(attributes, target_url: migratable_resource.target_url)
        if model.present?
          if renaming?(migratable_resource)
            migratable_resource.renamed
          else
            migratable_resource.imported
          end
        else
          if renaming?(migratable_resource)
            migratable_resource.failed_rename
          else
            migratable_resource.failed_import
          end
        end
        model
      end

      def merge_migratable_resource(model_by_id, migratable_resource, importer, attributes)
        return unless migratable_resource.should_merge?(model_by_id)
        model = importer.merge(attributes, model_by_id)
        if model.present?
          migratable_resource.merged
        else
          migratable_resource.failed_merge
        end
        model
      end

      def map_migratable_resource(model_by_id, migratable_resource)
        return unless migratable_resource.should_map?(model_by_id)
        migratable_resource.mapped
        model_by_id
      end

      def skip_migratable_resource(migratable_resource, attributes)
        return unless migratable_resource.skip?

        model = migratable_resource.model

        unless model
          model = Mannequin.new(
            source_login: attributes["login"],
            migration: migration,
          )

          emails = attributes["emails"]

          if emails.present?
            emails.each do |email|
              model.emails.new(
                email:   email["address"],
                primary: email["primary"],
              )
            end
          end

          begin
            model.save!
          rescue ActiveRecord::ActiveRecordError => exception
            migratable_resource.failed_skip
            raise(FailedSkip, exception)
          end
        end

        migratable_resource.skipped

        model
      end

      def migration
        @migration ||= ::Migration.find_by(guid: guid)
      end

      def merge_owners_team(model_by_id, migratable_resource, importer, attributes)
        return unless migratable_resource.should_merge_owners_team?
        model = importer.merge(attributes, model_by_id)
        if model.present?
          migratable_resource.merged
        else
          migratable_resource.failed_merge
        end
        model
      end

      def log_error_and_fail(error, migratable_resource, attributes)
        GitHub::Logger.log(
          fn:             "GitHub::Migrator#import!",
          class:          error.class.name,
          model_name:     attributes["type"],
          model_id:       migratable_resource.try(:model_id),
          source_url:     attributes["url"],
          target_url:     migratable_resource.try(:target_url),
          translator_url: translator_url(migratable_resource),
          state:          migratable_resource.try(:state),
        )

        if migratable_resource
          migratable_resource.failed_import unless migratable_resource.failed?
          migratable_resource.save
        end
      end

      def renaming?(migratable_resource)
        if translate_urls?
          migratable_resource.rename?
        else
          migratable_resource.target_url.present?
        end
      end

      def translator_url(migratable_resource)
        if translate_urls?
          url_translator.translate(
            migratable_resource.model_type, migratable_resource.source_url
          )
        end
      end
    end
  end
end
