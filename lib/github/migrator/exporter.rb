# rubocop:disable Style/FrozenStringLiteralComment

require "github/migrator/tar_utils"

# Exporter is a service object that iterates through a migration's MigratableResources,
# serializes the models to JSON and writes them to a file, and archives any related files
# such as git repositories, file attachments or release archives.
#
module GitHub
  class Migrator
    class Exporter
      class ExportFailure < MigrationError; end

      include GitHub::Migrator::TarUtils
      include AreasOfResponsibility

      areas_of_responsibility :migration

      # Public: Instantiates an Exporter for the migration represented by guid.
      #
      # guid                  - A unique identifier representing the migration to be
      #                         exported.
      # migration_path        - The path that the JSON file containing all of the
      #                         serialized models, and all of the dependent files
      #                         (repos, attachments, releases, wikis) will be stored
      #                         in order to create the final archive
      # archive_export_path   - The filepath where the final archive (tarball) will
      #                         be located.
      # actor                 - The actor performing the export, must have read
      #                         permissions for all of the models being exported.
      # options values:
      # events:               - An instance of GitHub::Migrator::Events that will notify
      #                         its subscribers of progress.
      #
      def initialize(guid:, migration_path:, archive_export_path:, actor:, owner:, options: nil)
        options ||= {}
        @guid                 = guid
        @migration_path       = migration_path
        @archive_export_path  = archive_export_path
        @actor                = actor
        @owner                = owner
        @events               = options.fetch(:events) { GitHub::Migrator::Events::Null.new }
        @cache                = GitHub::Migrator::Cache.new
        @schema_version       = GitHub::Migrator::SchemaVersion.new
        @model_url_service    = GitHub::Migrator::ModelUrlService.new(cache: cache)
        @current_migration    = MigratableResource.for_guid(guid)
      end

      # Public: Execute the action. Serializes all of the MigratableResources for the
      # migration represented by guid and all of its file dependencies, then creates
      # the final archive as a tarball at archive_export_path
      #
      # Raises an EmptyMigration if no MigratableResources exist for the guid.
      #
      # Returns a Hash with a count of each model type that was exported.
      def call
        raise GitHub::Migrator::EmptyMigration unless current_migration.count > 0

        Dir.mkdir(migration_path) unless File.exist?(migration_path)

        GitHub::Migrator::MIGRATABLE_MODELS.each do |migratable_model|
          serialize_model_and_dependent_files(migratable_model)
        end

        write_schema_version_to_archive
        create_archive(migration_path, archive_export_path)

        GitHub::Migrator::MigrationReporter.migrator_result(
          current_migration.by_states(:exported),
          archive_path: archive_export_path.to_s,
        )
      ensure
        FileUtils.rm_rf(migration_path, secure: true)
        cache.clear
      end

      private

      attr_reader :model_url_service, :migration_path, :cache, :guid, :actor, :owner, :current_migration, :events, :schema_version, :archive_export_path

      # Take the given MigratableModel and retrieve its serializer and archiver if it has one.
      #
      # Iterate through all of the MigratableResources for this migration guid and given
      # MigratableModel, serialize the related models to the JSON file, and write any related
      # files to the filesystem at migration_path.
      #
      # Copy files to migration_path, to prep for archiving (repositories, attachments, releases.).
      def serialize_model_and_dependent_files(migratable_model)
        serializer = migratable_model.serializer.new(
          model_url_service: model_url_service,
          actor: actor,
          owner: owner,
        )
        archiver = migratable_model.build_archiver(actor: actor, migration_path: migration_path)
        model_type = migratable_model.model_type
        batch = 1

        current_migration.by_model_type(model_type).find_in_batches(batch_size: 100) do |migratable_resources|
          models = MigratableResource.models_for_migratable_resources(migratable_resources, scope: serializer.scope)

          begin
            serialized_models = models.map do |model|
              serializer.serialize(model)
            end.compact

            write_serialized_models_to_migration_path(serialized_models, model_type, batch)
          rescue => e
            set_state_on_migratable_resources(:failed_export, migratable_resources)
            raise ExportFailure.new("error exporting #{model_type} (model ids: #{migratable_resources.map(&:model_id)}): (#{e.class}) #{e}")
          end

          if archiver
            models.each do |model|
              archiver.save(model)
            end
          end

          set_state_on_migratable_resources(:exported, migratable_resources)

          batch += 1

          events.fire(:progress_with_count, migratable_resources.size)
        end
      end

      # Internal: Writes the JSON representing the current batch of serialized models to
      # migration_pathto a filename indicating the model type and batch number, to the
      # folder at migration_path, for instance:
      #
      # repositories_000003.json
      #
      # serialized_models:    - An array representing the current batch of serialized models
      # model_type            - The downcased string class name of the models (will be pluralized)
      # batch                 - The number of the current batch
      def write_serialized_models_to_migration_path(serialized_models, model_type, batch)
        return if serialized_models.empty?

        filename = "#{model_type.pluralize}_#{batch.to_s.rjust(6, '0')}.json"
        File.open(File.join(migration_path, filename), "w") do |file|
          file.write(::JSON.pretty_generate(serialized_models.as_json))
        end
      end

      # Internal: Set state on migratable resources. It's necessary to use the write db
      # here because the gh-migrator cli otherwise is set to use the read-only db.
      #
      # state                - Symbol state for MigratableResource.
      # migratable_resources - Array of MigratableResource instances.
      def set_state_on_migratable_resources(state, migratable_resources)
        ActiveRecord::Base.connected_to(role: :writing) do
          current_migration.set_state!(state, migratable_resources)
        end
      end

      # Internal: Writes the current version of gh-migrator's schema to the archive.
      # The schema version allows the migrator to understand what it can and can't
      # import from a given archive.
      #
      # See: GitHub::Migrator::SchemaVersion
      def write_schema_version_to_archive
        File.open(File.join(migration_path, "schema.json"), "w") do |file|
          info = {
            version: schema_version.for_export(current_migration),
            github_sha: GitHub.current_sha,
          }
          file.write info.to_json
        end
      end
    end
  end
end
