# rubocop:disable Style/FrozenStringLiteralComment

require "rubygems/package"
require "tempfile"
require "zlib"

module GitHub
  class Migrator
    module TarUtils

      # Public: Create a gzipped archive with the contents of source_path.
      #
      # source_path  - Pathname or String path to files that need archived.
      # archive_path - Pathname or String path and filename for created archive.
      #
      # Returns Pathname or String archive_path.
      def create_archive(source_path, archive_path)
        options = ["-czf", "#{archive_path}", "-C", "#{source_path}", "."]

        # On OSX add the --disable-copyfile option to prevent ._ entries.
        if RbConfig::CONFIG["host_os"].start_with? "darwin"
          options.unshift("--disable-copyfile")
        end

        child = POSIX::Spawn::Child.new("tar", *options)
        raise child.err unless child.success?

        return archive_path
      end

      # Public: Extract file(s) from archive to destination path and optionally
      # cleanup extracted files after yielding a block if it is provided.
      #
      # archive_path     - Pathname or String path to archive.
      # destination_path - Pathname or String destination path for files.
      #
      # Returns Pathname or String destination_path (or NilClass if block given).
      def extract_archive(archive_path, destination_path, &block)
        options = ["-xzf", "#{archive_path}", "-C", "#{destination_path}"]

        FileUtils.mkdir_p(destination_path)

        child = POSIX::Spawn::Child.new("tar", *options)
        raise child.err unless child.success?

        recursively_rm_symlinks(destination_path) if File.exist?(destination_path)

        if block_given? && File.exist?(destination_path)
          yield
          FileUtils.rm_rf(destination_path, secure: true)
          return nil
        else
          return destination_path
        end
      end

      def read_from_archive(pattern)
        Dir["#{migration_path}/#{pattern}"].each do |file_path|
          File.open(file_path, "r") do |file|
            yield(file)
          end
        end
      end

      def read_file_from_archive(path)
        full_path = File.join(migration_path, path)
        File.open(full_path, "r")
      end

      private

      def recursively_rm_symlinks(destination_path)
        Find.find(destination_path).each do |path|
          File.delete(path) if File.symlink?(path)
        end
      end
    end
  end
end
