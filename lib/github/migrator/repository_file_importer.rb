# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class RepositoryFileImporter < Importer
      include GitHub::Migrator::TarUtils

      def import(attributes, options = {})
        file_asset_path =
          begin
            URI.parse(attributes["file_url"]).path[1..-1]
          rescue URI::InvalidURIError => e
            attributes.fetch("file_url").sub(%r{\Atarball://root/}, "")
          end
        repository_file = RepositoryFile.new do |repository_file|
          repository_file.uploader = model_from_source_url!(attributes["user"], attributes) || User.ghost
          repository_file.repository = model_from_source_url!(attributes["repository"], attributes)
          repository_file.name = attributes["file_name"]
          repository_file.content_type = attributes["file_content_type"]
          repository_file.created_at = attributes["created_at"]
        end

        tmp_file = read_file_from_archive(file_asset_path)

        asset_file = ActionDispatch::Http::UploadedFile.new(
          filename: attributes["file_name"],
          tempfile: tmp_file,
          type: attributes["file_content_type"],
        )

        import_asset(repository_file, asset_file)
      rescue GitHub::Migrator::AssociationFailed => error
        GitHub::Logger.log(
          fn: "GitHub::Migrator::RepositoryFileImporter#import",
          class: error.class.name,
          model_name: "repository_file",
          source_url: attributes["url"],
          resolution: "Skipped import",
        )

        return nil
      rescue GitHub::Storage::Uploader::Error,
             ActiveRecord::NotNullViolation,
             ActiveRecord::RecordInvalid => error
        GitHub::Logger.log_exception({
          at: "raise",
          fn: "GitHub::Migrator::RepositoryFileImporter#import",
          class: error.class.name,
          model_name: "repository_file",
          source_url: attributes["url"],
          resolution: "Skipped import",
        }, error)

        return nil
      ensure
        tmp_file.close
      end
    end
  end
end
