# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class IssuePostProcessor < BasePostProcessor

      def joins
        [:comments, :labels, { repository: :labels }]
      end

      # Public: Post processing an issue does the following:
      #
      #   1. Applies labels.
      #   2. Rewrites urls referencing other issues in comment bodies.
      #   3. Rewrites @mentions in comment bodies.
      #   4. Rewrites urls referencing other issues in issue body.
      #   5. Rewrites @mentions in the issue body.
      #   6. Sets up the Conversation for the issue.
      def process(issue, attributes)
        # Apply labels.
        attributes["labels"].each do |label_source_url|
          label_name = CGI.unescape(label_source_url.split("/")[6..-1].join("/"))
          if label = issue.repository.labels.detect { |l| l.name == label_name }
            next if issue.labels.include?(label)
            issue.labels << label
          end
        end

        issue.comments.update_all("issue_comments.updated_at = issue_comments.created_at")
        issue.comments.each do |comment|
          comment.body = user_content_rewriter.process(comment)
          next unless comment.changed?
          update_body_with_references(comment)
        end

        issue.body = user_content_rewriter.process(issue)
        update_body_with_references(issue)

        # Sets up the Conversation for the issue (which also creates Sequence
        # for the repository if a Sequence doesn't already exist).
        issue.setup_conversation unless issue.conversation.present?

        # Update milestone counts.
        issue.update_milestone_counts!
      end
    end
  end
end
