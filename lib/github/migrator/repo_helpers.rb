# frozen_string_literal: true

module GitHub
  class Migrator
    module RepoHelpers
      def local_repo?(repo)
        repo.host == GitHub.local_git_host_name
      end
    end
  end
end
