# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class AttachmentImporter < Importer
      include GitHub::Migrator::TarUtils

      def import(attributes, options = {})
        attachment = Attachment.new do |attachment|
          attachable_key = [
            "issue",
            "pull_request",
            "issue_comment",
            "commit_comment",
            "pull_request_review_comment",
          ].find { |attachable_key| attributes.has_key?(attachable_key) }

          attachable = model_from_source_url!(attributes[attachable_key], attributes)
          attachable = attachable.issue if attachable.is_a?(PullRequest)

          attachment.attachable = attachable
          attachment.attacher = model_from_source_url!(attributes["user"], attributes) || User.ghost
          attachment.entity = attachable.repository
          attachment.created_at = attributes["created_at"]

          asset_path =
            begin
              URI.parse(attributes["asset_url"]).path[1..-1]
            rescue URI::InvalidURIError => e
              attributes.fetch("asset_url").sub(%r{\Atarball://root/}, "")
            end
          read_from_archive(asset_path) do |file|
            asset = UserAssetExtendedContentTypes.new
            asset.uploader = attachment.attacher
            asset.user_id ||= attachment.attacher_id
            asset.name = attributes["asset_name"]

            asset_file = ActionDispatch::Http::UploadedFile.new({
              filename: attributes["asset_name"],
              tempfile: file,
              type: attributes["asset_content_type"],
            })
            attachment.asset = import_asset(asset, asset_file)
          end
        end

        import_model(attachment)

      rescue GitHub::Migrator::AssociationFailed => error
        GitHub::Logger.log({
          fn: "GitHub::Migrator::AttachmentImporter#import",
          class: error.class.name,
          model_name: "attachment",
          source_url: attributes["url"],
          resolution: "Skipped import",
        })

        return nil
      rescue GitHub::Storage::Uploader::Error,
             ActiveRecord::NotNullViolation,
             ActiveRecord::RecordInvalid => error
        GitHub::Logger.log_exception({
          at: "raise",
          fn: "GitHub::Migrator::AttachmentImporter#import",
          class: error.class.name,
          model_name: "attachment",
          source_url: attributes["url"],
          resolution: "Skipped import",
        }, error)

        return nil
      end

      class UserAssetExtendedContentTypes < ::UserAsset
        include ::Storage::Uploadable
        set_content_types \
          "application/pdf" => ".pdf",
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => ".docx",
          "application/vnd.openxmlformats-officedocument.presentationml.presentation" => ".pptx",
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => ".xlsx",
          "application/zip" => ".zip",
          "application/x-zip-compressed" => ".zip",
          "application/gzip" => ".gz",
          "application/x-gzip" => ".gz",
          "text/plain" => ".txt",
          "text/x-log" => ".log",
          "image/gif"  => ".gif",
          "image/jpeg" => %w(.jpg .jpeg),
          "image/png"  => ".png"
      end
    end
  end
end
