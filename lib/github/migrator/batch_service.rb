# frozen_string_literal: true

require "set"

module GitHub
  class Migrator
    class BatchService
      MAX_RETRIES = 5.freeze

      include AreasOfResponsibility

      attr_reader :batch_size, :retry_exception_class

      areas_of_responsibility :migration

      def initialize(options = nil, &block)
        options = options || {}
        @block = block
        @batch_size = options[:batch_size] || default_batch_size
        @retry_exception_class = options[:retry_on]
      end

      def add(thing)
        set_counter(batch.add(thing).size)

        if counter == batch_size
          block.call(batch)
          reset_batch
        end
      end

      def complete
        if batch.any?
          call_with_retry { block.call(batch) }
        end
        reset_batch
      end

      private

      attr_reader :block

      def batch
        @batch ||= Set.new
      end

      def counter
        @counter ||= 0
      end

      def set_counter(value)
        @counter = value
      end

      def reset_batch
        @counter = 0
        @batch = Set.new
      end

      def call_with_retry(retries = 0, &block)
        return block.call unless retry_exception_class
        begin
          block.call
        rescue retry_exception_class
          if retries < MAX_RETRIES
            sleep((2 ** retries - 1) / 4.0) if Rails.env.production?
            call_with_retry(retries + 1, &block)
          else
            raise
          end
        end
      end

      # Internal: Determine default batch size for BatchService
      # If we're exporting/importing into Enterprise, use 1000
      # If we're exporting/importing into GitHub.com, 100
      def default_batch_size
        return 1000 if GitHub.enterprise?
        50
      end
    end
  end
end
