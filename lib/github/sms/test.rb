# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module SMS
    class Test < Provider
      NON_MOBILE_NUMBER    = "+1 3072581234"
      NON_VALID_NUMBER     = "+1 3072582345"
      SERVER_ERROR_NUMBER  = "+1 3072583456"
      TIMEOUT_ERROR_NUMBER = "+1 3072584567"

      # Normalize a number for comparing test numbers.
      def self.normalize(number)
        Phonelib.parse(number).e164
      end

      ERROR_MAPPING = {
        normalize(NON_MOBILE_NUMBER)    => :number_not_mobile,
        normalize(NON_VALID_NUMBER)     => :number_not_valid,
        normalize(SERVER_ERROR_NUMBER)  => :server,
        normalize(TIMEOUT_ERROR_NUMBER) => :timeout,
      }

      # Send an SMS.
      #
      # to      - The number to send to.
      # message - The message to send.
      #
      # Returns a GitHub::SMS::Receipt. Raises GitHub::SMS::Error.
      def send_message(to, message)
        noop

        if code = ERROR_MAPPING[self.class.normalize(to)]
          raise_for_code(code)
        end

        Receipt.new(
          provider: self,
          message_id: SecureRandom.hex,
        )
      end

      # Get SMS logs for a given number.
      #
      # to - The number to get logs for.
      #
      # Returns an Array of log messages.
      def log_for(to)
        noop
        []
      end

      private
      # A noop method called by send_message so Mocha expectations can count
      # the number of times send_message is called without stubbing send_message
      #
      # Returns nothings.
      def noop
      end
    end
  end
end
