# rubocop:disable Style/FrozenStringLiteralComment

#
# Nexmo API client and SMS::Provider subclass. Used by GitHub::SMS
# API Docs: https://docs.nexmo.com/index.php/sms-api/send-message
#
module GitHub
  module SMS
    class Nexmo < Provider
      TIME_ZONE = ActiveSupport::TimeZone["UTC"]

      # Public: Send an SMS
      #
      # to      - The number to send the SMS to.
      # message - The body of the message.
      #
      # Returns a GitHub::SMS::Receipt. Raises GitHub::SMS:Error on error responses.
      def send_message(to, message)
        params = {
          from: from,
          to: to,
          text: message,
        }
        http_res = connection.post("https://rest.nexmo.com/sms/json", params)

        # Handle unexpected status code.
        raise_for_code(:server) unless http_res.success?

        res_messages = parse_response(http_res)["messages"]

        # We only send one SMS at a time, so we should only receive one receipt.
        raise_for_code(:server) unless res_messages.length == 1
        res_message = res_messages.first
        receipt = build_receipt(res_message)

        # Check for an error response.
        unless res_message["status"] == "0"
          raise_for_code(res_message["status"], receipt.message_id)
        end

        receipt
      rescue Faraday::TimeoutError
        raise_for_code(:timeout)
      rescue Faraday::ClientError
        raise_for_code(:server)
      end

      # Get SMS logs for a given number. Only grabs the last two days of logs.
      #
      # to - The number to get logs for.
      #
      # Returns an Array of log messages.
      def log_for(to)
        log_for_date(to, TIME_ZONE.today) +
        log_for_date(to, TIME_ZONE.today.yesterday)
      end

      # Checks the value of our account balance.
      #
      # Returns a Float.
      def account_balance
        http_res = connection.get("https://rest.nexmo.com/account/get-balance")
        case http_res.status
        when 200
          res = parse_response(http_res)
          res["value"]
        else
          raise_for_code(:server)
        end
      end

      if Rails.test?
        def req_stubs
          @req_stubs ||= Faraday::Adapter::Test::Stubs.new
        end
      end

      private
      # Private: Get the log of SMS messages for a given number on a given date.
      #
      # to   - The number to get logs for.
      # date - The Date to get logs for.
      #
      # Returns an Array of log messages.
      def log_for_date(to, date)
        params = {
          to: to.gsub(/\D/, ""),
          date: "#{date.year}-#{date.month}-#{date.day}",
        }
        http_res = connection.get("https://rest.nexmo.com/search/messages", params)

        case http_res.status
        when 200
          items = parse_response(http_res)["items"]
          items.map do |item|
            {
              body: item["body"],
              date: TIME_ZONE.parse(item["date-received"]),
            }
          end
        when 400
          # Nexmo returns a 400 if the search returns no results.
          []
        else
          raise_for_code(:server)
        end
      end

      # Private: Faraday connection for HTTP requests.
      #
      # Returns a Faraday::Connection instance.
      def connection
        @connection ||= Faraday.new(faraday_options) do |conn|
          conn.request :url_encoded
          if Rails.test?
            conn.adapter :test, req_stubs
          else
            conn.adapter :net_http
          end
        end
      end

      # Private: Options to instantiate the Faraday::Connection with.
      #
      # Returns a Hash.
      def faraday_options
        {
          request: {
            timeout: TIMEOUT,
          },
          params: {
            api_key: GitHub.nexmo_api_key,
            api_secret: GitHub.nexmo_api_secret,
          },
        }
      end

      # Private: Parse the response from Nexmo.
      #
      # res - A Faraday::Response object.
      #
      # Returns a Hash.
      def parse_response(res)
        begin
          JSON.parse(res.body)
        rescue JSON::ParserError
          {}
        end
      end

      # Private: Build a Receipt from an API response.
      #
      # res - A Hash parsed from an API response.
      #
      # Returns a GitHub::SMS::Receipt.
      def build_receipt(res)
        Receipt.new(
          provider: self,
          message_id: res["message-id"],
        )
      end

      # Private: A Hash mapping provider error codes to GitHub::SMS::Error
      # subclasses.
      #
      # Returns a Hash.
      def error_code_mapping
        super.merge(
          "3"  => NumberNotValidError,
          "5"  => ServerError,
          "6"  => NumberNotMobileError,
          "9"  => BillingError,
          "13" => ServerError,
        )
      end
    end
  end
end
