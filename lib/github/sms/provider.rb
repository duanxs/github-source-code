# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module SMS
    class Provider
      # Timeout to use for each SMS delivery attempt.
      TIMEOUT = GitHub.default_request_timeout - 2

      # Send an SMS.
      #
      # to      - The number to send to.
      # message - The message to send.
      #
      # Returns a GitHub::SMS::Receipt. Raises GitHub::SMS::Error.
      def send_message(to, message)
        raise NotImplementedError
      end

      # Get SMS logs for a given number.
      #
      # to - The number to get logs for.
      #
      # Returns an Array of log messages.
      def log_for(to)
        []
      end

      # Picks a number to send the SMS from.
      #
      # Returns a String.
      def from
        GitHub.sms_numbers[provider_name].sample
      end

      # Public: The name of this provider.
      #
      # Returns a String.
      def self.provider_name
        @provider_name ||= pretty_class_name(self)
      end

      # Public: The name of this provider.
      #
      # Returns a String.
      def provider_name
        self.class.provider_name
      end

      # Get the last section of a class name, underscore, and symbolize
      # it.
      #
      # klass - The class to pretify the name of.
      #
      # Returns a String.
      def self.pretty_class_name(klass)
        klass.name.split("::").last.underscore.to_sym
      end

      private
      # Private: Raises an appropriate error from a provider error code.
      #
      # code       - The error code sent by the provider.
      # messafe_id - The message_id returned by the provider.
      #
      # Raises a GitHub::SMS::Error instance.
      def raise_for_code(code, message_id = nil)
        klass = error_code_mapping.fetch(code, UnknownError)
        error = klass.new
        error.set_backtrace(caller)

        # Instrument and report to Haystack since the exception will be rescued.
        GitHub.dogstats.increment("sms", tags: ["type:error", "providers:#{provider_name}", "error:#{code}", "subject:#{self.class.pretty_class_name(klass)}"])

        Failbot.push(
          app: "github-user",
          sms_provider: provider_name,
          provider_code: code,
          message_id: message_id,
        )
        Failbot.report(error) rescue nil

        raise error
      end

      # Private: A Hash mapping provider error codes to GitHub::SMS::Error
      # subclasses.
      #
      # Returns a Hash.
      def error_code_mapping
        {
          number_not_mobile: NumberNotMobileError,
          number_not_valid: NumberNotValidError,
          area_not_supported: AreaNotSupportedError,
          server: ServerError,
          timeout: TimeoutError,
        }
      end
    end
  end
end
