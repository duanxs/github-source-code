# frozen_string_literal: true

module GitHub
  module TrilogyFactory
    extend self

    def new(config)
      if config.key?(:resilient_properties)
        build_resilient_trilogy(config)
      else
        ::Trilogy.new(config)
      end
    end

    private

    def build_resilient_trilogy(config)
      resilient_properties = config.delete(:resilient_properties).symbolize_keys
      resilient_properties[:instrumenter] = ActiveSupport::Notifications

      Resilient::Trilogy.new(
        trilogy_options: config,
        resilient_properties: resilient_properties,
      )
    end
  end
end
