# rubocop:disable Style/FrozenStringLiteralComment

require "active_support/core_ext/class/attribute"

module GitHub
  module Jobs
    # Extend this module to obtain automatic retry helpers for known errors.
    #
    #   class MyJob
    #     extend GitHub::Jobs::RetryErrors
    #
    #     # If `self.perform_with_retry` raises either of these errors,
    #     # retry the job automatically.
    #     retry_on_error SomethingWentBoom, RemoteServiceUnavailable
    #
    #     # Instead of defining `self.perform`, define `perform_with_retry`.
    #     # The last argument must be an options hash to preserve retry count
    #     # information for the retry system.
    #     def self.perform_with_retry(arg, opts={})
    #       # ...
    #     end
    #   end
    #
    module RetryErrors

      # Public: define one or more error classes as retry-able.
      #
      # If the job's `self.perform` method raises one of these errors, the job
      # will be placed on the retry queue.
      #
      # The retry_error_classes attribute is defined on the class that exends
      # this module since `class_attribute` isn't available for Module.
      def retry_on_error(*error_classes)
        # Reassign to self, rather than concat, to prevent inplace modification
        # of a parent class' retry_error_classes collection.
        self.retry_error_classes += error_classes
      end

      # Replace a job's `perform` method with a version that automatically
      # retries when it encounters an error that it knows how to handle.
      #
      # The method that actually does the work in a job must be defined as
      # `self.perform_with_retry`, and must take an options hash as its final
      # argument.
      def perform(*args)
        # Massage the arguments to ensure there's always an options hash at
        # the end. This is required so that retry counts will propagate
        # between jobs. Without an args hash, the retry information is always
        # defaulted to "retry immediately", which prevents exponential
        # backoff from functioning.
        added_options_hash = false
        unless args.last.is_a?(Hash)
          added_options_hash = true
          args << {}
        end

        # Call the job class' `perform_with_retry` method.
        perform_with_retry(*args)

      rescue ArgumentError => e
        # Try to be nice if we've broken the `perform_with_retry` call.
        if added_options_hash && e.message =~ /wrong number/ && e.backtrace.first =~ /:in `perform_with_retry'/
          e.message << "\nWhen using RetryErrors, `perform_with_retry` must accept an options hash as its final argument."
        end
        raise

      rescue *retry_error_classes
        options = {retries: 0}
        if args.last.is_a?(Hash)
          # `options` to `retry_later` must have symbol keys, but the job
          # options hash will likely have string keys. Check for symbol keys
          # too, just in case.
          options[:retries] = args.last[:retries] || args.last["retries"] || 0
          # If we added retries ourselves, make sure it's in args too:
          args.last["retries"] = options[:retries]
          args.last.delete(:retries)
        end
        raise unless LegacyApplicationJob.retry_later(self.active_job_class, args, options)
      end

      # Internal: module extended callback.
      def self.extended(base)
        base.module_eval do
          # Internal: a collection of error classes that are retry-able.
          #
          # This is done via an `extended` callback since `class_attribute`
          # only works in a class definition context.
          class_attribute :retry_error_classes
          self.retry_error_classes = [] # set a default
        end
      end

      # Internal: don't allow inclusion, as the right callbacks won't run.
      def self.included(base)
        raise ArgumentError, "extend this module, not include"
      end

      # Internal: don't allow prepend, as the right callbacks won't run.
      def self.prepended(base)
        raise ArgumentError, "extend this module, not prepend"
      end
    end
  end
end
