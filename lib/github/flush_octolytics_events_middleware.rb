# frozen_string_literal: true

require "github/config/analytics"

module GitHub
  # Middleware that sends any queued analytics events to Resque at the end of a
  # request. The events are pushed to GitHub.analytics_queue, a standard Ruby
  # array.
  class FlushOctolyticsEventsMiddleware
    def self.areas_of_responsibility
      [:analytics]
    end

    def initialize(app)
      @app = app
      GitHub.enable_async_analytics_queue
    end

    def call(env)
      GitHub.with_async_analytics do
        @app.call(env)
      end
    end
  end
end
