# frozen_string_literal: true
module GitHub
  module Greenhouse
    BOARD_ID = "github"
    HOST = "https://api.greenhouse.io"
    JOBS_PATH = "/v1/boards/#{BOARD_ID}/jobs?content=true"
    DEPARTMENT_NAME_MAPPINGS = {
      "Intern" => "Internships",
    }

    def self.positions_by_department
      @_positions_by_department ||= begin
        initial_departments = Hash.new do |hash, key|
          hash[key] = { "title" => key, "postings" => [] }
        end

        jobs["jobs"].each_with_object(initial_departments) do |job, departments|
          Array.wrap(job["departments"]).each do |department|
            name = department_for(job, department)
            departments[name]["postings"] << posting_for(job)
          end
        end.values
      end
    end

    def self.full_time_jobs
      positions_by_department.reject do |position|
        position["title"] == "Internships"
      end
    end

    def self.internships
      positions_with_title("Internships")
    end

    def self.positions_with_title(title)
      specific_department = positions_by_department.find do |department|
        department["title"] == title
      end

      specific_department&.dig("postings") || []
    end

    def self.department_for(job, department)
      employment_type = job["metadata"].find { |datum| datum["name"] == "Employment Type" }

      DEPARTMENT_NAME_MAPPINGS[employment_type["value"]] || department["name"]
    end

    def self.posting_for(job)
      {
        "text" => job["title"],
        "hostedUrl" => job["absolute_url"],
        "location" => job["location"]["name"],
      }
    end

    def self.jobs
      GitHub.cache.fetch("greenhouse:v3:jobs", ttl: 15.minutes) do
        request(JOBS_PATH)
      end

    rescue Faraday::Error, Yajl::ParseError, BadResponse => error
      Failbot.report(error)

      { "jobs" => [] }
    end

    class BadResponse < StandardError
    end

    def self.request(path)
      return unless GitHub.online?

      response = connection.get(path)

      unless response.status == 200
        raise BadResponse, "Got bad response from Greenhouse: #{response.status} #{response.body}"
      end

      GitHub::JSON.decode(response.body)
    end

    REQUEST_TIMEOUT = 2.0

    def self.connection
      @connection ||= Faraday.new(url: HOST) do |f|
        f.adapter Faraday.default_adapter
        f.options[:timeout]      = REQUEST_TIMEOUT
        f.options[:open_timeout] = REQUEST_TIMEOUT
      end
    end

    # Test-only method to reset the class-level memoization
    # to protect against data leakage
    def self.reset_caches
      @connection = nil
      @_positions_by_department = nil
    end
  end
end
