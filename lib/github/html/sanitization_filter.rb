# rubocop:disable Style/FrozenStringLiteralComment

require "goomba"
require "scientist"

module GitHub::HTML
  # HTML filter with sanization routines and whitelists. This module defines
  # what HTML is allowed in user provided content and fixes up issues with
  # unbalanced tags and whatnot.
  #
  # See the Sanitize docs for more information on the underlying library:
  #
  # https://github.com/rgrove/sanitize/#readme
  #
  # Context options:
  #   :whitelist - The sanitizer whitelist configuration to use. This can be one
  #                of the options constants defined in this class or a custom
  #                sanitize options hash.
  #
  # The following keys are written to the result hash:
  #   :html_safe - A boolean flag to indicate that results from this filter
  #                are safe to use in html unescaped (safe to mark html_safe).
  class SanitizationFilter < ::HTML::Pipeline::SanitizationFilter
    include Scientist

    def call
      doc = sanitized_doc
      result[:html_safe] = true
      doc
    end

    private

    def sanitized_doc
      return goomba_sanitize if context[:goomba_sanitize]
      experiment = context[:goomba_sanitize_experiment]
      return standard_sanitize unless experiment

      nwo = if repository.nil?
        nil
      elsif repository.is_a?(GitHub::Unsullied::Wiki)
        "wiki: #{repository.repository.name_with_owner}"
      else
        repository.name_with_owner
      end
      blob = context[:blob] && context[:blob].info.except("content", "data", "symlink_target_object")

      science(experiment) do |e|
        e.context repository: nwo, blob: blob

        e.use { standard_sanitize }
        e.try { goomba_sanitize }

        # Binary blobs get sent through here by MarkupPipeline
        # (https://github.com/github/github/issues/39022). That's totally bogus
        # but we shouldn't let it get in the way of experimenting on non-binary
        # blobs.
        e.run_if { !context[:blob].try(:binary?) }

        e.compare do |standard_doc, goomba_html|
          GitHub::Goomba::Experiment.compare_output(standard_doc, goomba_html)
        end
        e.clean do |value|
          GitHub::Goomba::Experiment.clean_output(value)
        end
      end
    end

    def standard_sanitize
      doc = ::HTML::Pipeline::SanitizationFilter.instance_method(:call).bind(self).call
      fix_list_items_nesting
      doc
    end

    def goomba_sanitize
      sanitizer = GitHub::Goomba::Sanitizer.from_whitelist(whitelist)
      Goomba::DocumentFragment.new(html, sanitizer).to_html
    rescue => boom
      Failbot.report_trace(boom)
      raise
    end

    # Find all elements that contain LIs but that are not UL/OL. Assume that
    # the LI elements ended up in this container by HTML parser mistake, and
    # extract them out of the container.
    #
    # This compensates for Nokogiri's inability to auto-close open elements
    # inside a LI when the parser encounters a `</LI>` tag.
    def fix_list_items_nesting
      while li = doc.at_xpath('.//*[name() != "ul" and name() != "ol"]/li')
        parent = li.parent
        pos = parent.children.index(li)
        nodes = parent.children[pos..-1]
        parent.after(nodes)
      end
    end
  end
end
