# rubocop:disable Style/FrozenStringLiteralComment

require "addressable/uri"

module GitHub::HTML
  # Override the default CamoFilter and provide our secret key.
  class CamoFilter < ::HTML::Pipeline::CamoFilter
    def initialize(doc, starting_context = nil, result = nil)
      context = starting_context ? starting_context.dup : {}
      context[:asset_proxy] ||= GitHub.image_proxy_url
      context[:disable_asset_proxy] ||= !GitHub.image_proxy_enabled?
      context[:asset_proxy_secret_key] ||= GitHub.image_proxy_key
      context[:asset_proxy_whitelist] ||= GitHub.image_proxy_host_whitelist

      super(doc, context, result)
    end

    # Get the asset proxy URL for the given URI.
    #
    # Note that we instantiate a new empty filter to grab the default context
    # values, and that no whitelist checks are performed.
    def self.asset_proxy_url(uri, context = nil)
      camo = new(nil, context)
      if camo.asset_proxy_enabled?
        camo.asset_proxy_url(uri)
      else
        uri
      end
    end

    # We only support http and https
    WHITELISTED_SCHEMES = %w(http https).freeze

    # Internal: Does the URL contain only printable characters?
    #
    # We don't allow any non-printable characters (NULL, newlines, etc).
    # And we don't allow backslashes that might have slipped through
    def allowed_characters?(uri)
      uri.to_s =~ /\A(?!\\)[[:print:]]*\z/
    end

    # Internal: Is the given URI a simple relative URI?
    #
    # We only allow simple relative URIs such as `/foo/bar.gif`.  We do not
    # allow relative URIs such as `//github.com/foo/bar/gif`, `///foo/bar.gif`,
    # or any other ambiguous relative URI.
    def relative_uri?(uri)
      uri.relative?      &&
      uri.scheme.nil?    &&
      uri.userinfo.nil?  &&
      uri.host.nil?      &&
      uri.port.nil?      &&
      uri.path.present?  &&
      allowed_characters?(uri)
    end

    # Internal: Is the given URI a simple absolute URI?
    #
    # We only allow simple absolute URIs such as
    # `https://github.com/foo/bar.gif`.  We do not allow absolute URIs such as
    # `https://foo:bar@github.com/foo/bar/gif`, `javascript://`,
    # `https:///foo/bar.gif`, or any other ambiguous absolute URI.
    def absolute_uri?(uri)
      !uri.relative?                           &&
      uri.scheme.present?                      &&
      WHITELISTED_SCHEMES.include?(uri.scheme) &&
      uri.userinfo.nil?                        &&
      uri.host.present?                        &&
      allowed_characters?(uri)
    end

    # Internal: Is the given URI on our asset whitelist?
    #
    # We only allow simple absolute URIs to our whitelisted asset hosts.
    def whitelisted_host?(uri)
      absolute_uri?(uri) && asset_host_whitelisted?(uri.host)
    end

    def call
      return doc unless asset_proxy_enabled?

      doc.search("img").each do |element|
        camo_image_filter(element)
      end
      doc
    end

    def camo_image_filter(element)
      original_src = element["src"]
      return unless original_src

      begin
        # Addressable::URI differentiates between relative URLS with leading
        # whitespace (ex. " //" vs "//"). We will perform all URL evaluation
        # without leading space, as browsers tend to ignore them. This should
        # help minimize the differences between how Addressable::URI and
        # browsers will interpret URLs.
        stripped_src = original_src.lstrip

        uri = Addressable::URI.parse(stripped_src)

        if relative_uri?(uri) || whitelisted_host?(uri)
          return
        elsif absolute_uri?(uri)
          element["src"] = asset_proxy_url(uri.to_s)
          element["data-canonical-src"] = uri.to_s
        else
          element["src"] = ""
        end
      rescue Addressable::URI::InvalidURIError
        element["src"] = ""
      end
    end
  end
end
