# frozen_string_literal: true

module GitHub
  module Cache
    module DisableWrite
      def set(key, value, ttl = 0, raw = false)
        if disable_write
          return value
        end

        super
      end

      def incr(key, value = 1)
        if disable_write
          return get(key).to_i + value
        end
        super
      end

      def decr(key, value = 1)
        if disable_write
          return get(key).to_i - value
        end
        super
      end

      def delete(key)
        if disable_write
          return get(key)
        end
        super
      end

      def disable_write
        if block_given?
          @disable_write.tap do |old_value|
            @disable_write = true
            begin
              yield
            ensure
              @disable_write = old_value
            end
          end

        else
          @disable_write
        end
      end
    end
  end
end
