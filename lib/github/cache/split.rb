# rubocop:disable Style/FrozenStringLiteralComment

require "digest"

module GitHub
  module Cache
    module Split
      # Helper exception class used only for reporting via Failbot.
      # This will be used when there is a checksum mismatch between the original
      # value and the recomposed value after fetching again.
      class InvalidChecksumError < StandardError; end

      # Helper exception class used only for reporting via Failbot.
      # This will be used when some or all of the part keys came back as cache
      # misses. Thus preventing us from rebuilding the original value
      # completely.
      class MissingPartsError < StandardError; end

      SPLIT_HEADER = :s

      # The amount of headroom we want to give split values to make sure there's
      # enough space in the serialized value for serialization overhead, but
      # will still fit within the memcached configured maximum value size.
      SERIALIZATION_HEADROOM = 2048

      # This is the maximum size we'll allow for an un-split value.
      #
      # GitHub::Cache::Failover::MAX_VALUE_SIZE is already defined to check
      # against our server-side configured value limit, so let's go with a limit
      # just under that to leave room for serialization overhead downstream from
      # us.
      MAX_UNSPLIT_SIZE = GitHub::Cache::Failover::MAX_VALUE_SIZE - SERIALIZATION_HEADROOM

      def set(original_key, original_value, ttl = 0, original_raw = false)
        if splitting_enabled?
          try_split(original_key, original_value, original_raw) do |key, val, raw|
            super(key, val, ttl, raw)
          end
        else
          super(original_key, original_value, ttl, original_raw)
        end
      end

      def add(original_key, original_value, ttl = 0, original_raw = false)
        if splitting_enabled?
          try_split(original_key, original_value, original_raw) do |key, val|
            super(key, val, ttl, raw)
          end
        else
          super(original_key, original_value, ttl, original_raw)
        end
      end

      def get_multi(keys, raw = false)
        results = {}

        super(keys, raw).each_with_index do |(key, value), idx|
          if idx == 0 && value.is_a?(Array) && value[0] == SPLIT_HEADER
            _, composed_value, checksum, split_keys = value

            # Grab the split values out of cache and rebuild them
            split_values = super(split_keys, true)

            # The hash we pass at the end is context needed for error reporting.
            next if parts_missing?(split_keys, split_values.keys, {
              original_key: key,
            })

            # Make sure we put the parts in the right order.
            composed_value = [composed_value, *split_values.values_at(*split_keys)].join

            # Finally, let's make sure our final composed value is exactly as it
            # was before splitting.
            #
            # NOTE: we're intentionally not adding anything to the results hash
            # if the checksum is invalid, because that's how cache misses work
            # for get_multi.
            if valid_checksum?(checksum, composed_value, {
              original_checksum: checksum,
              part_keys: split_keys,
              original_key: key,
            })
              # This is for compatibility with the GitHub::Cache::Zlib mixin which
              # is upstream from us in the chain.
              composed_value = Marshal.load(composed_value) if !raw

              results[key] = composed_value
            end
          else
            results[key] = value
          end
        end

        results
      end

      def get(key, raw = false)
        value = super(key, raw)

        if value.is_a?(Array) && value[0] == SPLIT_HEADER
          _, composed_value, checksum, split_keys = value

          # Grab the split values out of cache and rebuild them
          split_values = get_multi(split_keys, true)

          # The hash we pass at the end is context needed for error reporting.
          return nil if parts_missing?(split_keys, split_values.keys, {
            original_key: key,
          })

          # Make sure we put the parts in the right order.
          composed_value = [composed_value, *split_values.values_at(*split_keys)].join

          # Finally, let's make sure our final composed value is exactly as it
          # was before splitting.
          if valid_checksum?(checksum, composed_value, {
            original_checksum: checksum,
            part_keys: split_keys,
            original_key: key,
          })
            # This is for compatibility with the GitHub::Cache::Zlib mixin which
            # is upstream from us in the chain.
            composed_value = Marshal.load(composed_value) if !raw

            value = composed_value
          else
            value = nil
          end
        end

        value
      end

      # This enables value splitting for the duration of the block passed in.
      #
      # Returns nothing.
      def with_value_splitting(&block)
        splitting_before, @perform_splitting = @perform_splitting, true

        yield
      ensure
        @perform_splitting = splitting_before
      end

      # Check if splitting is currently enabled.
      #
      # Returns nothing.
      def splitting_enabled?
        @perform_splitting == true
      end

    private
      attr_reader :perform_splitting

      # This is where we check if the original value needs to be split or not.
      # If so, we split it in to segments and store them each individually.
      #
      # Returns nothing.
      def try_split(original_key, original_value, original_raw, &block)
        return yield(original_key, original_value, original_raw) if original_value.bytesize <= MAX_UNSPLIT_SIZE

        first_segment = []
        segment_keys = []
        segments = []
        checksum = checksum_value(original_value)

        split_value(original_value) do |new_key, segment, segment_number|
          if segment_number == 1
            # The first segment is special in that it is stored as an Array
            # describing how to find the rest of the segments.
            first_segment += [SPLIT_HEADER, segment, checksum]
          else
            segment_keys << new_key
            segments << segment
          end
        end

        # Append list of segment keys to the end of the first segment
        first_segment << segment_keys

        # Store the first segment under the original key to make this whole
        # slitting process transparent to the caller.
        ret = yield(original_key, Marshal.dump(first_segment), true)

        segment_keys.zip(segments) do |new_key, segment|
          yield(new_key, segment, true)
        end

        ret
      end

      # Splits the value passed in into chunk sizes of at most MAX_UNSPLIT_SIZE
      # bytes each.
      #
      # Yields a Hash of keys and values based on the original value.
      #
      # Returns nothing.
      def split_value(value, &block)
        binary_value = value.b
        length_left = binary_value.bytesize
        offset = 0
        segment_number = 0

        while length_left > 0
          # TODO: do this more efficiently, taking advantage of Ruby's string
          # sharing stuff.
          split_value_n = binary_value.byteslice(offset, MAX_UNSPLIT_SIZE)

          split_value_key = checksum_value(split_value_n)

          offset += MAX_UNSPLIT_SIZE
          length_left -= MAX_UNSPLIT_SIZE
          segment_number += 1

          yield(split_value_key, split_value_n, segment_number)
        end
      end

      def checksum_value(value)
        Digest::SHA256.hexdigest(value)
      end

      # Make sure all of the part keys we asked for were cache hits.
      # Otherwise treat this entire thing as a miss.
      #
      # Returns true or false.
      def parts_missing?(required_keys, available_keys, context = {})
        missing_keys = available_keys - required_keys
        if missing_keys.any?
          err = MissingPartsError.new("#{missing_keys.size} parts missing")

          context[:app] = "github-tracing"

          Failbot.report!(err, context)

          true
        end

        false
      end

      # Verify the new value's checksum matches an expected checksum.
      #
      # This is used to verify the recomposed value we rebuilt from parts
      # exactly matches the value before we decomposed it.
      #
      # Returns true or false
      def valid_checksum?(expected_checksum, new_value, context = {})
        new_checksum = checksum_value(new_value)
        if new_checksum == expected_checksum
          true
        else
          err = InvalidChecksumError.new("invalid checksum for reomposed value")

          context.merge!(app: "github-tracing", new_checksum: new_checksum)

          Failbot.report!(err, context)

          false
        end
      end
    end
  end
end
