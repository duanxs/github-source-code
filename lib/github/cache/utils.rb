# rubocop:disable Style/FrozenStringLiteralComment

require "github/dogstats"
require "github/stats"

module GitHub
  module Cache
    # Methods available on GitHub::Cache::Client (GitHub.cache) that extend the basic
    # Rails memcache interface with convenience methods. This is a mixin because
    # we need to attach it to live clients and the fake memcache interface used
    # in test environments.
    module Utils
      # Handles the common case of fetching a value or running a block and
      # setting the result when not already cached. This is heavily inspired by
      # cache_fu's cache(key, &block) form.
      #
      # key     - The cache key to get/set.
      # options - Hash of options.
      #           :ttl       => Integer time-to-live value.
      #           :force     => Force the value to be recached.
      #           :raw       => Do not marshal cache value.
      #           :stats_key => Record timing and hit/miss stats under this key.
      # block   - Called when the value is not cached. If not given, no new
      #           value is written to cache.
      #
      # Returns the cached value or the result of the block.
      def fetch(key, options = {})
        options = { force: !!options } if !options.is_a?(Hash)
        write_cache = options[:force]
        stats_key = options[:stats_key]

        start = GitHub::Dogstats.monotonic_time if stats_key

        if !write_cache
          cache_result = get_multi([key], options[:raw])
          cache_value = cache_result[key]
          write_cache = !cache_result.key?(key)
        end

        if write_cache && block_given?
          cache_value = yield
          set(key, cache_value, (options[:ttl] || 0), options[:raw])
        end

        if stats_key
          subkey = if options[:force]
            "force"
          elsif write_cache
            "miss"
          else
            "hit"
          end

          tags = ["type:#{subkey}"]
          if stats_tags = options[:stats_tags]
            tags.concat(stats_tags)
          end
          GitHub.dogstats.timing_since(stats_key, start, tags: tags)
        end

        cache_value
      end

      def get(key, raw = false)
        value = super
        value = fix_nil_value(value) if !raw
        value
      end

      def get_multi(keys, raw = false)
        kvs = super
        kvs.transform_values! { |value| fix_nil_value(value) } unless raw
        kvs
      end

      private def fix_nil_value(value)
        # We used to transform `nil` values to `:_nil` on write, because of
        # broken `nil` value handling in the `GitHub::Cache::Local` module
        # and because `#fetch` was implemented using `#get`, but `#get` does
        # not allow to differentiate between missing keys and `nil` values.
        #
        # All of these issues were fixed, but we need to keep translating `:_nil`
        # values back to `nil` on read for the foreseeable future.
        value == :_nil ? nil : value
      end

      # Check if a value exists in the cache.
      #
      # key - The String cache key to check.
      # options - ignored, but needed by rails `fragment_exist?`
      #
      # Returns true if the value exists in cache, false if not.
      def exist?(key, options = nil)
        get_multi([key]).key?(key)
      end

      ##
      # Rails MemCacheStore interface compatibility

      def read(key, options = nil)
        raw = (options && options[:raw]) || false
        key = key.to_s.tr(" ", "_")
        get(key, raw)
      end

      def write(key, value, options = nil)
        method = (options && options[:unless_exist]) ? :add : :set
        ttl = (options && (options[:ttl] || options[:expires_in])) || 0
        raw = (options && options[:raw]) || false
        value = value.to_s if raw
        key = key.to_s.tr(" ", "_")
        send(method, key, value, ttl, raw)
      end
    end
  end
end
