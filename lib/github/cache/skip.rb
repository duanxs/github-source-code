# frozen_string_literal: true

module GitHub
  module Cache
    # Mixin for configuring all cache gets to register as misses. This is
    # typically set at the beginning of a web request to cause all caches to be
    # refreshed to newly generated values.
    module Skip
      attr_accessor :skip

      def get(key, raw = false)
        if skip
          delete(key)
          return nil
        end

        super
      end

      def get_multi(keys, raw = false)
        if skip
          keys.each { |key| delete(key) }
          return {}
        end

        super
      end
    end
  end
end
