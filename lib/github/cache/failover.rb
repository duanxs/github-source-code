# rubocop:disable Style/FrozenStringLiteralComment

require "memcached"
require "failbot"

class Memcached::Error
  attr_accessor :memcached_key
end

class Memcached::ValueTooBig < Memcached::Error
end

module GitHub
  module Cache
    # A variety of exceptions may be raised by the memcached library when
    # unmarshalling fails. This method wraps the mess of checking for them.
    #
    # boom - An exception to check.
    #
    # Returns true if the exception is likely due to an unmarshalling error.
    def self.unmarshal_error?(boom)
      ret = case boom
      when ArgumentError
        msg = boom.to_s
        msg.include?("undefined class") ||
          msg.include?("dump format error")
      when TypeError
        msg = boom.to_s
        msg.include?("needs to have method `_load'") ||
          msg.include?("needs to have method `marshal_load'") ||
          msg.include?("exception class/object expected") ||
          msg.include?("instance of IO needed")
      when NameError
        msg = boom.to_s
        msg.include?("uninitialized constant")
      end

      GitHub.dogstats.increment("cache.unmarshal_error") if ret
      ret
    end

    # Mixin for Memcached::Rails that adds error swallowing and reliable
    # server failover. When a connection, read, or write error occurs,
    # the memcache operations take the following action:
    #
    #   - Retries the operation on the same server up to the configured
    #     server_failure_limit times (2 by default).
    #
    #   - If the retry limit is exceeded and the server was marked dead,
    #     the operation is attempted once more under the new server list.
    #
    #   - If the retry limit was exceeded and the server configuration
    #     did not change (e.g., only one server is available), nil is
    #     returned and the error is swallowed.
    #
    # When no servers are available, or in other rare cases where an
    # error is not retryable, the memcache operations do nothing and
    # return nil. The calling code should continue to function, though
    # without caching.
    #
    # This module must be included in a subclass of Memcached::Rails.
    module Failover
      # These errors are ignored and not retried.
      IGNOREABLE = [
        Memcached::ActionQueued,
        Memcached::NoServersDefined,
        Memcached::NotFound,
        Memcached::NotStored,
        Memcached::ValueTooBig,
        Memcached::ABadKeyWasProvidedOrCharactersOutOfRange,
      ]

      # These errors trigger the retry and failover logic.
      RETRYABLE = [
        # frequently occuring
        Memcached::ATimeoutOccurred,
        Memcached::ReadFailure,
        Memcached::ClientError,
        Memcached::HostnameLookupFailure,
        Memcached::ConnectionFailure,
        Memcached::SystemError,

        # weirdsies
        Memcached::ConnectionBindFailure,
        Memcached::ConnectionDataDoesNotExist,
        Memcached::ConnectionDataExists,
        Memcached::ConnectionSocketCreateFailure,
        Memcached::CouldNotOpenUnixSocket,
        Memcached::Failure,
        Memcached::FetchWasNotCompleted,
        Memcached::PartialRead,
        Memcached::ProtocolError,
        Memcached::ServerDelete,
        Memcached::ServerEnd,
        Memcached::ServerError,
        Memcached::SomeErrorsWereReported,
        Memcached::TheHostTransportProtocolDoesNotMatchThatOfTheClient,
        Memcached::UnknownReadFailure,
        Memcached::WriteFailure,
      ]

      MAX_VALUE_SIZE = 1024 * 1023

      # Main retry, ejection, and error swallowing logic. The overable
      # argument determines whether the operation should be retried after
      # a server has been evicted.
      def failover(overable = true)
        retries ||= 0
        yield
      rescue Memcached::NotFound, Memcached::NotStored
        nil
      rescue Memcached::ServerIsMarkedDead
        if retries < 100
          retries = 100
          record_error $!, "memcached dead: #{$!.to_s[0..1023]}. failing over."
          retry if overable
        else
          record_error $!, "memcached dead: #{$!.to_s[0..1023]} and was not evicted. bailing."
          return nil
        end
      rescue *RETRYABLE
        if retries < options[:server_failure_limit]
          retries += 1
          record_error $!, "memcached error: #{$!.class} #{$!.to_s[0..1023]} (retry ##{retries})"
          retry
        else
          record_error $!, "memcached error: #{$!.class} #{$!.to_s[0..1023]} (swallowing)"
          return nil
        end
      rescue *IGNOREABLE
        record_error $!, "memcached error: #{$!.class} #{$!.to_s[0..1023]} (swallowing)"
        return nil
      end

      # these methods trigger error swallowing and server eviction
      def get(key, raw = false)
        failover { super }
      rescue => boom
        raise if raw || !GitHub::Cache.unmarshal_error?(boom)
        # value could not be unmarshalled due to a class name change. delete the key
        # and return nil so it will be recached.
        delete(key)
        nil
      end

      def set(key, value, ttl = 0, raw = false)
        ttl ||= 0 # for GitRPC::Cache
        failover do
          raise augment_error(Memcached::ValueTooBig.new("value size is #{value.size}"), key) if value_to_big?(value)
          super
        end
      end

      def add(key, value, ttl = 0, raw = false)
        failover do
          raise augment_error(Memcached::ValueTooBig.new("value size is #{value.size}"), key) if value_to_big?(value)
          super
        end
      end

      # these don't make any sense to retry after the server
      # has been ejected from the list.
      def delete(*args)    failover(false) { super }   end
      def incr(*args)      failover(false) { super }   end
      def decr(*args)      failover(false) { super }   end

      # multigets need special logic. if we get back a nil, fall back to
      # individual `get_multi` calls.
      def get_multi(keys, raw = false)
        begin
          result = failover { super(keys, raw) }
          return result if result
        rescue => boom
          raise if raw || !GitHub::Cache.unmarshal_error?(boom)
        end

        # If we did get here, we failed loading all keys in one batch,
        # either due to some memcache error or due to an unmarshal error.
        #
        # Let's retry loading each key individually, and clean up any
        # broken data along they way.
        keys.each_with_object({}) do |key, hash|
          begin
            hash.update(failover { super([key], raw) } || {})
          rescue => err
            raise unless GitHub::Cache.unmarshal_error?(err)
            delete(key)
          end
        end
      end

      def record_error(error, message)
        logger.warn "#{message}" if logger

        # This variable is being set in this weird way so that, if we end up
        # with an uncaught error before the actual hostname value is retrieved,
        # we'll be able to output "unknown-hostname" in the method-wide `rescue`.
        hostname = "unknown-host"
        hostname_from_key = if error.respond_to?(:memcached_key)
          begin
            server = server_by_key(error.memcached_key)

            if server.present?
              hostname_or_ip = server.split(":", 2).first

              # Graphite uses . as a separator, so convert the hostname's
              # separator from . to - so it can be used in a Graphite key
              # without any weirdness.
              hostname_or_ip.tr(".", "-") if hostname_or_ip.present?
            end
          rescue Memcached::Error
            nil
          end
        end
        hostname = hostname_from_key || hostname

        # increment stats instead of sending a haystack exception in some
        # non-fatal warning / noise cases
        case error
        when Memcached::ValueTooBig
          GitHub.stats.increment("memcached.error.value-too-big") if GitHub.enterprise?
          GitHub.dogstats.increment("rpc.memcached.error.value-too-big")
          # Failbot.report(error, :note => message)
        when Memcached::ABadKeyWasProvidedOrCharactersOutOfRange
          Failbot.report(error, note: message)
        when Memcached::ServerIsMarkedDead
          GitHub.stats.increment("memcached.#{hostname}.error.server-evicted") if GitHub.enterprise?
          GitHub.dogstats.increment("rpc.memcached.error.server-evicted", tags: ["rpc_host:#{hostname}"])
        when *RETRYABLE
          GitHub.stats.increment("memcached.#{hostname}.error.retry") if GitHub.enterprise?
          GitHub.dogstats.increment("rpc.memcached.error.retry", tags: ["rpc_host:#{hostname}"])
        when *IGNORABLE
          GitHub.stats.increment("memcached.#{hostname}.error.ignored") if GitHub.enterprise?
          GitHub.dogstats.increment("rpc.memcached.error.ignored", tags: ["rpc_host:#{hostname}"])
        else
          Failbot.report(error, note: message)
        end

      rescue => boom
        begin
          GitHub.stats.increment("memcached.#{hostname}.error.unexpected") if GitHub.enterprise?
          GitHub.dogstats.increment("rpc.memcached.error.unexpected", tags: ["rpc_host:#{hostname}"])
        rescue
          # never fail
        end
      end

      def value_to_big?(value)
        value.respond_to?(:to_str) && value.size > MAX_VALUE_SIZE
      end

      private

      # Override check_return_code so we can store the key on the error.
      def check_return_code(ret, key = nil)
        super
      rescue Memcached::Error => e
        raise augment_error(e, key)
      end

      def augment_error(error, key)
        error.memcached_key = key if error.respond_to?(:memcached_key=)
        error
      end
    end
  end
end
