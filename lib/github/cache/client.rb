# frozen_string_literal: true

module GitHub
  module Cache
    # Rails memcached interface with custom extensions.
    class Client < Memcached::Rails
      attr_accessor :logger

      # NOTE: The order these mixins are included is important. Especially the
      # Zip, Split and Failover modules.
      include GitHub::Cache::Failover
      include GitHub::Cache::Split
      include GitHub::Cache::Zip
      include GitHub::Cache::Local
      include GitHub::Cache::Utils
      include GitHub::Cache::Skip
      include GitHub::Cache::DisableWrite
      include GitHub::Cache::HashKeys
      include GitHub::Cache::LongTTL

      def fetch(*args)
        GitHub::CacheLeakDetector.caching do
          super(*args)
        end
      end
    end
  end
end
