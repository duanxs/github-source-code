# rubocop:disable Style/FrozenStringLiteralComment

require "zlib"

module GitHub
  module Cache
    module Zip
      MAX_ATTEMPT_CACHE_SIZE = 10_000_000
      MAX_UNZIPPED_SIZE      = 500_000
      ZIP_HEADER             = :z

      def set(key, value, ttl = 0, raw = false)
        if attempt_cache?(value)
          value, raw, success = zip_value(key, value, raw)
          super(key, value, ttl, raw) if success
        end
      end

      def add(key, value, ttl = 0, raw = false)
        if attempt_cache?(value)
          value, raw, success = zip_value(key, value, raw)
          super(key, value, ttl, raw) if success
        end
      end

      def get_multi(keys, raw = false)
        hash = {}
        super(keys, raw).each do |key, value|
          hash[key] = unzip_value(key, value)
        end
        hash
      end

      def get(key, raw = false)
        return super if raw
        unzip_value(key, super)
      end

    private
      def zip_value(key, value, raw)
        if !raw
          value = Marshal.dump(value)

          if value.bytesize > MAX_ATTEMPT_CACHE_SIZE
            return [nil, true, false]
          end

          if value.bytesize > MAX_UNZIPPED_SIZE
            begin
              value = Marshal.dump([ZIP_HEADER, Zlib::Deflate.deflate(value)])
            rescue Zlib::DataError => e
              Failbot.report!(e, app: "github-zlib-cache")
              delete(key)
              return [nil, true, false]
            end
          end
        end

        [value, true, true]
      end

      def unzip_value(key, value)
        return value unless value.is_a?(Array) && value[0] == ZIP_HEADER
        begin
          Marshal.load(Zlib::Inflate.inflate(value[1]))
        rescue Zlib::BufError => e
          Failbot.report!(e, app: "github-zlib-cache")
          delete(key)
          nil
        rescue => boom
          raise unless GitHub::Cache.unmarshal_error?(boom)
          delete(key)
          nil
        end
      end

      def attempt_cache?(value)
        !value.respond_to?(:bytesize) || value.bytesize < MAX_ATTEMPT_CACHE_SIZE
      end
    end
  end
end
