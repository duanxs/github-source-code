# frozen_string_literal: true

module GitHub
  class MysqlQueryCounter
    class << self
      def start
        counts.clear
        @started = true
      end

      def count(type, config_host)
        return unless started?
        counts[config_host][type] += 1
      end

      def stop
        counts.clear
        @started = false
      end

      def counts
        @counts ||= Hash.new { |h, k| h[k] = Hash.new(0) }
      end

      private

      def started?
        @started ||= false
      end
    end
  end
end
