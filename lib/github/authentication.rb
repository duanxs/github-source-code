# rubocop:disable Style/FrozenStringLiteralComment

module GitHub

  # GitHub authentication adapters
  #
  # The Default adapter is used by github.com and any enterprise instance
  # that uses 'GitHub' authentication. Provides a common interface for
  # all authentication schemes used by any type of request (web, API,
  # gerve, etc.).
  module Authentication
    AUTH_MODES = [
      :cas,
      :default,
      :github_oauth,
      :ldap,
      :saml,
    ]

    autoload :UserHandler,            "github/authentication/user_handler"
    autoload :Default,                "github/authentication/default"
    autoload :OmniAuth,               "github/authentication/omniauth"
    autoload :CAS,                    "github/authentication/cas"
    autoload :LDAP,                   "github/authentication/ldap"
    autoload :GitHubOauth,            "github/authentication/github_oauth"
    autoload :Attempt,                "github/authentication/attempt"
    autoload :Result,                 "github/authentication/result"
    autoload :SignedAuthToken,        "github/authentication/signed_auth_token"
    autoload :SAML,                   "github/authentication/saml"
    autoload :PrivateInstanceRuntime, "github/authentication/private_instance_runtime"
    autoload :Feed,                   "github/authentication/feed"

    module GitAuth
      autoload :SignedAuthToken,      "github/authentication/git_auth/signed_auth_token"
    end
  end
end
