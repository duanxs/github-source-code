# rubocop:disable Style/FrozenStringLiteralComment

require "memcached"

module GitHub
  # The GitHub memcache client stack. This is basically Rails's standard
  # memcache interface with a variety of extra features strapped on top.
  #
  # The GitHub::Cache::Client class is the main client interface available via
  # the top-level GitHub.cache instance. It mixes in a variety of features from the
  # other modules defined here.
  module Cache
    autoload :Client,          "github/cache/client"
    autoload :DisableWrite,    "github/cache/disable_write"
    autoload :Failover,        "github/cache/failover"
    autoload :Fake,            "github/cache/fake"
    autoload :HashKeys,        "github/cache/hash_keys"
    autoload :Local,           "github/cache/local"
    autoload :LongTTL,         "github/cache/long_ttl"
    autoload :ResetMiddleware, "github/cache/reset_middleware"
    autoload :Skip,            "github/cache/skip"
    autoload :Timid,           "github/cache/timid"
    autoload :Utils,           "github/cache/utils"
    autoload :Zip,             "github/cache/zip"
    autoload :Split,           "github/cache/split"
  end
end

# Massage the Memcached::Rails interface to be a bit more compliant with Rails's
# built-in MemCache interface.
Memcached::Rails.class_eval do
  alias_method :servers=, :set_servers
  public :servers=

  # Revert to pre-v1.7.0 raise errors behavior for compatibility with Cache::Client
  def log_exception(e)
    raise e
  end
end
