# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/stats"
require "github/config/graphite" # for rails version key
require "rack/process_utilization"
require "github/tagging_helper"
require "set"

module GitHub
  module Middleware
    class Stats
      UNKNOWN = "unknown".freeze
      APPLICATION_LOG_DATA = "APPLICATION_LOG_DATA".freeze
      COLON = ":".freeze

      DEFAULT_SAMPLE_RATE = 0.5
      REPOSITORY_SAMPLE_RATE = 1.0

      ALLOWED_AORS = [:codespaces, :repositories]

      def initialize(app)
        @app = app
      end

      def call(env)
        real_start = realtime
        cpu_start = cputime
        cpu_throttle_start_ns = cputhrottletime_ns
        status, headers, body = @app.call(env)
      ensure
        # Skip stats on warmup requests in unicorn master
        if GitHub.unicorn_master_pid != Process.pid
          request = Rack::Request.new(env)
          status = status || UNKNOWN
          real_ms = (realtime - real_start) * 1_000
          cpu_ms = (cputime - cpu_start) * 1_000
          idle_ms = real_ms - cpu_ms

          cpu_throttle_time_ms = 0.0
          cpu_throttle_end_ns = cputhrottletime_ns
          if (!cpu_throttle_start_ns.nil? && !cpu_throttle_end_ns.nil?)
            cpu_throttle_time_ms = ((cpu_throttle_end_ns - cpu_throttle_start_ns) / 1000000.0).round(3)
          end

          sample_rate = DEFAULT_SAMPLE_RATE
          tags = []
          add_tag tags, GitHub::TaggingHelper::STATUS_TAG, status
          add_tag tags, GitHub::TaggingHelper::STATUS_RANGE_TAG, status_range(status)
          add_tag tags, GitHub::TaggingHelper::METHOD_TAG, request.request_method.downcase
          add_tag tags, GitHub::TaggingHelper::RAILS_VERSION_TAG, GitHub::TaggingHelper.rails_version
          request_category = GitHub::TaggingHelper.category(env)
          add_tag tags, GitHub::TaggingHelper::CATEGORY_TAG, request_category

          pjax = GitHub::TaggingHelper.pjax(env)
          add_tag(tags, GitHub::TaggingHelper::PJAX_TAG, pjax) if pjax != GitHub::TaggingHelper::UNKNOWN

          logged_in = GitHub::TaggingHelper.logged_in(env)
          add_tag(tags, GitHub::TaggingHelper::LOGGED_IN_TAG, logged_in) if logged_in != GitHub::TaggingHelper::UNKNOWN

          robot_name = GitHub::TaggingHelper.robot_name(env)
          add_tag(tags, GitHub::TaggingHelper::ROBOT_TAG, robot_name) if robot_name != GitHub::TaggingHelper::UNKNOWN

          purpose = GitHub::TaggingHelper.purpose(env)
          add_tag(tags, GitHub::TaggingHelper::PURPOSE_TAG, purpose) if purpose != GitHub::TaggingHelper::UNKNOWN

          controller = GitHub::TaggingHelper.controller(env)
          action = GitHub::TaggingHelper.action(env)

          if GitHub::TaggingHelper.stafftools_request?(controller)
            add_tag(tags, GitHub::TaggingHelper::STAFFTOOLS_TAG, "true")
          end

          if request_category == "api" # GitHub::Routers::Api::ProcessCategory
            # Use this tag to split REST API requests from GraphQL requests,
            # but don't bother attaching it to other requests.
            if GitHub::TaggingHelper.graphql_api_request?(controller)
              add_tag(tags, GitHub::TaggingHelper::GRAPHQL_API_TAG, "true")
            else
              add_tag(tags, GitHub::TaggingHelper::GRAPHQL_API_TAG, "false")
            end
          end

          owner = GitHub::TaggingHelper.owner(env)
          repository = GitHub::TaggingHelper.repository(env)

          if owner && repository && GitHub::TaggingHelper.repository_tracking_enabled?(owner, repository)
            add_tag(tags, GitHub::TaggingHelper::OWNER_TAG, owner)
            add_tag(tags, GitHub::TaggingHelper::REPOSITORY_TAG, repository)
            sample_rate = REPOSITORY_SAMPLE_RATE
          end

          quota_limited = GitHub::TaggingHelper.quota_limiting_enabled?(env)
          add_tag(tags, GitHub::TaggingHelper::QUOTA_LIMITING_ENABLED, quota_limited)

          add_controller_and_action_tags(tags, controller, action, env)
          add_aor(tags, env)
          add_service_mapping(tags, env)

          GitHub.dogstats.batch do |dogstats|
            dogstats.timing("request.time", real_ms, tags: tags, sample_rate: sample_rate)
            dogstats.distribution("request.dist.time", real_ms, tags: tags)
            dogstats.distribution("request.dist.cpu_time", cpu_ms, tags: tags)
            dogstats.distribution("request.dist.idle_time", idle_ms, tags: tags)
            dogstats.distribution("request.dist.cpu_throttle_time_ms", cpu_throttle_time_ms, tags: tags)

            if real_ms / 1000 > GitHub.slow_request_threshold
              slow_request_tags = []
              add_tag slow_request_tags, GitHub::TaggingHelper::CATEGORY_TAG, request_category
              add_controller_and_action_tags(slow_request_tags, controller, action, env)
              dogstats.increment("request.slow", tags: slow_request_tags, sample_rate: sample_rate)
            end
          end
        end
      end

      def status_range(status)
        if status.is_a?(Integer)
          # e.g. 4xx
          "#{status.to_s[0]}xx"
        else
          status
        end
      end

      def add_tag(tags, tag, tag_value)
        tags << "#{tag}#{COLON}#{tag_value}"
      end

      private

      def add_controller_and_action_tags(tags, controller, action, env)
        return unless controller && action

        add_tag(tags, GitHub::TaggingHelper::CONTROLLER_TAG, controller)

        singleton = env[Rack::ProcessUtilization::ENV_KEY]
        action_name = (singleton.nil? || singleton.statsd_tag_actions?) ? action : "untagged"
        add_tag(tags, GitHub::TaggingHelper::ACTION_TAG, action_name)
      end

      def add_aor(tags, env)
        aors = env[GitHub::TaggingHelper::PROCESS_AOR_KEY]
        aor = "unowned"

        if aors
          tagged_aors = aors & ALLOWED_AORS
          aor = tagged_aors.first unless tagged_aors.empty?
        end

        add_tag(tags, GitHub::TaggingHelper::AOR_TAG, aor)
      end

      def add_service_mapping(tags, env)
        # keep this in sync with GitHub::ServiceMapping#push_service_mapping_context to ensure
        # unknown services are always consistently labelled.
        service_name = env[GitHub::TaggingHelper::PROCESS_SERVICE_KEY] || "github/unknown"
        add_tag(tags, GitHub::TaggingHelper::SERVICE_TAG, service_name)
      end

      def realtime
        Process.clock_gettime(Process::CLOCK_MONOTONIC)
      end

      def cputime
        Process.clock_gettime(Process::CLOCK_PROCESS_CPUTIME_ID)
      end

      def cputhrottletime_ns
        _, _, t_ns = GitHub::CPU::cpu_throttled_stats
        t_ns
      end
    end
  end
end
