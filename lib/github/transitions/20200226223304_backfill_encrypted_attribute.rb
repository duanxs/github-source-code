# frozen_string_literal: true

require "#{Rails.root}/config/environment"

require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200226223304_backfill_encrypted_attribute.rb -v -m TwoFactorCredential -s recovery_secret -d encrypted_recovery_secret
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200226223304_backfill_encrypted_attribute.rb -v -w -m TwoFactorCredential -s recovery_secret -d encrypted_recovery_secret
#
module GitHub
  module Transitions
    class BackfillEncryptedAttribute < Transition

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        @klass = Object.const_get(other_args[:model])
        @ciphertext_name = other_args[:destination]
        @mirror_name = other_args[:source]

        @ciphertext_literal = GitHub::SQL::LITERAL(@ciphertext_name)
        @mirror_literal = GitHub::SQL::LITERAL(@mirror_name)
        @table_literal = GitHub::SQL::LITERAL(@klass.table_name)

        if GitHub.encrypted_attribute_enabled?(@klass, @ciphertext_name)
          # Check if we can do local encryption. If so, this also precaches the key
          @key = @klass.encrypted_attribute_key(@ciphertext_name)
          @local = !@key.llk.export_latest_version.secret_key.blank?
        end

        min_id = readonly { @klass.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM :table", table: @table_literal) }
        max_id = readonly { @klass.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM :table", table: @table_literal) }

        @iterator = @klass.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add(<<-SQL, table: @table_literal, mirror: @mirror_literal, ciphertext: @ciphertext_literal)
          SELECT id, :mirror FROM :table
          WHERE id BETWEEN :start AND :last
          AND :mirror IS NOT NULL
          AND :mirror != ""
          AND :ciphertext IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        @klass.throttle_with_retry(max_retry_count: 5) do
          log "encrypting #{@mirror_name} into #{@ciphertext_name} in #{@klass.table_name} for rows #{rows.map(&:first)}" if verbose?

          updates = rows.map do |id, mirror_value|
            if GitHub.encrypted_attribute_enabled?(@klass, @ciphertext_name)
              [id, @key.encrypt(mirror_value, local: @local)]
            else
              [id, mirror_value]
            end
          end

          run_batch_update(updates) unless dry_run?
        end
      end

      def run_batch_update(updates)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::Repositories.github_sql.new(<<-SQL, table: @table_literal, ciphertext: @ciphertext_literal)
            UPDATE :table SET :ciphertext = CASE
          SQL

          updates.each do |id, value|
            sql.add(<<-SQL, id: id, value: GitHub::SQL::BINARY(value))
              WHEN id = :id THEN :value
            SQL
          end

          sql.add "END WHERE id IN :ids", ids: updates.map(&:first)
          sql.run
          sql.affected_rows
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "m", "model=", "Name of the model with the encrypted attribute", required: true
    on "s", "source=", "Name of the mirror attribute/column to backfill from", required: true
    on "d", "destination=", "Name of the encrypted attribute/column to backfill to", required: true
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillEncryptedAttribute.new(options)
  transition.run
end
