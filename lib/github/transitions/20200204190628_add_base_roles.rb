# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200204190628_add_base_roles.rb -v | tee -a /tmp/add_base_roles.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200204190628_add_base_roles.rb -v -w | tee -a /tmp/add_base_roles.log
#
module GitHub
  module Transitions
    class AddBaseRoles < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Returns nothing.
      def perform
        unless dry_run?
          Role.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            # create read, write, admin roles
            read = Role.create!(name: "read")
            write = Role.create!(name: "write")
            Role.create!(name: "admin")
            Role.create!(name: "triage") unless Role.find_by(name: "triage")
            Role.create!(name: "maintain") unless Role.find_by(name: "maintain")

            # Add in the base role to triage/maintain
            triage = Role.find_by(name: "triage")
            maintain = Role.find_by(name: "maintain")

            triage.update!(base_role_id: read.id)
            maintain.update!(base_role_id: write.id)
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddBaseRoles.new(options)
  transition.run
end
