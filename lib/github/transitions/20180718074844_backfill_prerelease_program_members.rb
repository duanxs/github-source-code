# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180718074844_backfill_prerelease_program_members.rb -v | tee -a /tmp/backfill_prerelease_program_members.log
#
module GitHub
  module Transitions
    class BackfillPrereleaseProgramMembers < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { PrereleaseProgramMember.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM prerelease_program_members") }
        max_id = readonly { PrereleaseProgramMember.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM prerelease_program_members") }

        @iterator = PrereleaseProgramMember.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, user_id
          FROM prerelease_program_members
          WHERE id BETWEEN :start AND :last
          AND member_id IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        total_rows = 0
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
          total_rows += rows.size
        end
        log "Processed #{total_rows} total rows" if verbose?
      end

      def process(rows)
        return if rows.empty?
        log "Copying user_id to member_id for #{rows.size} prerelease_program_members rows" if verbose?
        return if dry_run?

        PrereleaseProgramMember.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          # Each row contains [id, user_id]
          # Copy user_id to member_id as part of deprecating user_id:
          # https://github.com/github/github/issues/93829
          PrereleaseProgramMember.github_sql.run <<-SQL, input_values: GitHub::SQL::ROWS(rows)
            INSERT INTO prerelease_program_members (id, member_id)
            VALUES :input_values
            ON DUPLICATE KEY UPDATE member_id = VALUES(member_id)
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillPrereleaseProgramMembers.new(options)
  transition.run
end
