# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191015172839_create_mobile_preview_waitlist_survey.rb -v | tee -a /tmp/create_mobile_preview_waitlist_survey.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191015172839_create_mobile_preview_waitlist_survey.rb -v -w | tee -a /tmp/create_mobile_preview_waitlist_survey.log
#
module GitHub
  module Transitions
    class CreateMobilePreviewWaitlistSurvey < Transition
      SURVEY_SLUG = "mobile_preview_waitlist"
      SURVEY_QUESTION_NAMES = %w(
        github_user_id
        android_email
        iphone_email
      )

      def perform
        log "Starting transition #{self.class.to_s.underscore}"

        if Survey.exists?(slug: SURVEY_SLUG)
          log "Survey with slug #{SURVEY_SLUG} already exists, skipping transition."
          return
        end

        survey = Survey.new(
          title: SURVEY_SLUG.titleize,
          slug: SURVEY_SLUG,
        )

        choices = []

        SURVEY_QUESTION_NAMES.each_with_index do |question_name, index|
          question = survey.questions.build(
            display_order: index + 1,
            short_text: question_name,
            text: question_name.titleize,
          )

          choices << SurveyChoice.new(
            question: question,
            short_text: question_name,
            text: question_name.titleize,
          )
        end

        if dry_run
          log "Would have saved survey #{survey.attributes} and "\
            "choices #{choices.map(&:attributes)}."
        else
          survey.save!
          choices.each(&:save!)
          log "Saved survey #{survey.id} with questions #{survey.questions.map(&:attributes)}."
        end

        log "Done!"
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CreateMobilePreviewWaitlistSurvey.new(options)
  transition.run
end
