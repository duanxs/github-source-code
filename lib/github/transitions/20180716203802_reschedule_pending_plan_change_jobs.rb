# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180716203802_reschedule_pending_plan_change_jobs.rb -v | tee -a /tmp/reschedule_pending_plan_change_jobs.log
#
module GitHub
  module Transitions
    class ReschedulePendingPlanChangeJobs < Transition
      attr_reader :iterator
      BATCH_SIZE = 100

      def after_initialize
        max_id = readonly do
          ::Billing::PendingPlanChange.github_sql.value("SELECT COALESCE(max(id), 0) FROM pending_plan_changes")
        end
        @iterator = ::Billing::PendingPlanChange.github_sql_batched_between \
          start: 0,
          finish: max_id,
          batch_size: BATCH_SIZE
        @iterator.add <<-SQL, today: GitHub::Billing.today
          SELECT pending_plan_changes.id
          FROM pending_plan_changes
          WHERE pending_plan_changes.id BETWEEN :start AND :last
          AND pending_plan_changes.is_complete = false
          AND pending_plan_changes.active_on >= :today
        SQL
      end

      # Returns nothing.
      def perform
        return unless GitHub.billing_enabled?
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        pending_plan_changes = ::Billing::PendingPlanChange.find rows.flatten

        unless dry_run?
          ::Billing::PendingPlanChange.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            pending_plan_changes.each do |change|
              reschedule_job(change)
            end
          end
        end
      end

      def reschedule_job(change)
        retry_time = change.active_on.to_datetime - ::Billing::SchedulePlanChange::BILLING_CYCLE_BUFFER_TIME

        RunPendingPlanChangeJob.set(wait_until: retry_time).perform_later(change)
        log "Scheduled Job for Pending Plan Change ##{change.id}" if verbose?
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ReschedulePendingPlanChangeJobs.new(options)
  transition.run
end
