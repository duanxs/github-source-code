# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200630215107_add_discussion_spotlight_fine_grained_permissions.rb --verbose | tee -a /tmp/add_discussion_spotlight_fine_grained_permissions.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200630215107_add_discussion_spotlight_fine_grained_permissions.rb --verbose -w | tee -a /tmp/add_discussion_spotlight_fine_grained_permissions.log
#
module GitHub
  module Transitions
    class AddDiscussionSpotlightFineGrainedPermissions < Transition
      # Returns nothing.
      def perform
        GitHub.system_roles.reconcile(dry_run: dry_run?, verbose: verbose?)
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddDiscussionSpotlightFineGrainedPermissions.new(options)
  transition.run
end
