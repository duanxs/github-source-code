# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200526204457_create_git_hub_codespaces_product_in_zuora.rb --verbose | tee -a /tmp/create_git_hub_codespaces_product_in_zuora.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200526204457_create_git_hub_codespaces_product_in_zuora.rb --verbose -w | tee -a /tmp/create_git_hub_codespaces_product_in_zuora.log
#
module GitHub
  module Transitions
    class CreateGithubCodespacesProductInZuora < Transition

      # Returns nothing.
      def perform
        ApplicationRecord::Collab.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Syncing GitHub Codespaces rate plans to Zuora"
          ::Billing::Codespaces::ZuoraProduct.sync_to_zuora unless dry_run?
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CreateGithubCodespacesProductInZuora.new(options)
  transition.run
end
