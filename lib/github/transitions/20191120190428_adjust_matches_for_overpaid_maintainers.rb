# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191120190428_adjust_matches_for_overpaid_maintainers.rb -v --transfers_file=/tmp/[filename.csv]/ | tee -a /tmp/adjust_matches_for_overpaid_maintainers.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191120190428_adjust_matches_for_overpaid_maintainers.rb -v -w --transfers_file=/tmp/[filename.csv]/ | tee -a /tmp/adjust_matches_for_overpaid_maintainers.log
#
module GitHub
  module Transitions
    class AdjustMatchesForOverpaidMaintainers < Transition
      BATCH_SIZE = 100
      USD_CURRENCY_CODE = "USD"

      def after_initialize
        file = CSV.read(@other_args[:transfers_file], headers: true, header_converters: :symbol, converters: :all)
        required_headers = [:login, :zuora_id, :overpaid_amount]
        required_headers.each do |header|
          unless file.headers.include?(header)
            raise HeaderError.new("Required header #{header} is missing")
          end
        end
        @user_totals = Hash.new(0)
        @transfers = file.map { |row| row.to_hash }
      end

      def perform
        @transfers.each do |transfer|
          process(transfer)
        end

        if dry_run?
          log "Total amounts of overpayments that would have been accounted for in ledger entries per user: #{@user_totals}" if verbose?
        else
          log "Total amounts of overpayments accounted for in ledger entries per user: #{@user_totals}" if verbose?
        end
      end

      def process(transfer)
        login = transfer[:login]
        maintainer = User.find_by(login: login)
        stripe_connect_account = maintainer.sponsors_listing.stripe_connect_account
        overpaid_amount = transfer[:overpaid_amount]
        zuora_id = transfer[:zuora_id]

        if overpaid_amount <= 0
          log "No overpayment for user #{login} and Zuora ID #{zuora_id}. Skipping." if verbose?
          return
        end

        log "Creating ledger entries for user #{login} and Zuora ID #{zuora_id}. Current total match amount for #{login} is #{stripe_connect_account.total_match_in_cents}." if verbose?

        transaction = ::Billing::PayoutsLedgerTransaction.new(stripe_connect_account)

        transaction.add_entry(
          transaction_type: :payment,
          amount_in_subunits: overpaid_amount,
          currency_code: USD_CURRENCY_CODE,
          primary_reference_id: zuora_id,
        )

        transaction.add_entry(
          transaction_type: :github_match,
          amount_in_subunits: -(overpaid_amount),
          currency_code: USD_CURRENCY_CODE,
          primary_reference_id: zuora_id,
        )

        if dry_run?
          unless transaction.valid?
            log "Failed to create valid Billing::PayoutsLedgerTransaction for user #{login} and Zuora ID #{zuora_id}!" if verbose?
            return
          end

          transaction.entries.each do |entry|
            log "Would have created #{entry.transaction_type} ledger entry for user #{maintainer.login} in amount of #{entry.amount_in_subunits} with primary_reference_id #{entry.amount_in_subunits}." if verbose?
          end
        else
          transaction.save!

          transaction.entries.each do |entry|
            log "Created #{entry.transaction_type} ledger entry for user #{maintainer.login} in amount of #{entry.amount_in_subunits} with primary_reference_id #{entry.amount_in_subunits}." if verbose?
          end
        end

        log "Done creating ledger entries for user #{login} and Zuora ID #{zuora_id}. Current total match amount for #{login} is #{stripe_connect_account.reload.total_match_in_cents}." if verbose?
        @user_totals[login] += overpaid_amount
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "transfers_file=", "CSV of the Stripe transfers that need to be reversed", as: String
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AdjustMatchesForOverpaidMaintainers.new(options)
  transition.run
end
