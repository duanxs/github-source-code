# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191011172227_perform_dependabot_integration_update.rb -v | tee -a /tmp/perform_dependabot_integration_update.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191011172227_perform_dependabot_integration_update.rb -v -w | tee -a /tmp/perform_dependabot_integration_update.log
#
module GitHub
  module Transitions
    class PerformDependabotIntegrationUpdate < Transition
      BATCH_SIZE = 1000

      attr_reader :integration, :iterator, :latest_version

      def after_initialize
        @integration = GitHub.dependabot_github_app
        @latest_version = @integration.latest_version
        @iterator = @integration.outdated_installations.
                                 preload(:target).
                                 find_in_batches(batch_size: BATCH_SIZE)
      end

      def perform
        iterator.each { |installs| process(installs) }
      end

      def process(installations)
        if dry_run?
          log "Would have attempted to upgrade #{installations.size} installations"
          return
        end

        IntegrationInstallation.throttle do
          installations.each do |installation|
            installation.auto_update_version(editor: installation.target, version: latest_version)
            log "Updated '#{installation.target.id}' to latest version" if verbose?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::PerformDependabotIntegrationUpdate.new(options)
  transition.run
end
