# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190913154959_clear_enterprise_marketplace_redis_queue.rb -v | tee -a /tmp/clear_enterprise_marketplace_redis_queue.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190913154959_clear_enterprise_marketplace_redis_queue.rb -v -w | tee -a /tmp/clear_enterprise_marketplace_redis_queue.log
#
module GitHub
  module Transitions
    class ClearEnterpriseMarketplaceRedisQueue < Transition
      MARKETPLACE_QUEUE = "marketplace"

      def perform
        Resque.remove_queue(MARKETPLACE_QUEUE)
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ClearEnterpriseMarketplaceRedisQueue.new(options)
  transition.run
end
