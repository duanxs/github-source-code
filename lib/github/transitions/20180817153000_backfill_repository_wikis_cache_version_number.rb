# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180817153000_backfill_repository_wikis_cache_version_number.rb -v -w | tee -a tmp/backfill_repository_wikis_cache_version_number.log
#
module GitHub
  module Transitions
    class BackfillRepositoryWikisCacheVersionNumber < Transition
      DEFAULT_BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        readonly do
          @iterator = RepositoryWiki.github_sql_batched(limit: DEFAULT_BATCH_SIZE)
          @iterator.add <<-SQL
            SELECT target.id
            FROM repository_wikis target
            WHERE target.id > :last
            ORDER BY target.id
            LIMIT :limit
          SQL
        end
      end

      def perform
        total = iterator.batches.reduce(0) do |sum, batch|
          ids = batch.flatten
          process ids
          sum += ids.size
        end

        log "Processed #{total} total rows." if verbose?
      end

      def process(ids)
        return if ids.empty?
        log "Copying cache_version to cache_version_number for #{ids.size} repository_wikis rows." if verbose?

        return if dry_run?

        ActiveRecord::Base.connected_to(role: :writing) do
          log "Backfilling cache_version_number for repository wikis with IDs: #{ids}"

          RepositoryWiki.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            RepositoryWiki.github_sql.run <<-SQL, ids: ids
              UPDATE repository_wikis w
              SET w.cache_version_number = w.cache_version
              WHERE w.id IN :ids
            SQL
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillRepositoryWikisCacheVersionNumber.new(options)
  transition.run
end
