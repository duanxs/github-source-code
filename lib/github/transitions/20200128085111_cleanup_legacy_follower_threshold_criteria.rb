# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200128085111_cleanup_legacy_follower_threshold_criteria.rb -v | tee -a /tmp/cleanup_legacy_follower_threshold_criteria.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200128085111_cleanup_legacy_follower_threshold_criteria.rb -v -w | tee -a /tmp/cleanup_legacy_follower_threshold_criteria.log
#
module GitHub
  module Transitions
    class CleanupLegacyFollowerThresholdCriteria < Transition

      DEPRECATED_SLUG = "follower_threshold"
      BATCH_SIZE = 100

      attr_reader :iterator, :depecated_criterion

      def after_initialize
        min_id = readonly do
          ::SponsorsMembershipsCriterion.github_sql.value(
            "SELECT COALESCE(MIN(id), 0) FROM sponsors_memberships_criteria",
          )
        end

        max_id = readonly do
          ::SponsorsMembershipsCriterion.github_sql.value(
            "SELECT COALESCE(MAX(id), 0) FROM sponsors_memberships_criteria",
          )
        end

        @depecated_criterion = readonly { ::SponsorsCriterion.find_by(slug: DEPRECATED_SLUG) }

        if depecated_criterion.present?
          @iterator = ::SponsorsMembershipsCriterion.github_sql_batched_between(
            start: min_id,
            finish: max_id,
            batch_size: BATCH_SIZE,
          )
          @iterator.add <<-SQL, { criterion_id: depecated_criterion.id }
            SELECT sponsors_memberships_criteria.id
            FROM sponsors_memberships_criteria
            INNER JOIN sponsors_memberships
              ON sponsors_memberships.id = sponsors_memberships_criteria.sponsors_membership_id
            WHERE sponsors_memberships.state = 0
              AND sponsors_memberships_criteria.sponsors_criterion_id = :criterion_id
              AND sponsors_memberships_criteria.id BETWEEN :start AND :last
          SQL
        end
      end

      def perform
        if depecated_criterion.present?
          log "Found deprecated criterion (#{DEPRECATED_SLUG})."
          destroy_deprecated_records_for(depecated_criterion)
        else
          log "Couldn't find deprecated criterion (#{DEPRECATED_SLUG}), skipping destroy action"
        end
      end

      private

      # Find deprecated criteria records for pending applications and destroy
      # them in favor of the new criterion. The new criterion records will
      # be automatically created by the RefreshSponsorsMembershipsCriteriaJob
      # scheduled job.
      def destroy_deprecated_records_for(depecated_criterion)
        GitHub::SQL::Readonly.new(@iterator.batches).each do |ids|
          records_to_destroy = readonly do
            ::SponsorsMembershipsCriterion.where(id: ids.flatten)
          end

          records_to_destroy.each do |record|
            if dry_run?
              log "Would have destroyed membership criterion #{record.id}."
            else
              log "Destroying membership criterion #{record.id}."
              ::SponsorsMembershipsCriterion.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
                records_to_destroy.each(&:destroy!)
              end
            end
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CleanupLegacyFollowerThresholdCriteria.new(options)
  transition.run
end
