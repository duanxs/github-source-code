# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190411011109_backfill_integration_aliases.rb -v | tee -a /tmp/backfill_integration_aliases.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190411011109_backfill_integration_aliases.rb -v -w | tee -a /tmp/backfill_integration_aliases.log
#
module GitHub
  module Transitions
    class BackfillIntegrationAliases < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM integrations") }
        max_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM integrations") }

        @iterator = ApplicationRecord::Domain::Integrations.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, slug FROM integrations
          WHERE id BETWEEN :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          IntegrationAlias.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "Creating alias for #{item[1]}" if verbose?
            write_somewhere_with(item) unless dry_run?
          end
        end
      end

      def write_somewhere_with(item)
        integration_id = item[0]
        slug           = item[1]

        begin
          ActiveRecord::Base.connected_to(role: :writing) do
            IntegrationAlias.create!(integration_id: integration_id, slug: slug)
          end
        rescue ActiveRecord::RecordInvalid => e
          log "[#{slug}] #{e.message}"
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillIntegrationAliases.new(options)
  transition.run
end
