# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200319154902_add_discussion_permissions_to_roles.rb --verbose | tee -a /tmp/add_discussion_permissions_to_roles.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200319154902_add_discussion_permissions_to_roles.rb --verbose -w | tee -a /tmp/add_discussion_permissions_to_roles.log
#
module GitHub
  module Transitions
    class LegacyFineGrainedPermission < ApplicationRecord::Iam
      self.table_name = :fine_grained_permissions
    end

    class LegacyRolePermission < ApplicationRecord::Iam
      self.table_name = :role_permissions
    end

    class LegacyRole < ApplicationRecord::Iam
      self.table_name = :roles
    end

    class AddDiscussionPermissionsToRoles < Transition
      FGP_ACTIONS = %w(create_discussion toggle_discussion_answer toggle_discussion_comment_minimize).freeze
      ROLE_NAMES = %w(triage).freeze

      def perform
        write_fine_grained_permissions
        add_role_permissions
      end

      private

      def write_fine_grained_permissions
        log "Creating fine-grained permissions: #{FGP_ACTIONS}" if verbose?
        return if dry_run?

        FGP_ACTIONS.each do |action|
          LegacyFineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            LegacyFineGrainedPermission.create(action: action)
          end
        end
      end

      def add_role_permissions
        FGP_ACTIONS.each do |action|
          permission = LegacyFineGrainedPermission.find_by_action(action)
          permission_id = nil

          if permission
            permission_id = permission.id
          elsif dry_run?
            permission_id = -1
          else
            log "ERROR: Missing fine-grained permission for action '#{action}'" if verbose?
            next
          end

          add_role_permissions_to(action: action, permission_id: permission_id)
        end
      end

      def add_role_permissions_to(action:, permission_id:)
        role_ids_by_name = LegacyRole.where(name: ROLE_NAMES).pluck(:name, :id).to_h
        log "Roles to update: #{role_ids_by_name.inspect}" if verbose?

        ROLE_NAMES.each do |role_name|
          role_id = role_ids_by_name[role_name]
          if role_id
            log "Granting permission '#{action}' ##{permission_id} to " \
              "role '#{role_name}' ##{role_id}" if verbose?
          else
            log "ERROR: Could not find role '#{role_name}', not granting " \
              "permission '#{action}' ##{permission_id}" if verbose?
            next
          end

          unless dry_run?
            LegacyRolePermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              role_permission = LegacyRolePermission.create(role_id: role_id,
                fine_grained_permission_id: permission_id,
                action: action)

              log "ERROR: Could not save role permission" if !role_permission.persisted? && verbose?
            end
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddDiscussionPermissionsToRoles.new(options)
  transition.run
end
