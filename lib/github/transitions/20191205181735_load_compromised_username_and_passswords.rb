# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191205181735_load_compromised_username_and_passswords.rb -v | tee -a /tmp/compromised_passwords.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191002155304_qintel_backfill.rb -v -w | tee -a /tmp/qintel_backfill.log
#
module GitHub
  module Transitions
    class LoadCompromisedUsernameAndPassswords < Transition
      def after_initialize
        @file = @other_args[:file]
        @datasource = @other_args[:datasource]
        @version = @other_args[:version]
      end

      # Returns nothing.
      def perform
        return if dry_run
        return if !File.file?(@file)

        log "ingesting"

        CompromisedPasswordDatasource.store_passwords(
          name: @datasource,
          version: @version,
          sha1_passwords: each_password_digest(each_record_logger(parser(@file))),
        )

        status_update
        log "Done ingesting"
        log "marking"

        CompromisedPasswordDatasource.check_for_compromise(
          name: @datasource,
          version: @version,
          compromised_records: each_record_logger(parser(@file)),
        )

        status_update
        log "done marking"
      end

      def parser(file)
        return enum_for(:parser, file) unless block_given?
        File.open(file) do |f|
          f.each_line do |l|
            begin
              yield JSON.parse(l)
            rescue Yajl::ParseError => e
              log e
            end
          end
        end
      end

      def each_record_logger(credential_enumerator)
        return enum_for(:each_record_logger, credential_enumerator) unless block_given?

        @file_blank_username = 0
        @file_blank_password = 0
        @file_processed = 0

        credential_enumerator.each do |username, password|
          if username.blank?
            @file_blank_username += 1
          end

          if password.blank?
            @file_blank_password += 1
          end
          yield [username, password]

          @file_processed += 1

          status_update if @file_processed % 10000 == 0
        end
        status_update
      end

      def each_password_digest(credential_enumerator)
        unless block_given?
          return enum_for(:each_password_digest, credential_enumerator)
        end

        credential_enumerator.each do |username, password|
          next if password.blank?
          yield Digest::SHA1.hexdigest(password)
        end
      end

      def status_update
        log [
          "processed=#{@file_processed}",
          "blank_usernames=#{@file_blank_username}",
          "blank_passwords=#{@file_blank_password}",
        ].join(" ")
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "f", "file=", "plaintext credential pairs"
    on "d", "datasource=", "The source of the data", required: true
    on "z", "version=", "The source of the data", required: true
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::LoadCompromisedUsernameAndPassswords.new(options)
  transition.run
end
