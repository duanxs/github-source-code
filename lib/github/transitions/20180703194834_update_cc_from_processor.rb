# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180703194834_update_cc_from_processor.rb -v -f /path/to/logins_file
#
#   This script requires a file with User logins to process with one login per line
#
module GitHub
  module Transitions
    class UpdateCcFromProcessor < Transition

      def after_initialize
        @login_file = other_args.fetch(:file)
        @input = other_args.fetch(:input, $stdin)
        @force = false
      end

      # Returns nothing.
      def perform
        total_logins = logins.length
        processed_logins = 0

        logins.each do |login|
          process(login)

          processed_logins += 1
          if processed_logins.multiple_of?(20)
            percent_complete = (100.0 * processed_logins.to_f / total_logins.to_f).round(1)
            puts "#{processed_logins} / #{total_logins} complete ( #{percent_complete}% )"
          end
        end
      end

      def process(login)
        user = nil
        payment_method = nil

        readonly do
          user = User.find_by(login: login)
          payment_method = user.payment_method
        end

        if payment_method.using_zuora_processor?
          puts "Could not update payment method #{payment_method.payment_token} for user #{user.login}: Zuora is not supported"
          return
        end

        if payment_method.paypal?
          puts "Could not update payment method #{payment_method.payment_token} for user #{user.login}: PayPal is not supported"
          return
        end

        braintree_payment_method = Braintree::PaymentMethod.find(payment_method.payment_token)

        unless braintree_payment_method.is_a?(Braintree::CreditCard)
          puts "Could not update payment method #{payment_method.payment_token} for user #{user.login}: #{braintree_payment_method.class} is not supported"
          return
        end

        payment_method.truncated_number = braintree_payment_method.masked_number
        payment_method.expiration_month = braintree_payment_method.expiration_month
        payment_method.expiration_year = braintree_payment_method.expiration_year

        unless payment_method.changed?
          puts "No changes to save for payment method #{payment_method.payment_token} for user #{user.login}"
          return
        end

        unless @force
          puts "Will update payment method #{payment_method.payment_token} for user #{user.login} as follows:\n"
          puts ("  PAN:       %19s -> %-19s" % payment_method.changes[:truncated_number]) if payment_method.truncated_number_changed?
          puts ("  Exp Month: %19s -> %-19s" % payment_method.changes[:expiration_month]) if payment_method.expiration_month_changed?
          puts ("  Exp Year:  %19s -> %-19s" % payment_method.changes[:expiration_year]) if payment_method.expiration_year_changed?
          puts ""

          return unless continue?
        end

        if dry_run?
          puts "Would have saved payment method #{payment_method.payment_token} for user #{user.login}"
          return
        end

        payment_method.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) { payment_method.save! }

        puts "Updated payment method #{payment_method.payment_token} for user #{user.login}"
      end

      private

      def continue?
        print "Continue? (y/a/n/q/?): "

        answer = @input.gets

        case answer.downcase[0]
        when "y"
          return true
        when "a"
          @force = true
          return true
        when "n"
          return false
        when "q"
          abort
        when "?"
          puts "y - Save changes for this payment method"
          puts "a - Save changes for this and all following payment methods without asking"
          puts "n - Do not save changes for this payment method"
          puts "q - Quit without saving changes to this payment method"
          puts "? - Display this help"
          return continue?
        end
      end

      def logins
        @logins ||= File.read(@login_file).lines.map(&:chomp)
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "f", "file=", "Input file with logins to process"
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UpdateCcFromProcessor.new(options)
  transition.run
end
