# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190318161654_synchronize_edu_coupons_with_zuora.rb -v | tee -a /tmp/synchronize_edu_coupons_with_zuora.log
#
module GitHub
  module Transitions
    class SynchronizeEduCouponsWithZuora < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        # The most common, and preferred, approach is to define your iterator
        # here using `BatchedBetween`. Prefer using a min_id with BatchedBetween
        # queries. Not doing so can lead to _very_ slow transition tests (https://github.com/github/github/pull/92565)
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM plan_subscriptions") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM plan_subscriptions") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT `plan_subscriptions`.`id` FROM `plan_subscriptions`
          INNER JOIN `users` ON `users`.`id` = `plan_subscriptions`.`user_id`
          INNER JOIN `coupon_redemptions` ON `coupon_redemptions`.`user_id` = `users`.`id`
          INNER JOIN `coupons` ON `coupons`.`id` = `coupon_redemptions`.`coupon_id`
          INNER JOIN `customer_accounts` ON `customer_accounts`.`user_id` = `users`.`id`
          INNER JOIN `customers` ON `customers`.`id` = `customer_accounts`.`customer_id`
          INNER JOIN `payment_methods` ON `payment_methods`.`customer_id` = `customers`.`id`
          WHERE `plan_subscriptions`.`id` BETWEEN :start AND :last
          AND `coupon_redemptions`.`expired` = 0
          AND `coupons`.`code` = 'students-2019'
          AND `users`.`plan` = 'pro'
          AND `users`.`disabled` = 0
          AND `users`.`spammy` = 0
          AND `plan_subscriptions`.`zuora_subscription_id` IS NOT NULL
          AND payment_methods.payment_token IS NOT NULL
          AND `payment_methods`.`payment_token` != 'payment-token-cleared'
        SQL

        @users_synchronized = Hash.new(0)
        @user_transactions_refunded = Hash.new(0)
        @counter = 0
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows.flatten)
          @counter += rows.count
          $stderr.printf(" currnent count #{@counter}\r") if verbose?
        end

        log "Synchronized users #{@users_synchronized.count}"
        log "Synchronize count #{@users_synchronized}"
        log "Transactions refunded #{@user_transactions_refunded}"
      end

      def process(plan_subscription_ids)
        plan_subscription_ids.each do |plan_subscription_id|
          plan_subscription = readonly { ::Billing::PlanSubscription.find(plan_subscription_id) }
          user = readonly { plan_subscription.user }


          if dry_run?
            external_subscription = plan_subscription.external_subscription
            if external_subscription
              discount = external_subscription.discount.cents
              if discount != 700 && discount != 8400
                @users_synchronized[user.login] += 1
              end
            end
          else
            plan_subscription.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              result = plan_subscription.synchronize
              if result
                @users_synchronized[user.login] += 1
              end
            end
          end

          user.billing_transactions.where("created_at > '2019-01-01'").includes(:line_items).find_each do |transaction|
            next if transaction.voided? || transaction.was_refunded?

            # Skip transactions that are not GitHub plan transactions
            next if transaction.line_items.any?

            result = nil
            unless dry_run?
              result = refund_transaction(user.login, transaction.transaction_id)
            end

            if dry_run? || result
              @user_transactions_refunded[user.login] += 1
            end
          end
        end
      end

      def refund_transaction(login, transaction_id)
        refund_result = GitHub::Billing.refund_transaction(transaction_id)

        if refund_result.success?
          log "[#{login}] Transaction #{transaction_id} was refunded"
        else
          void_result = GitHub::Billing.void_transaction(transaction.transaction_id)

          unless void_result.success?
            log "[#{login}] Transaction #{transaction.transaction_id} wasn't refunded or voided"
            return false
          end

          log "[#{login}] Transaction #{transaction.transaction_id} was voided"
        end

        true
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::SynchronizeEduCouponsWithZuora.new(options)
  transition.run
end
