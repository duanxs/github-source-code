# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
# This accepts user_ids, token_last_eights, or both
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190821214133_reset_personal_access_tokens.rb -v -user_ids=123456,654321| tee -a /tmp/reset_personal_access_tokens.log
#
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190821214133_reset_personal_access_tokens.rb -v -w -user_ids=123456,654321| tee -a /tmp/reset_personal_access_tokens.log
#
module GitHub
  module Transitions
    class ResetPersonalAccessTokens < Transition

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      # Returns nothing.
      def perform
        if other_args[:user_ids]
          log "deleting PATs with user_ids in #{other_args[:user_ids]}"
          OauthAuthorization.personal_tokens.where(
            user_id: other_args[:user_ids],
          ).in_batches(of: BATCH_SIZE) do |group|
            OauthAuthorization.throttle_with_retry(max_retry_count: 8) do
              delete_records(group)
            end
          end
        end
      end

      def delete_records(group)
        if group.empty?
          log "No PATs to destroy."
          return
        end

        if dry_run?
          log "Would have destroyed #{group.size} PATs"
        else
          OauthAuthorization.throttle_writes do
            group.each do |pat|
              pat.destroy_with_explanation(:site_admin)
            end
          end
          log "Destroyed #{group.size} PATs"
        end
        GitHub.dogstats.count("account_security.oauth_access", group.size, tags: ["action:destroy", "explanation:reset", "dry_run:#{dry_run?}"])
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "u", "user_ids=", "Comma-separated user_ids", as: Array, delimiter: ","
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ResetPersonalAccessTokens.new(options)
  transition.run
end
