# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190903211502_add_additional_sponsors_criteria.rb -v | tee -a /tmp/add_additional_sponsors_criteria.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190903211502_add_additional_sponsors_criteria.rb -v -w | tee -a /tmp/add_additional_sponsors_criteria.log
#
module GitHub
  module Transitions
    class AddAdditionalSponsorsCriteria < Transition

      CRITERIA = [
        {
          slug: "public_contributions",
          description: "Public contributions",
          automated: true,
        },
        {
          slug: "follower_threshold",
          description: "Account has > 100 followers?",
          automated: true,
        },
        {
          slug: "external_contributions",
          description: "The user has public contributions outside of GitHub (optional)",
        },
      ]

      # Returns nothing.
      def perform
        CRITERIA.each do |criterion_attrs|
          if dry_run?
            log "Would have created #{criterion_attrs[:slug]}"
          else
            create_sponsors_criterion(criterion_attrs)
          end
        end
      end

      def create_sponsors_criterion(attrs)
        SponsorsCriterion.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Creating #{attrs[:slug]}..."

          criterion = SponsorsCriterion.new(attrs)

          if criterion.save
            log "Created #{criterion.slug}."
          else
            log "Failed to create #{criterion.slug}: #{criterion.errors.full_messages.to_sentence}"
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddAdditionalSponsorsCriteria.new(options)
  transition.run
end
