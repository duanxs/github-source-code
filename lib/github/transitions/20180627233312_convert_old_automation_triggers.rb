# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180627233312_convert_old_automation_triggers.rb -v | tee -a /tmp/convert_old_automation_triggers.log
#
module GitHub
  module Transitions
    class ConvertOldAutomationTriggers < Transition

      BATCH_SIZE = 100
      attr_reader :iterator
      attr_reader :archived_iterator

      # defining here so we can remove from project model
      CONTENT_REOPENED_TRIGGER = "content_reopened".freeze
      PENDING_CARD_ADDED_TRIGGER = "pending_card_added".freeze

      def after_initialize
        @processed_count = 0
        @skipped_count = 0
        @archived_processed_count = 0
        @archived_skipped_count = 0

        max_id = readonly { ApplicationRecord::Domain::Projects.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM project_workflows") }
        @iterator = ApplicationRecord::Domain::Projects.github_sql_batched_between(start: 0, finish: max_id, batch_size: BATCH_SIZE)

        @iterator.bind(trigger_types: [
          CONTENT_REOPENED_TRIGGER,
          PENDING_CARD_ADDED_TRIGGER,
        ])

        @iterator.add <<-SQL
          SELECT id FROM project_workflows
          WHERE id BETWEEN :start AND :last
            AND trigger_type in :trigger_types
        SQL

        max_id = readonly { ApplicationRecord::Domain::Projects.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM archived_project_workflows") }
        @archived_iterator = ApplicationRecord::Domain::Projects.github_sql_batched_between(start: 0, finish: max_id, batch_size: BATCH_SIZE)

        @archived_iterator.bind(trigger_types: [
          CONTENT_REOPENED_TRIGGER,
          PENDING_CARD_ADDED_TRIGGER,
        ])

        @archived_iterator.add <<-SQL
          SELECT id, trigger_type FROM archived_project_workflows
          WHERE id BETWEEN :start AND :last
            AND trigger_type in :trigger_types
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows, project_class: ProjectWorkflow)
        end
        log "Converted #{@processed_count} project workflows."
        log "Skipped #{@skipped_count} project workflows."

        GitHub::SQL::Readonly.new(archived_iterator.batches).each do |rows|
          process(rows, project_class: Archived::ProjectWorkflow)
        end
        log "Converted #{@archived_processed_count} archived project workflows."
        log "Skipped #{@archived_skipped_count} archived project workflows."
      end

      def process(rows, project_class:)
        rows.each do |item|
          project_class.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            workflow = project_class.find_by_id(item[0])
            if workflow.present? && !skip_workflow_conversion?(workflow, project_class: project_class)
              if verbose?
                log "updating #{workflow.id} - has a trigger type of #{workflow.trigger_type}"
              end
              unless dry_run?
                update_workflow(workflow)
              end
            else
              if project_class == ProjectWorkflow
                @skipped_count += 1
              else
                @archived_skipped_count += 1
              end
            end
          end

          if project_class == ProjectWorkflow
            @processed_count += 1
          else
            @archived_processed_count += 1
          end
        end
      end

      # For each generalized workflow, update existing one to the Issue version
      # and create a duplicate for the PR version
      def update_workflow(workflow)
        new_workflows = if workflow.trigger_type == CONTENT_REOPENED_TRIGGER
          [
            duplicate_workflow_with_pr_trigger(ProjectWorkflow::PR_REOPENED_TRIGGER, workflow),
            update_workflow_with_issue_trigger(ProjectWorkflow::ISSUE_REOPENED_TRIGGER, workflow),
          ]
        elsif workflow.trigger_type == PENDING_CARD_ADDED_TRIGGER
          [
            duplicate_workflow_with_pr_trigger(ProjectWorkflow::PR_PENDING_CARD_ADDED_TRIGGER, workflow),
            update_workflow_with_issue_trigger(ProjectWorkflow::ISSUE_PENDING_CARD_ADDED_TRIGGER, workflow),
          ]
        end

        ActiveRecord::Base.connected_to(role: :writing) do
          new_workflows.each do |new_workflow|
            new_workflow.save!
          end
        end
      end

      # Take the existing workflow and update with the Issue version of the
      # generalized trigger
      def update_workflow_with_issue_trigger(new_trigger_type, workflow)
        # Using ghost here if the original creator is gone so we don't run into
        # validation errors. Not sure we need it here but want to stay
        # consistent with the new workflow creation.
        old_creator = workflow.creator || User.ghost
        workflow.trigger_type = new_trigger_type
        workflow.creator_id = old_creator.id
        # Skip instrumentation so we don't end up with new audit log entries
        if workflow.is_a?(ProjectWorkflow)
          workflow.skip_instrument_creation_callback = true
        end
        workflow
      end

      # Take the existing workflow and duplicate with the PR version of the
      # generalized trigger
      def duplicate_workflow_with_pr_trigger(new_trigger_type, old_workflow)
        # Using ghost here if the original creator is gone so we don't run into
        # validation errors.
        old_creator = old_workflow.creator || User.ghost
        workflow = old_workflow.dup
        workflow.trigger_type = new_trigger_type
        workflow.created_at = old_workflow.created_at
        workflow.updated_at = old_workflow.updated_at
        workflow.creator_id = old_creator.id
        if workflow.is_a?(ProjectWorkflow)
          workflow.actions.build(action_type: ProjectWorkflowAction::TRANSITION_TO_COLUMN, creator_id: old_creator.id, project_id: workflow.project_id, created_at: workflow.created_at)
        # Skip instrumentation so we don't end up with new audit log entries
          workflow.skip_instrument_creation_callback = true
        else
          workflow.project_workflow_actions.build(action_type: ProjectWorkflowAction::TRANSITION_TO_COLUMN, creator_id: old_creator.id, project_id: workflow.project_id, created_at: workflow.created_at)
        end
        workflow
      end


      # Check to see if the project has automation in an inconsistent state
      def skip_workflow_conversion?(workflow, project_class:)
        trigger_types = if workflow.trigger_type == CONTENT_REOPENED_TRIGGER
          [ProjectWorkflow::ISSUE_REOPENED_TRIGGER, ProjectWorkflow::PR_REOPENED_TRIGGER, CONTENT_REOPENED_TRIGGER]
        elsif workflow.trigger_type == PENDING_CARD_ADDED_TRIGGER
          [ProjectWorkflow::ISSUE_PENDING_CARD_ADDED_TRIGGER, ProjectWorkflow::PR_PENDING_CARD_ADDED_TRIGGER, PENDING_CARD_ADDED_TRIGGER]
        end
        related_workflows = project_class.where(project_id: workflow.project_id, trigger_type: trigger_types)
        if related_workflows.size != 1
          log "there were old and new workflows for the project #{workflow.project_id} current set #{related_workflows.collect(&:trigger_type)}"
          return true
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ConvertOldAutomationTriggers.new(options)
  transition.run
end
