# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190314230219_backfill_enterprise_installation_owners.rb -v | tee -a /tmp/backfill_enterprise_installation_owners.log
#
module GitHub
  module Transitions
    class BackfillEnterpriseInstallationOwners < Transition
      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM enterprise_installations") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM enterprise_installations") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, organization_id, 'User' FROM enterprise_installations
          WHERE id BETWEEN :start AND :last
          AND owner_id IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        count = GitHub::SQL::Readonly.new(iterator.batches).sum do |rows|
          process(rows)
        end

        log "updated #{count} enterprise installation rows"
      end

      def process(rows)
        return if rows.empty?

        first_id = rows.first.first
        last_id = rows.last.first
        log "setting owner for enterprise installations #{first_id} through #{last_id}" if verbose?
        return rows.size if dry_run?

        EnterpriseInstallation.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          # use insert into + duplicate key update to run the update as a batch, avoiding
          # an UPDATE for each row
          EnterpriseInstallation.github_sql.run(<<-SQL, { rows: GitHub::SQL::ROWS(rows) })
            INSERT INTO
              enterprise_installations (id, owner_id, owner_type)
            VALUES
              :rows
            ON DUPLICATE KEY UPDATE
              id         = id,
              owner_id   = VALUES(owner_id),
              owner_type = VALUES(owner_type)
          SQL
        end

        rows.size
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillEnterpriseInstallationOwners.new(options)
  transition.run
end
