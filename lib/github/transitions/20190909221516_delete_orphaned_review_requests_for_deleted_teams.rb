# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190909221516_delete_orphaned_review_requests_for_deleted_teams.rb -v | tee -a /tmp/delete_orphaned_review_requests_for_deleted_teams.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190909221516_delete_orphaned_review_requests_for_deleted_teams.rb -v -w | tee -a /tmp/delete_orphaned_review_requests_for_deleted_teams.log
#
module GitHub
  module Transitions
    class DeleteOrphanedReviewRequestsForDeletedTeams < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator, :max_id, :min_id

      def after_initialize
        @min_id = @other_args[:start_id] || readonly { ReviewRequest.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM review_requests") }
        @max_id = @other_args[:end_id] || readonly { ReviewRequest.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM review_requests") }
        @batch_size = @other_args[:batch_size] || BATCH_SIZE
        log "Starting with ID #{@min_id} and finishing with ID #{@max_id}. Batch size is #{@batch_size}." if verbose?

        @iterator = ReviewRequest.github_sql_batched_between(start: min_id, finish: max_id, batch_size: @batch_size)

        # get team review requests within the id range
        @iterator.add <<-SQL
          SELECT id, reviewer_id FROM review_requests req
          WHERE id BETWEEN :start AND :last
          AND req.reviewer_type = 'Team'
        SQL

        @existing_teams = {}
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each_slice(@batch_size) do |items|
          ReviewRequest.throttle do
            # log "Examining review requests #{items.first.first} - #{items.last.first}" if verbose?

            # create an association of team id => [review_request id]
            teams_to_reviews = Hash.new { |hash, key| hash[key] = [] }
            items.each do |(review_id, team_id)|

              # we don't need to check teams we know exist
              unless @existing_teams.include? team_id
                teams_to_reviews[team_id] << review_id
              end

            end

            team_ids = teams_to_reviews.keys
            unless team_ids.empty?
              # check to see which teams exist
              found_teams = readonly { Team.github_sql.values("SELECT id FROM teams WHERE id IN :team_ids", team_ids: team_ids) }

              # remove all of the teams that do exist
              found_teams.each do |team_id|
                teams_to_reviews.delete team_id

                # cache them so we don't need to check them in the future
                @existing_teams[team_id] = true
              end

              reviews_to_delete = teams_to_reviews.values.flatten

              unless reviews_to_delete.empty?
                log "deleting #{reviews_to_delete.count} ReviewRequests: #{reviews_to_delete}" if verbose?

                destroy_review_requests(reviews_to_delete) unless dry_run?
              end
            end
          end
        end
      end

      def destroy_review_requests(ids)
        ReviewRequest.github_sql.run(<<-SQL, ids: Array(ids).compact)
          DELETE FROM review_requests
          WHERE id IN :ids
        SQL
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "start_id=", "Ability ID to start processing at", as: Integer
    on "end_id=", "Ability ID to end processing at", as: Integer
    on "batch_size=", "Number of rows to process at a time", as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DeleteOrphanedReviewRequestsForDeletedTeams.new(options)
  transition.run
end
