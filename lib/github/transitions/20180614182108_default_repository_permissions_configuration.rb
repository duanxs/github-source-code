# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180614182108_default_repository_permissions_configuration.rb -v | tee -a /tmp/default_repository_permissions_configuration.log
#
module GitHub
  module Transitions
    class DefaultRepositoryPermissionsConfiguration < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        # ugh, going to be a lot of results :(
        # we can do a bit of filtering to remove orgs that already have
        # a configuration entry for this setting
        min_id = readonly { User.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM users") }
        max_id = readonly { User.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM users") }
        @iterator = User.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT users.id, users.raw_data FROM users
          WHERE users.type = "Organization"
                AND users.id between :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        total = 0
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
          total += rows.size
        end

        log "Processed #{total} total rows" if verbose?
      end

      def process(rows)
        values = { none: [], write: [], admin: [] }

        # Get organization ids from the rows, each row is in the form [id, raw_data]
        organization_ids = rows.map(&:first)

        # Get organization ids to be excluded for organizations that have configuration entries
        exclude_organization_ids = readonly do
          Configuration::Entry.github_sql.values(<<-SQL, { organization_ids: organization_ids })
            SELECT entries.target_id
            FROM configuration_entries entries
            WHERE entries.target_id IN :organization_ids
              AND entries.target_type = "User"
              AND entries.name = "#{Configurable::DefaultRepositoryPermission::KEY}"
          SQL
        end

        organization_ids = organization_ids - exclude_organization_ids
        rows_hash = rows.to_h

        organization_ids.each do |organization_id|
          raw_data = rows_hash[organization_id]
          data = GitHub::ZSON.decode(raw_data)

          case data["default_repository_permission"]
          when nil
            values[:none].push(organization_id)
          when Ability.actions[:read]
            # 0 == :read, the default value.  we can skip this scenario as it doesn't
            # result in writing a configuration entry
          when Ability.actions[:write]
            values[:write].push(organization_id)
          when Ability.actions[:admin]
            values[:admin].push(organization_id)
          end
        end

        Configuration::Entry.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          insert_configuration_entries(values)
        end
      end

      def insert_configuration_entries(data)
        organization_ids = data.flat_map { |k, v| v }
        return if organization_ids.empty?

        log "Setting default repository permission :none configuration entries for orgs with ids #{data[:none]}" if verbose?
        log "Setting default repository permission :write configuration entries for orgs with ids #{data[:write]}" if verbose?
        log "Setting default repository permission :admin configuration entries for orgs with ids #{data[:admin]}" if verbose?
        return if dry_run?

        now = GitHub::SQL::NOW
        rows = GitHub::SQL::ROWS(
          data.flat_map do |key, organization_ids|
            organization_ids.map do |id|
              [
                "User", # target_type
                id, # target_id
                id, # updater_id
                Configurable::DefaultRepositoryPermission::KEY, # name
                key, # value
                now, # created_at
                now, # updated_at
              ]
            end
          end,
        )

        Configuration::Entry.github_sql.run(<<-SQL, { rows: rows })
          INSERT INTO configuration_entries
            (target_type, target_id, updater_id, name, value, created_at, updated_at)
          VALUES
            :rows
        SQL
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DefaultRepositoryPermissionsConfiguration.new(options)
  transition.run
end
