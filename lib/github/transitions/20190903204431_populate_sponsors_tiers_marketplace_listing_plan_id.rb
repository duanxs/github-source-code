# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190903204431_populate_sponsors_tiers_marketplace_listing_plan_id.rb -v | tee -a /tmp/populate_sponsors_tiers_marketplace_listing_plan_id.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190903204431_populate_sponsors_tiers_marketplace_listing_plan_id.rb -v -w | tee -a /tmp/populate_sponsors_tiers_marketplace_listing_plan_id.log
#
module GitHub
  module Transitions
    class PopulateSponsorsTiersMarketplaceListingPlanId < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_tiers") }
        max_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_tiers") }

        @iterator = ApplicationRecord::Collab.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, created_at, name, sponsors_listing_id FROM sponsors_tiers
          WHERE id BETWEEN :start AND :last
          AND marketplace_listing_plan_id IS NULL
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          SponsorsTier.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "Backfilling marketplace_listing_plan_id for SponsorsTier with ID #{item[0]} and name #{item[2]}" if verbose?
            backfill_marketplace_listing_plan_id(item) unless dry_run?
          end
        end
      end

      def backfill_marketplace_listing_plan_id(item)
        ActiveRecord::Base.connected_to(role: :writing) do
          tier_id = item[0]
          tier_created_at = item[1]
          tier_name = item[2]
          tier_sponsors_listing_id = item[3]

          sponsors_listing_slug = SponsorsListing.find(tier_sponsors_listing_id).slug
          marketplace_listing = Marketplace::Listing.find_by_slug(sponsors_listing_slug)

          marketplace_listing_plan = Marketplace::ListingPlan.find_by(
            listing: marketplace_listing,
            name: tier_name,
            created_at: tier_created_at,
          )

          if marketplace_listing_plan.nil?
            log "No corresponding Marketplace::ListingPlan found for SponsorsTier with ID #{tier_id} and name #{tier_name}" if verbose?
          else
            ApplicationRecord::Collab.github_sql.run(
              "UPDATE sponsors_tiers SET marketplace_listing_plan_id = :marketplace_listing_plan_id WHERE id = :sponsors_tier_id",
              marketplace_listing_plan_id: marketplace_listing_plan.id,
              sponsors_tier_id: tier_id,
            )
            log "Set marketplace_listing_plan_id to #{marketplace_listing_plan.id} for SponsorsTier with ID #{tier_id} and name #{tier_name}" if verbose?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::PopulateSponsorsTiersMarketplaceListingPlanId.new(options)
  transition.run
end
