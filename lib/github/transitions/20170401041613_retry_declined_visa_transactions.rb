# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20170401041613_retry_declined_visa_transactions.rb -v | tee -a /tmp/retry_declined_visa_transactions.log
#
module GitHub
  module Transitions
    class RetryDeclinedVisaTransactions < Transition
      # Returns nothing.
      def perform
        puts "Dry run: #{dry_run}" if verbose
        count = 0

        failed_visa_transactions.each do |transaction|
          if hard_declined_and_past_due?(transaction)
            if verbose
              puts "Retrying subscription '#{transaction.subscription_id}'"
            end

            count = count + 1
            retry_charge(transaction) unless dry_run
          end
        end

        puts "Retried #{count} subscriptions" if verbose
      end

      private

      def hard_declined_and_past_due?(transaction)
        transaction.processor_response_code == "2019" &&
        transaction.subscription_id.present?          &&
        (subscription = Braintree::Subscription.find(transaction.subscription_id)) &&
        subscription.status == "Past Due"
      end

      def retry_charge(transaction)
        Braintree::Subscription.retry_charge(transaction.subscription_id)
      end

      def failed_visa_transactions
        Braintree::Transaction.search do |search|
          search.type.is "sale"
          search.source.is "Recurring"
          search.payment_instrument_type.is "credit_card"
          search.credit_card_card_type.is "Visa"
          search.status.is "processor_declined"
          search.processor_declined_at.between(start_time, end_time)
        end
      end

      def start_time
        Time.zone.parse("2017-03-21 03:52 CDT").utc
      end

      def end_time
        Time.zone.parse("2017-03-21 07:49 CDT").utc
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RetryDeclinedVisaTransactions.new(options)
  transition.run
end
