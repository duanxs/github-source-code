# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200309155608_backfill_sponsorship_active_flag.rb -v | tee -a /tmp/backfill_sponsorship_active_flag.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200309155608_backfill_sponsorship_active_flag.rb -v -w | tee -a /tmp/backfill_sponsorship_active_flag.log
#
module GitHub
  module Transitions
    class BackfillSponsorshipActiveFlag < Transition
      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsorships") }
        max_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsorships") }

        @iterator = ApplicationRecord::Domain::Sponsors.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          -- id must go first for batched between to work properly
          SELECT id, active, subscription_item_id FROM sponsorships
          WHERE id BETWEEN :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        active, inactive = rows.partition { |id, active, subscription_item_id| active == 1 }
        active_ids = active.map(&:last)
        inactive_ids = inactive.map(&:last)

        if active_ids.any?
          ids_to_deactivate = ApplicationRecord::Domain::Users.github_sql.values(
            "SELECT id FROM subscription_items WHERE id IN :ids AND quantity = 0",
            ids: active_ids,
          )

          if ids_to_deactivate.any?
            if dry_run?
              log "Found sponsorships to deactivate for subscription items: #{ids_to_deactivate.join(", ")}" if verbose?
            else
              deactivate_sponsorships(ids_to_deactivate)
            end
          end
        end

        if inactive_ids.any?
          ids_to_reactivate = ApplicationRecord::Domain::Users.github_sql.values(
            "SELECT id FROM subscription_items WHERE id IN :ids AND quantity > 0",
            ids: inactive_ids,
          )

          if ids_to_reactivate.any?
            if dry_run?
              log "Found sponsorships to reactivate for subscription items: #{ids_to_reactivate.join(", ")}" if verbose?
            else
              reactivate_sponsorships(ids_to_reactivate)
            end
          end
        end
      end

      def deactivate_sponsorships(subscription_item_ids)
        ActiveRecord::Base.connected_to(role: :writing) do
          log "Deactivating sponsorships for subscription items: #{subscription_item_ids.join(", ")}" if verbose?
          Sponsorship.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            sql = ApplicationRecord::Domain::Sponsors.github_sql.run(
              "UPDATE sponsorships SET active=0 WHERE subscription_item_id IN :ids",
              ids: subscription_item_ids,
            )
            log "Deactivated #{sql.affected_rows} sponsorships" if verbose?
          end
        end
      end

      def reactivate_sponsorships(subscription_item_ids)
        ActiveRecord::Base.connected_to(role: :writing) do
          log "Reactivating sponsorships for subscription items: #{subscription_item_ids.join(", ")}" if verbose?
          Sponsorship.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            sql = ApplicationRecord::Domain::Sponsors.github_sql.run(
              "UPDATE sponsorships SET active=1 WHERE subscription_item_id IN :ids",
              ids: subscription_item_ids,
            )
            log "Reactivated #{sql.affected_rows} sponsorships" if verbose?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillSponsorshipActiveFlag.new(options)
  transition.run
end
