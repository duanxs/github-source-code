# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200211114821_backfill_repository_edits.rb -v | tee -a /tmp/backfill_20200211114821_backfill_repository_edits.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200211114821_backfill_repository_edits.rb -v -w | tee -a /tmp/backfill_20200211114821_backfill_repository_edits.log
#
module GitHub
  module Transitions
    class BackfillRepositoryEdits < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      TYPES = %(CommitComment IssueComment PullRequestReview PullRequestReviewComment RepositoryAdvisory RepositoryAdvisoryComment)

      attr_reader :iterator

      def after_initialize
        @target_type = @other_args[:type]

        unless GitHub::Transitions::BackfillRepositoryEdits::TYPES.include?(@target_type)
          raise "`type` must be one of ${GitHub::Transitions::BackfillRepositoryEdits::TYPES.map(&:inspect).join(", ")}"
        end

        @target_edit_type = "#{@other_args[:type]}Edit"
        @target_edit_table_name = @target_edit_type.tableize

        @start = readonly { UserContentEdit.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM user_content_edits") }
        @finish = readonly { UserContentEdit.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM user_content_edits") }
      end

      def batch_size
        @other_args[:"batch-size"] || BATCH_SIZE
      end

      def verify?
        @other_args[:verify]
      end

      def run_query(start:, last:)
        if verify?
          query = UserContentEdit.github_sql.new <<~SQL, start: start, last: [last, @finish].min, target_type: @target_type
            SELECT
              `user_content_edits`.`id` as t1_id,
              `user_content_edits`.`user_content_id` as t1_user_content_id,
              `user_content_edits`.`user_content_type` as t1_user_content_type,
              `user_content_edits`.`edited_at` as t1_edited_at,
              `user_content_edits`.`editor_id` as t1_editor_id,
              `user_content_edits`.`created_at` as t1_created_at,
              `user_content_edits`.`updated_at` as t1_updated_at,
              `user_content_edits`.`performed_by_integration_id` as t1_performed_by_integration_id,
              `user_content_edits`.`deleted_at` as t1_deleted_at,
              `user_content_edits`.`deleted_by_id` as t1_deleted_by_id,
              `user_content_edits`.`diff` as t1_diff,

              `#{@target_edit_table_name}`.`id` as t2_id,
              `#{@target_edit_table_name}`.`#{@target_type.dasherize.underscore}_id` as t2_user_content_id,
              `#{@target_edit_table_name}`.`edited_at` as t2_edited_at,
              `#{@target_edit_table_name}`.`editor_id` as t2_editor_id,
              `#{@target_edit_table_name}`.`created_at` as t2_created_at,
              `#{@target_edit_table_name}`.`updated_at` as t2_updated_at,
              `#{@target_edit_table_name}`.`performed_by_integration_id` as t2_performed_by_integration_id,
              `#{@target_edit_table_name}`.`deleted_at` as t2_deleted_at,
              `#{@target_edit_table_name}`.`deleted_by_id` as t2_deleted_by_id,
              `#{@target_edit_table_name}`.`diff` as t2_diff,
              `#{@target_edit_table_name}`.`user_content_edit_id` as t2_user_content_edit_id
            FROM `user_content_edits`
            LEFT OUTER JOIN `#{@target_edit_table_name}` ON `#{@target_edit_table_name}`.`user_content_edit_id` = `user_content_edits`.`id`
            WHERE `user_content_edits`.`user_content_type` = :target_type AND `user_content_edits`.`id` BETWEEN :start AND :last
            ORDER BY `user_content_edits`.`id` ASC
            /* cross-schema-domain-query-exempted */
          SQL

          query.hash_results.each do |row|
            if row["t2_id"].nil?
              log "UserContentEdit##{row["t1_id"]} has no corresponding #{@target_edit_type}!"
            else
              equal = true
              equal &&= row["t1_id"] == row["t2_id"]
              equal &&= row["t1_user_content_id"] == row["t2_user_content_id"]
              equal &&= row["t1_edited_at"] == row["t2_edited_at"]
              equal &&= row["t1_editor_id"] == row["t2_editor_id"]
              equal &&= row["t1_created_at"] == row["t2_created_at"]
              equal &&= row["t1_updated_at"] == row["t2_updated_at"]
              equal &&= row["t1_performed_by_integration_id"] == row["t2_performed_by_integration_id"]
              equal &&= row["t1_deleted_at"] == row["t2_deleted_at"]
              equal &&= row["t1_deleted_by_id"] == row["t2_deleted_by_id"]
              equal &&= row["t1_diff"] == row["t2_diff"]

              log "UserContentEdit##{row["t1_id"]} does not match #{@target_edit_type}##{row["t2_id"]}!" unless equal
            end
          end
        elsif dry_run?
          query = UserContentEdit.github_sql.new <<~SQL, start: start, last: [last, @finish].min, target_type: @target_type
            SELECT
              `id`,
              `user_content_id`,
              `edited_at`,
              `editor_id`,
              `created_at`,
              `updated_at`,
              `performed_by_integration_id`,
              `deleted_at`,
              `deleted_by_id`,
              `diff`,
              `id`
            FROM `user_content_edits`
            WHERE
              `user_content_type` = :target_type AND `id` BETWEEN :start AND :last
            ORDER BY `id` ASC
          SQL

          query.results
        else
          query = UserContentEdit.github_sql.new <<~SQL, start: start, last: [last, @finish].min, target_type: @target_type
            INSERT IGNORE INTO `#{@target_edit_table_name}` (
              `id`,
              `#{@target_type.dasherize.underscore}_id`,
              `edited_at`,
              `editor_id`,
              `created_at`,
              `updated_at`,
              `performed_by_integration_id`,
              `deleted_at`,
              `deleted_by_id`,
              `diff`,
              `user_content_edit_id`
            ) SELECT
              `id`,
              `user_content_id`,
              `edited_at`,
              `editor_id`,
              `created_at`,
              `updated_at`,
              `performed_by_integration_id`,
              `deleted_at`,
              `deleted_by_id`,
              `diff`,
              `id`
            FROM `user_content_edits`
            WHERE
              `user_content_type` = :target_type AND `id` BETWEEN :start AND :last
            ORDER BY `id` ASC
            /* cross-schema-domain-query-exempted */
          SQL

          query.results
        end
      end

      # Returns nothing.
      def perform
        next_id = @start
        batch_count = 0

        loop do
          break if next_id > @finish

          if verbose && batch_count % 50 == 0
            log "Reached UserContentEdit##{next_id}"
          end

          UserContentEdit.throttle_with_retry(max_retry_count: 8) do
            run_query start: next_id, last: (next_id + batch_size - 1)
          end

          batch_count += 1
          next_id += batch_size
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "verify", "Verify that backfilled data is correct"
    on "type=", "UserContenteEdit type to backfill", required: true
    on "batch-size=", "Batch size", default: GitHub::Transitions::BackfillRepositoryEdits::BATCH_SIZE, as: Integer
  end
  options = slop.to_hash

  raise "Can't specify both `--verify` and `--write`" if options[:write] && options[:verify]

  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillRepositoryEdits.new(options)
  transition.run
end
