# rubocop:disable Style/FrozenStringLiteralComment

require "github/transitions/enterprise_310"

module GitHub
  module Transitions
    class Enterprise320

      @has_been_run = false

      def self.has_been_run?
        @has_been_run
      end

      def self.perform
        transition = new.perform
        @has_been_run = true
        transition
      end

      def perform
        return if Rails.test? # test environments are current

        if Enterprise310.has_been_run?
          create_pull_requests_index
        else
          remove_indices

          set_number_of_replicas_to_zero
          enable_code_search

          create_code_search_index
          create_issues_index
          create_pull_requests_index
        end

        self
      end

    private

      def remove_indices
        client.cluster.indices.each_key do |name|
          next unless name =~ /^(code-search|issues|pull-requests)/
          client.index(name).delete
        end
      end

      def set_number_of_replicas_to_zero
        client.put "/_settings", body: '{"index" : {"number_of_replicas" : 0}}'
      end

      def enable_code_search
        cluster = ::Search::ClusterStatus.new
        cluster.enable_code_search
        cluster.enable_code_search_indexing
      end

      def create_code_search_index
        index = Elastomer::Indexes::CodeSearch.new "code-search"
        index.create

        job = GitHub::Jobs::RepairCodeSearchIndex.new(index.name)
        job.reset! if job.exists?
        job.enable
        job.requeue
      end

      def create_issues_index
        index = Elastomer::Indexes::Issues.new "issues"
        index.create
        index.add_alias "issues-search"

        job = GitHub::Jobs::RepairIssuesIndex.new(index.name)
        job.reset! if job.exists?
        job.enable
        job.requeue
      end

      def create_pull_requests_index
        index = Elastomer::Indexes::PullRequests.new "pull-requests"
        index.create
        index.add_alias "issues-search"

        job = GitHub::Jobs::RepairPullRequestsIndex.new(index.name)
        job.reset! if job.exists?
        job.enable
        job.requeue
      end

      def client
        Elastomer.client
      end

    end  # Enterprise320
  end  # Transitions
end  # GitHub
