# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191210220630_convert_public_ghes_repos_to_internal.rb -v | tee -a /tmp/convert_public_ghes_repos_to_internal.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191210220630_convert_public_ghes_repos_to_internal.rb -v -w | tee -a /tmp/convert_public_ghes_repos_to_internal.log
#
module GitHub
  module Transitions
    class ConvertPublicGhesReposToInternal < Transition
      COMPLETION_KEY = "ghes_public_to_internal_migration_complete"
      NETWORK_COMPLETION_KEY_PREFIX = "ghes_public_to_internal_network_migrated_"

      def after_initialize
        @sql = ApplicationRecord::Domain::Repositories.github_sql
      end

      # Returns nothing.
      def perform
        if !GitHub.enterprise?
          log "Exiting: not GHES"
          return
        end
        if !GitHub.private_mode_enabled?
          log "Exiting: not in private mode"
          return
        end
        if GitHub.global_business.nil?
          log "Exiting: global business not yet created"
          return
        end

        log "Migrating org-owned repositories."
        # Get all repo networks having a public, org-owned root repo.
        org_repos = sql.new <<-SQL
          SELECT rn.id AS network_id, r.id AS root_repo_id
          FROM repository_networks rn
            INNER JOIN repositories r ON rn.root_id = r.id
          WHERE r.public = true
            AND r.organization_id IS NOT NULL
        SQL
        migrate_network(org_repos.results, set_root_internal: true)

        log "Migrating user-owned repositories."
        # Get all repo networks having a public, user-owned root repo.
        user_repos = sql.new <<-SQL
          SELECT rn.id AS network_id, r.id AS root_repo_id
          FROM repository_networks rn
            INNER JOIN repositories r ON rn.root_id = r.id
          WHERE r.public = true
            AND r.organization_id IS NULL
        SQL
        migrate_network(user_repos.results, set_root_internal: false)

        update_default_repo_visibility
        disable_public_repo_creation_by_policy
        mark_migration_complete
      end

      def migration_marked_complete?
        GitHub.kv.exists(COMPLETION_KEY).value!
      end

      def network_migrated?(network_id)
        GitHub.kv.exists("#{NETWORK_COMPLETION_KEY_PREFIX}#{network_id}").value!
      end

      private

      attr_reader :sql
      attr_reader :tx_now

      def migrate_network(rows, set_root_internal:)
        anonymous_git_access_enabled = GitHub.anonymous_git_access_available? && GitHub.anonymous_git_access_enabled?

        ActiveRecord::Base.connected_to(role: :writing) do
          @tx_now = GitHub::SQL::NOW

          rows.each do |network_id, root_repo_id|
            sql.transaction do
              set_network_private(network_id)
              set_root_repo_internal(network_id, root_repo_id) if set_root_internal
              fix_up_newly_private_repo_access(network_id)
              disable_anonymous_git_access(root_repo_id) if anonymous_git_access_enabled
              reindex_repos_for_search(network_id)
              reindex_repo_owners_for_search(network_id)
              mark_network_migrated(network_id)

              log "Network #{network_id} successfully migrated."
            end
          end
        end
      end

      def update_default_repo_visibility
        return unless GitHub.default_repo_visibility == "public"
        log "Setting default new repo visibility to internal."
        return if dry_run?
        ActiveRecord::Base.connected_to(role: :writing) do
          GitHub.set_default_repo_visibility("internal", User.ghost)
        end
      end

      def disable_public_repo_creation_by_policy
        log "Disabling public repo creation by policy."
        return if dry_run?
        GitHub.global_business.allow_members_can_create_repositories_with_visibilities(
          force: true,
          actor: User.ghost,
          public_visibility: false,
          private_visibility: true,
          internal_visibility: true,
        )
      end

      def set_network_private(network_id)
        log "Setting network #{network_id} to private."
        return if dry_run?
        sql.run(<<-SQL, { network_id: network_id })
          UPDATE repositories
          SET public = false, public_fork_count = 0
          WHERE source_id = :network_id
        SQL
      end

      def set_root_repo_internal(network_id, root_repo_id)
        log "Setting network #{network_id} root repo (id:#{root_repo_id}) to internal."
        return if dry_run?
        sql.run(<<-SQL, { root_repo_id: root_repo_id, biz_id: GitHub.global_business.id, tx_now: tx_now })
          INSERT INTO internal_repositories
            (repository_id, business_id, created_at, updated_at)
          VALUES
            (:root_repo_id, :biz_id, :tx_now, :tx_now)
        SQL
      end

      def fix_up_newly_private_repo_access(network_id)
        repos = sql.new(<<-SQL, network_id: network_id)
          SELECT r.id
          FROM repositories r
          WHERE r.source_id = :network_id
        SQL

        repos.values.each do |repo_id|
          repo = Repository.find(repo_id)
          next unless repo

          if repo.parent && !repo.parent.internal?
            log "Granting fork owner #{repo.owner.login} read access to newly private parent #{repo.parent.nwo}."
            repo.parent.add_member_without_validation_or_notifications(repo.owner, User.ghost, action: :read) unless dry_run?
          end

          unless repo.internal?
            log "Removing watchers and stargazers without access from newly private #{repo.nwo}."
            repo.correct_watchers unless dry_run?
            repo.correct_stargazers unless dry_run?
          end
        end
      end

      def disable_anonymous_git_access(root_repo_id)
        # Anonymous git access is set per-network by the root repo, so it only needs to be disabled there.
        root_repo = Repository.find(root_repo_id)
        return unless root_repo.anonymous_git_access_enabled?
        log "Disabling anonymous git access for network #{root_repo.source_id} root repo (id:#{root_repo_id})."
        return if dry_run?
        root_repo.unlock_anonymous_git_access(User.ghost)
        root_repo.disable_anonymous_git_access(User.ghost)
      end

      def reindex_repos_for_search(network_id)
        network_repo_ids = sql.new <<-SQL, network_id: network_id
          SELECT id
          FROM repositories
          WHERE source_id = :network_id
        SQL

        network_repo_ids.values.each do |id|
          log "Reindexing for search: network #{network_id}, repo #{id}."
          unless dry_run?
            # Update all repo components for public->private visibility change
            # in search indexes.
            Search.add_to_search_index("repository", id)
            Search.add_to_search_index("code", id, "purge" => true)
            Search.add_to_search_index("commit", id, "purge" => true)
            Search.add_to_search_index("wiki", id, "purge" => true)
            Search.add_to_search_index("bulk_issues", id, "purge" => true)
            Search.add_to_search_index("bulk_pull_requests", id, "purge" => true)
            Search.add_to_search_index("bulk_projects", id, "purge" => true)
          end
        end
      end

      # Update owner public repo counts in search index, skipping "system
      # accounts" as defined in `User#searchable?` and `User#system_account?'.
      # Equivalent to calling `Repository#owner#synchronize_search_index`.
      def reindex_repo_owners_for_search(network_id)
        system_user_ids = sql.run <<-SQL
          SELECT id
          FROM users
          WHERE login IN ('ghost', 'github-enterprise')
        SQL

        # It would be more efficient to maintain a list of all affected users
        # and ensure we only reindex once per user. This approach will result in
        # reindexing some users multiple times, when users/orgs own repos in
        # multiple networks. But keeping it this way allows us to keep the
        # db-transaction-per-repo-network semantics that make the whole
        # transition more fault-resilient and re-runnable.
        repo_owner_ids = sql.new \
          network_id: network_id,
          system_user_ids: system_user_ids.values
        repo_owner_ids.add <<-SQL
          SELECT DISTINCT owner_id
          FROM repositories
          WHERE source_id = :network_id
            AND owner_id NOT IN :system_user_ids
        SQL
        repo_owner_ids.values.each do |owner_id|
          log "Reindexing for search: user #{owner_id}."
          Search.add_to_search_index("user", owner_id) unless dry_run?
        end
      end

      def mark_network_migrated(network_id)
        GitHub.kv.set("#{NETWORK_COMPLETION_KEY_PREFIX}#{network_id}", GitHub::SQL::NOW) unless dry_run?
      end

      def mark_migration_complete
        GitHub.kv.set(COMPLETION_KEY, GitHub::SQL::NOW) unless dry_run?
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ConvertPublicGhesReposToInternal.new(options)
  transition.run
end
