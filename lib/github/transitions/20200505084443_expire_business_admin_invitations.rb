# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200505084443_expire_business_admin_invitations.rb --verbose | tee -a /tmp/expire_business_admin_invitations.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200505084443_expire_business_admin_invitations.rb --verbose -w | tee -a /tmp/expire_business_admin_invitations.log
#
module GitHub
  module Transitions
    class ExpireBusinessAdminInvitations < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        expiry_date = GitHub.invitation_expiry_period.days.ago
        min_id = readonly { BusinessMemberInvitation.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM business_member_invitations") }
        max_id = readonly { BusinessMemberInvitation.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM business_member_invitations") }

        @iterator = BusinessMemberInvitation.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL, expiry_date: expiry_date
          SELECT id FROM business_member_invitations
          WHERE id BETWEEN :start AND :last
          AND accepted_at IS NULL
          AND cancelled_at IS NULL
          AND expired_at IS NULL
          AND created_at < :expiry_date
        SQL

        @rows_updated = 0
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end

        if dry_run?
          log "Would have expired #{@rows_updated} invitations in total." if verbose?
        else
          log "Expired #{@rows_updated} invitations in total." if verbose?
        end
      end

      def process(rows)
        rows = rows.flatten.compact # Turn rows into an Array of IDs
        ActiveRecord::Base.connected_to(role: :writing) do
          BusinessMemberInvitation.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            if dry_run?
              log "Would have expired invitations with IDs: #{rows}" if verbose?
            else
              ActiveRecord::Base.connected_to(role: :writing) do
                BusinessMemberInvitation.github_sql.run <<-SQL, ids: rows, now: GitHub::SQL::NOW
                  UPDATE business_member_invitations
                  SET expired_at = :now
                  WHERE id IN :ids
                SQL
              end

              log "Expired invitations with IDs: #{rows}" if verbose?
            end
          end
        end
        @rows_updated += rows.size
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ExpireBusinessAdminInvitations.new(options)
  transition.run
end
