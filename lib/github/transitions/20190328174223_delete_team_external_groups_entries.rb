# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190328174223_delete_team_external_groups_entries.rb -v | tee -a /tmp/delete_team_external_groups_entries.log
#
module GitHub
  module Transitions
    # An AR model representing the legacy state of this table used in tests
    class LegacyTeamExternalGroup < ApplicationRecord::Domain::Users
      self.table_name = :team_external_groups
    end

    class DeleteTeamExternalGroupsEntries < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM team_external_groups") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM team_external_groups") }
        log "Removing team_external_groups entries from #{min_id} to #{max_id}" if verbose?

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id FROM team_external_groups
          WHERE id BETWEEN :start AND :last
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        ids = rows.flatten
        ApplicationRecord::Domain::Users.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Removing #{ids.count} entries with ids between #{ids.min} - #{ids.max}" if verbose?
          destroy_team_external_groups(ids) unless dry_run?
        end
      end

      def destroy_team_external_groups(ids)
        ApplicationRecord::Domain::Users.github_sql.run(<<-SQL, ids: ids)
          DELETE FROM team_external_groups
          WHERE id IN :ids
        SQL
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DeleteTeamExternalGroupsEntries.new(options)
  transition.run
end
