# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190628155203_delete_invalid_user_and_team_abilities_for_protected_branches.rb -v | tee -a /tmp/delete_invalid_user_and_team_abilities_for_protected_branches.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190628155203_delete_invalid_user_and_team_abilities_for_protected_branches.rb -v -w | tee -a /tmp/delete_invalid_user_and_team_abilities_for_protected_branches.log
#
module GitHub
  module Transitions
    class DeleteInvalidUserAndTeamAbilitiesForProtectedBranches < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        @min_id = readonly {
          Ability.github_sql.value <<-SQL
            SELECT COALESCE(MIN(id), 0) FROM abilities
            /* abilities-select-no-priority-needed */
          SQL
        }
        @max_id = readonly {
          Ability.github_sql.value <<-SQL
            SELECT COALESCE(MAX(id), 0) FROM abilities
            /* abilities-select-no-priority-needed */
          SQL
        }
        @iterator = Ability.github_sql_batched_between(start: @min_id, finish: @max_id, batch_size: BATCH_SIZE)
        @iterator.add(<<-SQL)
          SELECT
            a.id, a.actor_type, a.actor_id, a.subject_id
          FROM abilities a
          WHERE
            a.id BETWEEN :start AND :last
            AND a.actor_type IN ('User', 'Team')
            AND a.subject_type = 'ProtectedBranch'
          /* abilities-select-no-priority-needed */
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      private

      # Delete all protected branch abilities for users and teams that no
      # longer have write+ access to the associated repository (through the
      # protected branch)
      def process(rows)
        rows.each_slice(BATCH_SIZE) do |slice|
          abilities_to_delete = []
          protected_branch_ids = Set.new
          actor_ids_by_type = Hash.new { |hash, key| hash[key] = Set.new }

          slice.each do |(ability_id, actor_type, actor_id, protected_branch_id)|
            protected_branch_ids << protected_branch_id
            actor_ids_by_type[actor_type] << actor_id
          end

          protected_branch_with_repository_by_id = ActiveRecord::Base.connected_to(role: :reading) do
            ProtectedBranch.where(id: protected_branch_ids.to_a).preload(:repository).index_by(&:id)
          end

          actor_by_type_and_id = {}
          actor_ids_by_type.each do |actor_type, actor_ids|
            actor_by_type_and_id[actor_type] = ActiveRecord::Base.connected_to(role: :reading) do
              actor_type.constantize.where(id: actor_ids.to_a).index_by(&:id)
            end
          end

          slice.each do |(ability_id, actor_type, actor_id, protected_branch_id)|
            actor = actor_by_type_and_id[actor_type][actor_id]
            repository = protected_branch_with_repository_by_id[protected_branch_id].try(:repository)
            # Silently ignore orphaned protected branch abilities so the
            # migration doesn't bail on GHE if there's bad data
            next unless repository

            unless repository.writable_by?(actor, include_child_teams: true)
              log "Deleting protected branch Ability: #{ability_id} for #{actor_type}: #{actor_id}" if verbose?
              abilities_to_delete << ability_id
            end
          end

          if abilities_to_delete.any? && !dry_run?
            delete_abilities(abilities_to_delete)
          end
        end
      end

      def delete_abilities(ids)
        Ability.throttle do
          Ability.github_sql.run(<<~SQL, ids: ids)
            DELETE
            FROM abilities
            WHERE id IN :ids
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DeleteInvalidUserAndTeamAbilitiesForProtectedBranches.new(options)
  transition.run
end
