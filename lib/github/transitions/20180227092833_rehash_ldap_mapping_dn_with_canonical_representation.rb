# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180227092833_rehash_ldap_mapping_dn_with_canonical_representation.rb -v | tee -a /tmp/rehash_ldap_mapping_dn_with_cannonical_representation.log
#
module GitHub
  module Transitions
    class RehashLdapMappingDnWithCanonicalRepresentation < Transition

      BATCH_SIZE = 100

      def after_initialize
        @total_rows = 0
        @rehashed_rows = 0
      end

      # Returns nothing.
      def perform
        raise "This transition is only supported in Enterprise" if !GitHub.enterprise?

        if verbose?
          dry_run? ? log("Will run in dry-run mode") : log("This will not run in dry-run mode! Changes will be committed")
        end

        LdapMapping.find_in_batches(batch_size: BATCH_SIZE) do |batch|
          batch.each do |mapping|
            process(mapping)
          end
        end

        if verbose?
          dry_run? ?
              log("Candidates to be updated: #{@rehashed_rows}/#{@total_rows} ldap mappings") :
              log("Updated #{@rehashed_rows}/#{@total_rows} ldap mappings")
        end
      end

      # Reads every LDAP mapping, calculates the hash from the DN, and in case there is a mismatch
      # due to a non-canonical hash, it's updated with the canonical value
      #
      # Returns nothing
      def process(mapping)
        log "Processing LDAP Mapping for #{mapping.dn} with hash #{mapping.dn_hash}" if verbose?
        @total_rows += 1
        new_dn_hash = LdapMapping.hash_dn(mapping.dn)

        if new_dn_hash != mapping.dn_hash
          log "Non-canonical DN found, will change hash #{mapping.dn_hash} to #{new_dn_hash}" if verbose?
          @rehashed_rows += 1
          mapping.update_attribute(:dn_hash, new_dn_hash) unless dry_run?
          log "Hash #{mapping.dn_hash} updated to #{new_dn_hash}" if verbose?
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RehashLdapMappingDnWithCanonicalRepresentation.new(options)
  transition.run
end
