# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20170912040652_backfill_configuration_entries_allow_private_repository_forking.rb -v | tee -a /tmp/backfill_configuration_entries_allow_private_repository_forking.log
#
#   Use the --cutoff_date= argument to specify the latest
#   Organization#created_at date to backfill configuration entries for.
#
module GitHub
  module Transitions
    class BackfillConfigurationEntriesAllowPrivateRepositoryForking < Transition
      SELECT_BATCH_SIZE = 1000
      INSERT_BATCH_SIZE = 100
      CONFIGURATION_NAME = "allow_private_repository_forking"
      CONFIGURATION_ENTRIES_COLUMNS = [
        :target_id,
        :target_type,
        :updater_id,
        :name,
        :value,
        :final,
        :created_at,
        :updated_at,
      ]
      CONFIG_ENTRIES_COLUMNS_LITERALS = CONFIGURATION_ENTRIES_COLUMNS.map { |c| GitHub::SQL::LITERAL(c) }

      attr_reader :cutoff_date, :backfill_count

      def after_initialize
        @cutoff_date = Time.parse(@other_args[:cutoff_date] || Time.zone.now.to_s)
        @backfill_count = 0
      end

      # Returns nothing.
      def perform
        readonly do
          log "Using SELECT batch size configuration of #{SELECT_BATCH_SIZE}"
          log "Using cutoff date configuration of #{cutoff_date}"

          all_org_ids do |org_ids|
            org_ids_without_existing_config_entries(org_ids).each_slice(INSERT_BATCH_SIZE) do |ids|
              write_configuration_entries(ids)
              @backfill_count += ids.size
            end

            log "Backfilled batch: #{backfill_count} configuration entries" if verbose?
          end
          log "Total: #{backfill_count} configuration entries backfilled" if verbose?
        end
      end

      private

      # All organization IDs created before cutoff_date
      #
      # When a block is given, yields organization IDs in batches of
      # SELECT_BATCH_SIZE as a flattened list.
      #
      # When no block is given, returns GitHub::SQL::Batched#batches.
      def all_org_ids
        iterator = ApplicationRecord::Domain::Users.github_sql_batched(limit: SELECT_BATCH_SIZE)
        iterator.add <<-SQL, cutoff_date: cutoff_date
          SELECT u.id FROM users u
          WHERE u.type = "Organization"
          AND u.created_at < :cutoff_date
          AND u.id > :last
          ORDER BY u.id ASC
          LIMIT :limit
        SQL

        return iterator.batches unless block_given?

        iterator.batches.each do |batch|
          yield batch.flatten
        end
      end

      def org_ids_without_existing_config_entries(org_ids)
        org_ids - org_ids_with_existing_configuration_entry(org_ids)
      end

      # All organization IDs within a given list that have an existing
      # configuration entry for CONFIGURATION_NAME.
      #
      # Returns a list of organization IDs.
      def org_ids_with_existing_configuration_entry(org_ids)
        sql = Configuration::Entry.github_sql.new
        sql.add <<-SQL, config_name: CONFIGURATION_NAME, org_ids: org_ids
          SELECT c.target_id FROM configuration_entries c
          WHERE c.name = :config_name
          AND c.target_id IN :org_ids
        SQL
        sql.results.flatten
      end

      # The configuration_entries values for a list of IDs
      #
      # Returns an Array of GitHub::SQL::ROWS.
      def config_entries(ids)
        config_entries = GitHub::SQL::ROWS(ids.map { |org_id|
          [
            org_id,
            "User",
            10137, # User.ghost.id (hardcoded to allow transition to run in test/development)
            CONFIGURATION_NAME,
            "true",
            0, # *Not* final!
            Time.zone.now,
            Time.zone.now,
          ]
        })
      end

      # Writes (throttled) configuration_entries rows for the given IDs
      #
      # Returns nothing.
      def write_configuration_entries(ids)
        sql = Configuration::Entry.github_sql.new
        sql.add <<-SQL, config_entries: config_entries(ids), config_entries_columns: CONFIG_ENTRIES_COLUMNS_LITERALS
          INSERT INTO configuration_entries :config_entries_columns
          VALUES :config_entries
        SQL

        if !dry_run
          ActiveRecord::Base.connected_to(role: :writing) do
            Configuration::Entry.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              sql.run
            end
          end
        end
        sql
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "cutoff_date=", "All organizations created *before* this date will be backfilled.", default: Time.zone.now.to_s
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillConfigurationEntriesAllowPrivateRepositoryForking.new(options)
  transition.run
end
