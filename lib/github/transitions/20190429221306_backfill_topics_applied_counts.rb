# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190429221306_backfill_topics_applied_counts.rb -v | tee -a /tmp/backfill_topics_applied_counts.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190429221306_backfill_topics_applied_counts.rb -v -w | tee -a /tmp/backfill_topics_applied_counts.log
#
module GitHub
  module Transitions
    class BackfillTopicsAppliedCounts < Transition

      BATCH_SIZE = 1_000

      def perform
        Topic.ids_in_batches(batch_size: BATCH_SIZE) do |ids|
          log "Updating applied counts for topics rows #{ids.first}-#{ids.last}" if verbose?
          Topic.update_applied_counts(ids) unless dry_run?
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillTopicsAppliedCounts.new(options)
  transition.run
end
