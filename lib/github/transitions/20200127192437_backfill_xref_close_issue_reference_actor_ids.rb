# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200127192437_backfill_xref_close_issue_reference_actor_ids.rb -v | tee -a /tmp/backfill_xref_close_issue_reference_actor_ids.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200127192437_backfill_xref_close_issue_reference_actor_ids.rb -v -w | tee -a /tmp/backfill_xref_close_issue_reference_actor_ids.log
#
module GitHub
  module Transitions
    class BackfillXrefCloseIssueReferenceActorIds < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      MY_TABLE = "close_issue_references"

      attr_reader :iterator

      def after_initialize
        min_id = readonly { CloseIssueReference.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM #{MY_TABLE}") }
        max_id = readonly { CloseIssueReference.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM #{MY_TABLE}") }

        @iterator = CloseIssueReference.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id FROM #{MY_TABLE}
          WHERE id BETWEEN :start AND :last
          AND source = 0
          AND actor_id IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        return if rows.empty?

        log "Backfill actor ids of #{rows.size} xref close issue references." if verbose?
        return if dry_run?

        ApplicationRecord::Collab.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          run_batch_update_actor_ids rows
        end
      end

      def run_batch_update_actor_ids(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          CloseIssueReference.github_sql.run <<-SQL, ids: rows.flatten
            UPDATE #{MY_TABLE} SET actor_id = pull_request_author_id
            WHERE id IN :ids
          SQL
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillXrefCloseIssueReferenceActorIds.new(options)
  transition.run
end
