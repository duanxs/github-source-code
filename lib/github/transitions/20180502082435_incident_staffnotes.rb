# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180502082435_incident_staffnotes.rb -v | tee -a /tmp/incident_staffnotes.log
#
module GitHub
  module Transitions
    class IncidentStaffnotes < Transition

      attr_reader :actor, :users

      def after_initialize
        @actor = readonly { User.where(login: @other_args[:actor]).first }
        raise StandardError.new("Actor does not exist") unless @actor

        csv = CSV.parse(File.read(@other_args[:csv_path]), headers: true, header_converters: :symbol)
        hashes = csv.map(&:to_hash)
        @users = hashes.map do |hash|
          readonly { User.where(id: hash[:user_id]).first }
        end.compact
      end

      # Returns nothing.
      def perform
        @users.each do |user|
          user.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "writing staffnote to #{user.login} (#{user.id})" if verbose?
            user.staff_notes.create(user: @actor, note: @other_args[:note]) unless dry_run?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "actor=", "Author of staffnote", required: true
    on "csv_path=", "Path to csv file contain user ids", required: true
    on "note=", "Contents of staffnote", required: true
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::IncidentStaffnotes.new(options)
  transition.run
end
