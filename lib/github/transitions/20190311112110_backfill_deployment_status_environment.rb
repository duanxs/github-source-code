# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190311112110_backfill_deployment_status_environment.rb -v | tee -a /tmp/backfill_deployment_status_environment.log
#
module GitHub
  module Transitions
    class BackfillDeploymentStatusEnvironment < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        # The most common, and preferred, approach is to define your iterator
        # here using `BatchedBetween`. Prefer using a min_id with BatchedBetween
        # queries. Not doing so can lead to _very_ slow transition tests (https://github.com/github/github/pull/92565)
        min_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM deployment_statuses") }
        max_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM deployment_statuses") }

        @iterator = ApplicationRecord::Domain::Repositories.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT deployment_statuses.id,
            deployments.environment AS deployment_environment,
            (
              SELECT previous.environment
              FROM deployment_statuses previous
              WHERE previous.deployment_id = deployment_statuses.deployment_id
                AND previous.id < deployment_statuses.id
                AND previous.environment IS NOT NULL
              ORDER BY previous.id DESC
              LIMIT 1
            ) AS previous_deployment_status_environment
          FROM deployment_statuses
          JOIN deployments ON deployment_statuses.deployment_id = deployments.id AND deployments.environment IS NOT NULL
          WHERE deployment_statuses.environment IS NULL
          AND deployment_statuses.id BETWEEN :start AND :last
        SQL
        #
        # If the above doesn't work for you, please check
        # https://githubber.com/article/technology/dotcom/transitions
        # for more information on other approaches.
      end

      # Returns nothing.
      def perform
        # A common approach is to only use the perform method to iterate through
        # your iterator, passing off the actual work to another method. Notice
        # that the actual iteration happens inside the `Readonly` model so
        # that we make sure we're hitting the read-only replicas and we can avoid
        # spiking replication lag.

        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end

        # For simpler read-only queries, use a readonly replica via:
        #
        # readonly do
        #   # ... your queries here
        # end
        #
        # For more information, please consult the documentation:
        # https://githubber.com/article/technology/dotcom/transitions
      end

      def process(rows)
        # If you're touching the database, please use the throttler to ensure
        # that this transition doesn't cause unnecessary replication delay:

        DeploymentStatus.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "setting environment of deployment status with ids #{rows.map { |item| item[0] }}" if verbose?
          set_deployment_status_environment rows unless dry_run?
        end

        # For more information, please consult the documentation:
        # https://githubber.com/article/technology/dotcom/transitions
      end

      def set_deployment_status_environment(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::Repositories.github_sql.new "UPDATE deployment_statuses SET environment = CASE"

          rows.each do |item|
            id = item[0]
            environment = item[2] || item[1]
            sql.add <<-SQL, id: id, environment: environment
              WHEN id = :id THEN :environment
            SQL
          end

          sql.add "END WHERE id IN :ids", ids: rows.map { |item| item[0] }
          sql.run
          nrows = sql.affected_rows

          if nrows > rows.size
            raise "There were more rows affected than expected"
          end

          nrows
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillDeploymentStatusEnvironment.new(options)
  transition.run
end
