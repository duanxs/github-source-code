# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180627184157_roll_all_page_certificates_for_new_key.rb -v | tee -a /tmp/roll_all_page_certificates_for_new_key.log
#
module GitHub
  module Transitions
    class RollAllPageCertificatesForNewKey < Transition

      # @github/database-transitions-code-review is your friend, and happy to help
      # code review transitions before they're run to make sure they're being nice
      # to our database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 50

      attr_reader :iterator

      # This is the key ID all SQL queries should be filtered by. If this key is present, skip the row.
      def desired_earthsmoke_key_version_id
        @desired_earthsmoke_key_version_id ||= GitHub.acme_cert_key.csr.key_version_id
      end

      def batch_size
        other_args[:batch_size] || BATCH_SIZE
      end

      def after_initialize
        filter_query = <<~SQL
          (
            earthsmoke_key_version_id != :desired_earthsmoke_key_version_id OR earthsmoke_key_version_id IS NULL
          )
        SQL
        filter_args = { desired_earthsmoke_key_version_id: desired_earthsmoke_key_version_id }

        min_id = readonly {
          ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM page_certificates WHERE #{filter_query}", filter_args)
        }
        max_id = readonly {
          ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM page_certificates WHERE #{filter_query}", filter_args)
        }
        @iterator = ApplicationRecord::Domain::Repositories.github_sql_batched_between(start: min_id, finish: max_id, batch_size: batch_size)
        @iterator.add <<-SQL, filter_args
          SELECT id FROM page_certificates
          WHERE id BETWEEN :start AND :last
          AND #{filter_query}
        SQL
      end

      # Returns nothing.
      def perform
        log "scheduling renewal for certificates which are NOT using earthsmoke key ID #{desired_earthsmoke_key_version_id.inspect}" if verbose?

        new_expires_at = Time.now
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows, new_expires_at)
          new_expires_at += 10.minutes
        end

        nil
      end

      def process(rows, new_expires_at)
        Page::Certificate.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          ids = rows.map(&:first)
          log "expiring certificates #{ids.join(", ")} at #{new_expires_at}" if verbose?
          Page::Certificate.where(id: ids).update_all(expires_at: new_expires_at) unless dry_run?
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RollAllPageCertificatesForNewKey.new(options)
  transition.run
end
