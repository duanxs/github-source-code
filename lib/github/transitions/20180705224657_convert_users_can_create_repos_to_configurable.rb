# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180705224657_convert_users_can_create_repos_to_configurable.rb -v | tee -a /tmp/convert_users_can_create_repos_to_configurable.log
#
module GitHub
  module Transitions
    class ConvertUsersCanCreateReposToConfigurable < Transition

      # @github/database-transitions-code-review is your friend, and happy to help
      # code review transitions before they're run to make sure they're being nice
      # to our database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_user_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM users") }
        max_user_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM users") }
        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_user_id, finish: max_user_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT users.id, users.raw_data
          FROM users
          WHERE users.type = "Organization"
            AND users.id between :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        total_rows = 0

        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
          total_rows += rows.size
        end

        write_configuration_entries unless @organization_ids.blank?

        log "Processed #{total_rows} total rows" if verbose?
      end

      def process(rows)
        return if rows.empty?

        # Get organization ids from the rows, each row is in the form [id, raw_data]
        organization_ids = rows.map(&:first)

        # Get organization ids to be excluded for organizations that have configuration entries
        exclude_organization_ids = readonly do
          Configuration::Entry.github_sql.results(<<-SQL, { organization_ids: organization_ids })
            SELECT entries.target_id
            FROM configuration_entries entries
            WHERE entries.target_id IN :organization_ids
              AND entries.target_type = "User"
              AND entries.name = "#{Configurable::MembersCanCreateRepositories::KEY}"
          SQL
        end.flatten

        organization_ids = organization_ids - exclude_organization_ids
        rows_hash = rows.to_h

        @organization_ids ||= []

        organization_ids.each do |organization_id|
          raw_data = rows_hash[organization_id]
          data = GitHub::ZSON.decode(raw_data)
          unless data["members_can_create_repositories"].to_s == "1"
            @organization_ids << organization_id
          end
        end

        return unless @organization_ids.length > BATCH_SIZE

        write_configuration_entries

        @organization_ids = []
      end

      def write_configuration_entries
        return if dry_run?

        log "Setting `can_create_repositories` to False for Organizations, ids: #{@organization_ids}" if verbose?

        Configuration::Entry.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          now = GitHub::SQL::NOW
          config_rows = GitHub::SQL::ROWS(
              @organization_ids.map do |organization_id|
                [
                    "User", # target_type
                    organization_id, # target_id
                    organization_id, # updater_id
                    Configurable::MembersCanCreateRepositories::KEY, # name
                    "true", # value
                    now, # created_at
                    now, # updated_at
                ]
              end,
          )

          Configuration::Entry.github_sql.run(<<-SQL, { rows: config_rows })
            INSERT INTO configuration_entries
              (target_type, target_id, updater_id, name, value, created_at, updated_at)
            VALUES
              :rows
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ConvertUsersCanCreateReposToConfigurable.new(options)
  transition.run
end
