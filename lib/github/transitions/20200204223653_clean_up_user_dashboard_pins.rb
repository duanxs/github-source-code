# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200204223653_clean_up_user_dashboard_pins.rb -v | tee -a /tmp/clean_up_user_dashboard_pins.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200204223653_clean_up_user_dashboard_pins.rb -v -w | tee -a /tmp/clean_up_user_dashboard_pins.log
#
module GitHub
  module Transitions
    class CleanUpUserDashboardPins < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        # The most common, and preferred, approach is to define your iterator
        # here using `BatchedBetween`. Prefer using a min_id with BatchedBetween
        # queries. Not doing so can lead to _very_ slow transition tests (https://github.com/github/github/pull/92565)
        min_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM user_dashboard_pins") }
        max_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM user_dashboard_pins") }

        @iterator = ApplicationRecord::Collab.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          -- id must go first for batched between to work properly
          SELECT pins.id
          FROM user_dashboard_pins pins
          WHERE pins.pinned_item_type = 0 -- 0 represents a 'Repository'
          AND pins.id BETWEEN :start AND :last
        SQL
        #
        # If the above doesn't work for you, please check
        # https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions
        # for more information on other approaches.
      end

      # Returns nothing.
      def perform
        # A common approach is to only use the perform method to iterate through
        # your iterator, passing off the actual work to another method. Notice
        # that the actual iteration happens inside the `Readonly` model so
        # that we make sure we're hitting the read-only replicas and we can avoid
        # spiking replication lag.
        #
        GitHub::SQL::Readonly.new(iterator.batches).each do |ids|
          destroy_stale_records(ids)
        end
      end

      private

      # Find stale user dashboard pins not destroyed in a repositoriy#destroy callback
      # and destroy them.
      def destroy_stale_records(ids)
        pins = readonly do
          ::UserDashboardPin.includes(:pinned_item).where(id: ids.flatten)
        end

        # destroy all pins with already deleted pinned_items (Repository)
        pins.each do |pin|
          next if pin.pinned_item.present?

          if dry_run?
            log "Would have destroyed user dashboard pin with id=#{pin.id}."
          else
            log "Destroying user dashboard pin with id=#{pin.id}."
            pin.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              pin.destroy!
            end
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CleanUpUserDashboardPins.new(options)
  transition.run
end
