# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190311185819_populate_oauth_applications_limit_for_top_users.rb -v | tee -a /tmp/populate_oauth_applications_limit_for_top_users.log
#
module GitHub
  module Transitions
    class PopulateOauthApplicationsLimitForTopUsers < Transition
      DEFAULT_MAX_APPLICATIONS_CREATION_LIMIT = 100
      BATCH_SIZE = 100
      WHITELISTED_MAX_APPLICATIONS_CREATION_LIMIT = 25_000

      # This limit is safe
      # https://github.com/github/ecosystem-apps/issues/348#issuecomment-469481760
      TOP_USERS_LIMIT = 100

      def after_initialize
        @top_created_apps_users = readonly do
          ApplicationRecord::Domain::Integrations.github_sql.new(<<~SQL, limit: TOP_USERS_LIMIT)
            SELECT user_id, count(*) AS app_count
            FROM oauth_applications
            GROUP BY user_id
            ORDER BY app_count DESC
            LIMIT :limit
          SQL
        end
      end

      def perform
        @top_created_apps_users.results.each_slice(BATCH_SIZE) do |batch|
          process(batch)
        end
      end

      def process(rows)
        rows.each do |item|
          apps_count = item.last
          break if apps_count < DEFAULT_MAX_APPLICATIONS_CREATION_LIMIT

          ApplicationRecord::Domain::KeyValues.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            user_id = item.first
            write_custom_applications_limit(user_id) unless dry_run?
          end
        end
      end

      def write_custom_applications_limit(user_id)
        key = "users.oauth_application.creation_limit.#{user_id}"
        GitHub.kv.set(key, WHITELISTED_MAX_APPLICATIONS_CREATION_LIMIT.to_s)
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::PopulateOauthApplicationsLimitForTopUsers.new(options)
  transition.run
end
