# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191014173401_backfill_owner_login.rb -v | tee -a /tmp/backfill_owner_login.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191014173401_backfill_owner_login.rb -v -w | tee -a /tmp/backfill_owner_login.log

module GitHub
  module Transitions
    class BackfillOwnerLogin < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM repositories") }
        max_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM repositories") }

        @iterator = ApplicationRecord::Domain::Repositories.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, owner_id FROM repositories
          WHERE id BETWEEN :start AND :last
          ORDER BY owner_id
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        Repository.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          repository_ids_by_owner_id = Hash[rows.group_by(&:last).map { |h, k| [h, k.map(&:first)] }]
          owner_logins_by_id = fetch_logins(repository_ids_by_owner_id.keys)

          ActiveRecord::Base.connected_to(role: :writing) do
            repository_ids_by_owner_id.each do |owner_id, repository_ids|
              login = owner_logins_by_id[owner_id]

              if login.nil?
                log "missing login for owner: #{owner_id}"
                next
              end

              query = "UPDATE repositories SET owner_login = :login WHERE repositories.id IN :ids"

              sql = ApplicationRecord::Domain::Repositories.github_sql.new(query, ids: repository_ids, login: login)
              sql.run unless dry_run?

              log "updated repositories: #{repository_ids.count}" if verbose?
            end
          end
        end
      end

      private

      # Exchange a set of user IDs for a Hash of id -> login
      def fetch_logins(owner_ids)
        ActiveRecord::Base.connected_to(role: :reading) do
          query = "SELECT id, login FROM users WHERE id IN :ids"
          sql = ApplicationRecord::Domain::Users.github_sql.new(query, ids: owner_ids)
          rows = sql.run.results

          rows.each.with_object({}) do |(owner_id, owner_login), memo|
            memo[owner_id] = owner_login
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillOwnerLogin.new(options)
  transition.run
end
