# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180518151225_relink_zuora_payment_methods.rb -v -f /path/to/input_file | tee -a /tmp/relink_zuora_payment_methods.log
#
# Note that an input file (`-f /path/to/input_file`) is required! This file is a list of logins on
# which the script will operate, ideally taken from the account file we send to Zuora.
module GitHub
  module Transitions
    class RelinkZuoraPaymentMethods < Transition
      areas_of_responsibility :gitcoin

      # MUST NOT be greater than 200 to satisfy ZOQL condition limits
      BATCH_SIZE = 100

      def after_initialize
        @input_file = other_args.fetch(:file) do
          raise "--file argument required"
        end
      end

      # Returns nothing.
      def perform
        logins.each_slice(BATCH_SIZE) do |batch|
          users = readonly do
            User.where(login: batch).includes(customer: :payment_method).index_by(&:login)
          end

          unless users.length == batch.length
            missing_users = batch.sort - users.keys.sort
            error "Could not find the following users:\n#{missing_users.join("\n")}"
            return
          end

          zuora_accounts = zuora_accounts_for_logins(batch).index_by { |record| record["Name"] }

          unless zuora_accounts.length == batch.length
            missing_accounts = batch.sort - zuora_accounts.keys.sort
            error "Could not find the following Zuora accounts:\n#{missing_accounts.join("\n")}"
            return
          end

          zuora_payment_methods = zuora_payment_methods_for_accounts(zuora_accounts.map { |_, record| record["Id"] }).each_with_object({}) do |record, memo|
            memo[record["AccountId"]] ||= []
            memo[record["AccountId"]] << record
          end

          users.each do |login, user|
            if user.payment_method.using_zuora_processor?
              warn "Payment method for user '#{login}' is already on Zuora; skipping"
              next
            end

            zuora_account = zuora_accounts[login]
            if zuora_account.nil?
              warn "Zuora account not found for user '#{login}'"
              next
            end

            if zuora_account["BusinessSegment__c"] != "Self-Serve"
              warn "Zuora account for user '#{login}' is in the #{zuora_account['BusinessSegment__c']} business segment; skipping"
              next
            end

            if user.braintree_customer_id != zuora_account["BraintreeCustomerId__c"]
              warn "Braintree customer ID '#{zuora_account["BraintreeCustomerId__c"]}' does not match expected '#{user.braintree_customer_id}' for user '#{login}'; skipping"
              next
            end

            process(
              user: user,
              zuora_account: zuora_account,
              payment_methods: zuora_payment_methods[zuora_account["Id"]],
            )
          end
        end
      end

      # Re-link a single user's payment method to Zuora
      #
      # user            - The User to operate on
      # zuora_account   - The Zuora Account information
      # payment_methods - The Zuora payment methods for this account
      #
      # Returns nothing
      def process(user:, zuora_account:, payment_methods:)
        matching_payment_method = payment_methods.detect do |zuora_payment_method|
          if user.payment_method.credit_card?
            zuora_payment_method["Type"] == "CreditCard" &&
              zuora_payment_method["CreditCardExpirationMonth"].to_i == user.payment_method.expiration_month &&
              zuora_payment_method["CreditCardExpirationYear"].to_i == user.payment_method.expiration_year &&
              zuora_payment_method["BankIdentificationNumber"].to_s == user.payment_method.bank_identification_number.to_s &&
              zuora_payment_method["CreditCardMaskNumber"].to_s.last(4) == user.payment_method.last_four
          elsif user.payment_method.paypal?
            zuora_payment_method["Type"] == "PayPal" &&
              zuora_payment_method["PaypalEmail"] == user.payment_method.paypal_email
          else
            false
          end
        end

        if matching_payment_method.nil?
          error "Could not find a Zuora payment method matching the saved payment method for user '#{user.login}'"
          return
        end

        if dry_run?
          log "Would have updated user '#{user.login}' to use Zuora payment method '#{matching_payment_method['Id']}'"
          return
        end

        user.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          updated = user.payment_method.update(
            payment_processor_type: PaymentMethod.zuora_processor_slug,
            payment_processor_customer_id: zuora_account["Id"],
            payment_token: matching_payment_method["Id"],
          )

          unless updated
            error "Unable to update payment method for user '#{user.login}'"
            return
          end
        end

        unless zuora_account["DefaultPaymentMethodId"] == matching_payment_method["Id"]
          result = GitHub.zuorest_client.update_account zuora_account["Id"],
            DefaultPaymentMethodId: matching_payment_method["Id"]

          unless result["Success"]
            error "Unable to update Zuora default payment method for user '#{user.login}': #{result.inspect}"
          end
        end
      end

      private

      # Print a warning message
      #
      # message - The message to print
      #
      # Returns nothing
      def warn(message)
        puts "WARNING: #{message}"
      end

      # Print an error message
      #
      # message - The message to print
      #
      # Returns nothing
      def error(message)
        puts "ERROR: #{message}"
      end

      # Returns the contents of the input file
      def file_contents
        @file_contents ||= File.read(@input_file)
      end

      # Returns an Array of logins from the input file
      def logins
        @logins ||= file_contents.lines.compact.map(&:strip)
      end

      # Fetch account information from Zuora for a given set of logins
      #
      # logins - Array of logins
      #
      # Returns an Array of Hashes of the following shape:
      #   Id                     - The Zuora Account ID
      #   Name                   - The Zuora Account name, which should match the login
      #   BraintreeCustomerId__c - The Braintree Customer ID stored in Zuora
      #   BusinessSegment__c     - The business segment stored in Zuora, which should be Self-Serve
      #   DefaultPaymentMethodId - The Zuora Payment Method ID of the account's
      #                            default payment method
      def zuora_accounts_for_logins(logins)
        query = "select Id, Name, BraintreeCustomerId__c, BusinessSegment__c, DefaultPaymentMethodId from Account where "
        query += logins.map { |login| "Name = '#{login}'" }.join(" or ")

        results = GitHub.zuorest_client.query_action(queryString: query)
        results.fetch("records")
      end

      # Fetch payment method information from Zuora for a given set of Zuora accounts
      #
      # account_ids - Array of Zuora Account IDs
      #
      # Returns an Array of Hashes of the following shape:
      #   Id                        - The Zuora PaymentMethod ID
      #   AccountId                 - The Zuora Account ID
      #   Type                      - The type of payment method, either CreditCard or PayPal
      #   CreditCardExpirationMonth - For credit cards, the expiration month
      #   CreditCardExpirationYear  - For credit cards, the expiration year
      #   BankIdentificationNumber  - For credit cards, the BIN (first 6 digits
      #                               of the primary account number)
      #   CreditCardMaskNumber      - For credit cards, a masked form of the
      #                               primary account number showing the last 4 digits
      #   PaypalEmail               - For PayPal account, the email associated with the account
      def zuora_payment_methods_for_accounts(account_ids)
        query = "select Id, AccountId, Type, CreditCardExpirationMonth, CreditCardExpirationYear, BankIdentificationNumber, CreditCardMaskNumber, PaypalEmail from PaymentMethod where "
        query += account_ids.map { |id| "AccountId = '#{id}'" }.join(" or ")

        results = GitHub.zuorest_client.query_action(queryString: query)
        results.fetch("records")
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "f", "file", "The input file containing logins to re-link"
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RelinkZuoraPaymentMethods.new(options)
  transition.run
end
