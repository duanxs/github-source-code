# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"
require "pp"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191001141702_tgummerer_spokes_fileserver_site_column.rb -v | tee -a /tmp/tgummerer_spokes_fileserver_site_column.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191001141702_tgummerer_spokes_fileserver_site_column.rb -v -w | tee -a /tmp/tgummerer_spokes_fileserver_site_column.log
#
module GitHub
  module Transitions
    class TgummererSpokesFileserverSiteColumn < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        # The most common, and preferred, approach is to define your iterator
        # here using `BatchedBetween`. Prefer using a min_id with BatchedBetween
        # queries. Not doing so can lead to _very_ slow transition tests (https://github.com/github/github/pull/92565)
        min_id = readonly { ApplicationRecord::Spokes.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM fileservers") }
        max_id = readonly { ApplicationRecord::Spokes.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM fileservers") }

        @iterator = ApplicationRecord::Spokes.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, datacenter FROM fileservers
          WHERE id BETWEEN :start AND :last
            AND site IS NULL
            AND datacenter IS NOT NULL
        SQL
      end

      # Returns nothing.
      def perform
        # A common approach is to only use the perform method to iterate through
        # your iterator, passing off the actual work to another method. Notice
        # that the actual iteration happens inside the `Readonly` model so
        # that we make sure we're hitting the read-only replicas and we can avoid
        # spiking replication lag.
        #
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        # If you're touching the database, please use the throttler to ensure
        # that this transition doesn't cause unnecessary replication delay:
        #
        # Use a row by row approach:
        #
        rows.each do |item|
          ApplicationRecord::Domain::Spokes.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "writing site column for #{item}" if verbose?
            update_site item unless dry_run?
          end
        end
      end

      def site_for_dc(dc)
        case dc
        when "cp1"
          "cp1-iad"
        when "ash1iad"
          "ash1-iad"
        when "sdc42"
          "sdc42-sea"
        when "va3iad"
          "va3-iad"
        when "ac4iad"
          "ac4-iad"
        else
          dc
        end
      end

      def update_site(item)
        ActiveRecord::Base.connected_to(role: :writing) do
          id = item[0]
          site = site_for_dc(item[1])
          sql = ApplicationRecord::Spokes.github_sql.run(<<-SQL, {site: site, id: id})
              UPDATE fileservers
              SET site = :site
              WHERE id = :id
            SQL
          log "set site for #{sql.affected_rows} rows to #{site}" if verbose?
          sql.affected_rows
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::TgummererSpokesFileserverSiteColumn.new(options)
  transition.run
end
