# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20170120230520_move_to_topics.rb -v | tee -a /tmp/move_to_topics.log
#
module GitHub
  module Transitions

    # A temp AR class that points to the future home of topic information
    class FutureTopic < ApplicationRecord::Domain::Topics
      self.table_name = "topics"
    end

    # A temp AR class that points to the future home of repository_topic information
    class FutureRepositoryTopic < ApplicationRecord::Domain::Repositories
      self.table_name = "repository_topics"
    end

    class MoveToTopics < Transition
      def perform
        puts "This is just a dry run!" if dry_run
        transition_repository_topics
        transition_topics
      end

      def transition_topics
        Topic.find_each do |topic|
          puts "Copying #{topic.name}" if verbose

          topic.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            next if dry_run

            attrs = {
              name: topic.name,
              flagged: topic.flagged,
              created_at: topic.created_at,
              updated_at: topic.updated_at,
            }

            # I'm using a block after `create!` to manually override setting the id.
            FutureTopic.create!(attrs) { |t| t.id = topic.id }
          end
        end
      end

      def transition_repository_topics
        RepositoryTopic.find_each do |repository_topic|
          puts "Copying repository_topic for #{repository_topic.repository}" if verbose

          repository_topic.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            unless repository_topic.repository.admin_ids.first
              puts "Repository #{repository_topic.repository} has no admins. Creating RepositoryTopic object with ghost as the user."
            end

            next if dry_run

            FutureRepositoryTopic.create!(
              repository_id: repository_topic.repository_id,
              topic_id: repository_topic.hashtag_id,
              state: RepositoryTopic::STATE_MAPPINGS[repository_topic.state],
              user_id: repository_topic.repository.admin_ids.first || User.ghost.id,
              created_at: repository_topic.created_at,
              updated_at: repository_topic.updated_at,
            )
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::MoveToTopics.new(options)
  transition.run
end
