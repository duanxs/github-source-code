# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200623155909_reinstall_actions_on_organization_repositories.rb --verbose -f=/path/to/input/file | tee -a /tmp/reinstall_actions_on_organization_repositories.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200623155909_reinstall_actions_on_organization_repositories.rb --verbose -f=/path/to/input/file -w | tee -a /tmp/reinstall_actions_on_organization_repositories.log
#
module GitHub
  module Transitions
    class ReinstallActionsOnOrganizationRepositories < Transition
      areas_of_responsibility :actions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      # attr_reader :iterator
      def after_initialize
        @input_file = other_args.fetch(:file) do
          raise ArgumentError, "must provide a file name"
        end
      end

      # Returns nothing.
      def perform
        repos_affected = 0
        repo_ids.each_slice(BATCH_SIZE) do |batch|
          repos = readonly do
            Repository.where(id: batch)
          end

          repos_affected += process(repos)
        end

        message = dry_run? ? "Found #{repos_affected} repositories missing the Actions App" : "Reinstalled the Actions App on #{repos_affected} repositories"
        log message
      end

      def process(repos)
        num_installations = 0
        repos.each do |repo|
          # If the current repository has the Actions App installed already,
          # then we don't need to worry about reinstalling it.
          next if repo.actions_app_installed?

          unless dry_run?
            IntegrationInstallation.throttle do
              ActiveRecord::Base.connected_to(role: :writing) do
                repo.enable_actions_app
              end
            end
          else
            if verbose?
              log "Would have installed Actions on repository #{repo.nwo}"
            end
          end

          num_installations += 1
        end

        num_installations
      end

      # Returns the contents of the input file
      def file_contents
        @file_contents ||= File.read(@input_file)
      end

      # Return an Array of repository IDs from the input file
      def repo_ids
        @repo_ids ||= file_contents.lines.compact.map(&:strip)
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "f=", "file=", "The input file containing reopsitory IDs to reinstall Actions on"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ReinstallActionsOnOrganizationRepositories.new(options)
  transition.run
end
