# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200205210759_update_public_contributions_criterion_description.rb -v | tee -a /tmp/update_public_contributions_criterion_description.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200205210759_update_public_contributions_criterion_description.rb -v -w | tee -a /tmp/update_public_contributions_criterion_description.log
#
module GitHub
  module Transitions
    class UpdatePublicContributionsCriterionDescription < Transition
      SLUG = "public_contributions"
      NEW_DESCRIPTION = "Public contributions in last year"

      def perform
        criterion = SponsorsCriterion.find_by(slug: SLUG)

        if criterion
          log "Found criterion for #{SLUG}"

          if dry_run
            log "Would have updated description for #{SLUG}"
          else
            log "Updating description..."
            criterion.update!(description: NEW_DESCRIPTION)
            log "Description updated!"
          end
        else
          log "Couldn't find criterion with slug #{SLUG}"
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UpdatePublicContributionsCriterionDescription.new(options)
  transition.run
end
