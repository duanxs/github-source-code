# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190815050521_add_initial_sponsors_criteria.rb -v | tee -a /tmp/add_initial_sponsors_criteria.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190815050521_add_initial_sponsors_criteria.rb -v -w | tee -a /tmp/add_initial_sponsors_criteria.log
#
module GitHub
  module Transitions
    class AddInitialSponsorsCriteria < Transition

      CRITERIA = [
        {
          slug: "account_age",
          description: "Account is older than 6 months?",
          automated: true,
        },
        {
          slug: "suspension_status",
          description: "Suspended status",
          automated: true,
        },
        {
          slug: "spammy_status",
          description: "Spammy status",
          automated: true,
        },
        {
          slug: "blocked_threshold",
          description: "Blocked by < 15?",
          automated: true,
        },
        {
          slug: "abuse_reports",
          description: "No abuse reports?",
          automated: true,
        },
        {
          slug: "ofac_compliance",
          description: "OFAC Compliance",
          automated: true,
        },
        {
          slug: "objectionable_content",
          description: "A search for the user (Google, public website, Twitter) didn't result in any bigoted behavior",
        },
        {
          slug: "member_reputable_org",
          description: "The user is a member of an active or reputable GitHub organization",
        },
      ]

      # Returns nothing.
      def perform
        CRITERIA.each do |criterion_attrs|
          if dry_run?
            log "Would have created #{criterion_attrs[:slug]}"
          else
            create_sponsors_criterion(criterion_attrs)
          end
        end
      end

      def create_sponsors_criterion(attrs)
        SponsorsCriterion.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Creating #{attrs[:slug]}..."

          criterion = SponsorsCriterion.new(attrs)

          if criterion.save
            log "Created #{criterion.slug}."
          else
            log "Failed to create #{criterion.slug}: #{criterion.errors.full_messages.to_sentence}"
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddInitialSponsorsCriteria.new(options)
  transition.run
end
