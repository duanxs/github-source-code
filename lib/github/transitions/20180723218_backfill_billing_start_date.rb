# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

module GitHub
  module Transitions
    class BackfillBillingStartDate < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = 0
        max_id = readonly { ::Billing::PlanSubscription.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM plan_subscriptions") }

        @iterator = ::Billing::PlanSubscription.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT plan_subscriptions.id FROM plan_subscriptions
          LEFT JOIN users ON plan_subscriptions.user_id = users.id
          WHERE plan_subscriptions.id BETWEEN :start AND :last
          AND plan_subscriptions.billing_start_date IS NULL
          AND plan_subscriptions.zuora_subscription_id IS NOT NULL
          AND users.billing_type = 'invoice'
        SQL
      end

      def perform
        current_batch = 1
        total_rows = 0

        not_found = []

        @iterator.batches.each do |rows|
          log "Batch #{current_batch}: #{rows.length} rows, #{total_rows} so far"
          rows.each do |row|
            plan = ::Billing::PlanSubscription.find_by_id(row[0])
            subscription = plan.zuora_subscription
            billing_start_date = subscription.raw_subscription[:termStartDate]
            if billing_start_date.nil?
              not_found << row[0]
            else
              billing_start_date = Date.parse(billing_start_date)
              plan.billing_start_date = billing_start_date
              if dry_run
                log "Would update plan #{row[0]}'s billing_start_date to #{billing_start_date}"
              else
                log "Updating plan #{row[0]}'s billing_start_date to #{billing_start_date}"
                ::Billing::PlanSubscription.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) {
                  plan.save!
                }
              end
            end
          end
          current_batch += 1
          total_rows += rows.length
        end
        log "No termStartDate found for #{not_found.length} plans:"
        not_found.each do |p|
          log p
        end
      end
    end
  end
end

if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillBillingStartDate.new(options)
  transition.run
end
