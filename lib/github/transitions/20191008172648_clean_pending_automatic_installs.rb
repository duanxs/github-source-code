# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191008172648_clean_pending_automatic_installs.rb -v | tee -a /tmp/clean_pending_automatic_installs.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191008172648_clean_pending_automatic_installs.rb -v -w | tee -a /tmp/clean_pending_automatic_installs.log
#
module GitHub
  module Transitions
    class CleanPendingAutomaticInstalls < Transition
      BATCH_SIZE = 1000

      attr_reader :iterator

      def after_initialize
        @iterator = PendingAutomaticInstallation.installed.find_in_batches(batch_size: BATCH_SIZE)
      end

      # Returns nothing.
      def perform
        iterator.each do |completed_installs|
          log "Deleting '#{completed_installs.count}' completed installs" if verbose?

          next if dry_run

          delete_ids(completed_installs.pluck(:id))
        end
      end

      def delete_ids(ids)
        PendingAutomaticInstallation.throttle do
          PendingAutomaticInstallation.installed.where(id: ids).destroy_all
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CleanPendingAutomaticInstalls.new(options)
  transition.run
end
