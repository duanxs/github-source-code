# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180430211552_backfill_project_card_priority.rb -v | tee -a /tmp/backfill_project_card_priority.log
#
module GitHub
  module Transitions
    class BackfillProjectCardPriority < Transition
      BATCH_SIZE = 100

      attr_reader :cards_iterator
      attr_reader :archived_cards_iterator

      def after_initialize
        @processed_count = 0

        @cards_iterator = build_iterator(
          columns_table_name: "project_columns",
          cards_table_name: "project_cards",
        )

        @archived_cards_iterator = build_iterator(
          columns_table_name: "archived_project_columns",
          cards_table_name: "archived_project_cards",
        )
      end

      # Returns nothing.
      def perform
        if cards_iterator
          GitHub::SQL::Readonly.new(cards_iterator.batches).each do |rows|
            process(rows, column_class: ProjectColumn)
          end
        end

        if archived_cards_iterator
          GitHub::SQL::Readonly.new(archived_cards_iterator.batches).each do |rows|
            process(rows, column_class: Archived::ProjectColumn)
          end
        end

        log "Backfilled priority for #{@processed_count} #{"project column".pluralize(@processed_count)}."
      end

      def process(rows, column_class:)
        ActiveRecord::Base.connected_to(role: :reading) do
          columns = column_class.where(id: rows.map(&:first))

          columns.each do |column|
            log "Backfilling cards for #{column_class} #{column.id}"

            cards = column.cards.where(priority: nil, archived_at: nil).order("id DESC")
            cards.each do |card|
              log "Backfilling priority for #{card.class} #{card.id}"

              unless dry_run?
                column.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
                  ActiveRecord::Base.connected_to(role: :writing) do
                    column.prioritize_dependent!(card, after: nil, position: :top)
                  end
                end
              end
            end

            @processed_count += 1
          end
        end
      end

      def build_iterator(columns_table_name:, cards_table_name:)
        # Substantially streamlines our query to only affected columns
        min_id = readonly do
          Project.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            ApplicationRecord::Domain::Projects.github_sql.value <<-SQL, cards_table_name: GitHub::SQL::LITERAL(cards_table_name)
              SELECT column_id
              FROM :cards_table_name
              WHERE
                archived_at IS NULL
                AND priority IS NULL
                AND column_id IS NOT NULL
              ORDER BY column_id ASC
              LIMIT 1
            SQL
          end
        end

        # Bail out if we didn't find a good min_id
        return if min_id.nil?

        max_id = readonly do
          ApplicationRecord::Domain::Projects.github_sql.value <<-SQL, columns_table_name: GitHub::SQL::LITERAL(columns_table_name)
            SELECT IFNULL(MAX(id), 0)
            FROM :columns_table_name
          SQL
        end

        iterator = ApplicationRecord::Domain::Projects.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        iterator.add <<-SQL, columns_table_name: GitHub::SQL::LITERAL(columns_table_name), cards_table_name: GitHub::SQL::LITERAL(cards_table_name)
          SELECT DISTINCT :columns_table_name.id
          FROM :columns_table_name
          INNER JOIN :cards_table_name ON :cards_table_name.column_id = :columns_table_name.id
          WHERE
            :columns_table_name.id BETWEEN :start AND :last
            AND :cards_table_name.archived_at IS NULL
            AND :cards_table_name.priority IS NULL
        SQL

        iterator
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillProjectCardPriority.new(options)
  transition.run
end
