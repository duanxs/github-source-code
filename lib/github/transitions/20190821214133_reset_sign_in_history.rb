# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190821214133_reset_sign_in_history.rb -v -ip_addresses=123.123.123.123,321.321.321.321| tee -a /tmp/reset_sign_in_history.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190821214133_reset_sign_in_history.rb -v -w -ip_addresses=123.123.123.123,321.321.321.321 | tee -a /tmp/reset_sign_in_history.log
#
module GitHub
  module Transitions
    class ResetSignInHistory < Transition

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      # Returns nothing.
      def perform
        log "Destroying sign in history for ip(s): #{other_args[:ip_addresses]}"
        AuthenticatedDevice.joins(:authentication_records).where(authentication_records: { ip_address: other_args[:ip_addresses]}).in_batches(of: BATCH_SIZE) do |group|
          AuthenticationRecord.throttle_with_retry(max_retry_count: 8) do
            delete_records(group, "authenticated_device")
          end
        end

        AuthenticationRecord.where(ip_address: other_args[:ip_addresses]).in_batches(of: BATCH_SIZE) do |group|
          AuthenticationRecord.throttle_with_retry(max_retry_count: 8) do
            delete_records(group, "account_security.authentication_records")
          end
        end
      end

      def delete_records(group, key)
        if group.empty?
          log "No #{key}s to destroy."
          return
        end

        if dry_run?
          log "Would have destroyed #{group.count} #{key}s"
          GitHub.dogstats.count(key, group.count, tags: ["action:destroy", "explanation:reset", "dry_run:true"])
        else
          deleted_records = ActiveRecord::Base.connected_to(role: :writing) do
            group.delete_all
          end
          log "Destroyed #{deleted_records} #{key}s"
          GitHub.dogstats.count(key, deleted_records, tags: ["action:destroy", "explanation:reset", "dry_run:false"])
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "i", "ip_addresses=", "Comma-separated list of IPs", as: Array, delimiter: ","
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ResetSignInHistory.new(options)
  transition.run
end
