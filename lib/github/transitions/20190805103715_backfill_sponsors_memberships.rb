# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190805103715_backfill_sponsors_memberships.rb -v | tee -a /tmp/backfill_sponsors_memberships.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190805103715_backfill_sponsors_memberships.rb -v -w | tee -a /tmp/backfill_sponsors_memberships.log
#
module GitHub
  module Transitions
    class BackfillSponsorsMemberships < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ::EarlyAccessMembership.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM early_access_memberships") }
        max_id = readonly { ::EarlyAccessMembership.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM early_access_memberships") }

        @iterator = ::EarlyAccessMembership.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL, feature_slug: "github_sponsors"
          SELECT id, feature_enabled, member_id, survey_id, created_at, updated_at FROM early_access_memberships
          WHERE id BETWEEN :start AND :last
          AND feature_slug = :feature_slug
        SQL

        @total_memberships = 0
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end

        log "total memberships affected: #{@total_memberships}" if verbose?
      end

      private

      def process(rows)
        rows.each do |membership|
          @total_memberships += 1

          SponsorsMembership.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            if verbose?
              log "-------------------------"
              log "processing EarlyAccessMembership: #{membership[0]}"
            end

            write_sponsors_membership_with membership unless dry_run?
          end
        end
      end

      def write_sponsors_membership_with(membership)
        log "writing to database (not a dry run!!)" if verbose?

        ActiveRecord::Base.connected_to(role: :writing) do
          params = {
            state:            membership[1], # 0 for submitted, 1 for accepted
            sponsorable_type: 0, # enum value used for User type
            sponsorable_id:   membership[2],
            survey_id:        membership[3],
            created_at:       membership[4],
            updated_at:       membership[5],
          }

          sql = ::SponsorsMembership.github_sql.run <<-SQL, params
            INSERT INTO sponsors_memberships
              (state, sponsorable_type, sponsorable_id, survey_id, created_at, updated_at)
            VALUES
              (:state, :sponsorable_type, :sponsorable_id, :survey_id, :created_at, :updated_at)
            ON DUPLICATE KEY UPDATE
              state = :state,
              created_at = :created_at,
              updated_at = :updated_at
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillSponsorsMemberships.new(options)
  transition.run
end
