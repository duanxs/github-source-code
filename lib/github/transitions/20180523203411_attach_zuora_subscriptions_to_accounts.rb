# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180523203411_attach_zuora_subscriptions_to_accounts.rb -v | tee -a /tmp/attach_zuora_subscriptions_to_accounts.log
#
module GitHub
  module Transitions
    class AttachZuoraSubscriptionsToAccounts < Transition

      # @github/database-transitions-code-review is your friend, and happy to help
      # code review transitions before they're run to make sure they're being nice
      # to our database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM plan_subscriptions") }
        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: 0, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT plan_subscriptions.id FROM plan_subscriptions
          LEFT JOIN customers ON (customers.id = plan_subscriptions.customer_id)
          WHERE plan_subscriptions.id BETWEEN :start AND :last
          AND customers.zuora_account_id IS NOT NULL
          AND (plan_subscriptions.zuora_subscription_id IS NULL OR plan_subscriptions.zuora_subscription_number IS NULL)
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |plan_subscription_ids|
          plan_subscriptions = ::Billing::PlanSubscription.find plan_subscription_ids
          ::Billing::PlanSubscription.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            plan_subscriptions.each do |plan_subscription|
              if plan_subscription.user.invoiced?
                log "skipping invoiced user: #{plan_subscription.user}" if verbose?
              else
                log "checking for active subscription on #{plan_subscription.user}" if verbose?
                attach_zuora_subscription(plan_subscription) unless dry_run?
              end
            end
          end
        end
      end

      def attach_zuora_subscription(plan_subscription)
        if plan_subscription.attach_orphaned_zuora_subscription
          log "active subscription found for #{plan_subscription.user}" if verbose?
        else
          log "no subscription found for #{plan_subscription.user}" if verbose?
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AttachZuoraSubscriptionsToAccounts.new(options)
  transition.run
end
