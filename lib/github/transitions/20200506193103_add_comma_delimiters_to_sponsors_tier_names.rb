# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200506193103_add_comma_delimiters_to_sponsors_tier_names.rb --verbose | tee -a /tmp/add_comma_delimiters_to_sponsors_tier_names.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200506193103_add_comma_delimiters_to_sponsors_tier_names.rb --verbose -w | tee -a /tmp/add_comma_delimiters_to_sponsors_tier_names.log
#
module GitHub
  module Transitions
    class AddCommaDelimitersToSponsorsTierNames < Transition
      BATCH_SIZE = 100

      include ActionView::Helpers::NumberHelper

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_tiers") }
        max_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_tiers") }

        @iterator = ApplicationRecord::Domain::Sponsors.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, name, monthly_price_in_cents FROM sponsors_tiers
          WHERE id BETWEEN :start AND :last
          AND monthly_price_in_cents >= 100000
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |tier|
          id, name, monthly_price_in_cents = tier

          SponsorsTier.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "Renaming SponsorsTier #{id} from #{name} to #{new_name(monthly_price_in_cents: monthly_price_in_cents)}" if verbose?
            rename_tier(id: id, monthly_price_in_cents: monthly_price_in_cents) unless dry_run?
          end
        end
      end

      def rename_tier(id:, monthly_price_in_cents:)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::Sponsors.github_sql.run(
            "UPDATE sponsors_tiers SET name=:name WHERE id=:id",
            id: id,
            name: new_name(monthly_price_in_cents: monthly_price_in_cents),
          )
          sql.affected_rows
        end
      end

      def new_name(monthly_price_in_cents:)
        currency = number_to_currency(monthly_price_in_cents / 100, precision: 0)
        "#{currency} a month"
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddCommaDelimitersToSponsorsTierNames.new(options)
  transition.run
end
