# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190906142228_backfill_organization_labels.rb -v | tee -a /tmp/backfill_organization_labels.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190906142228_backfill_organization_labels.rb -v -w | tee -a /tmp/backfill_organization_labels.log
#
module GitHub
  module Transitions
    class BackfillOrganizationLabels < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        # The most common, and preferred, approach is to define your iterator
        # here using `BatchedBetween`. Prefer using a min_id with BatchedBetween
        # queries. Not doing so can lead to _very_ slow transition tests (https://github.com/github/github/pull/92565)
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM users") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM users") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id FROM users
          WHERE id BETWEEN :start AND :last
          AND type = "Organization"
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        UserLabel.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Creating user_labels for orgs #{rows.map { |item| item[0] }}" if verbose?
          run_batch_insert(rows) unless dry_run?
        end
      end

      def run_batch_insert(rows)
        org_ids = rows.map { |r| r[0] }
        # Find all the orgs in this batch that already have user_labels.
        # We don't want to create  new labels in this case
        org_ids_to_skip_query = ApplicationRecord::Collab.github_sql.run(<<~SQL, org_ids: org_ids)
          SELECT DISTINCT user_id
          FROM user_labels
          WHERE user_id IN :org_ids
        SQL
        org_ids_to_skip = org_ids_to_skip_query.values

        current_time = Time.now
        insert_rows = []
        (org_ids - org_ids_to_skip).map do |user_id|
          default_labels.each do |label_hash|
            insert_rows << [
              user_id,
              label_hash[:name],
              label_hash[:name],
              label_hash[:description],
              label_hash[:color],
              current_time,
              current_time,
            ]
          end
        end

        return if insert_rows.empty?

        ActiveRecord::Base.connected_to(role: :writing) do
          ApplicationRecord::Collab.github_sql.run(<<~SQL, rows: GitHub::SQL::ROWS(insert_rows))
            INSERT INTO user_labels
              (user_id, name, lowercase_name, description, color, created_at, updated_at)
            VALUES :rows
          SQL
        end
      end

      def default_labels
        # Duplicated from Label#initial_labels to not rely on model logic in transition
        [
          {
            name: "bug",
            color: "d73a4a",
            description: "Something isn't working",
          },
          {
            name: "documentation",
            color: "0075ca",
            description: "Improvements or additions to documentation",
          },
          {
            name: "duplicate",
            color: "cfd3d7",
            description: "This issue or pull request already exists",
          },
          {
            name: "enhancement",
            color: "a2eeef",
            description: "New feature or request",
          },
          {
            name: "help wanted",
            color: "008672",
            description: "Extra attention is needed",
          },
          {
            name: "good first issue",
            color: "7057ff",
            description: "Good for newcomers",
          },
          {
            name: "invalid",
            color: "e4e669",
            description: "This doesn't seem right",
          },
          {
            name: "question",
            color: "d876e3",
            description: "Further information is requested",
          },
          {
            name: "wontfix",
            color: "ffffff",
            description: "This will not be worked on",
          },
        ]
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillOrganizationLabels.new(options)
  transition.run
end
