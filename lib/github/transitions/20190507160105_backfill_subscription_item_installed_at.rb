# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190507160105_backfill_subscription_item_installed_at.rb -v | tee -a /tmp/backfill_subscription_item_installed_at.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190507160105_backfill_subscription_item_installed_at.rb -v -w | tee -a /tmp/backfill_subscription_item_installed_at.log
#
module GitHub
  module Transitions
    class BackfillSubscriptionItemInstalledAt < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        # nb: integration_installations.subscription_item_id is NOT indexed
        min_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM integration_installations") }
        max_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM integration_installations") }

        @iterator = ApplicationRecord::Domain::Integrations.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, created_at, subscription_item_id FROM integration_installations
          WHERE id BETWEEN :start AND :last
          AND subscription_item_id IS NOT NULL
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          log "processing integration_installation #{item[0]}" if verbose?
          write_somewhere_with item unless dry_run?
        end
      end

      def write_somewhere_with(item)
        ::Billing::SubscriptionItem.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          ActiveRecord::Base.connected_to(role: :writing) do
            id = item[0]
            created_at = item[1]
            subscription_item_id = item[2]

            sql = ::Billing::SubscriptionItem.github_sql.run(
              "UPDATE subscription_items SET installed_at = :created_at WHERE id = :subscription_item_id",
              created_at: created_at,
              subscription_item_id: subscription_item_id,
            )

            log "affected_rows for integration installation id #{id}: #{sql.affected_rows}" if verbose?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillSubscriptionItemInstalledAt.new(options)
  transition.run
end
