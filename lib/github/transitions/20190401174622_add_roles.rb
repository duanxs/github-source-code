# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190401174622_add_roles.rb -v | tee -a /tmp/add_roles.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190401174622_add_roles.rb -v -w | tee -a /tmp/add_roles.log
#
module GitHub
  module Transitions
    class AddRoles < Transition

      # Returns nothing.
      def perform
        create_roles
      end

      def create_roles
        unless dry_run?
          Role.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            Role.create(name: "triage")
            Role.create(name: "maintain")
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddRoles.new(options)
  transition.run
end
