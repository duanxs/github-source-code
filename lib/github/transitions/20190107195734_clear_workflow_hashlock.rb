# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190107195734_clear_workflow_hashlock.rb -v | tee -a /tmp/clear_workflow_hashlock.log
#
module GitHub
  module Transitions
    class ClearWorkflowHashlock < Transition

      # This clears the ProcessProjectWorkflows HashLock key, removing any
      # non-unique key that contains a `queued_at` string value.
      # Ref: github/github#104806
      def perform
        lock = "hashlock:GitHub::Jobs::ProcessProjectWorkflows"
        cursor = "0"
        deleted = 0
        loop do
          result = Resque.redis.hscan(lock, cursor, match: "*queued_at*", count: 1000)
          cursor = result.first
          result.last.map(&:first).each_slice(100) do |keys|
            deleted += Resque.redis.hdel(lock, keys)
          end
          break if cursor == "0"
        end
        log "cleared #{deleted} hash keys"
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ClearWorkflowHashlock.new(options)
  transition.run
end
