# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

module GitHub
  module Transitions
    class AcvContributorsNormalizeEmails < Transition
      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      def after_initialize
        # The most common, and preferred, approach is to define your iterator
        # here using `BatchedBetween`. Prefer using a min_id with BatchedBetween
        # queries. Not doing so can lead to _very_ slow transition tests (https://github.com/github/github/pull/92565)
        min_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM acv_contributors") }
        max_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM acv_contributors") }

        @iterator = ApplicationRecord::Collab.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          -- id must go first for batched between to work properly
          SELECT id, contributor_email FROM acv_contributors
          WHERE id BETWEEN :start AND :last
        SQL

        @updated_records = 0

        # The main RegExp for matching and fixing user emails
        @email_matcher = %r{
          # Start of string anchor
          \A
          # Possible leading control and/or spacing characters
          [\u0000-\u0020\u007F-\u009F\u00A0\u2000-\u200B\u2028\u2029\u202F\u205F\u3000\uFEFF\u2400-\u2436]*
          # Deal with some prefix oddities we missed during the initial sanitization pass
          (?:user\.email=|=)?
          # local part of an email address
          (?<local>[^@]+)
          # at
          @
          # domain part of an email address, including rough support for IPv6 and ports
          (?<domain>\[?[A-Za-z0-9:.-]+\]?(?::\d+)?)
          # Possible trailing control and/or spacing characters
          [\u0000-\u0020\u007F-\u009F\u00A0\u2000-\u200B\u2028\u2029\u202F\u205F\u3000\uFEFF\u2400-\u2436]*
          # End of string anchor
          \z
        }x
      end

      # Returns nothing.
      def perform
        # A common approach is to only use the perform method to iterate through
        # your iterator, passing off the actual work to another method. Notice
        # that the actual iteration happens inside the `Readonly` model so
        # that we make sure we're hitting the read-only replicas and we can avoid
        # spiking replication lag.
        GitHub::SQL::Readonly.new(@iterator.batches).each do |rows|
          process(rows)
        end

        log "Updated roughly #{@updated_records} total records" if verbose?
      end

      def process(rows)
        # Use the throttler to ensure that this transition doesn't cause
        # unnecessary replication delay
        AcvContributor.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Updating #{rows.count} rows with IDs: #{rows.first[0]} - #{rows.last[0]}" if verbose?
          run_batch_update rows
        end
      end

      def run_batch_update(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          # Must use `UPDATE IGNORE` to avoid duplication issues when a
          # contributor has contributions both with and without bad
          # characters in their email for the same repository
          sql = ApplicationRecord::Collab.github_sql.new "UPDATE IGNORE acv_contributors SET contributor_email = CASE"

          changeable_email_count = 0

          rows.each do |row|
            id = row[0]
            contributor_email = row[1]

            # Normalize the email format
            normalized_email = contributor_email.sub(@email_matcher) { |match|
              "#{$~[:local]}@#{$~[:domain].downcase}"
            }

            # If the email address changed...
            if normalized_email != contributor_email
              if verbose?
                if normalized_email.length == contributor_email.length
                  log "Lowercasing domain on ID #{id}: \"#{contributor_email}\" => \"#{normalized_email}\""
                else
                  log "Removing bad chars/prefixes on ID #{id}: \"#{contributor_email}\" => \"#{normalized_email}\""
                end
              end

              sql.add <<-SQL, id: id, contributor_email: normalized_email
                WHEN id = :id THEN :contributor_email
              SQL

              changeable_email_count += 1
            end
          end

          # HACK: This impossible `WHEN` option is added just in case no others
          # were added by the row iteration block above. Without it, unit tests
          # with limited data sets are failing due to syntax errors of having an
          # `ELSE` but no `WHEN`s.
          if changeable_email_count == 0
            sql.add "WHEN id = 0 THEN `contributor_email`"
          end

          # Default back to the original value if no special CASE is identified
          # WARNING: If this `ELSE` is not present and a none of the `WHEN`
          # options match, the value will be set to `NULL`!
          sql.add "ELSE `contributor_email`"
          sql.add "END"
          sql.add "WHERE id IN :ids", ids: rows.map { |row| row[0] }

          # Execute the query
          unless dry_run?
            sql.run
          end

          log "Updated roughly #{changeable_email_count} rows" if verbose?
          @updated_records += changeable_email_count
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AcvContributorsNormalizeEmails.new(options)
  transition.run
end
