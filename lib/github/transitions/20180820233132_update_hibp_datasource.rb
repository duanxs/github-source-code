# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180820233132_update_hibp_datasource.rb -v | tee -a /tmp/update_hibp_datasource.log
#
module GitHub
  module Transitions
    class UpdateHibpDatasource < Transition

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      def after_initialize
        @min_id = readonly do
          CompromisedPassword.github_sql.new(<<-SQL).value
            SELECT COALESCE(MIN(id), 0) FROM compromised_passwords
          SQL
        end

        @max_id = readonly do
          CompromisedPassword.github_sql.new(<<-SQL).value
            SELECT COALESCE(MAX(id), 0) FROM compromised_passwords
          SQL
        end
      end

      # Returns nothing.
      def perform
        count = 0
        (@min_id..@max_id).each_slice(BATCH_SIZE) do |batch|
          CompromisedPassword.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) { process(batch) }

          count += 1
          if count % 1000 == 0
            log("#{count} queries ran")
          end
        end
      end

      def process(batch)
        sql = CompromisedPassword.github_sql.new(<<-SQL, datasource: CompromisedPassword.datasource_bitmask(:HAVE_I_BEEN_PWNED), first: batch.first, last: batch.last)
          UPDATE compromised_passwords
          SET datasource = :datasource
          WHERE id BETWEEN :first AND :last
        SQL
        unless dry_run
          sql.run
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UpdateHibpDatasource.new(options)
  transition.run
end
