# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190124232950_cancel_org_invites_between_blocked_users.rb -v | tee -a /tmp/cancel_org_invites_from_blockee_to_blocker.log
#
module GitHub
  module Transitions
    class CancelOrgInvitesBetweenBlockedUsers < Transition

      BATCH_SIZE = 500

      attr_reader :iterator

      def after_initialize
        min_invite_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM organization_invitations") }
        max_invite_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM organization_invitations") }
        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_invite_id, finish: max_invite_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT * FROM (SELECT organization_invitations.id AS invite_id
            FROM ignored_users
            INNER JOIN organization_invitations
            ON ignored_users.user_id = organization_invitations.invitee_id
              AND ignored_users.ignored_id = organization_invitations.inviter_id
            WHERE organization_invitations.cancelled_at IS NULL
              AND organization_invitations.accepted_at IS NULL
              AND organization_invitations.id BETWEEN :start AND :last
            UNION
            SELECT organization_invitations.id
            FROM ignored_users
            INNER JOIN organization_invitations
            ON ignored_users.user_id = organization_invitations.inviter_id
              AND ignored_users.ignored_id = organization_invitations.invitee_id
            WHERE organization_invitations.cancelled_at IS NULL
              AND organization_invitations.accepted_at IS NULL
              AND organization_invitations.id BETWEEN :start AND :last
          ) AS all_invites
        SQL

        @staff_actor = User.staff_user
      end

      def perform
        @total_cancelled = 0

        GitHub::SQL::Readonly.new(iterator.batches).each do |batch|
          batch.each do |invite_ids|
            process(invite_ids)
          end
        end

        log_phrase = dry_run? ? "Would have cancelled" : "Cancelled"
        log "#{log_phrase} #{@total_cancelled} invitations" if verbose?
      end

      def process(invite_ids)
        return if invite_ids.empty?

        OrganizationInvitation.where(id: invite_ids).each do |invite|
          invite.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log_phrase = dry_run? ? "Would have cancelled" : "Cancelling"
            log "#{log_phrase} invitation #{invite.id}" if verbose?

            # Cancel invitations. Do not notify the user that it was cancelled,
            # because some of these are really old, and the user is blocked anyway.
            invite.cancel(actor: @staff_actor, notify: false) unless dry_run?

            @total_cancelled += 1
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CancelOrgInvitesBetweenBlockedUsers.new(options)
  transition.run
end
