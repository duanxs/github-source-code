# frozen_string_literal: true
require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/bulk_coupon_generator.rb | tee -a /tmp/bulk_coupon_generator.log
#   gudo bin/safe-ruby lib/github/transitions/bulk_coupon_generator.rb -c=YOUR_COUNT -p=YOUR_PREFIX -n=YOUR_NOTE
#   arguments: [quantity] [prefix] '[note]'
#   running without quantity does nothing

BATCH_LIMIT = 1000 # don't let this script go hog wild

module GitHub
  module Transitions
    class BulkCouponGenerator < LegacyTransition
      # Generate bulk coupons
      #
      # Returns nothing.

      def perform(count:, prefix: "gh-bulk", note: "bulk coupon",
                  dry_run: false, duration: "6 months", group: "si-bulk", discount: 1, plan: nil)
        count = count.to_i
        # prevent generating unpredictable quantities of coupon from weird input

        raise ArgumentError, "count should be greater than 0" unless count > 0
        #to_i deals with non-integer input

        # prevent generating enormous volumes of coupon through typos and such

        raise ArgumentError, \
        "That's a lot of coupons, buckeroo. Let's keep it under #{BATCH_LIMIT} per batch" \
        unless count <= BATCH_LIMIT

        log "Starting transition #{self.class.to_s.underscore}"

        total = 0

        until total == count
          create_coupon(prefix, note, duration, discount, group, plan, dry_run)
          total+= 1
        end

        log ""
        log "Generated #{total} coupons"
      end

      def create_coupon(prefix, note, duration, discount, group, plan, dry_run)
        non_sequential_token = SecureRandom.hex(4)[0, 7] #Prevent guessing more coupons

        coupon_name = "#{prefix}-#{non_sequential_token}"

        coupon = Coupon.new \
          code: coupon_name,
          group: group,
          discount: discount,
          limit: 1,
          expires_at: nil,
          duration: Coupon::FUN_DURATIONS.invert[duration],
          plan: plan,
          note: note

        begin
          Coupon.throttle { coupon.save! } unless dry_run
        rescue ActiveRecord::RecordInvalid
          log "There was an error saving the #{coupon_name} coupon."
          log "The errors were: #{coupon.errors.inspect}"
        end

        log coupon_name
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  options = Slop.parse do
    on "c=", "count", "number of coupons to create", as: Integer
    on "p=", "prefix", "prefix for generated coupons", as: String
    on "n=", "note", "note to attach to each coupon", as: String
    on "e=", "duration", "fun string format for the coupon duration e.g. (6 months)", as: String
    on "g=", "group", "group assigned to the coupon", as: String
    on "u=", "discount", "the discount for the coupon, 100% or 7.0", as: String
    on "r=", "plan", "the plan associated with the coupon: pro, business, or business_plus", as: String
    on "d", "dry_run", "don't save or update any data, just log changes"
  end

  transition = GitHub::Transitions::BulkCouponGenerator.new
  transition.perform(options.to_hash)
end
