# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180413233518_refund_zuora_test_transactions.rb -v | tee -a /tmp/refund_zuora_test_transactions.log
#
#   Example Arguments
#   lib/github/transitions/20180413233518_refund_zuora_test_transactions.rb -w -v -l="mockra,achiu" -d="2018-02-03"
#
module GitHub
  module Transitions
    class RefundZuoraTestTransactions < Transition
      areas_of_responsibility :gitcoin

      BATCH_SIZE = 100

      attr_reader :iterator, :logins, :date

      def after_initialize
        @logins = @other_args[:logins]
        @date = @other_args[:date]
      end

      def logins_array
        logins.split(",")
      end

      # Returns nothing.
      def perform
        raise ArgumentError, "can only refund 100 logins at once" if logins_array.count > 100
        raise ArgumentError, "a date must be provided" if date.blank?

        users = User.where(login: logins_array)
        User.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          users.each do |user|
            log "refunding transactions for #{user}" if verbose?
            refund_user user unless dry_run?
          end
        end
      end

      def refund_user(user)
        user.billing_transactions.where("created_at > '#{date}'").each do |transaction|
          next if transaction.voided? || transaction.was_refunded?

          refund_result = GitHub::Billing.refund_transaction(transaction.transaction_id)

          if refund_result.success?
            log "Transaction #{transaction.transaction_id} was refunded"
          else
            void_result = GitHub::Billing.void_transaction(transaction.transaction_id)

            if void_result.success?
              log "Transaction #{transaction.transaction_id} was voided"
            else
              log "Transaction #{transaction.transaction_id} wasn't refunded or voided"
            end
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "l=", "logins", "User logins we want to refund", as: String
    on "d=", "date", "Date we want to refund after", as: String
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RefundZuoraTestTransactions.new(options)
  transition.run
end
