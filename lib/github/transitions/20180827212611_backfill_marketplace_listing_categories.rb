# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180827212611_backfill_marketplace_listing_categories.rb -v | tee -a /tmp/backfill_marketplace_listing_categories.log
#
module GitHub
  module Transitions
    class BackfillMarketplaceListingCategories < Transition

      # @github/database-transitions-code-review is your friend, and happy to help

      BATCH_SIZE = 100

      attr_reader :iterator
      attr_accessor :total_listings, :total_primary_categories, :total_secondary_categories

      def after_initialize
        min_id = readonly { ::Marketplace::Listing.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM marketplace_listings") }
        max_id = readonly { ::Marketplace::Listing.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM marketplace_listings") }

        @iterator = ::Marketplace::Listing.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, primary_category_id, secondary_category_id
          FROM marketplace_listings
          WHERE id BETWEEN :start AND :last
        SQL

        @total_listings = 0
        @total_primary_categories = 0
        @total_secondary_categories = 0
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end

        log_stats if verbose?
      end

      private

      def process(rows)
        rows.each do |listing| # listing is an array with: listing_id, primary_category_id, and secondary_category_id
          self.total_listings += 1

          ::Marketplace::Listing.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "-------------------------" if verbose?
            log "processing listing: #{listing}" if verbose?
            backfill_categories_for *listing
          end
        end
      end

      def backfill_categories_for(listing_id, primary_category_id, secondary_category_id)
        # Associate the primary category first
        log "backfilling primary category: #{primary_category_id}" if verbose?
        self.total_primary_categories += 1
        add_category_to_listing(primary_category_id, listing_id)

        # Associate the secondary category if there is one
        return if secondary_category_id.blank?
        log "backfilling secondary category: #{secondary_category_id}" if verbose?
        self.total_secondary_categories += 1
        add_category_to_listing(secondary_category_id, listing_id)
      end

      def add_category_to_listing(category_id, listing_id)
        return if dry_run?

        log "writing to database (not a dry run)" if verbose?
        ::Marketplace::Listing.github_sql.value <<-SQL, category_id: category_id, listing_id: listing_id
          INSERT INTO marketplace_categories_listings
            (marketplace_category_id, marketplace_listing_id)
          VALUES
            (:category_id, :listing_id)
          ON DUPLICATE KEY UPDATE
            marketplace_category_id = :category_id,
            marketplace_listing_id = :listing_id
        SQL
      end

      def log_stats
        log "#########################"
        log "total listings affected: #{total_listings}"
        log "total primary categories associated: #{total_primary_categories}"
        log "total secondary categories associated: #{total_secondary_categories}"
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillMarketplaceListingCategories.new(options)
  transition.run
end
