# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190902224123_backfill_reviewed_at_for_sponsors_memberships.rb -v | tee -a /tmp/backfill_reviewed_at_for_sponsors_memberships.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190902224123_backfill_reviewed_at_for_sponsors_memberships.rb -v -w | tee -a /tmp/backfill_reviewed_at_for_sponsors_memberships.log
#
module GitHub
  module Transitions
    class BackfillReviewedAtForSponsorsMemberships < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ::SponsorsMembership.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_memberships") }
        max_id = readonly { ::SponsorsMembership.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_memberships") }

        @iterator = ::SponsorsMembership.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT sponsors_memberships.id FROM sponsors_memberships
          WHERE sponsors_memberships.state = 1
            AND sponsors_memberships.reviewed_at IS NULL
            AND sponsors_memberships.id BETWEEN :start AND :last
        SQL

        @total_memberships = 0
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |membership_ids|
          process(membership_ids.flatten)
        end

        log "total records updated: #{@total_memberships}" if verbose?
      end

      def process(membership_ids)
        memberships = ::SponsorsMembership.where(id: membership_ids)

        memberships.each do |membership|
          if dry_run?
            log "Would have updated membership (#{membership.id})" if verbose?
          else
            ::SponsorsMembership.throttle_writes do
              updated_at = membership.updated_at
              membership.reviewed_at = updated_at
              membership.save!
            end

            log "Updated membership (#{membership.id})" if verbose?
          end

          @total_memberships += 1
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillReviewedAtForSponsorsMemberships.new(options)
  transition.run
end
