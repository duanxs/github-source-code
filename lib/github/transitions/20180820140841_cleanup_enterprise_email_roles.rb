# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180820140841_cleanup_enterprise_email_roles.rb -v | tee -a /tmp/cleanup_enterprise_email_roles.log
#
module GitHub
  module Transitions
    class CleanupEnterpriseEmailRoles < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { EmailRole.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM email_roles") }
        max_id = readonly { EmailRole.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM email_roles") }

        @iterator = EmailRole.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id
          FROM email_roles
          WHERE id BETWEEN :start AND :last
          AND public <> '1'
        SQL
      end

      # Returns nothing.
      def perform
        return if GitHub.stealth_email_enabled?

        total_rows = 0
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
          total_rows += rows.size
        end
        log "Switched #{total_rows} total email_roles rows to public" if verbose?
      end

      def process(rows)
        return if rows.empty?
        log "Switching #{rows.size} email_roles rows to public" if verbose?
        return if dry_run?

        EmailRole.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          EmailRole.github_sql.run <<-SQL, input_values: rows.flat_map { |id_array| id_array }
            UPDATE email_roles
            SET public = '1'
            WHERE id IN :input_values
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CleanupEnterpriseEmailRoles.new(options)
  transition.run
end
