# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"
require "ruby-progressbar"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190515131346_nullify_blank_profile_fields.rb -v | tee -a /tmp/nullify_blank_profile_fields.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190515131346_nullify_blank_profile_fields.rb -v -w | tee -a /tmp/nullify_blank_profile_fields.log
#
module GitHub
  module Transitions
    class NullifyBlankProfileFields < Transition

      BATCH_SIZE = 100

      attr_reader :fields, :iterators, :progress_bar

      def after_initialize
        @fields = %w[blog company email location name]
        @iterators = Hash.new { |hash, key| hash[key] = profiles_iterator }
        @fields.each do |field|
          @iterators[field].add <<-SQL
            SELECT id FROM #{safe_table_name}
            WHERE id BETWEEN :start AND :last
            AND #{safe_table_name}.#{safe_field_name(field)} = ""
          SQL
        end
        @progress_bar = new_progress_bar(title: @fields.first)
      end

      # Returns nothing.
      def perform
        iterators.each do |field, iterator|
          @progress_bar = new_progress_bar(title: field)

          affected_rows = 0
          GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
            affected_rows += process(rows, field: field)
            if @progress_bar.progress + BATCH_SIZE > @max_id
              @progress_bar.finish
            elsif percentage_complete <= 99
              @progress_bar.progress += BATCH_SIZE
            end
          end

          if verbose?
            verb = dry_run? ? "Will repair" : "Repaired"
            @progress_bar.log "[#{field.upcase}] #{verb} #{affected_rows} record#{"s" unless affected_rows == 1}."
          end
        end
      end

      def process(rows, field:)
        return rows.size if dry_run? || rows.empty?

        Profile.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          run_batch_update(rows, field: field)
        end
      end

      def run_batch_update(items, field:)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = Profile.github_sql.new <<-SQL, ids: items.map(&:first)
            UPDATE #{safe_table_name}
            SET #{safe_field_name(field)} = NULL
            WHERE id IN :ids
          SQL

          sql.run
          sql.affected_rows
        end
      end

      def profiles_iterator
        @min_id ||= readonly { Profile.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM #{safe_table_name}") }
        @max_id ||= readonly { Profile.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM #{safe_table_name}") }

        Profile.github_sql_batched_between(start: @min_id, finish: @max_id, batch_size: BATCH_SIZE)
      end

      def safe_table_name
        @safe_table_name ||= Profile.connection.quote_table_name(Profile.table_name)
      end

      def safe_field_name(field)
        Profile.connection.quote_column_name(field)
      end

      def new_progress_bar(title:)
        ProgressBar.create(
          starting_at: @min_id,
          total: @max_id,
          format: "[Field: profiles.%t] %c/%C |%w| %a",
          throttle_rate: 1,
          title: title,
          output: Rails.env.test? ? StringIO.new : STDERR,
        )
      end

      def percentage_complete
        (@progress_bar.progress * 100 / @progress_bar.total).to_i
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::NullifyBlankProfileFields.new(options)
  transition.run
end
