# frozen_string_literal: true
# Sends coupon expiration notices for users whose coupon will be
# expiring two weeks from the "from" date specified.
# Ex. `GitHub::Transitions::SendExpirationNotices.new(from: 6.days.ago)` will
# send expiration notices for users whose coupons are expiring 2 weeks from 6
# days ago.

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20170330152412_send_expiration_notices.rb -v | tee -a /tmp/send_expiration_notices.log
#
module GitHub
  module Transitions
    class SendExpirationNotices < Transition
      areas_of_responsibility :gitcoin

      def initialize(from:, dry_run:)
        @from = from.to_date
        @dry_run = dry_run
      end

      # Returns nothing.
      def perform
        users = 0
        CouponRedemption.expiring_in_two_weeks(from: @from).find_each do |cr|
          cr.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            user = cr.user
            next if user.nil? || cr.coupon.nil?
            next if !user.paid_plan?
            unless dry_run
              user.activate_notice!(:coupon_will_expire) unless user.organization?
              BillingNotificationsMailer.coupon_will_expire_soon(user, cr).deliver_now
            end
            users += 1
          end
        end
        log("Sent expiration notices to #{users} users expiring two weeks from #{@from}")
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "from=", "The date we want to send notices two weeks from"
  end
  options = slop.to_hash
  options = {from: options[:from], dry_run: !options[:write]}

  transition = GitHub::Transitions::SendExpirationNotices.new(options)
  transition.run
end
