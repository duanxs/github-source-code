# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200226221805_create_after_sponsorship_create_survey.rb -v | tee -a /tmp/create_after_sponsorship_create_survey.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200226221805_create_after_sponsorship_create_survey.rb -v -w | tee -a /tmp/create_after_sponsorship_create_survey.log
#
module GitHub
  module Transitions
    class CreateAfterSponsorshipCreateSurvey < Transition
      SURVEY_SLUG = "sponsorship_created"

      def perform
        log "Starting transition #{self.class.to_s.underscore}"

        if dry_run?
          log "dry_run == true - Not creating a survey"
          return
        end

        if Survey.exists?(slug: SURVEY_SLUG)
          log "Survey with slug #{SURVEY_SLUG} already exists, skipping transition."
          return
        end

        survey = Sponsors::PostSponsorshipSurvey.find_or_create_survey!

        log "Saved survey #{survey.id} with questions #{survey.questions.map(&:attributes)}."
        log "Done!"
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CreateAfterSponsorshipCreateSurvey.new(options)
  transition.run
end
