# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200330153803_update_zuora_subscription_ids.rb -v | tee -a /tmp/update_zuora_subscription_ids.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200330153803_update_zuora_subscription_ids.rb -v -w | tee -a /tmp/update_zuora_subscription_ids.log
#
module GitHub
  module Transitions
    class UpdateZuoraSubscriptionIds < Transition
      OUTPUT_HEADERS = ["expired_subscription_id", "active_subscription_id", "reason"]

      attr_reader :subscriptions_csv, :offset, :output_csv

      def after_initialize
        out_dir = File.join(other_args[:outdir], "update_zuora_subscription_ids_#{"dry_run_" if dry_run? }#{Time.current.to_i}.csv")

        raise "Can only pass in output_csv in test environment" if !Rails.env.test? && other_args[:output_csv]
        @output_csv = other_args[:output_csv] || CSV.open(out_dir, "w")
        @output_csv << OUTPUT_HEADERS

        @subscriptions_csv = CSV.read(
          @other_args[:subscriptions_file],
          headers: true,
          header_converters: :symbol,
          converters: :all
        )
        @offset = other_args[:offset] || 0

        required_headers = [:expired_subscription_id, :active_subscription_id]
        required_headers.each do |header|
          unless subscriptions_csv.headers.include?(header)
            raise ArgumentError.new("Required header #{header} is missing")
          end
        end
      end

      def perform
        subscriptions_csv.drop(offset).each_with_index do |row, index|
          ::Billing::PlanSubscription.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            process(row.to_hash)
          end
        end

        output_csv.close
      end

      def process(row)
        expired_id, active_id = row[:expired_subscription_id], row[:active_subscription_id]

        log "Updating subscription #{expired_id} to #{active_id}"

        rows_to_update = readonly { ::Billing::PlanSubscription.where(zuora_subscription_id: expired_id).count }

        if rows_to_update == 0
          output_exception(
            expired_id: expired_id,
            active_id: active_id,
            reason: :no_subscription,
          )
          return
        end

        if !dry_run
          sql = ApplicationRecord::Domain::Users.github_sql.new "UPDATE plan_subscriptions SET zuora_subscription_id = :active_id", active_id: active_id
          sql.add "WHERE zuora_subscription_id = :expired_id", expired_id: expired_id
          sql.run

          if sql.affected_rows == 0
            output_exception(
              expired_id: expired_id,
              active_id: active_id,
              reason: :update_failed,
            )
          end
        end
      end

      def output_exception(expired_id:, active_id:, reason:)
        log "#{reason} expired_id:#{expired_id} active_id:#{active_id}" if verbose

        output_csv << [expired_id, active_id, reason]
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "outdir=", "Where to output exceptions file - this defaults to home directory ~/", default: "~/"
    on "subscriptions_file=", "Path to subscriptions update file"
    on "offset=", "Line offset to begin read from subscriptions file"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UpdateZuoraSubscriptionIds.new(options)
  transition.run
end
