# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190913160428_add_sponsors_tiers_to_zuora.rb -v | tee -a /tmp/add_sponsors_tiers_to_zuora.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190913160428_add_sponsors_tiers_to_zuora.rb -v -w | tee -a /tmp/add_sponsors_tiers_to_zuora.log
#
module GitHub
  module Transitions
    class AddSponsorsTiersToZuora < Transition
      BATCH_SIZE = 100
      RETRYABLE_EXCEPTIONS = [
        Errno::ECONNRESET,
        Faraday::ConnectionFailed,
        Faraday::TimeoutError,
        Net::OpenTimeout,
        OpenSSL::SSL::SSLError,
      ]
      MAX_CONSECUTIVE_EXCEPTIONS = 5

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_tiers") }
        max_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_tiers") }

        @iterator = ApplicationRecord::Collab.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id FROM sponsors_tiers
          WHERE id BETWEEN :start AND :last
          AND state IN (#{SponsorsTier.state_value(:published)}, #{SponsorsTier.state_value(:retired)})
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows.flatten)
        end
      end

      def process(tier_ids)
        sponsors_tiers = readonly { SponsorsTier.where(id: tier_ids) }
        sponsors_tiers.each do |tier|
          existing_uuids = ::Billing::ProductUUID.where(
            product_type: SponsorsTier::ZUORA_PRODUCT_TYPE,
            product_key: tier.id,
          )

          if existing_uuids.count == 2
            log "Tier #{tier.id} (#{tier.name}) is already synced to Zuora" if verbose?
          else
            consecutive_exceptions = 0
            begin
              tier.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) { tier.sync_to_zuora } unless dry_run?
              log "Added tier #{tier.id} (#{tier.name}) to Zuora" if verbose?
            rescue *RETRYABLE_EXCEPTIONS => e
              consecutive_exceptions += 1
              log "Error #{consecutive_exceptions} of #{MAX_CONSECUTIVE_EXCEPTIONS} syncing tier #{tier.id} (#{tier.name}) to Zuora: #{e}"
              log e.backtrace
              sleep (2 ** consecutive_exceptions)
              retry unless consecutive_exceptions > MAX_CONSECUTIVE_EXCEPTIONS
            rescue Zuorest::HttpError => e
              log "Error syncing tier #{tier.id} (#{tier.name}) to Zuora: #{e.inspect}\n  #{e.data}\n#{e.backtrace.join("\n")}"
            end
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddSponsorsTiersToZuora.new(options)
  transition.run
end
