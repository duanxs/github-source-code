# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190129052938_migrate_travis_hooks_to_webhooks.rb -v | tee -a /tmp/migrate_travis_hooks_to_webhooks.log
#
module GitHub
  module Transitions
    class MigrateTravisHooksToWebhooks < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      VALID_EVENTS = %w(push pull_request issue_comment public member create delete repository)

      attr_reader :iterator

      def after_initialize
        min_id = readonly { Hook.github_sql.value("SELECT id FROM hooks WHERE name = 'travis' AND active = 1 ORDER BY id ASC LIMIT 1") }
        max_id = readonly { Hook.github_sql.value("SELECT id FROM hooks WHERE name = 'travis' AND active = 1 ORDER BY id DESC LIMIT 1") }

        @iterator = Hook.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id
          FROM hooks
          WHERE id BETWEEN :start AND :last
          AND name = 'travis'
          AND active = 1
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          hooks = readonly { Hook.includes(:event_types, :config_attribute_records).where(id: rows.map(&:first)) }
          process(hooks)
        end
      end

      def process(hooks)
        migrated_hook_ids = []

        Hook.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          hooks.each do |travis_hook|

            if verbose?
              log "\ncreating webhook for #{travis_hook.installation_target_type} id ##{travis_hook.installation_target_id}..."
            end

            unless dry_run?
              webhook = Hook.new(
                name: :web,
                installation_target_id: travis_hook.installation_target_id,
                installation_target_type: travis_hook.installation_target_type,
                active: true,
              )

              config = travis_hook.config.slice("insecure_ssl", "content_type")

              config["url"] = if (domain = travis_hook.config["domain"])
                domain.strip!
                if domain.starts_with?("https://", "http://")
                  domain
                else
                  domain.prepend("https://")
                end
              else
                "https://notify.travis-ci.org"
              end

              config["replacement_for_travis_id"] = travis_hook.id

              webhook.config = config

              webhook.events = travis_hook.events.select do |event|
                VALID_EVENTS.include?(event)
              end

              if verbose? && !webhook.valid?
                log("error creating webhook for repository:")
                log(webhook.errors.full_messages.join(", "))
              end

              migrated_hook_ids.push(travis_hook.id) if webhook.save
            end
          end

          if verbose?
            migrated_hook_ids.each { |id| log "disabling travis service hook id ##{id}..." }
          end

          if !dry_run? && !migrated_hook_ids.empty?
            Hook.github_sql.run <<-SQL, ids: migrated_hook_ids
              UPDATE hooks SET active = 0 WHERE id IN :ids
            SQL
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::MigrateTravisHooksToWebhooks.new(options)
  transition.run
end
#
