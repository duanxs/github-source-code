# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190402232159_populate_close_issue_references.rb -v | tee -a /tmp/populate_close_issue_references.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190402232159_populate_close_issue_references.rb -v -w | tee -a /tmp/populate_close_issue_references.log
#
#   Set `STARTING_ISSUE_ID` or pass `--start_id=X` as an issue ID to resume processing at that
#   issue.
#   Set `BATCH_SIZE` or pass `--batch_size=X` as an integer to specify how many issues should be
#   fetched at a time.
#
module GitHub
  module Transitions
    class PopulateCloseIssueReferences < Transition
      DEFAULT_BATCH_SIZE = 1_000

      # See GitHub::HTML::IssueMentionFilter::CLOSE_WORD_AND_SUFFIX
      SELECT_QUERY = <<-SQL
        SELECT issues.id,
        issues.pull_request_id,
        issues.updated_at,
        issues.created_at
        FROM issues
        WHERE issues.id BETWEEN :start AND :last
        AND issues.pull_request_id IS NOT NULL
        AND issues.body IS NOT NULL
        AND (
          LOCATE('fix', issues.body) <> 0 OR
          LOCATE('close', issues.body) <> 0 OR
          LOCATE('resolve', issues.body) <> 0
        )
      SQL

      INSERT_QUERY = <<-SQL
        INSERT INTO close_issue_references
        (
          pull_request_id,
          issue_id,
          pull_request_author_id,
          issue_repository_id,
          created_at,
          updated_at
        )
        VALUES
        (
          :pull_request_id,
          :issue_id,
          :pull_request_author_id,
          :issue_repository_id,
          :created_at,
          :updated_at
        )
        ON DUPLICATE KEY
        UPDATE id = id,
               issue_id = VALUES(issue_id),
               issue_repository_id = VALUES(issue_repository_id),
               updated_at = VALUES(updated_at)
      SQL

      attr_reader :iterator, :pull_request_count, :closed_issue_count, :min_id, :max_id,
                  :batch_size

      def after_initialize
        return if GitHub.enterprise?
        @pull_request_count = 0
        @closed_issue_count = 0
        @min_id = get_min_issue_id || 0
        @max_id = readonly do
          ApplicationRecord::Domain::Repositories.github_sql.value("SELECT id FROM issues " \
                            "WHERE pull_request_id IS NOT NULL " \
                            "ORDER BY id DESC LIMIT 1")
        end
        @max_id ||= 0
        @batch_size = @other_args[:batch_size] || ENV.fetch("BATCH_SIZE", DEFAULT_BATCH_SIZE)
        @iterator = ApplicationRecord::Domain::Repositories.github_sql_batched_between(
          start: @min_id, finish: @max_id, batch_size: @batch_size,
        )
        @iterator.add(SELECT_QUERY)
      end

      # Returns nothing.
      def perform
        return if GitHub.enterprise?
        if verbose
          log "Updating close_issue_references..."
          log "Starting with pull request issue ID #{min_id} and going to ID #{max_id} with " \
              "a batch size of #{batch_size}; will give updates every 5,000 rows"
        end
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process_rows(rows)
        end
        if verbose
          log "Processed #{pull_request_count} pull requests that closed " \
              "#{closed_issue_count} issues"
          log "There are #{CloseIssueReference.count} close_issue_references"
        end
      end

      private

      def get_min_issue_id
        if @other_args[:start_id]
          @other_args[:start_id]
        elsif ENV["STARTING_ISSUE_ID"].present?
          ENV["STARTING_ISSUE_ID"].to_i
        else
          readonly do
            ApplicationRecord::Domain::Repositories.github_sql.value("SELECT id FROM issues " \
                              "WHERE pull_request_id IS NOT NULL " \
                              "ORDER BY id ASC LIMIT 1")
          end
        end
      end

      def process_rows(rows)
        pulls_by_id = load_pull_requests_by_id(rows)

        rows.each do |row|
          process_row(row, pulls_by_id)
        end
      end

      def load_pull_requests_by_id(rows)
        pull_request_ids = rows.map { |row| row[1] }
        PullRequest.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          PullRequest.where(id: pull_request_ids).
            includes(:repository, issue: [:repository, { events: :actor }]).index_by(&:id)
        end
      end

      def process_row(row, pulls_by_id)
        pull_issue_id, pull_request_id, updated_at, created_at = row
        pull_request = pulls_by_id[pull_request_id]
        return unless pull_request

        close_issues = calculate_close_issues(pull_request)
        return if close_issues.empty?

        @pull_request_count += 1
        @closed_issue_count += close_issues.size
        if verbose && @pull_request_count % 5_000 == 0
          log "Processed #{@pull_request_count} pull requests with #{@closed_issue_count} " \
              "closed issues..."
          log "Reached issue ID #{pull_issue_id}"
        end

        return if dry_run?

        ActiveRecord::Base.connected_to(role: :writing) do
          close_issues.each do |issue|
            CloseIssueReference.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              ApplicationRecord::Collab.github_sql.run(
                INSERT_QUERY, pull_request_id: pull_request_id, updated_at: updated_at,
                created_at: created_at, pull_request_author_id: pull_request.user_id,
                issue_id: issue.id, issue_repository_id: issue.repository_id
              )
            end
          end
        end
      end

      # Build out a denormalised replacement for
      # `pull_request.calculate_close_issues`
      # to avoid using GitHub::Goomba::CustomKeyLinkFilter and any other new filters
      # that may not be available in an enterprise environment.
      def calculate_close_issues(pull_request)
        goomba_context = {
          asset_root: "#{GitHub.asset_host_url}/images/icons",
          render_url: GitHub.render_host_url,
          base_url: GitHub.url,
          http_url: "http://#{GitHub.host_name}", # for the HttpsFilter
          name_prefix: GitHub::Goomba::NAME_PREFIX,
        }
        goomba_context[:flags] = Rinku::AUTOLINK_SHORT_DOMAINS if GitHub.enterprise?

        pipeline = GitHub::Goomba::WarpPipe.new(
          input_filters: [
            GitHub::Goomba::UTF8Filter,
            GitHub::Goomba::MarkdownFilter,
          ],
          sanitizer: GitHub::Goomba::Sanitizer.from_whitelist(GitHub::HTML::MARKUP_WHITELIST),
          node_filters: [
            GitHub::Goomba::DowncaseNameFilter,
            GitHub::Goomba::TaskListFilter,
            GitHub::Goomba::CloseKeywordFilter,
            GitHub::Goomba::CamoFilter,
            GitHub::Goomba::ImageMaxWidthFilter,
            (GitHub.ssl? ? GitHub::Goomba::HttpsFilter : nil),
            GitHub::Goomba::SuggestedChangeFilter,
            GitHub::Goomba::MentionFilter,
            GitHub::Goomba::TeamMentionFilter,
            GitHub::Goomba::IssueBlobFilter,
            GitHub::Goomba::IssueMentionFilter,
            GitHub::Goomba::CommitMentionFilter,
            GitHub::Goomba::AdvisoryMentionFilter,
            GitHub::Goomba::DuplicateKeywordFilter,
            GitHub::Goomba::ColonEmojiFilter,
            GitHub::Goomba::UnicodeEmojiFilter,
            GitHub::Goomba::MathFilter,
            GitHub::Goomba::SyntaxHighlightFilter,
            GitHub::Goomba::MarkdownElementClassFilter,
            GitHub::Goomba::ColorFilter,
            GitHub::Goomba::RelNofollowFilter,
            GitHub::Goomba::UrlUnfurlFilter,
          ].compact,
          output_filters: [
            GitHub::Goomba::GithubReferenceFilter,
          ],
          default_context: goomba_context.merge(gfm: true),
          result_class: GitHub::HTML::Result,
          stats_key: "MarkdownPipeline",
        )

        context =     {
          entity: pull_request.entity,
          current_user: pull_request.merged_by,
          location: pull_request.class.name,
        }

        if pull_request.repository && pull_request.body.present?
          (
            GitHub::HTML::BodyContent.new(
              pull_request.body, context, pipeline
            ).result.issues || []
          ).select(&:close?).collect(&:issue)
        else
          []
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "start_id=", "Issue ID to start processing at", as: Integer
    on "batch_size=", "How many issues to look up at a time", as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::PopulateCloseIssueReferences.new(options)
  transition.run
end
