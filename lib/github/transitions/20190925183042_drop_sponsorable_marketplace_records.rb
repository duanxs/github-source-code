# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190925183042_drop_sponsorable_marketplace_records.rb -v | tee -a /tmp/drop_sponsorable_marketplace_records.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190925183042_drop_sponsorable_marketplace_records.rb -v -w | tee -a /tmp/drop_sponsorable_marketplace_records.log
#
module GitHub
  module Transitions
    class DropSponsorableMarketplaceRecords < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM marketplace_listings") }
        max_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM marketplace_listings") }

        @iterator = ApplicationRecord::Domain::Integrations.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT marketplace_listings.id, GROUP_CONCAT(marketplace_listing_plans.id)
          FROM marketplace_listings
          LEFT JOIN marketplace_listing_plans
          ON marketplace_listings.id = marketplace_listing_plans.marketplace_listing_id
          WHERE marketplace_listings.id BETWEEN :start AND :last
          AND marketplace_listings.listable_type in ("User", "Organization")
          GROUP BY marketplace_listings.id
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          ApplicationRecord::Domain::Integrations.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            listing_id = item[0]
            listing_plan_ids = item[1] ? item[1].split(",").map(&:to_i) : []
            subscription_item_ids = fetch_subscription_item_ids(listing_plan_ids: listing_plan_ids)

            if subscription_item_ids.any?
              log "Not deleting sponsorable Marketplace::Listing #{listing_id}, found Billing::SubscriptionItems #{subscription_item_ids}." if verbose?
            else
              log "Deleting sponsorable Marketplace::Listing ##{listing_id}" if verbose?
              delete_listing(id: listing_id) unless dry_run?

              if listing_plan_ids.any?
                log "Deleting Marketplace::ListingPlans #{listing_plan_ids} for sponsorable Marketplace::Listing #{listing_id}" if verbose?
                delete_listing_plans(ids: listing_plan_ids) unless dry_run?
              else
                log "No Marketplace::ListingPlans found for sponsorable Marketplace::Listing #{listing_id}" if verbose?
              end
            end
          end
        end
      end

      def fetch_subscription_item_ids(listing_plan_ids:)
        return [] unless listing_plan_ids.any?

        readonly do
          ::Billing::SubscriptionItem.github_sql.run <<-SQL, listing_plan_ids: listing_plan_ids
            SELECT id FROM subscription_items
            WHERE subscription_items.subscribable_id in :listing_plan_ids
            # subscription_items.subscribable_type is a Rails enum where Marketplace::ListingPlan.name maps to 0
            AND subscription_items.subscribable_type = 0
          SQL
        end.results.flatten
      end

      def delete_listing(id:)
        ::Marketplace::Listing.github_sql.run <<-SQL, id: id
          DELETE FROM marketplace_listings WHERE id = :id
        SQL

        log "Deleted Marketplace::Listing #{id}." if verbose?
      end

      def delete_listing_plans(ids:)
        ::Marketplace::ListingPlan.github_sql.run <<-SQL, ids: ids
          DELETE FROM marketplace_listing_plans WHERE id in :ids
        SQL

        log "Deleted Marketplace::ListingPlans #{ids}." if verbose?
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DropSponsorableMarketplaceRecords.new(options)
  transition.run
end
