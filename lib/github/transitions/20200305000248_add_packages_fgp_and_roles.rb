# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# This transition adds three new fine grained permissions: read_package,
# write_package, and admin_package. It creates three new system roles
# package_writer, package_reader, and package_admin.

module GitHub
  module Transitions
    class AddPackagesFgpAndRoles < Transition
      PACKAGES_FGPS = %w(read_package write_package admin_package)
      PACKAGE_ROLES_TO_FGPS = {
        package_reader: ["read_package"],
        package_writer: ["read_package", "write_package"],
        package_admin: ["read_package", "write_package", "admin_package"],
      }.stringify_keys

      def perform
        unless dry_run?
          write_fine_grained_permissions
          write_roles_and_associate_fgps
        end
      end

      def write_fine_grained_permissions
        FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          PACKAGES_FGPS.each do |action|
            FineGrainedPermission.create(action: action)
          end
        end
      end

      def write_roles_and_associate_fgps
        PACKAGE_ROLES_TO_FGPS.each do |role_name, fgp_actions|
          role = Role.create(name: role_name.dup)
          fgp_actions.each do |action|
            fgp = FineGrainedPermission.find_by(action: action)
            role.permissions.create(fine_grained_permission: fgp, action: action)
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddPackagesFgpAndRoles.new(options)
  transition.run
end
