# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"
require "ruby-progressbar"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190114184523_migrate_fgps_to_collab.rb -v -s <subject_type> >> /tmp/migrate_fg_ps_to_collab.log
#
module GitHub
  module Transitions
    class MigrateFGPsToCollab < Transition

      BATCH_SIZE = 100

      attr_reader :iterator, :subject_type
      attr_accessor :total_count

      def after_initialize
        @subject_type = @other_args[:'subject-type']
        raise ArgumentError, "must provide a subject type to migrate" unless subject_type
        raise ArgumentError, "Subject type does not contain a '/', is this an FGP? '#{subject_type}'" unless subject_type.include?("/")
        log "Starting migration of subject type #{subject_type}"

        @min_id = readonly { Ability.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM abilities WHERE abilities.subject_type = :subject_type /* abilities-select-no-priority-needed */", subject_type: @subject_type) }
        @max_id = readonly { ApplicationRecord::Iam.github_sql.value("SELECT COALESCE(MIN(ability_id), 0) FROM permissions WHERE permissions.subject_type = :subject_type", subject_type: @subject_type) }

        if @max_id == 0
          log "WARN: You're running this migration and no permissions have been dual written for '#{subject_type}'... Hope you know what you're doing!"
          log "fetching max id from abilities for subject type #{subject_type}"
          @max_id = readonly { Ability.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM abilities WHERE abilities.subject_type = :subject_type /* abilities-select-no-priority-needed */", subject_type: @subject_type) }
          @max_id += 1 # to be inclusive. This is only for development.
        end

        if @min_id >= @max_id
          log "FATAL: min_id(#{@min_id}) >= max_id(#{@max_id}), there's nothing to copy!"
          @data_to_copy = false
        else
          @data_to_copy = true
          @total_count = Ability.github_sql.run(
            "SELECT COUNT(*) FROM abilities WHERE subject_type = :subject_type AND id < :max_id /* abilities-select-no-priority-needed */",
            subject_type: @subject_type,
            max_id: @max_id,
          ).results.first.first

          @iterator = Ability.github_sql_descending_batched_between(start: @max_id - 1, finish: @min_id, batch_size: BATCH_SIZE)
          @iterator.add(<<-SQL, subject_type: @subject_type)
            SELECT * FROM abilities
            WHERE id BETWEEN :start AND :last
            AND subject_type = :subject_type
            ORDER BY id desc
            /* abilities-select-no-priority-needed */
          SQL

          @bar = ProgressBar.create(
            starting_at: 0,
            total: @total_count + 1,
            format: "%a %e %c/%C (%j%%) %R |%B|",
            throttle_rate: 0.5,
            output: Rails.env.test? ? StringIO.new : STDERR,
          )
        end
      end

      # Returns nothing.
      def perform
        return unless data_to_copy?
        chatterbox(:starting)
        mark_subject_types(as: :in_progress)
        @bar.progress = 1
        @count = 0
        begin
          GitHub::SQL::Readonly.new(iterator_for_subject_type).each do |rows|
            process(rows)
            row_count = rows.count
            @count += row_count
            progressbar_increment(row_count)
          end

          mark_subject_types(as: :migrated)
          chatterbox(:finished)
        rescue RuntimeError => boom
          mark_subject_types(as: :failed)
          chatterbox(:failed)
          raise boom
        end
      end

      def process(rows)
        log "writing permission records for #{rows.map(&:first)}" if verbose?
        write_permission(rows) unless dry_run?
      end

      def write_permission(ability_row)
        Permission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          Permission.github_sql.run(<<-SQL, rows: GitHub::SQL::ROWS(ability_row))
          INSERT IGNORE INTO permissions
          (ability_id, actor_id, actor_type, action, subject_id, subject_type,
           priority, created_at, updated_at, parent_id)
          VALUES :rows
          SQL
        end
      end

      def mark_subject_types(as:)
        log "Marking subject type #{as}" if verbose?
        ::Permissions::RoutingTable.send(:"mark_as_#{as}!", subject_type: subject_type) unless dry_run?
      end

      def progressbar_increment(amount)
        begin
          @bar.progress += amount
        rescue ProgressBar::InvalidProgressError
        end
      end

      def iterator_for_subject_type
        if use_id_slice_iterator?
          log "descending id slices between #{@max_id} and #{@min_id}"
          Enumerator.new do |batches|
            ids = Ability.github_sql.run(
              "SELECT id FROM abilities WHERE id < :max_id AND subject_type = :subject_type ORDER BY id DESC /* abilities-select-no-priority-needed */",
              max_id: @max_id,
              subject_type: @subject_type,
            ).results.flatten(1)

            ids.each_slice(BATCH_SIZE) do |id_slice|
              batches << Ability.github_sql.run(
                "SELECT * from abilities where id IN :id_slice ORDER BY id DESC /* abilities-select-no-priority-needed */",
                id_slice: id_slice,
              ).results
            end
          end
        else
          log "descending batches between #{@max_id} and #{@min_id}"
          @iterator.batches
        end
      end

      def max_non_batched_count
        ENV.fetch("FGP_TRANSITION_BATCHING_THRESHOLD", 5000)
      end

      def use_id_slice_iterator?
        @total_count < max_non_batched_count.to_i
      end

      def chatterbox(msg)
        return if dry_run?

        message = case msg
        when :starting
          iterator_info = use_id_slice_iterator? ? "id slice iterator" : "batches: #{((@max_id - @min_id) / BATCH_SIZE) + 1}"
          "Beginning copy of #{@subject_type}. max_id: #{@max_id}, min_id: #{@min_id}, row count: #{@total_count}, iterator info - #{iterator_info}"
        when :finished
          "Finished copying #{@subject_type}. #{@count} rows copied"
        when :failed
          "Copying #{@subject_type} FAILED after #{@count} rows copied. Check logs for details."
        end

        GitHub::Chatterbox.client.say!("#fgp-migration-ops", message)
      end

      def data_to_copy?
        !!@data_to_copy
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "s=", "subject-type=", "The subject type to migrate", as: String
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::MigrateFGPsToCollab.new(options)
  transition.run
end
