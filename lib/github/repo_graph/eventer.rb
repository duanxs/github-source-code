# frozen_string_literal: true

module GitHub
  module RepoGraph
    class Eventer
      ADDITIONS = "RepoGraphs_Additions"
      DELETIONS = "RepoGraphs_Deletions"
      COMMITS = "RepoGraphs_Commits"
      INDEXED = "RepoGraphs_Indexed"

      CONTRIBUTOR_DATA_EVENTS = [
        ADDITIONS,
        DELETIONS,
        COMMITS,
      ].freeze

      CODE_FREQUENCY_EVENTS = [
        ADDITIONS,
        DELETIONS,
      ].freeze

      def initialize(repository_id)
        @repository_id = repository_id
      end

      attr_reader :repository_id

      # Public: Fetches all additions and deletions for the repository.
      #
      # Returns the data fetched from Eventer.
      def fetch_code_frequency_data
        GitHub.eventer_client.counters(
          owner_type: "REPOSITORY",
          owner_id: repository_id,
          bucket_size: "DAY",
          start_time: Time.at(0).utc,
          end_time: Time.now.utc,
          update_time_keys: false,
          events: CODE_FREQUENCY_EVENTS,
        )
      end

      # Public: Fetches all metrics for the given set of emails in the repository.
      #
      #  emails - a list of Strings representing the emails of the authors whose metrics you want.
      #
      # Returns the data fetched from Eventer.
      def fetch_contributors_data(emails)
        return {} if emails.empty?

        GitHub.eventer_client.counters_by_tag(
          owner_type: "REPOSITORY",
          owner_id: repository_id,
          bucket_size: "DAY",
          start_time: Time.at(0).utc,
          end_time: Time.now.utc,
          update_time_keys: false,
          events: CONTRIBUTOR_DATA_EVENTS,
          tags: emails.map(&:downcase),
        )
      end

      # Public: Fetches commit metrics for the last 52 weeks for the repository.
      #
      # Returns the data fetched from Eventer.
      def fetch_commit_activity_data
        GitHub.eventer_client.counters(
          owner_type: "REPOSITORY",
          owner_id: repository_id,
          bucket_size: "DAY",
          start_time: 52.weeks.ago.utc.beginning_of_week(:sunday),
          end_time: Time.now.utc,
          update_time_keys: false,
          events: [COMMITS],
        )
      end

      # Public: Indicates if eventer has all the data up to and including the given oid by
      #   checking for the presence of an indexed "marker" event.
      #
      # Returns true if up to date, false if not, and nil if no metrics were ever sent for the repo.
      def up_to_date?(last_indexed_oid)
        response = GitHub.eventer_client.counters(
          owner_type: "REPOSITORY",
          owner_id: repository_id,
          bucket_size: "DAY",
          start_time: Time.at(0).utc,
          end_time: Time.now.utc,
          events: [INDEXED],
          tags: [last_indexed_oid],
        )

        response && response[INDEXED].any?
      end

      # Public: Indicates if data for this repository has ever been fully indexed.
      #
      # Returns a Boolean.
      def has_indexed_data?
        response = GitHub.eventer_client.counters(
          owner_type: "REPOSITORY",
          owner_id: repository_id,
          bucket_size: "DAY",
          start_time: Time.at(0).utc,
          end_time: Time.now.utc,
          events: [INDEXED],
        )

        response && response[INDEXED].any?
      end

      def delete_counters
        # Build stub repository if it has already been deleted.
        event_owner = repository || Repository.new(id: repository_id).freeze

        [ADDITIONS, DELETIONS, COMMITS, INDEXED].each do |event|
          ::Eventer::Event.delete_counters(
            event_name: event,
            owner: event_owner,
            async: false,
          )
        end
      end

      def repository
        return @repository if defined?(@repository)

        @repository = Repository.find_by(id: repository_id)
      end
    end
  end
end
