# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Reports
    class Developers

      DEVELOPER_EVENTS = %w(
        CreateEvent
        DeleteEvent
        DeploymentEvent
        DeploymentStatusEvent
        ForkEvent
        PullRequestEvent
        PushEvent
      ).freeze

      def data
        hash = {developers: 0, nondevelopers: 0}

        conditions = [
          "suspended_at IS NULL
            AND login != ?
            AND type = 'User'",
            GitHub.ghost_user_login,
        ]

        User.where(conditions).find_each do |user|
          if developer?(user)
            hash[:developers] += 1
          else
            hash[:nondevelopers] += 1
          end
        end

        hash
      end

      def developer?(user)
        # has SSH keys?
        return true if user.public_keys.any?

        # has developer events?
        user.events.each do |event|
          return true if DEVELOPER_EVENTS.include?(event.event_type)
        end

        false
      end
    end
  end
end
