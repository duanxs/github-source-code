# frozen_string_literal: true
require "fileutils"
require "net/http"
require "open3"

module GitHub
  class BonesExporter
    include GitHub::ServiceMapping

    map_to_service :bones_exporter

    FILES_KEY = "files"
    CODEOWNERS_KEY = "codeowners"
    AREAS_OF_RESPONSIBILITY_KEY = "areas_of_responsiblity"
    SERVICES_KEY = "services"
    SERVICE_CATALOG_KEY = "service_catalog"
    DISPLAY_WIDTH = 120
    JOB_NAME = "github-bones-data-export"

    IGNORE_REGEXES = [
      %r{\A\.}, # these usually aren't code files
      %r{\.md\Z}, # don't care about docs
      %r{\.gitignore\Z}, # nor gitignore

      # not sure what exactly these are used for, aside from being certificates
      # but they don't makes sense to track ownership of
      %r{\Aconfig/service_certificates/},

      # we don't care about migrations after they've been run. if they are in master, they've been run
      %r{\Adb/migrate/},
      # transitions typically are run once. they often don't even make it into the master branch
      %r{\Alib/github/transitions/\d+},

      # images, fonts, etc. javascript and css we might care about, but those should be elsewhere
      %r{\Apublic/},
      # ownership of fixtures isn't super important
      %r{\Atest/fixtures/},

      # a bunch of vendor things...
      # NOTE we don't ignore everything in vendor, because we have some like vendor/internal-gems that are 'vendor'
      # but are part of this repo and nowhere else
      %r{\Avendor/npm/},
      %r{\Avendor/cache/},
      %r{\Avendor/bundler/},
      %r{\Avendor/gitrpc/vendor/cache/},
      %r{\Avendor/gitignore/},
      %r{\Avendor/licenses/},

      # contains bunches of git files (as in, the things you'd find in a .git dir)
      %r{\Avendor/gitrpc/test/examples/},

      # why are these even in version control? seems like accidental commits
      %r{\Avendor/node-sass/v\.*/\.*binding.node},
      %r{\Avendor/npm-.*\.tgz},
    ].freeze

    def self.run!(argv = ARGV, output: $stdout)
      list_mode = argv.include?("--files")
      ci_mode = ENV["GITHUB_CI"]

      new(list_mode: list_mode, ci_mode: ci_mode, output: output).run!
    end

    attr_reader :list_mode, :output, :ci_mode

    def initialize(list_mode: false, files: nil, ci_mode: false, output: StringIO.new)
      @list_mode = list_mode
      @output = output
      @ci_mode = ci_mode

      # only assign @files if there's anything, to allow lazy load in files method to work
      if files
        @files = Array(files).each do |file| # intentional to use each to return what was iterated, rather than map
          path = Pathname.new(file)
          raise "expected relative path for #{file}" unless path.relative?
          raise "missing file #{file}" unless path.exist?
        end
      end
    end

    def map_to_service_files
      return @map_to_service_files if defined?(@map_to_service_files)

      puts "Gathering files with map_to_service..."
      @map_to_service_files = safe_system(%Q{git grep --files-with-matches map_to_service -- app lib 2>/dev/null})
        .lines
        .map(&:strip)
    end

    def ignore_regexp
      return @ignore_regexp if defined?(@ignore_regexp)
      @ignore_regexp = Regexp.union(*IGNORE_REGEXES)
    end

    def files
      return @files if defined?(@files)
      puts "Gathering files..." unless list_mode
      files = safe_system("git ls-files").lines.map(&:chomp)

      puts "Filtering files..." unless list_mode

      files.reject! do |file|
        file.gsub('"', "") =~ ignore_regexp
      end

      if limit
        files = files.slice 0, limit
      end

      @files = files
    end

    def areas_of_responsibilities_for_line(line)
      line = line.gsub(/#.*/, "") # remove comments
        .strip # leading and trailing space
        .squeeze(" ") # squeeze whitespace

      # skip places that have areas_of_responsibility as like a named key or whatever
      return [] unless line.start_with?("areas_of_responsibility ")
      line = line.delete_prefix("areas_of_responsibility ") # remove method call

      # split whatever is left as arguments
      line.split(", ").map do |feature|
        # detect :"this-kind-of-symbol" and strings
        feature.gsub(/[:"']/, "")
      end.select do |feature|
        # only select ones that are in areas_of_responsibility.yaml
        areas_of_responsibilities.has_key?(feature)
      end
    end


    def areas_of_responsibilities_for_file(file)
      areas = []

      grep_output = safe_system("grep \"areas_of_responsibility :\" #{file} || :")

      grep_output.each_line do |line|
        this_lines_areas = areas_of_responsibilities_for_line(line)
        areas.concat(this_lines_areas)
      end

      areas
    end

    def serviceowners
      return @serviceowners if defined?(@serviceowners)

      serviceowners_path = Rails.root.join("SERVICEOWNERS")
      @serviceowners = GitHub::Serviceowners.new(serviceowners_path.read)
    end

    def areas_of_responsibilities
      return @areas_of_responsibilities if defined?(@areas_of_responsibilities)

      puts "Gathering areas of responsibilities..."
      aor_path = Rails.root.join("docs", "areas-of-responsibility.yaml")
      @areas_of_responsibilities = if aor_path.exist?
        YAML::load_file(aor_path.to_s)
      else
        {}
      end
    end

    def durable_ownership
      return @durable_ownership if defined?(@durable_ownership)

      puts "Gathering service catalog data..."

      ownership_yaml_path = Rails.root.join("ownership.yaml")
      @durable_ownership = YAML.safe_load(ownership_yaml_path.read)
    end

    def maintainership_for_file(file)
      maintainership = {}

      GitHub.codeowners.for(file).each_key do |owner|
        maintainership[CODEOWNERS_KEY] ||= []
        maintainership[CODEOWNERS_KEY].push owner.to_s
      end

      # sorry, this is terrible :D
      # if we could be sure of the constant the file determines, we could load it and ask
      # it its own areas_of_responsiblity instead of grepping
      if File.extname(file) == ".rb"
        areas = areas_of_responsibilities_for_file(file)
        maintainership[AREAS_OF_RESPONSIBILITY_KEY] = areas if areas.any?

        services = if map_to_service_files.include?(file)
          GitHub::ServiceMapping.services_from_class_file(file, prefix: true)
        else
          []
        end
        service = serviceowners.service_for_path(file, prefix: true)
        services.push service if service
        maintainership[SERVICES_KEY] = services.uniq if services.any?
      end

      maintainership
    end

    def gather_data
      data = {"sha" => GitHub.current_sha}

      data[SERVICE_CATALOG_KEY] = durable_ownership

      data[AREAS_OF_RESPONSIBILITY_KEY] = {}
      areas_of_responsibilities.each do |aor, aor_data|
        teams = (aor_data["teams"] || [])
        teams.push aor_data["escalation_team"] if aor_data["escalation_team"]

        teams.map! { |team| team.starts_with?("@github/") ? team : "@github/#{team}" }

        teams.uniq!

        data[AREAS_OF_RESPONSIBILITY_KEY][aor] = {
          "teams" => teams,
          "name" => aor_data["name"],
          "description" => aor_data["description"],
        }
      end

      puts "Reviewing files"
      puts "File count: #{files.length}"
      data[FILES_KEY] = {}
      files.each_with_index do |file, index|
        if ci_mode
          output.putc "."
          # add a newline break periodically to display nicer on CI
          # without it, they will all be on one file
          output.putc "\n" if index % DISPLAY_WIDTH == 0
        else
          print_percentage(files.length, index)
        end

        data[FILES_KEY][file] = maintainership_for_file(file)
      end


      puts "\nDone"

      data
    end

    def data_dir
      return @data_dir if defined?(@data_dir)
      @data_dir = if ci_mode
        Pathname.new("/tmp/#{JOB_NAME}-artifacts")
      else
        Rails.root.join("tmp")
      end
    end

    def data_path
      return @data_path if defined?(@data_path)
      @data_path = data_dir.join("ownership-data.yaml")
    end

    def run!
      if list_mode
        files.each do |file|
          puts file
        end
        return
      end

      data = gather_data

      puts "Writing data to #{data_path}"
      data_dir.mkpath
      data_path.write(data.to_yaml)

      if ci_mode
        bones_url = ENV.fetch("BONES_URL", "https://bones.githubapp.com")
        # Parse the URL for the bones application
        uri = URI.parse("#{bones_url}/maintainer_imports/webhook")
        puts "\nHitting Bones endpoint #{uri}"

        # Create the request object
        # Set the authentication token
        bones_token = ENV.fetch("BONES_AUTHENTICATION_TOKEN")
        header = { "Content-Type" => "text/json", "Authorization" => "Token token=#{bones_token}" }
        # Send the current GitHub sha so that it knows the job has been run
        body = {
          sha: GitHub.current_sha,
          build_branch: ENV["BUILD_BRANCH"],
          build_url: ENV["BUILD_URL"]
        }

        # Create the request objects
        request = Net::HTTP::Post.new(uri.request_uri, header)
        request.body = body.to_json
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = (uri.scheme == "https")

        # Send the request
        response = http.request(request)
        puts "Bones webhook response: #{response.code} #{response.msg}"
        puts "\nResponse body"
        puts response.body

        # Will raise an error if not 200
        # Artifacts will still be uploaded as per https://github.com/github/bp-buildrequest/pull/222
        response.error! unless response.code == "200"
      end

      return 0
    end

    def services_from_file(file)
      klass = nil
      ActiveSupport::Dependencies.loadable_constants_for_path(file).find do |c|
        klass = c.safe_constantize
      end

      if klass.blank?
        # This mapping doesn't work out-of-the-box with the default inflector.
        # (We have a custom mapping set up with Zeitwerk, which is hooked up to Rails autoloading.)
        if file.end_with?("app/api/graph_ql.rb")
          klass = Api::GraphQL
        else
          return []
        end
      end

      klass.services.dup
    end

    def limit
      return @limit if defined?(@limit)
      @limit = if ENV["BONES_EXPORT_FILE_LIMIT"]
        ENV["BONES_EXPORT_FILE_LIMIT"].to_i
      end
    end

    private

    def puts(string)
      output.puts(string)
    end

    # In safe-ruby we cannot use backticks
    # As we need the stdout this runs the command a returns it
    def safe_system(command)
      stdout_str, error_str, status = Open3.capture3(command)
      if status.success?
        stdout_str
      else
        puts error_str
        puts stdout_str
        raise "'#{command}' failed with status #{status}"
      end
    end

    # Just for fun, prints a progress bar
    def print_percentage(total, current)
      # Only print out every 100 files
      if current % 100 == 0
        x = (current * (100.to_f / total)).round(2)
        bar = "#{current}/#{total}:<#{"=" * x}#{"-" * (100 - x)}> #{x.round(2)}%"

        output.putc "\r"
        output.print bar
        output.flush
      end
    end

  end
end
