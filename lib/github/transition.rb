# frozen_string_literal: true

# Base class for safer GitHub data transitions
#
# This transition base class is safer in that it defaults
# to a dry_run mode.  In dry run mode it will use the read-only Mysql
# connection to ensure any errant writes fail.
#
module GitHub
  class Transition
    MAX_THROTTLE_RETRIES = 5

    include AreasOfResponsibility

    attr_reader :dry_run, :verbose, :other_args

    # Create an instance of the transition.
    #   dry_run: used for testing and viewing the output of what *would* change
    #   verbose: used for detailed output
    #   other_args: an arbitrary hash of any extra arguments needed for the transition
    def initialize(dry_run: true, verbose: false, **other_args)
      @started_at = Time.now.utc
      @verbose = verbose
      @dry_run = dry_run
      @other_args = other_args

      log "Creating transition #{name} at #{started_at}"
      if verbose?
        log "  dry_run: #{dry_run?} verbose: #{verbose?} other_args: #{other_args}"
      end

      STDOUT.sync = true # always print immediately even if piping to a log file
      init_context
      after_initialize
    end

    # Public: Hook method called after initialize is called
    #
    # Subclasses can override this to do any other setup needed if they so desire
    def after_initialize
    end

    # Public: Driver method to run the transition.
    #
    # Subclasses should override perform, not this method.  This method ensures
    # that we in a read only mode if the transition is run in dry_run mode.
    #
    # Returns the result of `perform`
    def run
      log "Starting #{name}#run"
      log "[dry_run] In dry_run mode" if dry_run?
      result = enforce_read_only_if_dry_run do
        perform
      end
    ensure
      set_ended_at

      log "Finished #{name}#run at #{ended_at}"
      result
    end

    # Do the actual work of the transition.
    #
    # Subclasses should implement this method.
    def perform
      raise "Implement perform for #{self}"
    end
    private :perform

  private
    # Private: The start time that this transition began at.
    #
    # Returns Time
    def started_at
      @started_at
    end

    # Private: The end time when things finished.
    #
    # Returns nil or Time
    def ended_at
      @ended_at
    end

    # Private: Is this transition in a dry run mode?
    def dry_run?
      @dry_run
    end

    # Private: Should we verbosely print out status?
    def verbose?
      @verbose
    end

    # Private: Write a log message - send to STDOUT if not in test
    def log(message)
      Rails.logger.debug message
      return if Rails.env.test?
      puts "[#{Time.now.iso8601.sub(/-\d+:\d+$/, '')}] #{message}"
      STDOUT.flush
    end

    # Private: Write live-updating status message to stderr.
    def status(message, *args)
      return if !$stderr.tty?
      $stderr.printf(" #{message}\n", *args)
    end

    # Private: Setup Failbot, GitHub & Audit context
    def init_context
      context = {
        actor: ENV["USER"],
        app: "github-transitions",
        console_host: Socket.gethostname,
        sudo_user: ENV["SUDO_USER"],
        transition: name,
        transition_started_at: started_at,
      }
      Failbot.push context
      GitHub.context.push context
      Audit.context.push context
    end

    # Private: Enforce a read only database connection if we are in dry_run mode
    # mode.
    def enforce_read_only_if_dry_run
      if dry_run?
        @original_read_only = GitHub.read_only?
        GitHub.read_only = true
      end
      yield
    ensure
      if dry_run?
        GitHub.read_only = @original_read_only
      end
    end

    # Private: name of the current transition for logs / debugging.
    def name
      self.class.to_s.underscore
    end

    def set_ended_at
      @ended_at = Time.now.utc

      context = { transition_ended_at: ended_at }

      GitHub.context.push context
      Audit.context.push context
      Failbot.push context
    end

    # Private: Wrap a block of code with readonly replica usage.
    #
    # Reads are not throttled automatically here, because writes to primaries
    # are slow enough that reads from replicas do not become an issue. If throttling
    # is needed for reads, that should done explicitly at the point where the reads
    # are happening.
    #
    # This also disables the slow query logger for the duration of the block, as
    # it's expected that readonly queries in transitions are frequently slow
    # enough to trigger the logger unnecessarily.
    #
    # IMPORTANT: if you're using this, and your query might really be a bad one,
    # please notify Ops. It's still possible to take down the database/site
    # with a bad query, even when using the read replicas.
    #
    def readonly(&block)
      ActiveRecord::Base.connected_to(role: :reading) do
        SlowQueryLogger.disabled(&block)
      end
    end
  end
end
