# rubocop:disable Style/FrozenStringLiteralComment

require "github/billing/result"
require "github/billing/error_message"
require "scientist"

module GitHub
  module Billing
    MAX_DAILY_TRANSACTIONS = 5
    BILLING_RATE_LIMIT_TTL = 24 * 60 * 60   # 24 hours

    class Error < StandardError
      def areas_of_responsibility
        [:gitcoin]
      end
    end

    class CustomerAlreadyExistsError < StandardError; end

    class << self

    end

    # Defines the timezone in which our daily billing cycle runs
    #
    # Our customer's plans start at 00:00:00 Pacific Time (US & Canada)
    # and end at 23:59:59 Pacific Time (US & Canada).
    #
    # Returns an ActiveSupport::TimeZone
    def self.timezone
      @timezone ||= ActiveSupport::TimeZone["America/Los_Angeles"]
    end

    # Returns today's date in the billing timezone
    #
    # Returns a Date
    def self.today
      timezone.today
    end

    # Returns the current time in the billing timezone
    #
    # Returns a TimeWithZone
    def self.now
      timezone.now
    end

    def self.date_in_timezone(date, hours: 0, minutes: 0, seconds: 0)
      timezone.local(
        date.year,
        date.month,
        date.day,
        hours,
        minutes,
        seconds,
      )
    end

    # Public: Generate a Braintree client token for accepting PayPal.
    #
    # Returns a String token nonce.
    def self.generate_braintree_client_token
      Timeout.timeout(1) do
        Braintree::ClientToken.generate if GitHub.braintree_client_token_enabled?
      end
    rescue Errno::ECONNREFUSED, Timeout::Error
      nil
    end

    # Attempts to signup a user or business for a paying plan subscription.
    #
    # This atomically charges the given credit card and creates a customer
    # record  with the given card details given the transaction is successful.
    #
    # user            - Saved and valid User record.
    # plan_name       - Name of the plan as a String the user is signing up for.
    # actor           - User initiating the upgrade, defaults to target.
    # payment_details - A Hash of payment details:
    #   :zuora_payment_method_id - The payment method ID returned by Zuora's Hosted Payment Page
    #   :billing_extra           - String extra billing information.
    #   :paypal_nonce            - String nonce representing a paypal account (optional).
    #   :billing_address         - The billing address as a Hash of optionally encrypted CC info
    #     * :country_name    - Country as a String
    #     * :region          - Region as a String
    #     * :postal_code     - Postal code as a String
    #
    # Returns a Billing::Result object indicating success or failure with an
    #   error message.
    def self.signup(user, plan_name, actor:, payment_details:)
      Failbot.push(user: user.login)

      if GitHub.flipper[:billing_signup_raiseless_validation].enabled?(actor)
        signup = ::Billing::Signup.new(user: user, plan_name: plan_name, actor: actor)
        unless signup.valid?
          if signup.customer_already_exists?
            raise CustomerAlreadyExistsError, "user has already signed up"
          end

          return Result.failure(signup.errors.full_messages.first)
        end
      else
        validate_signup_with_raise(user, plan_name, actor)
      end


      unless has_payment_details?(payment_details)
        return Result.failure("A valid payment method is required to sign up for a paid plan")
      end

      plan = if user.coupon.try(:trial?)
        user.coupon.plan
      else
        GitHub::Plan.find!(plan_name)
      end

      user.update(plan: plan.name, billed_on: today)

      begin
        result = create_customer(user, payment_details)
      rescue StandardError => boom
        Failbot.report boom
        message = "We were unable to process your payment information. Please try again."
        result = Result.failure message
      end

      if result.success?
        # Update user's signup transaction to correct paid plan
        tx = user.transactions.where(action: "signed-up").last \
          or raise Error, "Expected a transaction to exist"
        tx.update(current_plan: plan.name)
        GitHub.dogstats.increment("user.create.paid")
      else
        user.update(plan: GitHub::Plan.free, billed_on: nil)

        user.instrument :billing_signup_error,
          email: user.billing_email,
          plan: plan.name,
          error: result.error_message.to_s
      end

      result
    end

    # Charge a user for a paid upgrade when there's no card on file.
    #
    # target          - User or Organization that is upgrading
    # plan_name       - Name of Plan to upgrade to
    # payment_details - A Hash of payment details:
    #   :actor        - User initiating the upgrade, defaults to target, required if target is an Organization.
    #   :zuora_payment_method_id - The payment method ID returned by Zuora's Hosted Payment Page
    #   :billing_extra           - String extra billing information.
    #   :paypal_nonce            - String nonce representing a paypal account (optional).
    #   :billing_address         - The billing address as a Hash of optionally encrypted CC info
    #     * :country_name    - Country as a String
    #     * :region          - Region as a String
    #     * :postal_code     - Postal code as a String
    #
    # Returns a Billing::GitHub::Result specifying success or failure.
    def self.paid_upgrade(target, plan_name, payment_details, plan_duration = nil)
      payment_details.reverse_merge! actor: target
      Failbot.push user: target

      unless payment_details.fetch(:actor).user?
        raise ArgumentError, "The actor must be a human user"
      end

      validate_paid_upgrade(target, plan_name)

      if target.needs_valid_payment_method_to_switch_to_plan?(plan_name) && !has_payment_details?(payment_details)
        return Result.failure "A credit card or other payment method is required to upgrade to that plan."
      end

      plan     = GitHub::Plan.find!(plan_name)
      old_plan = target.plan

      target.plan          = plan.name
      target.plan_duration = plan_duration if plan_duration
      target.billing_extra = payment_details[:billing_extra] if payment_details.has_key?(:billing_extra)

      if target.payment_amount > 0
        target.billed_on = today
        result = create_customer(target, payment_details)
        if result.success?
          target.enable!
          target.track_plan_change(payment_details[:actor], old_plan)
        else
          target.plan      = old_plan
          target.billed_on = today
          target.save
        end
      else
        result = target.process_zero_charge_transaction("first-time-paid-upgrade")
      end

      result
    end

    # Public: Redeems a coupon and processes a payment, if necessary.
    #
    # target          - User or Organization to apply the coupon to
    # coupon          - Dat Coupon
    # payment_details - Hash of payment details:
    #   :actor         - User initiating the redemption, defaults to target, required if target is an Organization.
    #   :plan          - Plan to change to with this redemption
    #   :zuora_payment_method_id - The payment method ID returned by Zuora's Hosted Payment Page
    #   :billing_extra           - String extra billing information.
    #   :paypal_nonce            - String nonce representing a paypal account (optional).
    #   :billing_address         - The billing address as a Hash of optionally encrypted CC info
    #     * :country_name    - Country as a String
    #     * :region          - Region as a String
    #     * :postal_code     - Postal code as a String
    #
    # Either :zuora_payment_method_id of :paypal_nonce is required if the redemption
    # needs a charge and account has no billing record.
    #
    # Returns a GitHub::Billing::Result specifying the success or failure
    def self.redeem_coupon_and_charge(target, coupon, payment_details = nil)
      payment_details ||= {}
      payment_details.reverse_merge!({
        actor: target,
        plan: nil,
        credit_card: nil,
      })
      Failbot.push user: target

      if payment_details.fetch(:actor).organization?
        raise ArgumentError, "An Organization cannot be the actor"
      end

      old_plan             = target.plan
      target.billing_extra = payment_details[:billing_extra] if payment_details.has_key?(:billing_extra)

      # Don't set the plan if the coupon is only meant to be used for a specific
      # plan; redeem_coupon will choose the right one.
      if payment_details[:plan].present? && !coupon.trial?
        target.plan = payment_details[:plan]
      end

      result = Result.failure("transaction block never ran")

      target.transaction do
        if target.redeem_coupon(coupon, actor: payment_details[:actor], instrument: false)
          if target.payment_amount == 0
            result = target.process_zero_charge_transaction
          else
            result = process_payment_for_redemption(target, payment_details)
            raise ActiveRecord::Rollback if result.failed?
          end

          if result.success?
            target.track_plan_change(payment_details[:actor], old_plan, coupon: coupon)
          end
        else
          result = Result.failure(target.errors.full_messages.first ||
            raise(Error, "expected target to have errors if redeem_coupon returns falsy"))
        end
      end

      result
    end

    # Public: Updates payment method details of an existing customer.
    # This also tries to charge the new card (or paypal account) if the user
    # account is disabled or its billed_on is in the past.
    #
    # target          - A User object who's credit card data we're updating.
    # payment_details - A Hash of payment details:
    #   :charge        - Boolean whether to attempt a recurring charge with this update. Default: true
    #   :zuora_payment_method_id - The payment method ID returned by Zuora's Hosted Payment Page
    #   :billing_extra           - String extra billing information.
    #   :paypal_nonce            - String nonce representing a paypal account (optional).
    #   :billing_address         - The billing address as a Hash of optionally encrypted CC info
    #     * :country_name    - Country as a String
    #     * :region          - Region as a String
    #     * :postal_code     - Postal code as a String
    #
    # Returns a GitHub::Billing::Result object, indicating
    # success or failure, the user that was updated and an error message if the
    # customer's credit card could not be updated.
    def self.update_payment_method(target, payment_details)
      service = ::Billing::UpdatePaymentMethod.perform(target, payment_details)
      service.response
    end

    # Creates a new customer and credit card records.
    #
    # target          - A User-like object who's credit card we're creating
    # payment_details - A Hash of payment details:
    #   :zuora_payment_method_id - The payment method ID returned by Zuora's Hosted Payment Page
    #   :billing_extra           - String extra billing information.
    #   :paypal_nonce            - String nonce representing a paypal account (optional).
    #   :billing_address         - The billing address as a Hash of optionally encrypted CC info
    #     * :country_name    - Country as a String
    #     * :region          - Region as a String
    #     * :postal_code     - Postal code as a String
    #
    # Returns a GitHub::Billing::Result object, indicating
    # success or failure.
    def self.create_customer(target, payment_details)
      service = ::Billing::CreateCustomer.perform(target, payment_details)
      service.response
    end

    # Public: Create or Update a customer
    def self.create_or_update_customer(target, payment_details)
      if target.has_billing_record?
        self.update_payment_method(target, payment_details)
      else
        self.create_customer(target, payment_details)
      end
    end

    # Changes the plan subscription of the given user and possibly apply a
    # coupon. Records upgrades and downgrades in graphite and creates new
    # Transaction record.
    #
    # user          - A saved User or Organization record.
    # plan          - String name of the new plan.
    # actor         - Actor making the change
    # payment_details - A Hash of payment details:
    #   :coupon_code      - String coupon code (optional).
    #   :zuora_payment_method_id - The payment method ID returned by Zuora's Hosted Payment Page
    #   :billing_extra           - String extra billing information.
    #   :paypal_nonce            - String nonce representing a paypal account (optional).
    #   :billing_address         - The billing address as a Hash of optionally encrypted CC info
    #     * :country_name    - Country as a String
    #     * :region          - Region as a String
    #     * :postal_code     - Postal code as a String
    #
    # Returns a Result object, indicating success or failure,
    #   the user that was updated and an error message if the customer's plan
    #   could not be changed.
    def self.change_subscription(user, actor:, plan: nil, payment_details: nil, plan_duration: nil)
      payment_details ||= {}

      ::Billing::ChangeSubscription.perform \
        user,
        plan: plan,
        actor: actor,
        payment_details: payment_details,
        plan_duration: plan_duration
    end

    # Public: Change the number of paid seats for an organization. If seats
    # are added or removed, they will be charged/refunded accordingly.
    #
    # organization - Organization whose seats are changing.
    # options      - Hash of additional options.
    #               :seats - Integer number of seats.
    #               :actor - User performing the change.
    #
    # Returns a Billing::Result indicating success or failure
    def self.change_seats(organization, options)
      return ::Billing::ChangeSubscription.perform \
        organization,
        actor:      options[:actor],
        seats:      options[:seats],
        seat_delta: options[:seat_delta],
        plan_duration: options[:plan_duration]
    end

    # Has this user been migrated from Braintree Orange (legacy API)
    # over to Braintree Blue?
    #
    # user - a saved User (or Organization) record.
    #
    # Returns a Boolean.
    def self.migrated?(user)
      migration = BraintreeMigration.find_by_user_id(user.id)

      if migration
        !migration.completed_at.nil?
      else
        false
      end
    end

    # Finds and refunds multiple transactions until refund amount has been
    # reached.
    #
    # user            - The User to be refunded.
    # amount_in_cents - Integer amount in cents to be refunded. Can be positive or
    #                   negative. The absolute value will be used.
    #
    # Returns a GitHub::Billing::Result specifying the success or failure of
    # the refunds.
    def self.find_and_refund_transactions_for_amount(user, amount_in_cents)
      amount_in_cents = amount_in_cents.abs
      refunds = []
      refundable_sales = user.billing_transactions.refundable_sales

      while amount_in_cents > 0
        if transaction = refundable_sales.shift
          refunds << {
            amount_in_cents: [transaction.amount_in_cents, amount_in_cents].min,
            transaction: transaction,
          }

          amount_in_cents -= refunds.last[:amount_in_cents]
        else
          # This *should* never happen unless we've manually refunded or voided
          # transactions using Braintree. In these cases, we don't want to
          # return an error to the customer. Their money has already been
          # refunded. We'll log the error with Failbot so we can monitor these.
          message = "Not enough transactions to refund full amount"
          error = Error.new(message)
          error.set_backtrace(caller)
          Failbot.report(error)
          return Result.success
        end
      end

      results = refunds.map do |refund|
        result = void_or_refund_transaction(refund[:transaction], refund[:amount_in_cents])
        return result if result.failed? # Return early if we fail
        result
      end

      results.last
    end

    # Void or refund a transaction for the amount specified.
    #
    # transaction     - The transaction to be voided or refunded.
    # amount_in_cents - The amount to be refunded.
    #
    # Returns a GitHub::Billing::Result specifying the success or failure of
    # the void or refund.
    def self.void_or_refund_transaction(transaction, amount_in_cents = transaction.amount_in_cents)
      Failbot.push \
        areas_of_responsibility: [:gitcoin],
        billing_transaction_id: transaction.transaction_id

      if transaction.pending_status_update?
        transaction.update_status_from_processor
      end

      if transaction.submitted_for_settlement?
        # N.B. Voids will refund the entire amount.  This may result in "refunds"
        # for more than the expected amount. This would only ever be lost revenue
        # for service provided between now and when the transaction was
        # submitted for settlement - generally less than 3 days.
        void_transaction transaction.transaction_id
      else
        refund_transaction transaction.transaction_id, amount_in_cents
      end
    end

    # Returns true if old_plan to new_plan is an upgrade or the same price.
    #
    # old_plan - The old Plan they're moving from
    # new_plan - The new Plan they're moving to
    #
    # Retuns a Boolean.
    def self.upgrading?(old_plan, new_plan)
      new_plan.cost >= old_plan.cost
    end

    # Whether today is before the billing day?
    #
    # billing_day - Date they're billed on.
    #
    # Returns a Boolean.
    def self.before_billing_date?(billing_date)
      today.day < billing_date.day
    end

    # Whether today is after the billing day?
    #
    # billing_day - Date they're billed on.
    #
    # Returns a Boolean.
    def self.after_billing_date?(billing_date)
      today.day > billing_date.day
    end

    # Voids a transaction that has not settled yet.
    #
    # txn_id - External Transaction ID as a String.
    #
    # Returns a Result object
    def self.void_transaction(txn_id)
      Failbot.push(billing_transaction_id: txn_id)

      if txn = ::Billing::BillingTransaction.find_by_transaction_id(txn_id)
        txn.void
      else
        Result.failure("Transaction not found: #{txn_id}")
      end
    end

    # Refund up to the full amount of the given transaction ID. This works with
    # legacy Braintree (Orange), current Braintree (Blue), and Zuora accounts.
    #
    # txn_id                 - Transaction ID as a String.
    # user_id                - ID of GitHub user receiving the refund
    # refund_amount_in_cents - Integer amount in cents to refund. This must be
    #                          equal to or less than the full amount of the
    #                          transaction (optional, default: full transaction
    #                          amount).
    #
    # Returns a Result object.
    def self.refund_transaction(txn_id, refund_amount_in_cents = nil)
      Failbot.push(billing_transaction_id: txn_id)

      if txn = ::Billing::BillingTransaction.find_by_transaction_id(txn_id)
        txn.refund!(refund_amount_in_cents)
      else
        Result.failure("Transaction not found: #{txn_id}")
      end
    end

    # Blast emails to all users who have redeemed a coupon that will
    # expire in two weeks. This runs in the BillingCouponReminderJob job
    # that's enqueued daily by timerd.
    #
    # Graphite:
    #
    # billing.coupon_reminder.time  - Time it took to sends out the reminders.
    # billing.coupon_reminder.count - Number of users that were notified.
    #
    # Returns nothing.
    def self.send_coupon_reminders
      Failbot.push(areas_of_responsibility: [:gitcoin])
      GitHub.dogstats.time("billing.reminders.coupon_expiring_time") do
        logins = User.notify_coupon_expiring_in_two_weeks
        GitHub.dogstats.count("billing.reminders.coupon_expiring", logins.size)
      end
    end

    # Blast emails to all users who have a free trial that will
    # expire in 4 days. This runs in the BillingFreeTrialReminder Resque job
    # that's enqueued daily by timerd.
    #
    # Graphite:
    #
    # billing.free_trial_reminder.time  - Time it took to sends out the reminders.
    # billing.free_trial_ending.count - Number of users that were notified.
    #
    # Returns nothing.
    def self.send_free_trial_reminders
      Failbot.push(areas_of_responsibility: [:gitcoin])
      GitHub.dogstats.time("billing.reminders.free_trial_reminder_time") do
        total_reminders = User.notify_free_trial_ending.size
        GitHub.dogstats.count("billing.reminders.free_trial_ending", total_reminders)
      end
    end

    # Process a payment for a user.  If the user is on monthly billing, return
    # success and let the billing job handle payment. If the user is on yearly
    # billing charge the pro-rated mount.  If the user doesn't have an external
    # account, create one and let the charge happen in the billing run.
    #
    # ¿Is this more generally applicable?
    #
    # target          - User object
    # payment_details - Hash of payment details (optional):
    #                   :paypal_nonce     - String nonce representing a paypal account (optional).
    #                   :credit_card      - A Hash of potentially encrypted CC info (optional).
    #                     * :number           - CC number as a String.
    #                     * :expiration_month - Expiration month as a String of form MM
    #                     * :expiration_year  - Expiration year as a String of form YY
    #                     * :cvv              - CVV as a String (e.g. "420")
    #                   :billing_address  - The billing address as a Hash of optionally encrypted CC info
    #                     * :country_code_alpha3 - Country as a String
    #                     * :region              - Region as a String
    #                     * :postal_code         - Postal code as a String
    #
    # Returns a GitHub::Billing::Result indicating success or failure
    def self.process_payment_for_redemption(target, payment_details = nil)
      payment_details ||= {}

      if target.has_valid_payment_method?
        if target.yearly_plan?
          # No proration right now :(
          Result.success
        else
          Result.success
        end
      elsif GitHub::Billing::PaymentDetails.new(payment_details).valid?
        target.billed_on = today # TODO: Why are we setting this directly?
        target.save
        if target.has_billing_record?
          result = update_payment_method(target, payment_details.merge(charge: false))
        else
          result = create_customer(target, payment_details)
        end
        result.success? ? Result.success : Result.failure(result.error_message.to_s)
      else
        Result.failure("Credit card or other payment method required")
      end
    end

    def self.validate_signup_with_raise(user, plan_name, actor)
      if user.new_record?
        raise Error, "user is not saved"
      end

      if !user.valid?
        raise Error, "user is not valid"
      end

      plan = GitHub::Plan.find(plan_name)
      if plan.nil?
        raise Error, "unknown plan: #{plan_name.inspect}"
      end

      if plan.free?
        raise Error, "can not signup for free plan"
      end

      if user.has_billing_record?
        raise CustomerAlreadyExistsError, "user has already signed up"
      end

      if user.user? && plan.orgs?
        raise Error, "can not signup user for organization plan"
      end

      if user.organization? && !plan.orgs?
        raise Error, "can not signup organization for user plan"
      end

      if !plan.per_seat? && !user.can_change_plan_to?(plan, actor: actor)
        raise Error, "cannot move to a per-repository plan"
      end
    end

    # Validates whether a paid upgrade can happen.  Raises GitHub::Billing::Error
    # on any failure.
    #
    # target    - User or Organization that is upgrading to a paid plan without
    #               a billing record.
    # plan_name - the name of the plan to upgrade to
    #
    # Returns nothing.
    def self.validate_paid_upgrade(target, plan_name)
      plan = GitHub::Plan.find(plan_name)
      if plan.nil?
        raise Error, "unknown plan: #{plan_name}"
      end

      if target.organization? && !plan.orgs?
        raise Error, "organization can not use a user plan"
      end

      if plan.free?
        raise Error, "can not upgrade to a free plan"
      end
    end

    def self.transition_to_external_subscription(user)
      ::Billing::PlanSubscription::Transition.activate(user)
    end

    def self.has_payment_details?(payment_details)
      GitHub::Billing::PaymentDetails.new(payment_details).valid?
    end
  end
end
