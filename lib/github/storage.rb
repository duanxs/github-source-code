# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/mysql"
require "github/sql"

module GitHub
  module Storage
    autoload :Allocator,                "github/storage/allocator"
    autoload :Client,                   "github/storage/client"
    autoload :Command,                  "github/storage/command"
    autoload :Creator,                  "github/storage/creator"
    autoload :ClusterConsensus,         "github/storage/cluster_consensus"
    autoload :ClusterRepair,            "github/storage/cluster_repair"
    autoload :Destroyer,                "github/storage/destroyer"
    autoload :Maintenance,              "github/storage/maintenance"
    autoload :Offline,                  "github/storage/offline"
    autoload :Online,                   "github/storage/online"
    autoload :PartitionStats,           "github/storage/partition_stats"
    autoload :Rebalancer,               "github/storage/rebalancer"
    autoload :RebuildHostCommand,       "github/storage/rebuild_host_command"
    autoload :RepairUploadablesCommand, "github/storage/repair_uploadables_command"
    autoload :Replica,                  "github/storage/replica"
  end
end
