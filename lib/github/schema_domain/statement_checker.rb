# frozen_string_literal: true

# This class inspects SQL query statements for references to tables from different
# schema domains, reporting them or raising an error, depending on configuration, if so.
#
# See https://githubber.com/article/technology/dotcom/schema-domains for more context.
#
# Notes: The SQL "parsing" here is very limited. One trivial failure scenario is a name
# shared between a table and a column, for example.
class GitHub::SchemaDomain::StatementChecker
  QUERY_EXEMPTION_COMMENT_PATTERN = %r{/\*\s*cross-schema-domain-query-exempted\s*\*/}
  TRANSACTION_EXEMPTION_COMMENT_PATTERN = %r{/\*\s*cross-schema-domain-transaction-exempted\s*\*/}
  REMOVE_QUOTED_VALUES_REGEX = /(?<![\\])'(?:[^']|(?<=[\\])')*'/m.freeze

  # Only execute this check 1% of the time since it can be expensive
  EXECUTION_SAMPLE_RATE = 0.01
  # Only send timing metrics to datadog for 0.01% of all executions, after
  # accounting for EXECUTION_SAMPLE_RATE above.
  DOGSTATS_SAMPLE_RATE = 0.0001

  # A sentinel object returned by #check to indicate to callers that
  # the execution was skipped because of sampling.
  SKIPPED_BECAUSE_SAMPLING = Class.new

  EXCLUDED_TABLES = [
    "interactions", # The interactions table has columns `issues`, `releases`, etc., and is rarely used.
    "permissions", # The repository_invitations table has a column `permissions`.
    "downloads", # The release_assets table has a column `downloads`.
  ]

  # The return value for cross_domains_and_tables_referenced of a
  # query that doesn't violate any boundaries.
  EMPTY_RESULT = [[].freeze, [].freeze].freeze

  def self.table_names_regex
    @table_names_regex ||= Regexp.new("(?<=[,`\\s])(?:#{GitHub::SchemaDomain.tables.join("|")})(?=[,`\\s])", "i")
  end

  def initialize(checking_transactions: false, raise_errors: false)
    @checking_transactions = checking_transactions
    @raise_errors = raise_errors
    @execution_sample_rate = @raise_errors ? 1.0 : EXECUTION_SAMPLE_RATE

    # normalize this number to account for multiple-sampling reductions.  i.e.
    # in prod we sample at 0.01 to achieve 0.001 since 99% of executions are
    # skipped.
    @dogstats_sample_rate = DOGSTATS_SAMPLE_RATE / @execution_sample_rate
  end

  def checking_transactions?; @checking_transactions; end
  def raise_errors?; @raise_errors; end

  def check(queries:, report_errors: false)
    return SKIPPED_BECAUSE_SAMPLING if @execution_sample_rate != 1 && @execution_sample_rate < rand
    GitHub.dogstats.time(time_metric_name, sample_rate: @dogstats_sample_rate) do
      # Remove any text values that might incidentally contain table names or keywords.
      queries = remove_quoted_values_from_sql(queries)

      all_queries = queries.join("\n")
      crossed_domains, crossed_tables = cross_domains_and_tables_referenced(all_queries)

      if crossed_domains.any?
        exempted = exemption_comment_pattern.match?(all_queries)

        report_metrics(exempted, crossed_domains)

        return if exempted
        return if !report_errors && !raise_errors?

        this_stack = caller

        written_domains = cross_domains_written_to(queries)
        frame = GitHub::SchemaDomain::StackFilter.first_significant_frame(this_stack)

        queries = scrub_values_from_queries(queries)
        error = error_for(error_message(queries, crossed_domains, written_domains, crossed_tables, frame))

        if report_errors
          error.set_backtrace(this_stack)
          Failbot.report!(error, app: "github-schema-domains", rollup: rollup_for(queries))
        end

        if raise_errors?
          raise error
        end
      end
    end
  end

  private

    def exemption_comment_pattern
      if checking_transactions?
        TRANSACTION_EXEMPTION_COMMENT_PATTERN
      else
        QUERY_EXEMPTION_COMMENT_PATTERN
      end
    end

    def report_metrics(exempted, domains)
      metric_name = if checking_transactions?
        "schema_domains.cross_domain_transaction"
      else
        "schema_domains.cross_domain_query"
      end

      domains.each do |domain|
        GitHub.dogstats.increment(metric_name, tags: ["exempted:#{exempted}", "domain:#{domain}"])
      end
    end

    def time_metric_name
      if checking_transactions?
        "schema_domains.cross_domain_transactions_check"
      else
        "schema_domains.cross_domain_queries_check"
      end
    end

    def cross_domains_and_tables_referenced(query)
      queried_tables = referenced_tables_from_sql(query)
      queried_tables -= EXCLUDED_TABLES

      return EMPTY_RESULT if GitHub::SchemaDomain.same?(queried_tables)

      queried_domains = queried_tables.each_with_object(Set.new) do |table, domains|
        domains << GitHub::SchemaDomain.for(table).to_s
      end

      return [queried_domains, queried_tables]
    end

    # Replace substrings between single-quotes ('), allowing for escaped single quotes
    def remove_quoted_values_from_sql(queries)
      queries.map do |query|
        query.gsub(REMOVE_QUOTED_VALUES_REGEX, "'...'")
      end
    end

    def scrub_values_from_queries(queries)
      queries.map { |query| GitHub::SQL::Digester.digest_sql(query) }
    end

    def referenced_tables_from_sql(query)
      query.scan(self.class.table_names_regex).uniq
    end

    def cross_domain_queries_from_transactions_sql(query)
      query.scan("")
    end

    def cross_domains_written_to(queries)
      write_queries = queries.select do |query|
        query.match?(/\A\s*(INSERT|UPDATE)/i)
      end

      written_domains = write_queries.each_with_object(Set.new) do |write_query, domains|
        written_tables = referenced_tables_from_sql(write_query)
        written_tables -= EXCLUDED_TABLES

        written_tables.each do |table|
          domains << GitHub::SchemaDomain.for(table).to_s
        end
      end

      written_domains
    end

    def error_for(message)
      klass = if checking_transactions?
        GitHub::SchemaDomain::CrossDomainTransactionError
      else
        GitHub::SchemaDomain::CrossDomainQueryError
      end

      klass.new(message)
    end

    def error_message(queries, crossed_domains, written_domains, tables, frame)
      grouped_by_domain = tables.group_by { |table| GitHub::SchemaDomain.for(table).to_s }
      table_detail = grouped_by_domain.transform_keys { |domain| "From `#{domain}` domain" }.to_yaml.delete_prefix("---").chomp
      String.new("\n\n") << <<~MSG
        Unsafe mix of tables from different schema domains (`#{grouped_by_domain.keys.sort.join("`, `")}`) in #{"transactional " if checking_transactions?}query:
        #{table_detail}
        #{"Written to multiple domains: `#{written_domains.sort.join("`, `")}`\n" if written_domains.size > 1}
        #{queries.join("\n")}

        These tables will be separated at the schema level in the (near) future.

        Please see https://githubber.com/article/technology/dotcom/schema-domains#what-to-do-when-a-query-would-cross-domain-boundaries for more information and guidance on how to fix this.

        Called from `#{frame}`.
      MSG
    end

    def rollup_for(queries)
      Digest::MD5.hexdigest(queries.join("\n"))
    end
end
