# rubocop:disable Style/FrozenStringLiteralComment

require "digest/sha1"

module GitHub
  module Unsullied
    module HTML
      # Extract all code blocks into the codemap and replace with placeholders.
      class ExtractCodeBlocksFilter < ::HTML::Pipeline::TextFilter
        CodeBlockRegex = /^([ \t]*)``` ?([^\r\n]+)?\r?\n(.+?)\r?\n\1```\r?$/m

        def call
          return @text if context[:page].format == :markdown

          result[:code_blocks] = {}

          @text.gsub!(CodeBlockRegex) do |match|
            id = Digest::SHA1.hexdigest("#{$2}.#{$3}")
            result[:code_blocks][id] = {lang: $2, code: $3, indent: $1}
            "#{$1}#{id}" # print the SHA1 ID with the proper indentation
          end

          @text
        end
      end
    end
  end
end
