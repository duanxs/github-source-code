# frozen_string_literal: true

module GitHub
  module Unsullied
    module HTML
      # Replaces the source of home relative images in a wiki to have the "wiki/" prefix
      #
      #   Example: <img src="path.jpg">
      #   Becomes: <img src="wiki/path.jpg">
      #
      #
      class RelativeImageFilter < ::HTML::Pipeline::Filter

        def call
          if context[:prefix_relative_links]
            doc.search("img").each do |node|
              next unless node.attributes["src"]
              value = node.attributes["src"].value

              next if value.start_with?("wiki/") # handles ![](wiki/images/image.png)
              next if value.match(/\/wiki\//) # handles ![](../repo-name/wiki/images/image.png)
              next if value.match(%r{^[a-z][a-z0-9\+\.\-]+:}i)  # RFC 3986 <3 U
              next if value.match(%r{^//?})
              next if value.match(/^#/)

              node.attributes["src"].value = "wiki/#{value}"
            end
          end

          doc
        end
      end
    end
  end
end
