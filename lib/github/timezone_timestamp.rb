# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module TimezoneTimestamp

    # Parse ISO8601 timestamp. This has to be done manually
    # because Ruby's default Time.iso8601 always returns a Time
    # instance in UTC. This loses the timezone offset information
    # that is available in the original string.
    #
    # This custom version makes sure we keep this information and
    # we build a Time object with the proper UTC offset.
    def self.iso8601(date)
      if /\A\s*
          (-?\d+)-(\d\d)-(\d\d)
          T
          (\d\d):(\d\d):(\d\d)
          (\.\d+)?
          (Z|[+-]\d\d:\d\d)?
          \s*\z/x =~ date
        year = $1.to_i
        mon = $2.to_i
        day = $3.to_i
        hour = $4.to_i
        min = $5.to_i
        sec = $6.to_i
        usec = 0
        if $7
          usec = Rational($7) * 1000000
        end
        if tz = $8
          if tz == "Z"
            Time.utc(year, mon, day, hour, min, sec, usec)
          else
            Time.new(year, mon, day, hour, min, sec + usec / 100000.0, $8)
          end
        else
          Time.local(year, mon, day, hour, min, sec, usec)
        end
      else
        raise ArgumentError.new("invalid date: #{date.inspect}")
      end
    end

    def self.included(base) #:nodoc:
      base.extend(ClassMethods)
    end

    module ClassMethods
      def timezone_timestamp(*fields)
        fields.each do |field|
          self.class_eval <<-RUBY, __FILE__, __LINE__
            def #{field}
              if ts = read_attribute(#{field.inspect}_timestamp)
                Time.at(ts).localtime(read_attribute(#{field.inspect}_offset))
              end
            end

            def #{field}=(value)
              if value
                write_attribute(#{field.inspect}_timestamp, value.to_i)
                write_attribute(#{field.inspect}_offset, value.utc_offset)
              else
                write_attribute(#{field.inspect}_timestamp, nil)
                write_attribute(#{field.inspect}_offset, nil)
              end
            end
          RUBY
        end
      end
    end
  end
end
