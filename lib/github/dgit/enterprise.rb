# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/mysql"
require "github/dgit"
require "application_record"

module GitHub
  class DGit
    class Enterprise
      MAX_SQL_ROWS = 100

      def self.archived_replicas(network_id, repo_id, repo_types = GitHub::DGit::RepoType::REPO_ISH)
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?
        raise ArgumentError, "invalid network id" if !network_id.is_a?(Integer)
        raise ArgumentError, "invalid repo id" if !repo_id.is_a?(Integer)

        db = GitHub::DGit::DB.for_network_id(network_id)

        sql = db.SQL.new \
          network_id: network_id,
          repo_id: repo_id,
          repo_types: repo_types
        sql.add <<-SQL
          SELECT fs.host, fs.ip, fs.evacuating, fs.datacenter, fs.rack, fs.online, fs.embargoed, fs.non_voting, fs.hdd_storage, fs.quiescing, arr.checksum, arr.created_at, arr.updated_at
            FROM archived_repository_replicas arr
            JOIN fileservers fs ON arr.host = fs.host
            WHERE arr.repository_id = :repo_id
              AND arr.repository_type IN :repo_types
          /* cross-schema-domain-query-exempted */
        SQL

        results = []

        sql.hash_results.each do |row|
          fileserver = Fileserver.new \
            name: row["host"],
            ip: row["ip"],
            evacuating: row["evacuating"] == 1,
            datacenter: row["datacenter"] || GitHub.default_datacenter,
            rack: row["rack"] || GitHub.default_rack,
            online: row["online"] == 1,
            embargoed: row["embargoed"] == 1,
            voting: row["non_voting"] != 1,
            hdd_storage: row["hdd_storage"] == 1
          replica = Replica::Repo.new \
            db_name: db.name,
            fileserver: fileserver,
            read_weight: 0,
            quiescing: row["quiescing"] == 1,
            state: nil,
            checksum: row["checksum"],
            expected_checksum: row["checksum"],
            created_at: row["created_at"],
            updated_at: row["updated_at"],
            db_network_replica_id: nil
          results << replica
        end

        results
      end

      def self.archived_repo_replicas(network_id, repo_id)
        archived_replicas(network_id, repo_id, [GitHub::DGit::RepoType::REPO])
      end

      def self.delete_archived_replicas_and_checksums(network_id, repo_id, is_wiki)
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?
        raise ArgumentError, "invalid network id" if !network_id.is_a?(Integer)
        raise ArgumentError, "invalid repo id" if !repo_id.is_a?(Integer)

        t = is_wiki ? GitHub::DGit::RepoType::WIKI
                    : GitHub::DGit::RepoType::REPO

        db = GitHub::DGit::DB.for_network_id(network_id)
        ActiveRecord::Base.connected_to(role: :writing) do
          db.transaction do
            sql = db.SQL.new \
              network_id: network_id,
              repo_id: repo_id,
              repo_type: t
            sql.add <<-SQL
              DELETE FROM archived_repository_replicas
                    WHERE repository_id = :repo_id
                      AND repository_type = :repo_type
            SQL
            sql.run

            sql = db.SQL.new \
              network_id: network_id,
              repo_id: repo_id,
              repo_type: t
            sql.add <<-SQL
              DELETE FROM archived_repository_checksums
                    WHERE repository_id = :repo_id
                      AND repository_type = :repo_type
            SQL
            sql.run
          end
        end
      end

      def self.archived_gist_replicas(gist_id)
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?
        raise ArgumentError, "invalid gist id" if !gist_id.is_a?(Integer)

        db = GitHub::DGit::DB.for_gist_id(gist_id)

        sql = db.SQL.new \
          gist_id: gist_id,
          repo_type: GitHub::DGit::RepoType::GIST
        sql.add <<-SQL
          SELECT agr.gist_id, agr.host, agr.read_weight, fs.quiescing, fs.online, agr.state, agr.checksum, NULL, fs.evacuating, fs.datacenter, fs.rack, fs.embargoed, fs.ip, fs.non_voting, fs.hdd_storage, agr.created_at, agr.updated_at
            FROM archived_gist_replicas agr
            JOIN fileservers fs ON agr.host = fs.host
            WHERE agr.gist_id = :gist_id
          /* cross-schema-domain-query-exempted */
        SQL

        results = []

        sql.results.each do |row|
          gist_id = row[0]
          fileserver = Fileserver.new \
            name: row[1],
            ip: row[12],
            evacuating: row[8] == 1,
            datacenter: row[9] || GitHub.default_datacenter,
            rack: row[10] || GitHub.default_rack,
            online: row[4] == 1,
            embargoed: row[11] == 1,
            voting: row[13] != 1,
            hdd_storage: row[14] == 1
          replica = Replica::Gist.new \
            db_name: db.name,
            fileserver: fileserver,
            read_weight: row[2],
            quiescing: row[3] == 1,
            state: row[5],
            checksum: row[6],
            expected_checksum: row[7],
            created_at: row[15],
            updated_at: row[16]
          results << replica
        end

        results
      end

      def self.delete_archived_gist_replicas_and_checksums(gist_id)
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?
        raise ArgumentError, "invalid gist id" if !gist_id.is_a?(Integer)

        ActiveRecord::Base.connected_to(role: :writing) do
          gist_db = GitHub::DGit::DB.for_gist_id(gist_id)
          gist_db.transaction do
            gist_db.SQL.run(<<-SQL, gist_id: gist_id)
              DELETE FROM archived_gist_replicas
                    WHERE gist_id = :gist_id
            SQL

            gist_db.SQL.run(<<-SQL, gist_id: gist_id, repo_type: GitHub::DGit::RepoType::GIST)
              DELETE FROM archived_repository_checksums
                    WHERE repository_id = :gist_id
                      AND repository_type = :repo_type
            SQL
          end
        end
      end

      # Returns network paths and hosts for all repos and gists with maximum read_weight.
      def self.all_network_paths_with_max_read_weight
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?

        networks = {}
        gists = {}

        sql = ApplicationRecord::Domain::Repositories.github_sql.new <<-SQL
          SELECT rn2.id, nr2.host FROM
          (  SELECT DISTINCT rn1.id,  MAX(nr1.read_weight) as max_weight
             FROM repository_networks rn1
             JOIN network_replicas nr1 on nr1.network_id = rn1.id
             INNER JOIN fileservers f1 on f1.host = nr1.host
             WHERE nr1.state = 1 AND f1.online = 1
             GROUP BY rn1.id
          ) as x
          INNER JOIN repository_networks rn2 on rn2.id = x.id
          INNER JOIN network_replicas nr2 on nr2.network_id = rn2.id
          INNER JOIN fileservers f2 on f2.host = nr2.host
          WHERE nr2.read_weight = x.max_weight AND nr2.state = 1 AND f2.online = 1
          /* cross-schema-domain-query-exempted */
        SQL

        sql.results.each do |r|
          networks[r[0]] = "#{GitHub::Routing.nw_storage_path(network_id: r[0])} #{r[1]}"
        end

        # Get all archived networks, skip any network already scheduled to be backed up.
        sql = ApplicationRecord::Domain::Repositories.github_sql.new <<-SQL
          SELECT ar.source_id, MIN(arr.host)
            FROM archived_repositories ar
            INNER JOIN archived_repository_replicas arr ON arr.repository_id = ar.id
            INNER JOIN fileservers f ON f.host = arr.host
            LEFT JOIN network_replicas nr ON nr.network_id = ar.source_id
            WHERE f.online = 1
            GROUP BY ar.source_id
            /* cross-schema-domain-query-exempted */
        SQL

        sql.results.each do |r|
          if networks[r[0]].nil?
            networks[r[0]] = "#{GitHub::Routing.nw_storage_path(network_id: r[0])} #{r[1]}"
          end
        end

        sql = ApplicationRecord::Domain::Gists.github_sql.new <<-SQL
          SELECT g2.id, gr2.host, g2.repo_name FROM
          ( SELECT DISTINCT gr.gist_id, NULL, gr.host, g.repo_name, MAX(gr.read_weight) as max_weight
            FROM gist_replicas gr
            INNER JOIN gists g on g.id = gr.gist_id
            INNER JOIN fileservers f1 ON f1.host = gr.host
            WHERE gr.state = 1 AND f1.online = 1
            GROUP BY gr.gist_id
          ) as y
          INNER JOIN gist_replicas gr2 on gr2.gist_id = y.gist_id
          INNER JOIN gists g2 on g2.id = gr2.gist_id
          INNER JOIN fileservers f2 ON f2.host = gr2.host
          WHERE gr2.read_weight = y.max_weight AND gr2.state = 1 AND f2.online = 1
          /* cross-schema-domain-query-exempted */
        SQL

        sql.results.each do |r|
          gists[r[0]] = "#{Pathname(GitHub::Routing.gist_storage_path(repo_name: r[2]))} #{r[1]}"
        end

        sql = ApplicationRecord::Domain::Gists.github_sql.new <<-SQL
          SELECT ag2.id, agr2.host, ag2.repo_name FROM
          ( SELECT DISTINCT agr.gist_id, NULL, agr.host, ag.repo_name, MAX(agr.read_weight) as max_weight
            FROM archived_gist_replicas agr
            INNER JOIN archived_gists ag on ag.id = agr.gist_id
            INNER JOIN fileservers f1 ON f1.host = agr.host
            WHERE agr.state = 1 AND f1.online = 1
            GROUP BY agr.gist_id
          ) as z
          INNER JOIN archived_gist_replicas agr2 on agr2.gist_id = z.gist_id
          INNER JOIN archived_gists ag2 on ag2.id = agr2.gist_id
          INNER JOIN fileservers f2 ON f2.host = agr2.host
          WHERE agr2.read_weight = z.max_weight AND agr2.state = 1 AND f2.online = 1
          /* cross-schema-domain-query-exempted */
        SQL

        sql.results.each do |r|
          if gists[r[0]].nil?
            gists[r[0]] = "#{Pathname(GitHub::Routing.gist_storage_path(repo_name: r[2]))} #{r[1]}"
          end
        end

        networks.values + gists.values
      end

      def self.remove_archived_repo_replica_routes(network_id, repo_type)
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?
        raise ArgumentError, "invalid network id" if !network_id.is_a?(Integer)

        sql = ApplicationRecord::Domain::Repositories.github_sql.new <<-SQL, network_id: network_id
          SELECT id
          FROM archived_repositories
          WHERE source_id = :network_id
        SQL
        sql.results.each do |row|
          repo_id = row.first
          is_wiki = repo_type == GitHub::DGit::RepoType::WIKI
          GitHub::DGit::Enterprise::delete_archived_replicas_and_checksums(network_id, repo_id, is_wiki)
        end
      end

      def self.remove_archived_gist_replica_routes(gist_id)
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?
        raise ArgumentError, "invalid gist id" if !gist_id.is_a?(Integer)

        sql = ApplicationRecord::Domain::Gists.github_sql.new <<-SQL, gist_id: gist_id
          SELECT id
          FROM archived_gists
          WHERE id = :gist_id
        SQL
        sql.results.each do |row|
          gist_id = row.first
          GitHub::DGit::Enterprise::delete_archived_gist_replicas_and_checksums(gist_id)
        end
      end

      # Take a list of repository replica entries (rows), filter out all unarchived repositories
      # and then insert the archived repository entries into the archived_repository_replicas
      # and archived_repository_checksums tables.
      def self.add_archived_repo_replica_routes(rows, network_id, update_existing)
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?

        archived_rows = []
        rows.each do |repo_id, repo_type, network_replica_id, host, checksum, created_at, updated_at|
          if ApplicationRecord::Domain::Repositories.github_sql.new("SELECT 1 FROM archived_repositories WHERE source_id=:id LIMIT 1", id: repo_id).value?
            archived_rows << [repo_id, repo_type, host, checksum, created_at, updated_at]
          end
        end

        query = <<-SQL
          INSERT INTO archived_repository_replicas
            (repository_id, repository_type, host, checksum, created_at, updated_at)
          VALUES :rows
        SQL

        if update_existing
          query << <<-SQL
            ON DUPLICATE KEY UPDATE
              updated_at = VALUES(updated_at),
              checksum   = VALUES(checksum)
          SQL
        end

        db = GitHub::DGit::DB.for_network_id(network_id)

        archived_rows.each_slice(MAX_SQL_ROWS) do |some|
          ActiveRecord::Base.connected_to(role: :writing) do
            db.throttle do
              sql = db.SQL.new query, network_id: network_id, rows: GitHub::SQL::ROWS(some)
              sql.run
            end
          end
        end
      end

      # Take a list of gist replica entries (rows), filter out all unarchived gists
      # and then insert the archived gist entries into the archived_gist_replicas
      # and archived_gists_checksums tables.
      def self.add_archived_gist_replica_routes(rows, gist_id, update_existing)
        raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?

        archived_rows = rows.select do |gist_id, host, checksum, state, read_weight, created_at, updated_at|
          ApplicationRecord::Domain::Gists.github_sql.new("SELECT 1 FROM archived_gists WHERE id=:id LIMIT 1", id: gist_id).value?
        end

        query = <<-SQL
          INSERT INTO archived_gist_replicas
            (gist_id,  host, checksum, state, read_weight, created_at, updated_at)
          VALUES :rows
        SQL

        if update_existing
          query << <<-SQL
            ON DUPLICATE KEY UPDATE
              updated_at = VALUES(updated_at),
              checksum   = VALUES(checksum)
          SQL
        end

        db = GitHub::DGit::DB.for_gist_id(gist_id)
        archived_rows.each_slice(MAX_SQL_ROWS) do |some|
          ActiveRecord::Base.connected_to(role: :writing) do
            db.throttle do
              sql = db.SQL.new query, gist_id: gist_id, rows: GitHub::SQL::ROWS(some)
              sql.run
            end
          end
        end
      end

      # Remove all replica routes for the given repo network or gist.
      def self.remove_replica_routes(repo_type, id)
        case repo_type
        # normal repositories and wikis are only ever migrated
        # together, so they are both handled by the REPO case
        when GitHub::DGit::RepoType::REPO
          remove_repo_replica_routes(id)
        when GitHub::DGit::RepoType::GIST
          remove_gist_replica_routes(id)
        else
          desc = GitHub::DGit::RepoType::REPO_TYPES[repo_type]
          raise StandardError, "unhandled repo_type: #{repo_type} #{desc}"
        end
      end

      def self.delete_checksums_for_network(network_id)
        db = GitHub::DGit::DB.for_network_id(network_id)
        GitHub::DGit::Util.repo_ids_for(network_id).each_slice(MAX_SQL_ROWS) do |some|
          ActiveRecord::Base.connected_to(role: :writing) do
            db.throttle do
              sql = db.SQL.new \
                             network_id: network_id,
              types: GitHub::DGit::RepoType::REPO_ISH,
              ids: some
              sql.run <<-SQL
                DELETE FROM repository_checksums
                  WHERE repository_checksums.repository_id IN :ids
                    AND repository_checksums.repository_type IN :types
              SQL
              $stderr.puts "Deleted #{sql.affected_rows} from repository_checksums" unless GitHub.enterprise?
            end
          end
        end
      end

      # Remove all replica routes for a given repo network.
      def self.remove_repo_replica_routes(network_id)
        db = GitHub::DGit::DB.for_network_id(network_id)

        ActiveRecord::Base.connected_to(role: :writing) do
          db.SQL.run("DELETE FROM network_replicas WHERE network_id = :network_id",
                     network_id: network_id)
        end # connected_to

        GitHub::DGit::Util.repo_ids_for(network_id).each_slice(MAX_SQL_ROWS) do |some|
          ActiveRecord::Base.connected_to(role: :writing) do
            db.throttle do
              sql = db.SQL.new \
                network_id: network_id,
                types: GitHub::DGit::RepoType::REPO_ISH,
                ids: some
              sql.run <<-SQL
                DELETE FROM repository_replicas
                  WHERE repository_replicas.repository_id IN :ids
                    AND repository_replicas.repository_type IN :types
              SQL
              $stderr.puts "Deleted #{sql.affected_rows} from repository_replicas" unless GitHub.enterprise?
            end # db.throttle
          end # connected_to
        end # each_slice

        delete_checksums_for_network(network_id)
      end

      # Remove all replica routes for a given gist.
      def self.remove_gist_replica_routes(gist_id)
        ActiveRecord::Base.connected_to(role: :writing) do
          gist_db = GitHub::DGit::DB.for_gist_id(gist_id)
          gist_db.SQL.run("DELETE FROM gist_replicas WHERE gist_id = :gist_id",
                          gist_id: gist_id)
          gist_db.SQL.run(<<-SQL, gist_id: gist_id, repo_type: GitHub::DGit::RepoType::GIST)
            DELETE repository_checksums FROM repository_checksums
              WHERE repository_checksums.repository_id=:gist_id
                AND repository_checksums.repository_type=:repo_type
          SQL
        end
      end

      def self.add_replica_routes(logger, repo_type, id, hosts, checksums, update_existing = false)
        case repo_type
        # normal repositories and wikis are only ever migrated
        # together, so they are both handled by the REPO case
        when GitHub::DGit::RepoType::REPO
          add_repo_replica_routes(logger, id, hosts,
                                  checksums[GitHub::DGit::RepoType::REPO],
                                  checksums[GitHub::DGit::RepoType::WIKI],
                                  update_existing)
        when GitHub::DGit::RepoType::GIST
          add_gist_replica_routes(logger, id, hosts,
                                  checksums[GitHub::DGit::RepoType::GIST],
                                  update_existing)
        else
          desc = GitHub::DGit::RepoType::REPO_TYPES[repo_type]
          raise StandardError, "unhandled repo_type: #{repo_type} #{desc}"
        end
      end

      # Insert the given set of repository_checksums records.  If update_existing
      # is true, an insert that would result in a duplicate key update will only
      # update the checksum and updated_at values.
      #
      #  - network_id:       network ID for the given repositories
      #  - rows:             list of [repository_id, repository_type, checksum,
      #                      created_at, updated_at] tuples suitable for GitHub::SQL
      #  - update_existing:  set to update checksum and updated_at upon duplicate key
      def self.insert_repository_checksums(network_id, rows, update_existing = false)
        query = <<-SQL
          INSERT INTO repository_checksums
            (repository_id, repository_type, checksum, created_at, updated_at)
          VALUES :rows
        SQL

        if update_existing
          query << <<-SQL
            ON DUPLICATE KEY UPDATE
              updated_at = VALUES(updated_at),
              checksum   = VALUES(checksum)
          SQL
        end

        db = GitHub::DGit::DB.for_network_id(network_id)
        ActiveRecord::Base.connected_to(role: :writing) do
          rows.each_slice(MAX_SQL_ROWS) do |some|
            db.throttle do
              sql = db.SQL.new query, network_id: network_id, rows: GitHub::SQL::ROWS(some)
              sql.run
              $stderr.puts "Added #{sql.affected_rows} to repository_checksums" unless GitHub.enterprise?
            end # throttle
          end # each_slice
        end # connected_to
      end

      def self.insert_gist_checksums(gist_id, rows, update_existing = false)
        query = <<-SQL
          INSERT INTO repository_checksums
            (repository_id, repository_type, checksum, created_at, updated_at)
          VALUES :rows
        SQL

        if update_existing
          query << <<-SQL
            ON DUPLICATE KEY UPDATE
              updated_at = VALUES(updated_at),
              checksum   = VALUES(checksum)
          SQL
        end

        db = GitHub::DGit::DB.for_gist_id(gist_id)
        ActiveRecord::Base.connected_to(role: :writing) do
          rows.each_slice(MAX_SQL_ROWS) do |some|
            db.throttle do
              sql = db.SQL.new query, gist_id: gist_id, rows: GitHub::SQL::ROWS(some)
              sql.run
              $stderr.puts "Added #{sql.affected_rows} to repository_checksums" unless GitHub.enterprise?
            end # throttle
          end # each_slice
        end # connected_to
      end

      # Add entries for a repo network to all three DGit tables:
      # network_replicas, repository_replicas, and repository_checksums.
      #  - logger:      any Logger object
      #  - network_id:  the network to route in DGit
      #  - hosts:            an array of hosts (as strings) where the network
      #                      will live
      #  - repo_checksums:   a hash mapping {repo_id => checksum} for all repo
      #                      forks in the network
      #  - wiki_checksums:   a hash mapping {repo_id => checksum} for all wiki
      #                      forks in the network
      # Note that the SQL tables have a separate checksum entry for each
      # replica -- i.e., <repo_id,host> tuple -- even though the
      # `checksums` has only one entry for each repo_id.  That's because
      # we wouldn't have gotten here unless the hosts agreed on the
      # checksum for each fork.
      def self.add_repo_replica_routes(logger, network_id, hosts, repo_checksums, wiki_checksums, update_existing = false)
        weights = GitHub::DGit.alloc_read_weight(hosts)
        logger.info "adding network route to #{hosts.inspect} with weights=#{weights.inspect}"
        rows = hosts.map do |host|
          [network_id, host, GitHub::DGit::ACTIVE, weights[host],
           GitHub::SQL::NOW, GitHub::SQL::NOW]
        end
        query = <<-SQL
          INSERT INTO network_replicas
            (network_id, host, state, read_weight, created_at, updated_at)
          VALUES :rows
        SQL

        if update_existing
          query << <<-SQL
          ON DUPLICATE KEY UPDATE
            updated_at = VALUES(updated_at),
            state      = VALUES(state)
          SQL
        end

        db = GitHub::DGit::DB.for_network_id(network_id)

        network_replica_id = ActiveRecord::Base.connected_to(role: :writing) do
          sql = db.SQL.new query, network_id: network_id, rows: GitHub::SQL::ROWS(rows)
          sql.run
          sql.last_insert_id
        end

        num_forks = repo_checksums.keys.length
        logger.info "adding repo route for #{num_forks} fork(s) to #{hosts.inspect}"
        rows = []
        repo_checksums.each do |repo_id, checksum|
          hosts.each do |host|
            rows << [repo_id, GitHub::DGit::RepoType::REPO, network_replica_id, host, checksum, GitHub::SQL::NOW, GitHub::SQL::NOW]
          end
        end

        # only add wiki replicas where the wiki exists on disk (non-nil checksum)
        wiki_checksums = wiki_checksums.select { |repo_id, checksum| !checksum.nil? }
        wiki_checksums.each do |repo_id, checksum|
          hosts.each do |host|
            rows << [repo_id, GitHub::DGit::RepoType::WIKI, network_replica_id, host, checksum, GitHub::SQL::NOW, GitHub::SQL::NOW]
          end
        end

        query = <<-SQL
          INSERT INTO repository_replicas
            (repository_id, repository_type, network_replica_id, host, checksum, created_at, updated_at)
          VALUES :rows
        SQL

        if update_existing
          query << <<-SQL
            ON DUPLICATE KEY UPDATE
              updated_at = VALUES(updated_at),
              checksum   = VALUES(checksum)
          SQL
        end

        rows.each_slice(MAX_SQL_ROWS) do |some|
          ActiveRecord::Base.connected_to(role: :writing) do
            db.throttle do
              sql = db.SQL.new query, network_id: network_id, rows: GitHub::SQL::ROWS(some)
              sql.run
              $stderr.puts "Added #{sql.affected_rows} to repository_replicas" unless GitHub.enterprise?
            end
          end
        end

        # Archived repositories on Enterprise
        GitHub::DGit::Enterprise::add_archived_repo_replica_routes(rows, network_id, update_existing) if GitHub.enterprise?

        rows = repo_checksums.map do |repo_id, checksum|
          [repo_id, GitHub::DGit::RepoType::REPO, checksum, GitHub::SQL::NOW, GitHub::SQL::NOW]
        end

        wiki_checksums.each do |repo_id, checksum|
          rows << [repo_id, GitHub::DGit::RepoType::WIKI, checksum, GitHub::SQL::NOW, GitHub::SQL::NOW]
        end

        insert_repository_checksums(network_id, rows, update_existing)
      end

      # Add entries for a gist to its DGit tables:
      # gist_replicas, and repository_checksums.
      #  - logger:      any Logger object
      #  - gist_id:     the gist to route in DGit
      #  - hosts:           an array of hosts (as strings) where the gist
      #                     will live
      #  - gist_checksum:   a string checksum for the gist
      def self.add_gist_replica_routes(logger, gist_id, hosts, gist_checksum, update_existing = false)
        weights = GitHub::DGit.alloc_read_weight(hosts)
        logger.info "adding gist route to #{hosts.inspect} with weights=#{weights.inspect}"
        rows = hosts.map do |host|
          [gist_id, host, gist_checksum, GitHub::DGit::ACTIVE, weights[host],
            GitHub::SQL::NOW, GitHub::SQL::NOW]
        end
        query = <<-SQL
          INSERT INTO gist_replicas
            (gist_id, host, checksum, state, read_weight, created_at, updated_at)
          VALUES :rows
        SQL

        if update_existing
          query << <<-SQL
          ON DUPLICATE KEY UPDATE
            updated_at = VALUES(updated_at),
            state      = VALUES(state)
          SQL
        end

        # Insert archived gist replica routes to their own table on Enterprise
        if GitHub.enterprise? && ApplicationRecord::Domain::Gists.github_sql.new("SELECT 1 FROM archived_gists WHERE id=:id LIMIT 1", id: gist_id).value?
          GitHub::DGit::Enterprise::add_archived_gist_replica_routes(rows, gist_id, update_existing)
        else
          ActiveRecord::Base.connected_to(role: :writing) do
            GitHub::DGit::DB.for_gist_id(gist_id).SQL.run query, rows: GitHub::SQL::ROWS(rows), gist_id: gist_id
          end
          rows = [
           [gist_id, GitHub::DGit::RepoType::GIST, gist_checksum, GitHub::SQL::NOW, GitHub::SQL::NOW],
          ]

          insert_gist_checksums(gist_id, rows, update_existing)
        end
      end

    end # Enterprise
  end #DGit
end # GitHub
