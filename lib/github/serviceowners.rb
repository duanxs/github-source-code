# frozen_string_literal: true

module GitHub
  class Serviceowners
    def initialize(contents)
      parse(contents)
    end

    def service_for_class(klass)
      return if klass.blank?
      return if @patterns_services.blank?
      return @class_services_cache[klass] if @class_services_cache.has_key?(klass)

      path = ActiveSupport::Dependencies.search_for_file(klass.name.underscore)
      return if path.blank?

      service = service_for_path(path)
      @class_services_cache[klass] = service
      service
    end

    def service_for_path(path, prefix: false)
      return if @patterns_services.blank?
      return if path.blank?

      service = @patterns_services[path]
      service ||= begin
        if (pattern = @patterns_services.keys.find { |p| File.fnmatch(p, path) })
          @patterns_services[pattern]
        end
      end

      if prefix && service
        "#{GitHub::ServiceMapping::SERVICE_PREFIX}/#{service}"
      else
        service
      end
    end

    def services(prefix: false)
      return [] if @patterns_services.blank?

      result = @patterns_services.values
        .sort
        .uniq

      return result unless prefix
      result.map { |service| "#{GitHub::ServiceMapping::SERVICE_PREFIX}/#{service}" }
    end

    def directory_patterns
      # This is only used in CI and testing.
      return if Rails.env.production?

      @patterns_services.each_key.select do |pattern|
        next true if pattern.end_with?("/")
        next false if pattern.include?("*")

        File.directory?(pattern)
      end.map do |pattern|
        pattern.delete_prefix("#{Rails.root}/")
      end
    end

    def no_files_and_multiple_services_patterns
      # This is only used in CI and testing.
      return if Rails.env.production?

      pattern_matches = Hash.new { |h, k| h[k] = [] }
      file_matches = Hash.new { |h, k| h[k] = [] }

      git_files = IO.popen(["git", "ls-files", Rails.root.to_s])
                    .read
                    .split("\n")

      @patterns_services.each do |pattern, service|
        files = Dir.glob(pattern)
        pattern = pattern.delete_prefix("#{Rails.root}/")
        pattern_matches[pattern] += files
        files.each do |file|
          file = file.delete_prefix("#{Rails.root}/")
          next unless git_files.include?(file)

          file_matches[file] << [pattern, service]
        end
      end

      no_files_patterns = []
      pattern_matches.each do |pattern, files|
        next unless files.empty?

        no_files_patterns << pattern
      end

      multiple_services_patterns = Hash.new { |h, k| h[k] = [] }
      file_matches.each do |file, patterns_and_services|
        next if patterns_and_services.length < 2

        patterns = []
        services = Set.new
        patterns_and_services.each do |pattern, service|
          patterns << pattern
          services << service
        end
        next if patterns.include?(file)
        next if services.length < 2

        multiple_services_patterns[patterns_and_services] << file
      end

      [no_files_patterns, multiple_services_patterns]
    end

    private

    LEADING_COLON_REGEX = /\A:/.freeze
    TRAILING_GROUP_REGEX = /\.[\w\-]+\Z/.freeze

    def parse(contents)
      raise "Blank SERVICEOWNERS contents!" if contents.blank?

      @class_services_cache = {}
      @patterns_services = {}

      contents.each_line do |line|
        line.strip!
        next if line.blank?
        next if line.start_with?("#")

        words = line.split
        raise "Invalid SERVICEOWNERS line: #{line}" if words.length != 2

        pattern, service = *words
        service_symbol = service_to_symbol(service)
        raise "Invalid SERVICEOWNERS service: #{service}" unless service_symbol

        root_pattern = "#{Rails.root}/#{pattern}"
        raise "Duplicate SERVICEOWNERS pattern: #{pattern}" if @patterns_services[root_pattern]
        @patterns_services[root_pattern] = service_symbol
      end
    end

    def service_to_symbol(service)
      return if service.blank?
      return unless service.match?(LEADING_COLON_REGEX)

      service.gsub(LEADING_COLON_REGEX, "")
             .gsub(TRAILING_GROUP_REGEX, "")
             .to_sym
    end
  end
end
