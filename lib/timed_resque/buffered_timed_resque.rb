# frozen_string_literal: true

module TimedResque::BufferedTimedResque
  extend self

  HYDRO_PUBLISH_PATH = :hydro_path
  REDIS_PUBLISH_PATH = :redis_path

  def retry_at(job_class, job_args, at, queue:, retries: 0, guid: nil, publish_path: nil)
    publish_path = choose_publish_path(job_class) if !publish_path

    if publish_path == HYDRO_PUBLISH_PATH
      success = retry_with_hydro(job_class, job_args, at, retries, guid, queue)

      if !success
        success = retry_with_timed_resque(job_class, job_args, at, retries, guid, queue)
      end
    else
      begin
        success = retry_with_timed_resque(job_class, job_args, at, retries, guid, queue)
      rescue *GitHub::Config::Redis::REDIS_DOWN_EXCEPTIONS => e
        GitHub.dogstats.increment(
          "buffered_timed_resque.redis.errors",
          tags: [
            "class:#{job_class.to_s.underscore}",
            "queue:#{queue}",
            "error:#{e.class}",
          ],
        )
        success = retry_with_hydro(job_class, job_args, at, retries, guid, queue)
      end
    end

    success
  end

  def retry_with_timed_resque(job_class, job_args, at, retries, guid, queue)
    if job_class < ApplicationJob
      GitHub.timed_resque.retry_active_job_at(
        job_class,
        job_args,
        at: at,
        retries: retries,
        queue: queue,
        guid: guid,
      )
    else
      GitHub.timed_resque.retry_at(job_class, Array.wrap(job_args), at, retries, guid, queue)
    end
  end

  def retry_with_hydro(job_class, job_args, at, retries, guid, queue)
    result = GitHub.sync_hydro_publisher.publish(
      {
        queue: queue.to_s,
        class_name: job_class.to_s,
        args: job_args.to_json,
        retries: retries,
        retry_at: at,
        guid: guid,
        request_id: GitHub.context[:request_id],
        current_ref: GitHub.current_ref,
      },
      schema: "github.v1.ResqueJob",
      topic: hydro_topic,
    )

    report_hydro_retry_error(job_class, result.error, queue) unless result.success?

    result.success?
  rescue => e
    report_hydro_retry_error(job_class, e, queue)
    false
  end

  def choose_publish_path(job_class)
    if GitHub.flipper[:retry_to_hydro].enabled?(JobClassActor.new(job_class))
      HYDRO_PUBLISH_PATH
    else
      REDIS_PUBLISH_PATH
    end
  end

  def hydro_topic
    if GitHub.dynamic_lab?
      "review-lab.v1.ResqueJobRetries"
    else
      "github.v1.ResqueJobRetries"
    end
  end

  def report_hydro_retry_error(job_class, error, queue)
    GitHub.dogstats.increment(
      "buffered_timed_resque.hydro.errors",
      tags: [
        "class:#{job_class.to_s.underscore}",
        "queue:#{queue}",
        "error:#{error.class}",
      ],
    )
  end

  # An actor representing a job class.
  #
  # The flipper_id includes the class name so it integrates with our flipper
  # actor storage and the internal GraphQL API.
  class JobClassActor
    attr_reader :flipper_id

    def self.find_by_id(id) # rubocop:disable GitHub/FindByDef
      new(id)
    end

    def initialize(job_class_name)
      @flipper_id = "#{self.class.name}:#{job_class_name}"
    end

    def to_s
      @flipper_id
    end
  end
end
