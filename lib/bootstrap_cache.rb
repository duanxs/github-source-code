# rubocop:disable Style/FrozenStringLiteralComment

require "digest/md5"
require "fileutils"
require "find"

class BootstrapCache
  CACHE_FOLDER = ".bundle/checksums"

  def initialize(path)
    @path = path
    FileUtils.mkdir_p(File.join(path, CACHE_FOLDER))
  end

  def changed?
    !!changes(all_files, :find)
  end

  def changes(files = all_files, method = :select)
    # Add version file, as when there is a version change, we want to bust the cache.
    files.insert(0, version)
    files.send(method) do |cache_dep|
      cache_dep.changed?
    end
  end

  def with_gems_cache(force: false, &blk)
    with_cache([gems, gemfile, ruby_version], &blk)
  end

  def with_brew_cache(&blk)
    with_cache([brew_version, brewfile], &blk)
  end

  def with_node_cache(&blk)
    with_cache([npm, node_version, node_modules], &blk)
  end

  def with_file_cache(file, checksum, &blk)
    file_and_checksum = file_plus_checksum(file, checksum)
    with_cache([file_and_checksum], &blk)
  end

  def with_cache(files)
    files = changes(files)
    unless files.empty?
      yield if block_given?
      write_cache(files)
    end
  end

  def write_cache(files)
    files.each do |cache_dep|
      # don't commit the main version file
      next if cache_dep.body == version.body
      cache_dep.write!
    end
  end

  # write the main checksum version
  # This cannot happen on the `with_cache` method as
  # when this file is changed we want to bust all the
  # caches.
  def commit_version
    version.write!
  end

  # private: return all the files we cache
  def all_files
    [gems, gemfile, ruby_version, npm, node_version]
  end

  def ruby_version
    string_checksum "#{ENV["RUBY_NEXT"] ? "2.7" : "2.6"}:#{RUBY_VERSION}", "ruby_version"
  end

  private

  def gemfile
    file_plus_checksum ["Gemfile", gemfile_lock], "gemfile"
  end

  def gemfile_lock
   if ENV["RAILS_NEXT"] == "1"
    "Gemfile_next.lock"
   else
    "Gemfile.lock"
   end
  end

  def gems
    file_plus_checksum "vendor/cache/", "gems"
  end

  def npm
    file_plus_checksum ["package.json", "package-lock.json"], "npm"
  end

  def node_modules
    file_plus_checksum "vendor/npm/", "node_modules"
  end

  def node_version
    file_plus_checksum "config/node-version", "node_version"
  end

  def brewfile
    file_plus_checksum "Brewfile", "brew"
  end

  def version
    file_plus_checksum "config/bootstrap-version", "version"
  end

  def md5(file_body)
    Digest::MD5.hexdigest(file_body)
  end

  class CacheDependency
    attr_reader :filenames, :body, :checksum_file

    def initialize(filenames, body, checksum_file)
      @filenames = filenames
      @body = body
      @checksum_file = checksum_file
    end

    def changed?
      if File.exist?(checksum_file)
        stored_checksum = File.read(checksum_file)
        current_checksum = Digest::MD5.hexdigest(body)
        current_checksum != stored_checksum
      else
        true
      end
    end

    def write!
      File.open(checksum_file, "wb") do |fd|
        fd.write(Digest::MD5.hexdigest(body))
      end
    end
  end

  def file_plus_checksum(filenames, checksum_filename)
    bodies = Array(filenames).map do |filename|
      if filename.end_with?("/")
        read_directory(filename)
      else
        read_file(filename)
      end
    end
    CacheDependency.new(filenames, bodies.join(""), File.join(@path, CACHE_FOLDER, checksum_filename))
  end

  def string_checksum(data, checksum_filename)
    CacheDependency.new([], data, File.join(@path, CACHE_FOLDER, checksum_filename))
  end

  def read_file(file)
    File.read(File.join(@path, file))
  end

  # Private: Calculates the MD5 of all of the contents of a directory of
  # files. If the directory contains other directories they will be traversed
  # recurseively.
  #
  # directory - The String directory name to process
  #
  # Note: Unlike `read_file`, a MD5 hex digest is returned instead of the actual
  # directory contents, since the latter would be incredibly memory
  # inefficient.
  #
  # Returns the String hexdigest.
  def read_directory(directory)
    directory_path = File.join(@path, directory)
    md5 = Digest::MD5.new
    Find.find(directory_path) do |gem|
      next if File.directory?(gem)
      md5.file(gem)
    end
    md5.hexdigest
  end
end
