# rubocop:disable Style/FrozenStringLiteralComment

require "active_support/core_ext/string/inflections"
require "context"

module Instrumentation
  # A module of helper methods so classes can instrument themselves.
  module Model
    module ClassMethods
      # Internal: provide a default event_prefix that matches
      # nicely to the class name.  The default instance level `event_prefix`
      # uses this.
      def default_event_prefix
        @default_event_prefix ||= name.underscore.to_sym
      end
    end

    def self.included(base)
      base.extend(ClassMethods)
    end

    # Public: Instruments events for this object.
    #
    # key     - String suffix of the event key.  See #event_prefix.
    # payload - Hash of additional payload information. See #event_payload.
    # &block  - Optional Block for timing operations.
    #           See ActiveSupport::Notifications.instrument.
    #
    # Returns nothing.
    def instrument(key, payload = {}, &block)
      prefix = payload.delete(:prefix) || event_prefix
      combined_payload = event_payload.merge(payload)
      expanded_payload = Context::Expander.expand(combined_payload)

      GitHub.dogstats.increment("model_event_triggered.#{prefix}.count", tags: ["action:#{prefix}.#{key}"])

      instrumentation_service.instrument "#{prefix}.#{key}", expanded_payload, &block
    end

    # Internal: Defines the event key prefix for all model events.
    #
    # Defaults to :my_model if the class this thing is included in is MyModel.
    # Feel free to override to provide a more semantic, meaningful name
    # if you so desire.
    #
    # Returns a String.
    def event_prefix
      self.class.default_event_prefix
    end

    # Internal: Defines the default payload for every event.
    #
    # Returns a Hash.
    def event_payload
      {}
    end

    # Internal: Defines the payload to describe the current subject.
    # Can be overridden in model to change fields returned.
    #
    # prefix - Key prefix that can be set to override Hash prefix.
    #
    # Example
    #
    #   event_context                 # => { :user_id => 1 }
    #   event_context(prefix: :actor) # => { :actor_id => 1 }
    #
    # Returns a Hash.
    def event_context(prefix: event_prefix)
      {
        "#{prefix}_id".to_sym => id,
      }
    end

    # Internal: Defines the instrumentation service instance.
    #
    # Returns an ActiveSupport::Notifications::Service or compatible object.
    def instrumentation_service
      @instrumentation_service ||= GitHub.instrumentation_service
    end
    attr_writer :instrumentation_service

  end
end
