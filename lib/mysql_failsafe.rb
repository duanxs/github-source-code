# rubocop:disable Style/FrozenStringLiteralComment

# Allow the app to continue even if Mysql is down, for things like running rake
# tasks and doing deployments in case there is a database outage.
#
# This file should have zero other dependencies, as it will be called and used very early
# in the app's lifecycle (see config.ru for an example).
module MysqlFailsafe
  def self.failsafe(description)
    begin
      yield
    rescue StandardError => e
      if no_database_error?(e) || trilogy_error?(e)
        $stderr.puts "*** #{description} failed with #{e.class}, but booting up anyway.."
      else
        raise e
      end
    end
  end

  def self.no_database_error?(e)
    # Check if constant is defined first because it might not be loaded yet
    defined?(ActiveRecord::NoDatabaseError) && e.is_a?(ActiveRecord::NoDatabaseError)
  end

  def self.trilogy_error?(e)
    # Check if constant is defined first because it might not be loaded yet
    defined?(Trilogy::Error) && e.is_a?(Trilogy::Error)
  end
end
