# frozen_string_literal: true

require "graphql_extensions/max_query_depth"
require "graphql_extensions/with_paginator"

require "graphql_extensions/visibility_for_builtins"
require "graphql_extensions/scopes_for_builtins"
require "graphql_extensions/upcoming_change_for_builtins"

# GraphQL monkey-patches
module GraphQL
  # Make the built-ins (scalars, introspection) respond to our customizations
  EXTENSIONS_FOR_BUILTINS = [
    GraphQLExtensions::VisibilityForBuiltIns,
    GraphQLExtensions::ScopesForBuiltIns,
    GraphQLExtensions::UpcomingChangeForBuiltIns,
  ]

  EXTENSIONS_FOR_BUILTINS.each do |extension|
    Schema::Object.extend(extension)
    GraphQL::Types::Relay::Node.extend(extension)
    Schema::Scalar.extend(extension)
    Schema::Union.extend(extension)
    Schema::Interface::DefinitionMethods.include(extension)
    Schema::Enum.extend(extension)
    Schema::EnumValue.include(extension)
    Schema::Field.include(extension)
    Schema::Argument.include(extension)
  end

  # Lawd fahgive me.
  # So many things depend on a previous graphql-batch setup where
  # the executor was always present.
  module Batch
    class Executor
      class << self
        def current
          if existing = Thread.current[THREAD_KEY]
            existing
          else
            start_batch(Platform::Batch::RequestExecutor)
            Thread.current[THREAD_KEY]
          end
        end
      end
    end
  end

  Relay::BaseConnection.prepend(GraphQLExtensions::WithPaginator)
  Analysis::AST::MaxQueryDepth.prepend(GraphQLExtensions::MaxQueryDepth)
end
