# rubocop:disable Style/FrozenStringLiteralComment

require "set"
require "nokogiri"

# Markup aware HTML truncator.
#
# - Truncates based on visible (text node) character length.
# - Strips block elements but not their content (<h1>, <ul>, etc).
# - Allows emoji and small images through as is.
# - Resizes large images to 20x20.
# - Strips email replies ("Show quoted text")
#
class HTMLTruncator
  include GitHub::Encoding

  def initialize(doc, max)
    @max = max

    @html_safe = if doc.is_a?(String)
      doc.html_safe?
    else
      # Assume it's been through HTML::Pipeline if it's a Nokogiri node.
      true
    end

    @src = if doc.is_a?(String)
      Nokogiri::HTML::DocumentFragment.parse(try_guess_and_transcode(doc))
    else
      doc
    end

    @dest = Nokogiri::HTML::DocumentFragment.parse("<p></p>")
    @length = truncate(@src, @dest.child, 0)
  end

  # Maximum number of visible characters to allow through.
  attr_reader :max

  # The number of visible characters added to the result document. This value
  # will never exceed max.
  attr_reader :length

  # Truncated HTML as a DocumentFragment.
  def document
    @dest
  end

  # Truncated HTML as a String.
  def to_html(wrap: true)
    html = if wrap
      document.to_html
    else
      document.child.inner_html
    end

    if @html_safe
      html.html_safe # rubocop:disable Rails/OutputSafety
    else
      html
    end
  end

  # The truncated portion of the original document as a DocumentFragment.
  def remaining
    @src
  end

  # List of elements that are allowed through. Other elements are traversed and
  # their text is included but the elements are stripped.
  ALLOWED = %w[a b i q em strong code tt ins del span sup sub var g-emoji].to_set
  PREVENT = %w[table].to_set

  # List of classes that are not allowed through.
  BADCLASS = %w[email-hidden-toggle email-hidden-reply]

  # Copy nodes from the source document to the dest document by visiting each
  # node in document order until the max character limit is exceeded. Only text
  # node values count toward the character limit.
  def truncate(src, dest, len)
    return len if len >= @max

    next_sibling = src.next_sibling

    case
    when src.element?
      if BADCLASS.include?(src["class"])
        # do nothing
      elsif ALLOWED.include?(src.name.downcase)
        # copy elements on the allowed list into the dest document
        node = dest.document.create_element(src.name, src.attributes)
        node = dest.add_child(node)
        len = truncate(src.child, node, len) if src.child
      elsif src.name.downcase == "img"
        # for images, allow emoji and anything with a smallish width through as
        # is. other images are allowed through but their height and width is
        # adjusted.
        if allowed_image?(src)
          attributes = src.attributes
          node = dest.document.create_element("img", attributes)
          dest.add_child(node)
          len += 1
        end
      elsif src.child && !PREVENT.include?(src.name.downcase)
        # traverse into other elements but do not copy the element itself
        len = truncate(src.child, dest, len)
      end

    when src.text?
      # copy as much of the text node value as we can fit
      take = @max - len
      if take > 0
        text = try_guess_and_transcode(src.content).gsub(/[ \t\r\n]{2,}/, " ")
        used = text[0, take]
        len += used.length
        if used.length < text.length
          grab = used.length <= 3 ? used.length - 1 : 3
          src.content = "…#{used[-grab..-1]}#{text[take, text.length]}"
          used = "#{used[0...-grab]}…"
        else
          src.remove
        end
        node = dest.document.create_text_node(used.to_s)
        node = dest.add_child(node)
      end
    else
      # traverse into other stuff like document fragments
      len = truncate(src.child, dest, len) if src.child
    end

    # if we get here the current node, all children, and preceding nodes in
    # document order have been fully processed.
    if len < @max
      src.remove
      len = truncate(next_sibling, dest, len) if next_sibling
    end
    len
  end

  # Determine if an image element should be allowed through.
  #
  # Returns true for emoji and images that have a width of 16 pixels or less.
  def allowed_image?(node)
    node["class"].to_s.include?("emoji") ||
      ((node["width"] && node["width"].to_i <= 20) &&
       (node["height"].nil? || node["height"].to_i <= 20))
  end
end
