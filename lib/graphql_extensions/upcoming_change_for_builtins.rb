# frozen_string_literal: true

module GraphQLExtensions
  module UpcomingChangeForBuiltIns
    attr_reader :upcoming_change
  end
end
