# rubocop:disable Style/FrozenStringLiteralComment

require "forwardable"

module Resiliency
  class Response < SimpleDelegator

    # Internal: All exceptions that mean newsies could be unavailable due to
    # timeouts, network errors or mysql failures.
    UnavailableExceptions = [
      ActiveRecord::ConnectionNotEstablished, # don't believe we'll ever hit this
      ActiveRecord::StatementInvalid,
      SystemCallError, # Errno::ECONNREFUSED, Errno::ECONNRESET and friends
      Trilogy::Error, # lower level mysql driver error
    ]

    attr_reader :value
    attr_reader :error

    def initialize(default_value = nil, &block)
      @success = false
      @error = nil

      @value = if block_given?
        begin
          result = yield
          @success = true
          if result.nil? && default_value
            default_value
          else
            result
          end
        rescue *UnavailableExceptions => boom
          GitHub.dogstats.increment(stats_tag)
          Failbot.report(boom, app: failure_app_tag)
          @error = boom
          default_value
        end
      else
        @success = true
        default_value
      end

      super(@value)
    end

    def stats_tag
      raise NotImplementedError
    end

    def failure_app_tag
      raise NotImplementedError
    end

    def value!
      raise @error unless @success
      @value
    end

    def success?
      @success
    end

    def failed?
      !success?
    end
  end
end
