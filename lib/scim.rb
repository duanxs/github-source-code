# frozen_string_literal: true

require "scim/error"
require "scim/filter"
require "scim/operation"
require "scim/results_collection"

module SCIM
  CONTENT_TYPE = "application/scim+json"

  ERROR_SCHEMA = "urn:ietf:params:scim:api:messages:2.0:Error"
  LIST_SCHEMA = "urn:ietf:params:scim:api:messages:2.0:ListResponse"
  USER_SCHEMA = "urn:ietf:params:scim:schemas:core:2.0:User"
  GROUP_SCHEMA = "urn:ietf:params:scim:schemas:core:2.0:Group"
end
