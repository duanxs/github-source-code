# frozen_string_literal: true

module Audit
  module Test
    class Logger
      def perform(payload)
        # noop
      end
    end
  end
end
