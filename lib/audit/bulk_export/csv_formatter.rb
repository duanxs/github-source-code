# rubocop:disable Style/FrozenStringLiteralComment

module Audit
  class BulkExport
    class CsvFormatter
      # Public: Using a select number of fields convert a Array of Hashes to
      # the CSV format for user consumption.
      #
      # rows            - Array of Hashes
      # fields          - Array of Strings for each field to extract.
      # include_header: - The Boolean that shows or hides CSV header (default: true)
      #
      # Return String
      def self.process(rows, fields, include_header: true)
        GitHub::CSV.generate do |csv|
          csv << fields if include_header
          rows.each do |row|
            csv << fields.collect do |field|
              matches = field.match(/\Adata\.(?<field>[A-Za-z_]+)\z/)

              if matches && matches["field"].present? && row["data"].present?
                row["data"][matches["field"]]
              else
                row[field]
              end
            end
          end
        end
      end
    end
  end
end
