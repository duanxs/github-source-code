# rubocop:disable Style/FrozenStringLiteralComment

require "audit/transition/failed_documents"

module Audit
  module Transition
    class Document
      MAX_DOCUMENT_ID_LENGTH = 512
      TRANSITION_TYPE = "transition".freeze
      SUCCESS_STATE = "success".freeze
      IN_PROGRESS_STATE = "in_progress".freeze
      FAILED_STATE = "failed".freeze

      class DocumentIdKeyLengthError < StandardError
        def initialize(transition = nil)
          @transition = transition
        end

        def message
          "Document name '#{@transition}' can not exceed #{MAX_DOCUMENT_ID_LENGTH} bytes, was #{@transition.bytes.length}"
        end
      end

      # Public: Finds the first document with the given attributes, or creates a
      # record with the attributes if one is not found.
      #
      # client     - The Elastomer::Client to communicate with the index.
      # transition - The String class name of the transition being performed.
      # index      - The Elastomer::Index that the transition document should live in.
      #
      # Returns Audit::Transition::Document instance.
      def self.locate(client:, transition:, index:)
        document = new(client: client, transition: transition, index: index)
        document.find || document.create
        document
      end

      # Public: The String name of the transition being performed.
      attr_reader :transition

      # Public: The String state the transition is in, can be: IN_PROGRESS_STATE, SUCCESS_STATE, or FAILED_STATE.
      attr_reader :state

      # Public: The Integer number of documents the transition matches for an index.
      attr_reader :doc_count

      # Public: The Audit::Transition::FailedDocuments instance responsible for
      # reading, writing, and managing the file holding the failed document ids.
      attr_reader :failed_documents

      # Public: The String ISO8601 timestamp of the time the transition started at.
      attr_reader :started_at

      # Public: The String ISO8601 timestamp of the time the transition finished at.
      attr_reader :finished_at

      # Public: Initializes the transition document with the given attributes.
      #
      # client     - The Elastomer::Client to communicate with the index.
      # transition - The String class name of the transition being performed.
      # index      - The Elastomer::Index that the transition document should live in.
      #
      # Returns Audit::Transition::Document instance.
      def initialize(client:, transition:, index:)
        @client = client
        @transition = transition
        @index = index.is_a?(String) ? @client.index(index) : index
        @docs = @index.docs
        @version = 0
        @doc_count = 0

        filename = "%s-%s-failure-doc-ids.json" % [@transition.parameterize, @index.name]
        @failed_documents = Audit::Transition::FailedDocuments.new(
          path: Rails.root.join("tmp", filename),
        )

        ensure_document_id_length
        create_transition_type
      end

      def failed_documents?
        @failed_documents.any?
      end

      def persisted?
        @version > 0
      end

      def success?
        SUCCESS_STATE == @state
      end

      def in_progress?
        IN_PROGRESS_STATE == @state
      end

      def failed?
        FAILED_STATE == @state
      end

      def delete
        return unless persisted?
        response = @docs.delete(id: @transition, type: TRANSITION_TYPE)

        if response["found"]
          @version = 0
          @state = nil
          @doc_count = 0
          @finished_at = nil
          @started_at = nil
        end

        self
      end

      def find
        document = @docs.get(id: @transition, type: TRANSITION_TYPE)
        return unless document["found"]

        @version = document["_version"]
        hit = document["_source"]
        @state = hit["state"]
        @doc_count = hit["doc_count"]
        @finished_at = hit["finished_at"]
        @started_at = hit["started_at"] || Time.now.utc.iso8601

        self
      end

      def body
        builder = {
          _id: @transition,
          _type: TRANSITION_TYPE,
          state: @state,
          started_at: @started_at,
          doc_count: @doc_count,
        }

        if @finished_at
          builder[:finished_at] = @finished_at
        end

        builder
      end

      def create
        return self if persisted?

        @state = IN_PROGRESS_STATE
        @started_at = Time.now.utc.iso8601

        response = @docs.index(body)
        return false unless response["created"]
        @version = response["_version"]
        true
      end

      def failed(doc_count:, documents:)
        @state = FAILED_STATE
        @finished_at = Time.now.utc.iso8601
        @failed_documents.documents = documents
        @doc_count = doc_count

        response = @docs.index(body)
        new_version = response["_version"]
        return false unless new_version > @version
        @version = new_version
        true
      end

      def success(doc_count:)
        @state = SUCCESS_STATE
        @failed_documents.documents = nil
        @finished_at = Time.now.utc.iso8601
        @doc_count = doc_count

        response = @docs.index(body)
        new_version = response["_version"]
        return false unless new_version > @version
        @version = new_version
        true
      end

      private

      def transition_type_exists?
        @client.head("#{@index.name}/#{TRANSITION_TYPE}").success?
      end

      def create_transition_type
        return true if transition_type_exists?

        # COMPATIBILITY: remove wrapper after ES5 upgrade
        transition_mapping = Elastomer::SearchIndexManager.conform_to_es5(
            Elastomer::Indexes::AuditLog.mappings, @client.semantic_version).fetch(:transition)

        @client.put("#{@index.name}/_mapping/#{TRANSITION_TYPE}", body: {
          transition: transition_mapping,
        })
      end

      # Private: Elasticsearch has a 512 byte limit on the size of the ID used
      # for a document.
      def ensure_document_id_length
        return if @transition.bytes.length <= MAX_DOCUMENT_ID_LENGTH
        raise DocumentIdKeyLengthError.new(@transition)
      end
    end
  end
end
