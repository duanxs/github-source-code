# frozen_string_literal: true

module Audit
  module Transition
    class FailedDocuments
      include Enumerable

      def initialize(path:)
        @path = path
      end

      # Enumberable will return true for empty arrays.
      def any?
        documents.any?
      end

      def count
        documents.length
      end

      def each
        yield documents
      end

      def documents
        return @documents if defined?(@documents)
        @documents = read
      end

      def documents=(doc_ids)
        @documents = Array(doc_ids)
        any? ? write : unlink
        @documents
      end

      def exists?
        File.exist?(@path)
      end

      def unlink
        return unless exists?
        File.unlink(@path)
      end

      private

      def read
        return [] unless exists?
        GitHub::JSON.decode(File.read(@path))
      rescue Errno::ENOENT, Yajl::ParseError
        []
      end

      def write
        body = GitHub::JSON.encode(@documents)
        File.write(@path, body)
      end
    end
  end
end
