# frozen_string_literal: true

module Audit
  # Decorator around an Audit service, which will build the payload from the
  # inner_service and queue up the Audit for Resque to do the actual auditing.
  class Resque
    # Public: Create the Audit adapter wrapping the service.
    #
    # service - Audit service
    def initialize(service)
      @service = service
      @inline = false
    end

    # Public: Returns the wrapped Audit::Service for the actual auditing.
    attr_reader :service

    # Public: Queue the actual log.
    #
    # action - A String key identifying the action.
    # data   - The Hash of data that is sent over as JSON.  See
    #          Audit::Service#log
    #
    # Returns true.
    def log(action, payload)
      if @inline
        # We don't need the prepared payload here since the service will
        # prepare it for us
        service.log(action, payload)
      else
        prepared_payload = service.build_payload(action, payload)
        ensure_staff_action_privacy(action, prepared_payload)
        ::PostDelayedAuditJob.perform_later(action, prepared_payload)
      end
      true
    end

    # Used to run the job in line
    def inline
      old_inline = @inline
      @inline = true
      yield
    ensure
      @inline = old_inline
    end

    # Public: Sets an on-error callback.  See Audit::Search#on_error.
    def on_error(&block)
      service.on_error(&block)
    end

  private

    # Send any unknown calls on to the wrapped service
    def method_missing(name, *args)
      service.send(name, *args)
    end
  end
end
