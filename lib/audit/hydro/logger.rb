# frozen_string_literal: true

module Audit
  module Hydro
    AUDIT_ENTRY_SCHEMA = "audit_log.v0.AuditEntry"

    # Don't obfuscate these keys.
    EXCLUDED_KEYS = ["action", "country_code", "@timestamp"]

    class Logger
      # Internal: schedules the Audit Log entry to the AuditEntry hydro schema.
      #
      # payload - The Hash of data that is sent over as JSON.
      def perform(payload)
        payload = payload.stringify_keys
        return unless GitHub.flipper[:audit_log_hydro].enabled?

        document_id = payload.fetch("_document_id")
        audit_timestamp = Audit.milliseconds_to_time(payload.fetch("@timestamp"))

        if GitHub.flipper[:audit_log_unobfuscated].enabled?
          logged_data = payload
        else
          logged_data = obfuscate_hash(payload)
        end

        message = {
          document_id: document_id,
          document: logged_data.to_json,
          audit_timestamp: audit_timestamp,
          shard_key: shard_key(logged_data),
          country_code: payload.dig("actor_location", "country_code"),
        }

        GitHub.hydro_publisher.publish(message, schema: Audit::Hydro::AUDIT_ENTRY_SCHEMA)
        if Rails.development?
          # Local development needs a sleep timer for the hydro async to work
          sleep 3
        end
      end

      private

      def obfuscate_value(value)
        case value
        when String
          SecureRandom.alphanumeric(value.length)
        when Float
          value * SecureRandom.random_number
        when Integer
          (value * SecureRandom.random_number).to_i
        else
          value
        end
      end

      def obfuscate_hash(hash)
        builder = {}
        hash.each do |key, value|
          key = key.to_s
          builder[key] = get_value(key, value)
        end
        builder
      end

      def get_value(key, value)
        if EXCLUDED_KEYS.include?(key)
          value
        elsif value.is_a?(Hash)
          obfuscate_hash(value)
        elsif value.is_a?(Array)
          value.collect do |val|
            obfuscate_value(val)
          end
        else
          obfuscate_value(value)
        end
      end

      def shard_key(payload)
        # There is a known case where org_id can be an Array of integers.
        # We're only looking for events where they are routed to a single organization.
        if payload["org_id"].is_a?(Integer)
          "Organization;#{payload["org_id"]}"
        else
          if (user_id = payload["actor_id"] || payload["user_id"]).present?
            "User;#{user_id}"
          else
            ::Audit::Hydro::DEFAULT_SHARD
          end
        end.gsub(";", "-")
      end
    end
  end
end
