# frozen_string_literal: true

module Audit
  module Hydro
    class Hydrator
      def initialize(index:, cluster: nil, **other_args)
        Failbot.push app: "github-audit-log-transitions"

        @logger = Audit::Hydro::EventForwarder.new
        @index = index
        @cluster = cluster || GitHub.es_audit_log_cluster
        @pp = Audit::PrettyProgress.new(timestamp_prefix: true)
      end

      def hydrate
        index = client.index(@index)
        processed = 0

        # Find total number of documents in index
        response = index.docs.count(scan_query, type: Elastomer::Adapters::AuditEntry.document_type)
        found_count = response["count"]
        step = determine_step(found_count)

        # Iterate through each document
        scan = index.scan(scan_query, scan_options)
        scan.each_document do |doc|
          source = doc["_source"].dup

          # Hydro logger expects the _document_id to be set, so we need to
          # massage the data a little.
          doc = source.merge("_document_id" => doc["_id"]).deep_symbolize_keys

          action = doc[:action]

          message = @logger.build_publish_message(action, doc)

          schema = "audit_log.v2.AuditEntry"

          @logger.publish(message, schema: schema)
          processed += 1

          if (step != false && processed % step == 0) || processed == found_count
            @pp.done "[#{@index}] #{processed} #{"document".pluralize(processed)} hydrated"
          else
            @pp.step "[#{@index}] #{step} documents are being hydrated"
          end
        end
      end

      private

      def log(message)
        return if Rails.env.test?
        puts "[#{Time.now.strftime("%Y-%m-%dT%H:%M:%S")}] #{message}"
      end

      # Private: Determine the interval of repaired documents before for logging
      # progress.
      #
      # count - The total number of documents in the index.
      #
      # Returns Integer.
      def determine_step(count)
        case count
        when 0..10_000
          false
        when 10_001..1_000_000
          10_000
        else
          100_000
        end
      end

      def indices
        @indices ||= Audit::Elastic.indices(client.cluster)
      end

      def scan_options
        {
          size: 2_500,
          scroll: "5m",
          type: Elastomer::Adapters::AuditEntry.document_type,
        }
      end

      def scan_query
        {
          query: {
            match_all: {},
          },
        }
      end

      def bulk_options
        {
          request_size: 5.megabytes,
          timeout: "5m",
        }
      end

      def client
        @client ||= begin
          url = Elastomer.config.clusters.fetch(@cluster).fetch(:url)

          Elastomer::Client.new(
            url: url,
            read_timeout: 600, # 10m * 60s
            open_timeout: Elastomer.config.open_timeout,
            adapter: :persistent_excon,
            opaque_id: true,
            max_request_size: 25.megabytes,
            strict_params: true,
          )
        end
      end
    end
  end
end
