# frozen_string_literal: true
module Audit
  module SplunkHEC
    # Sends audit logs to a Splunk Http Event Collector (HEC)
    HEC_EVENT_ITEMS = [
      :actor_id,
      :actor,
      :oauth_app_id,
      :action,
      :user_id,
      :user,
      :repo_id,
      :repo,
      :actor_ip,
      :created_at,
      :from,
      :note,
      :org,
      :org_id,
      :data,
    ].freeze

    APP_NAME = "github_audit".freeze
    SOURCE = "gh-splunkhec-audit-logger".freeze
    SOURCETYPE = "GitHub:Audit"

    RETRY_STATUSES = [429, 502, 503, 504]

    class Logger
      attr_reader :index_name
      attr_reader :host
      attr_reader :token
      attr_reader :retry_options
      attr_accessor :connection

      class ResponseError < StandardError; end

      # Public: Splits non-indexed Audit payload data into a separate 'data'
      # key.
      #
      #   split_payload(:actor_id => 1, :foo => 'bar')
      #   # {:actor_id => 1, :data => {:foo => 'bar'}}
      #
      # Returns a Hash.
      def split_payload!(payload)
        data = {}
        payload.each_key do |key|
          next if HEC_EVENT_ITEMS.include?(key.to_sym)
          data[key] = payload.delete(key)
        end
        payload[:data] = data
        payload
      end

      def initialize(host, token, index_name)
        @index_name = index_name
        @host = host
        @token = token
        @retry_options = {
          max: 10,
          interval: 0.05,
          interval_randomness: 0.5,
          backoff_factor: 2,
          retry_statuses: RETRY_STATUSES,
          methods: [:post], #default methods list does not include POST
        }
      end

      def conn
        @connection ||= Faraday.new(url: @host) do |c|
          c.request :retry, @retry_options
          c.adapter Faraday.default_adapter
        end
      end
      # Internal: Formats the message into a string for splunk HE
      #
      # payload - The Hash of data that is sent to splunk HEC.
      #
      # reference https://docs.splunk.com/Documentation/Splunk/8.0.4/RESTREF/RESTinput#services.2Fcollector
      #
      # Returns the stringified payload that follows Splunk JSON
      def format_event(payload)
        event_time = payload[:@timestamp]
        split_payload!(payload)

        {
          time: event_time,
          event: payload,
          fields: {"app": APP_NAME},
          index: @index_name,
          source: SOURCE,
          sourcetype: SOURCETYPE,
        }.to_json
      end

      # Internal: Posts the data to a Splunk HTTP Event Collector
      #
      # payload - The Hash of data that is sent over as JSON.
      #
      # Returns the Faraday::Response.
      def perform(payload)
        return unless GitHub.splunk_hec_enabled? && GitHub.flipper[:splunk_hec_logger].enabled?

        formatted_payload = format_event(payload)

        response = conn.post do |req|
          req.url "/services/collector"
          req.headers["Content-Type"] = "application/json"
          req.headers["Authorization"] = "Splunk #{@token}"
          req.body  = formatted_payload
        end

        if !response.success?
          # error messages here will contain
          # a response similar to format specified in
          # https://docs.splunk.com/Documentation/Splunk/8.0.4/RESTREF/RESTinput#services.2Fcollector
          #
          # {"text":"Incorrect data format","code":5,"invalid-event-number":0}
          err = ResponseError.new("Error response from splunk_hec logger")
          Failbot.report(
            err,
            areas_of_responsibility: [:audit_log],
            message: response.body
          )

          # We want to raise the error here so that the `PostDelayedAuditJob` can catch it and retry
          # These are errors and data that we *do not* want to lose so it's a second catch for these errors
          # to make sure they are continuously retried
          if RETRY_STATUSES.include?(response.status)
            raise err
          end
        end
        response
      end
    end
  end
end
