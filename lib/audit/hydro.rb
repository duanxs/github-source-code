# frozen_string_literal: true

module Audit
  module Hydro
    autoload :EventForwarder, "audit/hydro/event_forwarder"

    DEFAULT_SHARD = "User;0"
  end
end
