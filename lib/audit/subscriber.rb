# rubocop:disable Style/FrozenStringLiteralComment

require "audit/event"

module Audit
  class Subscriber
    attr_writer :service

    def initialize(service = nil, logger = nil)
      @service = service
      @logger = logger || Rails.logger
    end

    # Public: Subscribes to the given events.
    #
    #     # Subscribe to issue.create, issue.update
    #     subscribe(:issue => [:create, :update])
    #
    # notifier - An ActiveSupport::Notifications::Service
    # actions  - An Array of action names.
    #
    # Returns nothing.
    def subscribe(notifier, actions)
      actions.each do |action|
        notifier.subscribe(action, self)
      end
    end

    def call(*args)
      event = Event.new(*args)
      service.log(event.name, event.payload)
    rescue Exception => e # rubocop:disable Lint/RescueException
      @logger.error "Could not log #{event.inspect} event. #{e.class}: #{e.message} #{e.backtrace}"
    end

    def service
      # Specifically avoiding memoizing the result of GitHub.audit here
      # as it's already memoized and will not allow reassignment.
      @service || GitHub.audit
    end
  end
end
