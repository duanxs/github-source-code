# rubocop:disable Style/FrozenStringLiteralComment

module Audit
  module Elastic
    # Runs a query against every audit log index in ElasticSearch,
    # collects, sorts, format the resulting entry set as JSON/CSV.
    #
    # phrase - A stafftools-style search string (optional).
    # subject - A User, Organization, or Business to scope query (optional).
    #
    # Usage:
    #
    # export = Audit::Elastic::BulkExport.new({
    #   :cluster_url => "http://localhost:9210",
    #   :phrase      => "org:github (action:repo.create OR action:repo.destroy)"
    # })
    # export.run
    #
    class BulkExport < Audit::BulkExport::Base
      attr_accessor :client

      def initialize(format: "json", phrase: nil, subject: nil)
        super(format: format, phrase: phrase, subject: subject, backend: "internal")
        @client = GitHub.audit.searcher.index.client
      end

      def query
        @query ||= Search::Queries::AuditLogQuery.new \
          phrase: @phrase,
          subject: @subject,
          raw: !custom_created_range?
      end

      def phrase=(phrase)
        @indices = nil
        @query = nil
        @phrase = phrase
        @phrase
      end

      # Public: Determines if the user provided a custom created range in the
      # search phrase.
      #
      # Returns true if created range is present, false otherwise.
      def custom_created_range?
        phrase =~ /created\:(>=?|<=?)?[0-9-]+/
      end

      # Public: Builds the list of indices required to fulfill the bulk export.
      #
      # Returns Array of indexes as Strings.
      def indices
        @indices ||= if custom_created_range?
          query.query_params[:index].split(",")
        else
          Audit::Elastic.indices(client.cluster)
        end
      end

      # This method will query against every audit log index in a cluster,
      # running each query (phrase) against that index and returning the entries.
      #
      # Returns an Array matching [Integer, Array].
      def fetch_results
        scroll_query = {
          _source: Audit::BulkExport::Base::FIELDS,
          query: query.build_query,
        }

        scroll_options = {
          size: 2_500,
          scroll: "15s",
          type: "audit_entry",
          read_timeout: 30,
        }

        hits = []

        indices.each do |index_name|
          index = client.index(index_name)
          scroll = index.scroll(scroll_query, scroll_options)

          scroll.each_document do |doc|
            hits << doc["_source"]
          end
        end

        GitHub.dogstats.histogram("audit.export.results.indices.total", indices.length)

        [hits.length, hits]
      end
    end
  end
end
