# rubocop:disable Style/FrozenStringLiteralComment

module Audit
  module Elastic
    class Hit < ActiveSupport::HashWithIndifferentAccess
      class UnknownCurrentActorError < StandardError; end

      # Public: Look up an individual audit log entry from a specific audit log index.
      #
      # id         - The String document id to look up.
      # index      - The Elastomer::Indexes::AuditLog index to look for the document.
      # type:      - The String document type to lookup (default: 'audit_entry').
      #
      # Examples
      #
      #   @index = Elastomer::Indexes::AuditLog.new("audit_log-2014-06")
      #
      #   find("2M9hQOLZQFuN5TmD95lPag", @index)
      #   find("2M9hQOLZQFuN5TmD95lPag", @index, type: "audit_entry_redux")
      #
      # Returns Audit::Elastic::Hit instance
      def self.find(id, index, type: nil)
        type   ||= Elastomer::Adapters::AuditEntry.document_type
        document = index.docs.get(type: type, id: id)

        if document["found"] || document["exists"] # exists is removed in 1.2.x
          hit = new(document)
          hit.after_initialize
          hit
        end
      end

      # Public: Store an audit entry to a specific audit log index.
      #
      # id         - The String document id to save the entry to.
      # index      - The Elastomer::Indexes::AuditLog index to store the document in.
      # entry      - The Hash source for audit entry.
      #
      # Examples
      #
      #   @index = Elastomer::Indexes::AuditLog.new("audit_log-2014-06")
      #   @source = { :user => "dewski" }
      #
      #   store("2M9hQOLZQFuN5TmD95lPag", @index, @source)
      #
      # Returns raw Hash response from ElasticSearch
      def self.store(id:, index:, entry:, type: nil)
        audit_entry = Elastomer::Adapters::AuditEntry.create(entry)
        type      ||= Elastomer::Adapters::AuditEntry.document_type
        index.docs.index(audit_entry.to_hash, id: id, type: type)
      end

      # Private: Remaps user, actor, and org to local ids if they exist.
      #
      # attributes - Hash source from ElasticSearch hit
      #
      # Returns Hash
      def self.remap_entities(source)
        if user = User.find_by_login(source["user"])
          source["user_id"] = user.id
        end

        if actor = User.find_by_login(source["actor"])
          source["actor_id"] = actor.id
        end

        if org = Organization.find_by_login(source["org"])
          source["org_id"] = if source["org_id"].respond_to?(:each)
            source["org_id"] << org.id
          else
            org.id
          end
        end

        source
      end
      private_class_method :remap_entities

      attr_reader :id
      attr_reader :index
      attr_reader :index_name
      attr_reader :type

      # Interface for interacting with ElasticSearch hits.
      #
      # Returns Audit::Elastic::Hit instance.
      def initialize(hit)
        @hit = hit.with_indifferent_access

        if @hit["_source"]
          @id = @hit["_id"]
          @index_name = @hit["_index"]
          @type = @hit["_type"]
          @index = Elastomer::Indexes::AuditLog.new(@index_name)

          super(@hit["_source"])
        else
          @type = Elastomer::Adapters::AuditEntry.document_type
          @id = SecureRandom.urlsafe_base64(16)

          super(@hit)
        end
      end

      # Causes SystemStackError error since class is already HashWithIndifferentAccess
      def with_indifferent_access
        self
      end

      # index - The index to store the audit entry (optional).
      #
      # Returns true if saved, false if not.
      def save
        self.class.store(id: @id, index: @index, entry: self)
      end

      # Public: Takes the data field and stores it into the raw_data field which
      # is unindexed and unsearchable.
      #
      # `raw_data` expects a JSON string.
      #
      # Returns String.
      def repair_data
        self["raw_data"] = GitHub::JSON.encode(delete("data"))
      end

      # Public: Takes the actor's location and repairs the `lat` and `lon` to not be nil.
      #
      # Returns Hash
      def repair_location
        self["actor_location"] ||= {}
        self["actor_location"]["location"] ||= {}
        self["actor_location"]["location"]["lat"] ||= 0.0
        self["actor_location"]["location"]["lon"] ||= 0.0
      end

      # Public: Returns the path for the ES document
      #
      # Returns String
      def path
        "/#{@index_name}/#{@type}/#{@id}"
      end

      # Public: Marks an audit entry as invalid. Invalid entries
      # will not be shown to users but will still show up in Stafftools.
      #
      # reason - The string reason for why the audit entry should not be shown to users.
      #
      # Returns a boolean representing if the update was successful.
      def invalidate(reason)
        actor = Audit.context[:actor].try(:to_s)
        raise UnknownCurrentActorError unless actor

        (self["data"] ||= {}).merge!({
          "_invalid" => true,
          "_invalid_actor" => actor,
          "_invalid_reason" => reason,
          "_invalid_at" => (Time.now.utc.to_f * 1000).round,
        })

        save
      end

      def global_relay_id
        # Given audit_log-1-2019-02-1 parse it and return 2019-02
        slicer = Elastomer::Slicers::AuditLogDateSlicer.new
        _, _, slice, _ = slicer.validate_fullname(index_name)
        document_id_with_slice = [document_id, slice].join(";")

        ::Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, document_id_with_slice)
      end

      def platform_type_name
        "#{action.tr(".", "_").camelize}AuditEntry"
      end

      def document
        self
      end

      def document_id
        @id
      end

      def action
        self["action"]
      end

      def created_at
        Audit.milliseconds_to_time(self["@timestamp"])
      end

      def [](key)
        super(key.to_s) || dig("data", key.to_s)
      end

      def get(key)
        self[key.to_s] || dig("data", key.to_s)
      end

      # Public: Determines if the given key is present in the Audit Entry document.
      #
      # key - String key name.
      #
      # Returns true if key is present, false otherwise.
      def key?(key)
        return true if super(key.to_s)
        return true if dig("data", key.to_s).present?
        false
      end

      # Public: Transforms `created_at` field to a readable timestamp string.
      #
      # Returns a Hit.
      def with_human_timestamp
        self["created_at"] = Time.at(self["created_at"] / 1000).to_s
        self
      end

      def data
        self["data"] || {}
      end

      def after_initialize
        load_repaired_data
        rewrite_actions
      end

      private

      # Private: Takes the raw_data field if present and merges it into the
      # existing data field. Any values in the data field take precedence over
      # raw_data.
      #
      # Returns nothing
      def load_repaired_data
        if self["raw_data"].present?
          self["data"] ||= {}
          raw_data = GitHub::JSON.decode(delete("raw_data"))

          self["data"] = raw_data.merge(self["data"])
        end
      end

      def rewrite_actions
        if self["action"] == "team.create" && self["from"] == "orgs/team_members#create"
          self["action"] = "team.add_member"
        elsif self["action"] == "team.delete" && ["orgs/teams#leave", "orgs/team_members#destroy"].include?(self["from"])
          self["action"] = "team.remove_member"
        elsif self["action"] == "team.create" && self["repo_id"].present?
          self["action"] = "team.add_repository"
        elsif self["action"] == "team.delete" && self["repo_id"].present?
          self["action"] = "team.remove_repository"
        end
      end
    end
  end
end
