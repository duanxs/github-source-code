# frozen_string_literal: true

module Audit
  module Driftwood
    class Query
      include Scientist
      extend Scientist
      extend Forwardable

      PER_PAGE = 50

      attr_reader :business,
        :business_id,
        :current_user,
        :org,
        :org_id,
        :original_query,
        :driftwood_query,
        :per_page

      def_delegators :driftwood_query,
        :after,
        :after=,
        :before,
        :before=,
        :has_next_page?,
        :has_previous_page?,
        :total_count

      def initialize(args = {})
        @business = args[:business]
        @business_id = args[:business_id]
        @current_user = args[:current_user]
        @org = args[:org]
        @org_id = args[:org_id]

        @original_query = args[:original_query]
        @driftwood_query = args[:driftwood_query]

        @per_page = args.fetch(:per_page, PER_PAGE)
        @original_query.per_page = @per_page
        @driftwood_query.per_page = @per_page
        configure_page_and_offset(args)

        @business ||= Business.find(business_id) if business_id
        @org ||= Organization.find(org_id) if org_id
      end

      def execute
        return original_query.execute unless driftwood_enabled?
        # Returns Driftwood::SearchResults
        r = driftwood_query.execute
        # We need to add all the things to `data` for compatibility with the old audit log
        r.results.map! do |x|
          y = x.clone.with_indifferent_access
          y[:data] = x
          y
        end
        r
      end

      def per_page=(num)
        @per_page = num
        @original_query.per_page = @per_page
        @driftwood_query.per_page = @per_page
      end

      def self.new_org_business_query(es_query)
        business = Business.find(es_query[:business_id]) if es_query[:business_id]
        org = Organization.find(es_query[:org_id]) if es_query[:org_id]

        enabled = driftwood_enabled?([es_query[:current_user], org, business])

        original_query = Search::Queries::AuditLogQuery.new(es_query)

        return original_query unless enabled

        driftwood_query =
          case
          when es_query.key?(:org_id)
            build_org_query(org.id, es_query)
          when es_query.key?(:business_id)
            build_business_query(business.id, es_query)
          else
            raise "Missing org_id or business_id"
          end

        Audit::Driftwood::Query.new(es_query.merge(
          business: business,
          org: org,
          original_query: original_query,
          driftwood_query: driftwood_query,
        ))
      end

      def self.new_project_query(options)
        project = options[:project]
        user = options[:current_user]
        org = nil
        org = project.owner if project.owner.is_a?(Organization)

        enabled = driftwood_enabled?([user, org])

        original_query = Search::Queries::Audit::ProjectQuery.new(options)

        return original_query unless enabled

        latest_time = nil
        if options[:latest_allowed_entry_time].present?
          time = Time.parse(options[:latest_allowed_entry_time])
          latest_time = Google::Protobuf::Timestamp.new(seconds: time.to_i, nanos: time.nsec)
        end

        driftwood_query = build_project_query(project, latest_time, options[:after])

        Audit::Driftwood::Query.new(options.merge(
          org: org,
          original_query: original_query,
          driftwood_query: driftwood_query,
        ))

      end

      def self.new_user_query(options)
        user_id = options[:user_id] || options[:actor_id]
        user = options[:current_user] || User.find(user_id)

        enabled = driftwood_enabled?([user])

        if Rails.env.test?
          options = options.merge(index_name: GitHub.audit.searcher.generate_index)
        end
        original_query = Search::Queries::AuditLogQuery.new(options)

        return original_query unless enabled

        driftwood_query = build_user_query(user_id, options[:phrase], options[:allowlist], options[:after], options[:before])

        Audit::Driftwood::Query.new(options.merge(
          current_user: user,
          original_query: original_query,
          driftwood_query: driftwood_query,
        ))
      end

      def self.new_dormant_user_query(options)
        user_id = options[:actor_id]
        user = User.find(user_id)

        enabled = driftwood_enabled?([user])

        original_query = Search::Queries::AuditLogQuery.new(options)

        return original_query unless enabled

        driftwood_query = GitHub.driftwood_client.dormant_user_query(
          user_id: user_id,
          # We only use the first entry here, so only fetch one
          per_page: 1,
        )

        Audit::Driftwood::Query.new(options.merge(
          current_user: user,
          original_query: original_query,
          driftwood_query: driftwood_query,
        ))
      end

      def self.new_2fa_user_query(options)
        user_id = options[:user_id] || options[:actor_id]
        user = User.find(user_id)

        enabled = driftwood_enabled?([user])

        original_options = options.merge(
          allowlist: ["user.two_factor_recovery_codes_downloaded", "user.two_factor_recovery_codes_printed", "user.two_factor_recovery_codes_viewed"],
        )

        original_options[:index_name] = GitHub.audit.searcher.generate_index if Rails.env.test?
        original_query = Search::Queries::AuditLogQuery.new(original_options)

        return original_query unless enabled

        driftwood_query = GitHub.driftwood_client.user_2fa_query(
          user_id: user_id,
          # We only use the first entry here, so only fetch one
          per_page: 1,
        )

        Audit::Driftwood::Query.new(options.merge(
          current_user: user,
          original_query: original_query,
          driftwood_query: driftwood_query,
        ))
      end

      def self.new_stafftools_query(options)
        user = options[:current_user]
        org = options.delete(:org)
        enabled = driftwood_enabled?([user, org])

        original_options = options
        original_options[:per_page] ||= PER_PAGE
        original_options[:page] ||= 1
        original_query = Search::Queries::StafftoolsQuery.new(original_options)

        return original_query unless enabled

        driftwood_query = GitHub.driftwood_client.stafftools_query(
          phrase: options[:phrase],
          per_page: options.fetch(:per_page, PER_PAGE),
          after: options[:after],
          before: options[:before],
        )

        Audit::Driftwood::Query.new(options.merge(
          current_user: user,
          original_query: original_query,
          driftwood_query: driftwood_query,
        ))
      end

      def self.driftwood_enabled?(options = [])
        options.compact.any? do |option|
          GitHub.flipper[:audit_log_driftwood_query].enabled?(option)
        end
      end

      def count
        @original_query.count
      end

      # Internal: Configures the :page and :offset from the given options hash.
      # Only one value can be given otherwise an ArgumentError will be raised.
      #
      # opts - Options Hash
      #   :page   - One based page number
      #   :offset - Zero based offset into the search results
      #
      # Returns this Query instance.
      # Raises ArgumentError
      def configure_page_and_offset(opts)
        @original_query.configure_page_and_offset(opts)
        # There's no need to do this for the driftwood query since we use a cursor
        # but the `results` method in lib/platform/connection_wrappers/driftwood_query.rb
        # will need to be changed once we switch over to driftwood

        self
      end

      private

      def driftwood_enabled?
        self.class.driftwood_enabled?([current_user, org, business])
      end

      def self.build_org_query(org_id, options)
        GitHub.driftwood_client.org_query(
          org_id: org_id,
          phrase: options[:phrase],
          per_page: options.fetch(:per_page, PER_PAGE),
          region: "US",
          public_platform: options.fetch(:public_platform, false),
          direction: options.fetch(:direction, "DESC"),
          limit_history: options.fetch(:limit_history, false),
          after: options[:after],
          before: options[:before],
        )
      end
      private_class_method :build_org_query

      def self.build_business_query(business_id, options)
        GitHub.driftwood_client.business_query(
          business_id: business_id,
          phrase: options[:phrase],
          per_page: PER_PAGE,
          region: "US",
          after: options[:after],
          before: options[:before],
        )
      end
      private_class_method :build_business_query

      def self.build_project_query(project, latest_allowed_entry_time, after)
        GitHub.driftwood_client.project_query(
          project_id: project.id,
          per_page: PER_PAGE,
          region: "US",
          latest_allowed_entry_time: latest_allowed_entry_time,
          after: after,
        )
      end
      private_class_method :build_project_query

      def self.build_user_query(user_id, phrase, allowlist, after, before)
        GitHub.driftwood_client.user_query(
          user_id: user_id,
          per_page: PER_PAGE,
          allowlist: allowlist,
          phrase: phrase,
          after: after,
          before: before,
        )
      end
      private_class_method :build_user_query
    end
  end
end
