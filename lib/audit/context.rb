# frozen_string_literal: true

require "context"

module Audit
  module Context

    # The context for the current audit logging environment, such as the
    # request_id, the actor, etc. This context is specific to the audit log
    # and isolated from other contexts which may exist in the application.
    def context
      @context ||= ::Context.new
    end
    attr_writer :context

  end

  extend Context
end
