# rubocop:disable Style/FrozenStringLiteralComment

require "#{Rails.root}/config/environment" unless defined?(Audit)
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/audit/transitions/20160929232312_remove_duplicate_data_event_dot_name_property.rb -v | tee -a /tmp/remove_duplicate_data_event_dot_name_property.log
#
module Audit
  module Transitions
    class RemoveDuplicateDataEventDotNameProperty < Audit::Transition::Base
      def perform
        log "Starting transition #{self.class.to_s.underscore}"

        count = with_each_document do |entry|
          log(entry) if verbose?

          unless dry_run?
            repair(entry)
          end
        end

        log "Finished #{self.class.to_s.underscore}, #{dry_run? ? "found" : "processed"} #{count} documents."

        count
      end

      def query
        {
          query: {
            constant_score: {
              filter: {
                exists: {
                  field: "data.event.name",
                },
              },
            },
          },
        }
      end

      def repair(entry)
        data = entry["data"]
        return false unless data.key?("event.name")

        if data["event.name"] == entry["action"]
          data.delete("event.name")
        else
          data["event_name"] = data.delete("event.name")
        end

        entry
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  opts = Slop.parse(help: true, strict: true) do
    on "i", "indices=", "Comma seperated list of indices to perform the transition on", as: Array, delimiter: ","
    on "c", "cluster=", "Which cluster to perform the transition (default: GitHub.es_audit_log_cluster)"
    on "v", "verbose", "Log verbose output (default: false)", default: false
    on "r", "run", "Run the transition (default: false)", default: false
    on "s", :ignore_failures, "Run the transition across all documents, not just failed documents if any exist (default: false)", default: false
  end

  options = opts.to_hash
  options = options.merge(dry_run: !options.delete(:run))

  transition = Audit::Transitions::RemoveDuplicateDataEventDotNameProperty.new(options)
  transition.perform
end
