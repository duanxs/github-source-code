# frozen_string_literal: true

require "#{Rails.root}/config/environment" unless defined?(Audit)
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/audit/transitions/20180131184420_remove_data_spamurai_form_signals.rb -v | tee -a /tmp/remove_data_spamurai_form_signals.log
#
module Audit
  module Transitions
    class RemoveDataSpamuraiFormSignals < Audit::Transition::Base
      def perform
        log "Starting transition #{self.class.to_s.underscore}"

        count = with_each_document do |entry|
          log(entry) if verbose?

          unless dry_run?
            entry
          end
        end

        log "Finished #{self.class.to_s.underscore}, #{dry_run? ? "found" : "processed"} #{count} documents."

        count
      end

      def query
        {
          query: {
            constant_score: {
              filter: {
                exists: {
                  field: "data.spamurai_form_signals.load_to_submit_in_milliseconds",
                },
              },
            },
          },
          _source: {
            excludes: ["data.spamurai_form_signals"],
          },
        }
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  opts = Slop.parse(help: true, strict: true) do
    on "i", "indices=", "Comma seperated list of indices to perform the transition on", as: Array, delimiter: ","
    on "c", "cluster=", "Which cluster to perform the transition (default: GitHub.es_audit_log_cluster)"
    on "v", "verbose", "Log verbose output (default: false)", default: false
    on "r", "run", "Run the transition (default: false)", default: false
    on "s", :ignore_failures, "Run the transition across all documents, not just failed documents if any exist (default: false)", default: false
  end

  options = opts.to_hash
  options = options.merge(dry_run: !options.delete(:run))

  transition = Audit::Transitions::RemoveDataSpamuraiFormSignals.new(options)
  transition.perform
end
