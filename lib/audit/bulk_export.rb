# rubocop:disable Style/FrozenStringLiteralComment

module Audit
  class BulkExport
    def self.create(format: "json", phrase: nil, subject:)
      options = {
        format: format,
        phrase: phrase,
        subject: subject,
      }

      if GitHub.flipper[:audit_log_driftwood_bulk_export].enabled?(subject)
        Driftwood::BulkExport.new(options)
      else
        Elastic::BulkExport.new(**options)
      end
    end
  end
end
