# rubocop:disable Style/FrozenStringLiteralComment

class TimedResque
  # TimedResque needs to atomically pop the next available job from the schedule
  # set, but Redis ZPOPMIN will always take the first element. This script
  # implements ZPOPMIN but only within a specific range.
  ZPOPMINRANGE = <<~LUA
    local keys = redis.call('ZRANGEBYSCORE', KEYS[1], ARGV[1], ARGV[2], 'LIMIT', 0, 1)
    if #keys > 0 then
      redis.call('ZREM', KEYS[1], keys[1])
      return keys[1]
    end
  LUA
  ZPOPMINRANGE_SHA = Digest::SHA1.hexdigest(ZPOPMINRANGE)

  REMOVE_PAYLOADS_IN_BATCHES_OF = 100

  attr_reader :schedule_key, :payload_key
  attr_writer :redis

  class InvalidJobClassError < StandardError
    def initialize
      super "You must provide a Class that responds to `.queue`"
    end
  end

  class InvalidActiveJobClassError < StandardError
    def initialize
      super "You must provide a Class that is a descendant of ApplicationJob"
    end
  end

  def initialize(options = {})
    @redis = Resque.redis
    @packer = GitHub::ZPack.method(:encode)
    @unpacker = GitHub::ZPack.method(:decode)
    @schedule_key = "#{GitHub.resque_queue_prefix}timed-resque"
    @payload_key = "#{@schedule_key}:jobs"
    @job_class = options[:job_class] || Job
  end

  # Public: Retries a given job at a specific time.
  #
  # job_class - The Class that executes the job. Needs to respond to #queue.
  # args      - Array of arguments for the job.  If the last is a Hash, the
  #             :retries key will be set for the next retry value.  Use this if
  #             the job fails again.
  # at        - The Time to retry the job.
  # retries   - Number of attempted retries.
  # guid      - Job GUID String used to ensure only a single job is added
  #             to the retry queue
  #
  # Returns true if job was queued, false if not.
  def retry_at(job_class, args, at, retries = 0, guid = nil, queue = nil)
    unless queue
      unless job_class.respond_to?(:queue) && job_class.is_a?(Class)
        raise InvalidJobClassError
      end

      queue = job_class.queue
    end

    if !args.kind_of?(Array)
      Failbot.push job_class: job_class.name
      raise TypeError, "args must be an Array, got #{args.class.name}"
    end

    if guid.nil?
      guid = SimpleUUID::UUID.new.to_guid
    else
      return false if @redis.hexists(payload_key, guid)
    end

    retries = -1 if retries < -1
    job = [guid, queue.to_s, job_class.to_s, args, retries]

    @redis.pipelined do
      @redis.hset(payload_key, guid, @packer.call(job))
      @redis.zadd(schedule_key, at.to_i, guid)
    end

    true
  end

  # Public: Retries an ActiveJob job at a specific time.
  #
  # job_class - The Class that executes the job. Needs to be a descendant of ApplicationJob
  # args      - The serialized args for the job. To maintain GlobalID support these should be
  #             serialized before passing them in.
  # at        - The Time to retry the job.
  # retries   - Number of attempted retries. This should come from `job.executions`. This isn't
  #             used internally but maintains a common interface with #retry_at
  # guid      - Job GUID String used to ensure only a single job is added to the retry queue
  #
  # Returns true if job was queued, false if not.
  def retry_active_job_at(job_class, args, at:, retries:, queue:, guid: nil)
    unless job_class <= ApplicationJob
      raise InvalidActiveJobClassError
    end

    if guid.nil?
      guid = SimpleUUID::UUID.new.to_guid
    else
      return false if @redis.hexists(payload_key, guid)
    end

    job = [guid, queue.to_s, job_class.to_s, args, retries]

    @redis.pipelined do
      @redis.hset(payload_key, guid, @packer.call(job))
      @redis.zadd(schedule_key, at.to_i, guid)
    end

    true
  end

  # Public: Queues all eligible jobs to run again.
  #
  # max_run_time - optional Integer number of seconds this method is allowed to
  #                run before exiting. Defaults to nil for no timeout.
  #
  # Returns the Integer number of jobs that were retried.
  def retry!(max_run_time = nil)
    start = Time.now
    retried = 0

    guids_to_remove = []
    each_since(Time.now) do |job|
      job.retry
      guids_to_remove << job.guid
      retried += 1

      if max_run_time && Time.now - start > max_run_time
        GitHub.dogstats.increment("job.retry.runtime_exceeded")
        break # only break after retrying the job so it's not lost
      end

      if retried % REMOVE_PAYLOADS_IN_BATCHES_OF == 0
        remove_payloads(guids_to_remove)
        guids_to_remove.clear
      end
    end
    remove_payloads(guids_to_remove)

    retried
  end

  # Public: returns a list of jobs scheduled for retry at the given timestamp.
  #
  # This is read-only and does not modify the schedule or payloads.
  def since(time, limit = 100)
    guids = @redis.zrangebyscore(schedule_key, "-inf", time.to_i.to_s,
      limit: [0, limit])
    return [] if guids.empty?
    @redis.hmget(payload_key, *guids).map { |data| unpack(data) }.compact
  end

  # Public: Gets the total number of scheduled jobs to retry.
  def count
    @redis.zcard(schedule_key)
  end

  # Public: Resets the scheduled jobs.
  def reset
    @redis.del(schedule_key, payload_key)
  end

  # Public: Remove a job from TimedResque entirely
  def remove_job(guid)
    @redis.pipelined do
      @redis.zrem(schedule_key, guid)
      @redis.hdel(payload_key, guid)
    end
  end

  # Internal: Remove a job payload
  def remove_payloads(guids)
    @redis.hdel(payload_key, guids) if guids.any?
  end

  # Internal: calls the block with all eligible jobs since `time`.
  #
  # This is destructive; it removes each eligible job from the schedule before
  # it is yielded to the block.
  def each_since(time, &block)
    loop do
      guid = zpopminrange(schedule_key, "-inf", time.to_i.to_s)
      break if guid.nil? # no more jobs, we're done!

      data = @redis.hget(payload_key, guid)
      job = unpack(data)
      block.call job if job
    end
  end

  # Public: Gets a Time object representing the next job in the queue.
  def next_run_at
    elements = @redis.zrangebyscore(schedule_key, "-inf", "+inf",
      limit: [0, 1],
      with_scores: true
    )

    _, epoch = elements.first
    ts = epoch.to_i
    ts > 0 ? Time.at(ts) : nil
  end

  # Public: Gets the delay in seconds until the next job runs.
  def delay
    return 0 unless t = next_run_at
    diff = Time.now - t
    diff < 0 ? 0 : diff.to_i
  end

  # Evaluate the ZPOPMINRANGE script. This tries to use the SHA first to save
  # the bandwidth in sending the script and Redis the trouble of re-hashing it.
  # If the script isn't in Redis' script cache, though, it'll eval the script
  # directly, and as a side effect, prime the script cache so the next
  # invocation by SHA will work.
  def zpopminrange(key, min, max)
    @redis.evalsha(ZPOPMINRANGE_SHA, [key], [min, max])
  rescue Redis::CommandError => err
    if err.message =~ /NOSCRIPT/
      @redis.eval(ZPOPMINRANGE, [key], [min, max])
    else
      raise
    end
  end

  def unpack(data)
    return if !data || data.to_s.empty?
    @job_class.new(*@unpacker.call(data))
  end

  class Job
    attr_accessor :guid, :queue, :job_class, :args, :retries

    def initialize(guid = nil, queue = nil, job_class = nil, args = nil, retries = nil)
      @guid      = guid
      @queue     = queue
      @job_class = job_class
      @args      = args
      @retries   = retries
    end

    def retry
      Failbot.push(retry_job_args: {guid: @guid, queue: @queue, job_class: @job_class, args: @args, retries: @retries}) do
        if job_class.constantize <= ApplicationJob
          retry_active_job(args)
        else
          if (options = args.last).is_a?(Hash)
            options["retries"] = retries + 1
          end

          retry_job(queue, job_class, args)
        end
      end
    rescue NameError => e
      # This happens when a job going through the retry queue was removed
      # during a deploy (such as a rollback or on-premise upgrade). If we don't
      # handle these gracefully they result in an infinite loop that completely
      # stops up the whole retry system.
      GitHub.dogstats.increment("job.retry.error", tags: ["error:#{e.class.name.underscore}"])
      Failbot.report(e, retry_job_args: {
        guid: guid,
        queue: queue,
        job_class: job_class,
        args: args,
        retries: retries,
      })
    end

    def retry_job(queue, job_class, args)
      GitHub.dogstats.increment("job.retry.rescheduled", tags: ["class:#{job_class.to_s.underscore}"])

      if job_class.constantize.respond_to?(:active_job_class)
        job_class.constantize.active_job_class.set(queue: queue).perform_later(*args)
      else
        raise TypeError, "expected #{job_class} to implement 'active_job_class'"
      end
    end

    def retry_active_job(serialized_job)
      job = ApplicationJob.deserialize(serialized_job)
      job.class.queue_adapter.enqueue(job)

      GitHub.dogstats.increment("active_job.retry.rescheduled", tags: ["queue:#{queue}", "class:#{job.class.name&.underscore || "(anonymous)"}"])
    end
  end
end
