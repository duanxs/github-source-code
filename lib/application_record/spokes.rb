# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Spokes < Base
    self.abstract_class = true

    connects_to database: { writing: :spokes_write, reading: :spokes_readonly, reading_slow: :spokes_readonly_slow }
  end
end
