# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Stratocaster < Base
    self.abstract_class = true

    connects_to database: { writing: :stratocaster_primary, reading: :stratocaster_readonly }
  end
end
