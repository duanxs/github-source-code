# frozen_string_literal: true

require_relative "../collab"

module ApplicationRecord
  module Domain
    class Ghvs < ApplicationRecord::Collab
      self.abstract_class = true
    end
  end
end
