# frozen_string_literal: true

require_relative "../collab"

module ApplicationRecord
  module Domain
    class CompromisedPasswords < ApplicationRecord::Collab
      self.abstract_class = true
    end
  end
end
