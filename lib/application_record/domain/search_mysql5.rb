# frozen_string_literal: true

require_relative "../mysql5"

module ApplicationRecord
  module Domain
    class SearchMysql5 < ApplicationRecord::Mysql5
      self.abstract_class = true
    end
  end
end
