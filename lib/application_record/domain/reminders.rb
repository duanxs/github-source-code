# frozen_string_literal: true

require_relative "../collab"

module ApplicationRecord
  module Domain
    class Reminders < ApplicationRecord::Collab
      self.abstract_class = true
    end
  end
end
