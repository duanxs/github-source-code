# frozen_string_literal: true

require_relative "../ballast"

module ApplicationRecord
  module Domain
    class UsersBallast < ApplicationRecord::Ballast
      self.abstract_class = true
    end
  end
end
