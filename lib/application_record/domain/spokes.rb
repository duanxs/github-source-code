# frozen_string_literal: true

require_relative "../spokes"

module ApplicationRecord
  module Domain
    class Spokes < ApplicationRecord::Spokes
      self.abstract_class = true

      def self.transaction
        super(requires_new: true)
      end
    end
  end
end
