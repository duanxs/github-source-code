# frozen_string_literal: true

require_relative "../collab"

module ApplicationRecord
  module Domain
    class IntegrationsCollab < ApplicationRecord::Collab
      self.abstract_class = true
    end
  end
end
