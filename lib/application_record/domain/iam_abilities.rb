# frozen_string_literal: true

require_relative "../iam_abilities"

module ApplicationRecord
  module Domain
    class IamAbilities < ApplicationRecord::IamAbilities
      self.abstract_class = true
    end
  end
end
