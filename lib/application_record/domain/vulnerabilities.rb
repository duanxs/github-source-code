# frozen_string_literal: true

require_relative "../notify"

module ApplicationRecord
  module Domain
    class Vulnerabilities < ApplicationRecord::Notify
      self.abstract_class = true
    end
  end
end
