# frozen_string_literal: true

require_relative "../collab"

module ApplicationRecord
  module Domain
    class FeaturePreviews < ApplicationRecord::Collab
      self.abstract_class = true
    end
  end
end
