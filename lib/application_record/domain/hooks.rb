# frozen_string_literal: true

require_relative "../mysql1"

module ApplicationRecord
  module Domain
    class Hooks < ApplicationRecord::Mysql1
      self.abstract_class = true
    end
  end
end
