# frozen_string_literal: true

require_relative "../gitbackups"

module ApplicationRecord
  module Domain
    class Gitbackups < ApplicationRecord::Gitbackups
      self.abstract_class = true
    end
  end
end
