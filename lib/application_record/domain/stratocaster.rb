# frozen_string_literal: true

require_relative "../stratocaster"

module ApplicationRecord
  module Domain
    class Stratocaster < ApplicationRecord::Stratocaster
      self.abstract_class = true
    end
  end
end
