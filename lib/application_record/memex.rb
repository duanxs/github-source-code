# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Memex < Base
    self.abstract_class = true

    def self.throttler_cluster_name
      :blanket
    end

    connects_to database: { writing: :memex_primary, reading: :memex_readonly }
  end
end
