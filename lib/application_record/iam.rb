# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Iam < Base
    self.abstract_class = true

    def self.throttler_cluster_name
      Collab.throttler_cluster_name
    end

    connects_to database: { writing: :iam_primary, reading: :iam_readonly }
  end
end
