# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Notify < Base
    self.abstract_class = true

    connects_to database: { writing: :notify_primary, reading: :notify_readonly }
  end
end
