# frozen_string_literal: true

module ExploreFeed
  module Recommendation
    class Developer
      RECOMMENDATION_DEVELOPER_LIMIT = 25
      attr_reader(
        :user_id,
        :score,
        :reasons,
        :relevant_topic_id,
        :relevant_topic_name,
      )

      delegate(
        :blocking?,
        :following?,
        :id,
        :login,
        :name,
        :no_verified_emails?,
        :most_popular_repository,
        :present?,
        :primary_avatar_url,
        :profile_name,
        :spammy?,
        :suspended?,
        to: :original_user,
      )

      def initialize(attributes)
        @user_id = attributes["user_id"]
        @score = attributes["score"]
        @reasons = attributes["reasons"]
        @relevant_topic_id = attributes["topic_id"]
        @relevant_topic_name = attributes["topic_name"]
      end

      class << self
        def all(user)
          recommended_developers = raw_recommended_developers(user.id).flat_map do |recommendation|
            recommendation["relevant_users"].map do |relevant_users|
              new({
                "reasons" => recommendation["reasons"],
                "topic_id" => recommendation["topic_id"],
                "topic_name" => recommendation["topic_name"],
                "score" => relevant_users["score"],
                "user_id" => relevant_users["user_id"],
              })
            end
          end

          Collection.new(recommended_developers)
            .sorted
            .limit(RECOMMENDATION_DEVELOPER_LIMIT)
            .recommendable_for(user)
        end

        def popular(user, remote_ip: nil, force_cache_miss: false)
          popular_developers = ExploreFeed::PopularDeveloper.all(remote_ip: nil, force_cache_miss: force_cache_miss)
          developers = popular_developers.map { |popular_developer| new(popular_developer) }
          Collection.new(developers)
            .sorted
            .limit(RECOMMENDATION_DEVELOPER_LIMIT)
            .recommendable_for(user)
        end

        private

        def raw_recommended_developers(user_id)
          GitHub.munger.recommended_developers(user_id)&.dig("recommendations").presence || []
        end
      end

      def original_user
        return @original_user if defined? @original_user
        @original_user = ::User.find_by(id: user_id)
      end

      class Collection
        include Enumerable

        def initialize(developers)
          @developers = developers
        end

        def each(&block)
          developers.each(&block)
        end

        def recommendable_for(user)
          recommended_developers = developers
            .select(&:present?)
            .reject(&:no_verified_emails?)
            .reject(&:spammy?)
            .reject(&:suspended?)
            .reject { |developer| user.following?(developer.original_user) }
            .reject do |developer|
              developer.original_user.blocking?(user) ||
              user.blocking?(developer.original_user)
            end


          self.class.new(recommended_developers)
        end

        def sorted
          sorted_developers = developers
            .sort_by(&:score)
            .reverse
            .lazy

          self.class.new(sorted_developers)
        end

        def limit(limit)
          limited_developers = developers
            .first(limit)

          self.class.new(limited_developers)
        end

        private

        attr_accessor :developers
      end
    end
  end
end
