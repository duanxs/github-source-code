# frozen_string_literal: true

module ExploreFeed
  module Trending
    class Developer < SimpleDelegator
      include ExploreHelper

      DEFAULT_PERIOD = "daily"
      TRENDING_DEVELOPER_LIST_LIMIT = 25
      UNKNOWN_LANGUAGE_ID = "null"
      UNKNOWN_LANGUAGE_NAME = "unknown"

      attr_reader(
        :most_popular_repository,
        :munger_profile_location,
        :score,
      )

      class << self
        def all(period: DEFAULT_PERIOD, language: nil)
          Collection
            .new(raw_trending_developers(
              language: language,
              period: period,
            ).map(&method(:new)))
            .sorted
            .recommendable
        end

        private

        def raw_trending_developers(period:, language:)
          GitHub.munger.trending_developers(
            language_id: language_id_for(language),
            page: 1,
            per_page: TRENDING_DEVELOPER_LIST_LIMIT,
            period: period.to_sym,
          ) || []
        end

        def language_id_for(language_name)
          return if language_name.nil?
          return UNKNOWN_LANGUAGE_ID if language_name == UNKNOWN_LANGUAGE_NAME

          language = safe_find_linguist_language(language_name)
          linguist_id = language&.language_id
          LanguageName.find_by(linguist_id: linguist_id)&.id
        end

        def safe_find_linguist_language(language_name)
          deparameterized_language_name = language_name.split("-").join(" ")

          Linguist::Language.find_by_name(deparameterized_language_name) ||
            Linguist::Language.find_by_name(language_name) ||
            Linguist::Language.find_by_alias(language_name)
        end
      end

      def initialize(attributes)
        @munger_profile_location = attributes["profile_location"]
        @score = attributes["score"]
        @most_popular_repository = ::Repository.find_by(id: attributes["most_popular_repo_id"])

        super(::User.find_by(id: attributes["user_id"]))
      end

      def location_info_match?
        profile_location == munger_profile_location
      end

      def original_user
        __getobj__
      end

      class Collection
        include Enumerable

        def initialize(developers)
          @developers = developers
        end

        def each(&block)
          developers.each(&block)
        end

        def recommendable
          recommended_developers = developers
            .select(&:present?)
            .reject(&:spammy?)

          self.class.new(recommended_developers)
        end

        def sorted
          sorted_developers = developers
            .sort_by(&:score)
            .reverse
            .lazy

          self.class.new(sorted_developers)
        end

        def limit(limit)
          limited_developers = developers
            .first(limit)

          self.class.new(limited_developers)
        end

        private

        attr_accessor :developers
      end
    end
  end
end
