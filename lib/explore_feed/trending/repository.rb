# frozen_string_literal: true

module ExploreFeed
  module Trending
    class Repository < SimpleDelegator
      include ApplicationHelper
      include ActionView::Helpers::UrlHelper
      include ActionView::Context
      include AvatarHelper
      include ExploreHelper

      CACHE_TIME_FORMAT = "%Y-%m-%e"
      DATE_OPTIONS = {
        daily: { text: "today", period: 1.day.ago },
        weekly: { text: "this week", period: 7.days.ago },
        monthly: { text: "this month", period: 1.month.ago },
      }.freeze
      DEFAULT_PERIOD = "daily"
      LANGUAGE_NAME_CHARACTER_LIMIT = 25
      MINIMUM_SPOKEN_LANGUAGE_CONFIDENCE = 0.15
      TRENDING_REPOSITORY_LIST_LIMIT = 25
      UNKNOWN_LANGUAGE_ID = "null"
      UNKNOWN_LANGUAGE_NAME = "unknown"
      UNKNOWN_SPOKEN_LANGUAGE_CODE = "unknown"

      attr_reader(
        :primary_language_id,
        :primary_language_name,
        :score,
        :spoken_language_code,
        :spoken_language_confidence,
      )

      def class
        self.repository.class
      end

      class << self
        def all(period: DEFAULT_PERIOD, language: nil, spoken_language_code: nil)
          Collection
            .new(raw_trending_repositories(
              language: language,
              period: period,
              spoken_language_code: spoken_language_code,
            ).map(&method(:new)))
            .sorted
            .by_spoken_language(spoken_language_code: spoken_language_code)
            .recommendable
        end

        private

        def raw_trending_repositories(period:, language:, spoken_language_code:)
          GitHub.munger.trending_repositories(
            language_id: language_id_for(language),
            page: 1,
            per_page: TRENDING_REPOSITORY_LIST_LIMIT,
            period: period.to_sym,
            spoken_language_code: spoken_language_query_param_for(spoken_language_code),
          ) || []
        end

        def language_id_for(language_name)
          return if language_name.nil?
          return UNKNOWN_LANGUAGE_ID if language_name == UNKNOWN_LANGUAGE_NAME

          language = safe_find_linguist_language(language_name)
          linguist_id = language&.language_id
          LanguageName.find_by(linguist_id: linguist_id)&.id
        end

        def safe_find_linguist_language(language_name)
          deparameterized_language_name = language_name.split("-").join(" ")

          Linguist::Language.find_by_name(deparameterized_language_name) ||
            Linguist::Language.find_by_name(language_name) ||
            Linguist::Language.find_by_alias(language_name)
        end

        def spoken_language_name_for(spoken_language_code)
          if spoken_language_code == UNKNOWN_SPOKEN_LANGUAGE_CODE
            "Unknown"
          else
            ::Trending::SpokenLanguageFinder
              .from_code(spoken_language_code)
              .name&.truncate(LANGUAGE_NAME_CHARACTER_LIMIT)
          end
        end

        def spoken_language_query_param_for(spoken_language_code)
          return if spoken_language_name_for(spoken_language_code).blank?

          if spoken_language_code == UNKNOWN_SPOKEN_LANGUAGE_CODE
            "null"
          else
            spoken_language_code
          end
        end
      end

      def initialize(attributes)
        @primary_language_id = attributes["primary_language_id"]
        @primary_language_name = attributes["primary_language_name"]
        @score = attributes["score"]
        @spoken_language_code = attributes["spoken_language_code"]
        @spoken_language_confidence = attributes["spoken_language_confidence"]

        super(::Repository.find_by(id: attributes["repository_id"]))
      end

      def owner_spammy?
        return true unless present?

        async_owner.then do |owner|
          owner.spammy?
        end.sync
      end

      def present?
        original_repository.present?
      end

      def meets_spoken_language_confidence?
        spoken_language_confidence.to_f >= MINIMUM_SPOKEN_LANGUAGE_CONFIDENCE
      end

      def octicon_name
        if fork? && parent.present?
          "repo-forked"
        elsif mirror.present?
          if public?
            "mirror"
          else
            "lock"
          end
        else
          "repo"
        end
      end

      def total_stars
        score && score.fetch(:stars, {}).fetch(:total, nil)
      end

      def total_forks
        score && score.fetch(:forks, {}).fetch(:total, nil)
      end

      def stars_since(period:)
        cache_key_time = DateTime.current.strftime(CACHE_TIME_FORMAT)
        cache_key = ["stars_since", period, "repository", id, cache_key_time].join(".")

        GitHub.cache.fetch(cache_key) do
          stars.where(created_at: created_at_starting_date_for(period: period)..DateTime.now).size
        end
      end

      def description_html
        GitHub::HTML::DescriptionPipeline.to_html(description)
      end

      def contributor_profile_links(force_cache_miss: nil)
        force_cache_miss = force_cache_miss == "true" ? true : false
        cache_key_time = DateTime.current.strftime(CACHE_TIME_FORMAT)
        cache_key = ["contributors", "repository", id, cache_key_time].join(".")

        GitHub.cache.fetch(cache_key, force: force_cache_miss) do
          response = original_repository.contributors

          users = if response.computed?
            response.value.first(5)
          else
            response.value
          end

          users.map do |user, _|
            hydro_attributes = explore_click_tracking_attributes(
              click_context: :TRENDING_REPOSITORIES_PAGE,
              click_target: :CONTRIBUTING_DEVELOPER,
              click_visual_representation: :DEVELOPER_AVATAR,
            )

            profile_link(user, class: "d-inline-block", data: hydro_attributes) do
              avatar_for(user, 20, class:  "avatar mb-1")
            end
          end
        end
      end

      def original_repository
        __getobj__
      end

      private

      def created_at_starting_date_for(period:)
        fallback_option = DATE_OPTIONS[:daily]

        DATE_OPTIONS.fetch(period, fallback_option)[:period]
      end

      class Collection
        include Enumerable

        def initialize(repositories)
          @repositories = repositories
        end

        def each(&block)
          repositories.each(&block)
        end

        def recommendable
          recommended_repositories = repositories
            .select(&:present?)
            .reject(&:private?)
            .reject(&:is_hidden_from_discovery?)
            .reject(&:owner_spammy?)
            .reject(&:trade_restricted?)
            .reject { |repo| repo.access.tos_violation? || repo.access.disabled_by_admin? || repo.access.disabled? }

          self.class.new(recommended_repositories)
        end

        def by_spoken_language(spoken_language_code:)
          trending_repositories = if spoken_language_code.present?
            repositories.select(&:meets_spoken_language_confidence?)
          else
            repositories
          end

          self.class.new(trending_repositories)
        end

        def sorted
          sorted_repositories = repositories
            .sort_by(&:score)
            .reverse
            .lazy

          self.class.new(sorted_repositories)
        end

        def limit(limit)
          limited_repositories = repositories
            .first(limit)

          self.class.new(limited_repositories)
        end

        def next_two(starting_point: 0)
          next_two_repositories = repositories
            .to_a[starting_point, 2]

          self.class.new(next_two_repositories || [])
        end

        def rest(starting_point: 0)
          rest_of_the_repositories = repositories
            .to_a[starting_point..-1]

          self.class.new(rest_of_the_repositories || [])
        end

        private

        attr_accessor :repositories
      end
    end
  end
end
