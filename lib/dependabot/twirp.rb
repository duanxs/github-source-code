# frozen_string_literal: true

module Dependabot
  module Twirp
    include GitHub::AreasOfResponsibility
    areas_of_responsibility :dependabot

    class BaseError < StandardError; attr_accessor :msg; end
    class Error < BaseError; end
    class ServiceUnavailableError < BaseError; end

    def self.update_configs_client
      @update_configs_client ||= UpdateConfigsClient.new
    end

    def self.update_jobs_client
      @update_jobs_client ||= UpdateJobsClient.new
    end
  end
end
