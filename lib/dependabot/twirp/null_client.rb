# frozen_string_literal: true

require "github/null_objects"

# If dependabot configuration has not been set for a GitHub install, we use a
# null client failsover any calls as a Twirp unavailable response so the UI
# behaves as expected.
module Dependabot
  module Twirp
    class NullClient < GitHub::NullObject
      def method_missing(*args, &block)
        ::Twirp::ClientResp.new(nil, unavailable_error)
      end

      private

      def unavailable_error
        ::Twirp::Error.unavailable("Dependabot service is not configured properly.")
      end
    end
  end
end
