# frozen_string_literal: true

module Resqued
  class WorkerPoolConfig
    AQUEDUCT_WORKER = :aqueduct_worker
    RESQUE_WORKER = :resque_worker

    def self.feature_flag_for(app_role)
      GitHub.flipper["aqueduct_workers_#{app_role}"]
    end

    def initialize(app_role:, worker_type:, total_pool_size:,
                   min_resque_workers: 1, min_aqueduct_workers: 1)
      @app_role = app_role
      @worker_type = worker_type
      @total_pool_size = total_pool_size
      @min_resque_workers = min_resque_workers
      @min_aqueduct_workers = min_aqueduct_workers
    end

    def aqueduct_worker?
      @worker_type == AQUEDUCT_WORKER
    end

    def resque_worker?
      !aqueduct_worker?
    end

    def evenly_split?
      rollout == 50
    end

    def aqueduct_majority?
      rollout > 50
    end

    def resque_majority?
      !aqueduct_majority?
    end

    # Apply the worker pool config to a resqued config. Applying enables the
    # aqueduct resqued adapter for aqueduct workers and addx an on_change hook
    # to restart the workers if the feature flag controlling pool allocation
    # changes.
    def apply(resqued_config)
      # Override the worker adapter for aqueduct
      if aqueduct_worker?
        resqued_config.worker_factory do |queues|
          Resqued::AqueductWorkerAdapter.new(*queues)
        end
      end

      # The resqued process tree look like:
      #
      # 1001  \_ resqued master
      # 1002     \_ resqued listener
      # 1003         \_ worker #1
      # 1004         \_ worker #2
      #
      # Resqued configurations are executed by both the listener process and
      # worker processes. Confusingly, `is_a?(Resqued::Config::Worker)` let's
      # us know we're in the listener process.
      if resqued_config.is_a?(Resqued::Config::Worker)
        Resqued::Logging.logger.info("Resqued::Listener") do
          "Starting workers with pool config #{self}"
        end

        # To update worker pool allocations when the worker pool config changes (i.e.
        # when the feature flag % changes), the listener sends a SIGHUP to the master
        # process. The master will create a new listener and worker pool and then
        # shutdown this listener and worker pool.
        on_change do
          Resqued::Logging.logger.info("Resqued::Listener") do
            "Worker pool config changed, restarting..."
          end
          Process.kill(:HUP, Process.ppid)
        end
      end
    end

    def on_change(interval: 5, expand_grace_period: 30, &block)
      initial_rollout = rollout

      Thread.new do
        loop do
          sleep interval

          next if rollout == initial_rollout

          # Are we expanding the aqueduct allocation of the worker pool?
          if rollout > initial_rollout
            # We're expanding the aqueduct pool and shrinking the resque pool.
            # If this is an aqueduct worker, pause for a moment to let the
            # resque pool shrink before expanding to avoid starting too many
            # worker processes.
            sleep expand_grace_period if aqueduct_worker?
          else
            # We're expanding the resque pool and shrinking the aqueduct pool.
            # If this is an resque worker, pause for a moment to let the
            # aqueduct pool shrink before expanding to avoid starting too many
            # worker processes.
            sleep expand_grace_period if resque_worker?
          end

          block.call
          initial_rollout = rollout
        end
      end
    end

    def resque_workers
      workers =[
        total_pool_size - aqueduct_workers,
        total_pool_size - min_aqueduct_workers,
      ].min

      [workers, min_resque_workers].max
    end

    def aqueduct_workers
     workers = [
        (@total_pool_size * rollout / 100).floor,
        total_pool_size - min_resque_workers,
      ].min

      [workers, min_aqueduct_workers].max
    end

    def to_s
      "#<Resqued::WorkerPoolConfig " +
        "worker_type=#{worker_type.inspect} " +
        "app_role=#{app_role.inspect} " +
        "aqueduct_rollout=#{rollout} " +
        "min_resque_workers=#{min_resque_workers} " +
        "min_aqueduct_workers=#{min_aqueduct_workers} " +
        "allocation=\"#{workers} of #{total_pool_size} total workers\">"
    end

    private

    attr_reader :app_role, :worker_type, :total_pool_size,
      :min_resque_workers, :min_aqueduct_workers

    def workers
      aqueduct_worker? ? aqueduct_workers : resque_workers
    end

    def rollout
      # Rollout is 0 if the master feature flag is disabled
      return 0 unless GitHub.aqueduct_enabled?

      feature_flag.percentage_of_time_value
    rescue
      0
    end

    def feature_flag
      self.class.feature_flag_for(app_role)
    end
  end
end
