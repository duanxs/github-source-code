# frozen_string_literal: true

# An actor representing a job class.
#
# The flipper_id includes the class name so it integrates with our flipper
# actor storage and the internal GraphQL API.
class ActiveJob::JobClassActor
  attr_reader :flipper_id

  def self.find_by_id(id) # rubocop:disable GitHub/FindByDef
    new(id)
  end

  def initialize(job_class_name)
    @flipper_id = "#{self.class.name}:#{job_class_name}"
  end

  def to_s
    @flipper_id
  end
end
