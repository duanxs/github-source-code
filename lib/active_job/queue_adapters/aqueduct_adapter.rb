# frozen_string_literal: true

require "active_job/queue_adapters/buffered_resque_adapter"

module ActiveJob
  module QueueAdapters
    # An ActiveJob adapter that publishes jobs to aqueduct. If publishing fails,
    # the adapter falls back to resque. If publishing fails consistently, the
    # circuit breaker will open and bypass aqueduct. Publish requests slower
    # than 1 second are considered a failure and count against circuit breaker
    # stats.
    #
    # To disable the adapter and revert to resque, disable the "aqueduct_publish"
    # feature flag.
    class AqueductAdapter < BufferedResqueAdapter
      # Public: Publish a job to aqueduct or fall back to resque.
      def enqueue(job)
        job.enqueue_backend = :resque
        return super unless GitHub::Aqueduct::Job.enqueue_to_aqueduct?(job.queue_name, job.class)

        job.enqueue_backend = :aqueduct
        result = GitHub::Aqueduct::Job.enqueue_active_job(job)

        if result.retry?
          tags = ["class:#{job.class.name.underscore}", "queue:#{job.queue_name}", "legacy:false"]
          GitHub.dogstats.increment("aqueduct.resque-fallback", tags: tags)
          super
        end
      end
    end
  end
end
