# frozen_string_literal: true

# This is a modified copy of the built-in ResqueAdapter that comes with ActiveJob. Modification was
# necessary for two reasons:
#
# 1. We wrap Resque with BufferedResque, which buffers Resque jobs via Hydro when redis is
#    unavailable, so the adapter needs to enqueue jobs to BufferedResque rather than Resque.
# 2. The ResqueAdapter depends on the resque-scheduler gem for #enqueue_at. This is what is used
#    when doing retries with delays. However this gem requires we upgrade our resque gem to a newer
#    version which isn't possible. We're on an older version that is forked and incompatible with
#    newer versions. To solve this we're using Resque.enqueue_to normally for regular jobs and our
#    existing TimedResque setup for #enqueue_at.
module ActiveJob
  module QueueAdapters
    class BufferedResqueAdapter
      LOCK_SET_PREFIX = "hashlock"

      def enqueue(job)
        publish_path = case job.enqueue_to_hydro
        when true then Resque::BufferedResque::HYDRO_PUBLISH_PATH
        when false then Resque::BufferedResque::RESQUE_PUBLISH_PATH
        end

        Resque::BufferedResque.enqueue_to(
          job.queue_name,
          ResqueAdapter::JobWrapper,
          job.serialize,
          job_identifier: job.class.to_s,
          publish_path: publish_path,
        )
      end

      def enqueue_at(job, timestamp)
        TimedResque::BufferedTimedResque.retry_at(
          job.class,
          job.serialize,
          timestamp,
          queue: job.queue_name,
          retries: job.executions,
        )
      end

      def locked?(job)
        now = Time.now.to_i
        lock_set_key = lock_set(job)
        timeout = now + job.lock_timeout + 1
        tags = ["class:#{job.class.name.underscore}", "queue:#{job.queue_name}", "legacy:false"]

        # If we were able to acquire a new lock then this job isn't locked
        if Resque.redis.hsetnx(lock_set_key, job.lock_key, timeout)
          GitHub.dogstats.increment("jobs.hash-lock.acquire", tags: tags)
          return false
        end

        # If an existing lock timeout exists, this job is locked
        old = Resque.redis.hget(lock_set_key, job.lock_key).to_i
        if now <= old
          GitHub.dogstats.increment("jobs.hash-lock.fail-to-aquire", tags: tags)
          return true
        end

        # This job is not locked but we need to update the timeout first
        Resque.redis.hset(lock_set_key, job.lock_key, timeout)
        GitHub.dogstats.increment("jobs.hash-lock.acquire", tags: tags)
        false
      end

      def clear_lock(job)
        if Resque.redis.hdel(lock_set(job), job.lock_key) > 0
          tags = ["class:#{job.class.name.underscore}", "queue:#{job.queue_name}", "legacy:false"]
          GitHub.dogstats.increment("jobs.hash-lock.release", tags: tags)
        end
      end

      def lock_set(job)
        "#{LOCK_SET_PREFIX}:#{job.class.name}"
      end
    end
  end
end
