# frozen_string_literal: true

module GitSigning
  # Where to report errors.
  HAYSTACK_BUCKET = "git_signing"

  # Which Resque queue to use for any jobs.
  QUEUE = "git_signing"

  # Reasons that signature verification may fail.
  VALID                 = "valid"
  INVALID               = "invalid"
  MALFORMED_SIG         = "malformed_signature"
  UNKNOWN_KEY           = "unknown_key"
  BAD_EMAIL             = "bad_email"
  UNVERIFIED_EMAIL      = "unverified_email"
  NO_USER               = "no_user"
  UNKNOWN_SIG_TYPE      = "unknown_signature_type"
  UNSIGNED              = "unsigned"
  GPGVERIFY_UNAVAILABLE = "gpgverify_unavailable"
  GPGVERIFY_ERROR       = "gpgverify_error"
  NOT_SIGNING_KEY       = "not_signing_key"
  EXPIRED_KEY           = "expired_key"
  OCSP_PENDING          = "ocsp_pending"
  OCSP_ERROR            = "ocsp_error"
  OCSP_REVOKED          = "ocsp_revoked"
  BAD_CERT              = "bad_cert"

  REASONS = [
    VALID, INVALID, MALFORMED_SIG, UNKNOWN_KEY, BAD_EMAIL, UNVERIFIED_EMAIL,
    NO_USER, UNKNOWN_SIG_TYPE, UNSIGNED, GPGVERIFY_UNAVAILABLE, GPGVERIFY_ERROR,
    NOT_SIGNING_KEY, EXPIRED_KEY, OCSP_PENDING, OCSP_ERROR, OCSP_REVOKED
  ]

  # Verify a set of messages/signatures.
  #
  # requests - An Array of request Hashes.
  #            :message   - The String message.
  #            :signature - The String signature.
  #            :email     - The email of the User that allegedly signed the
  #                         message.
  #            :id        - A unique String to identify this Hash.
  #
  # Returns request Hashes, updated to include :valid and :reason keys, plus
  # other metadata.
  def verify_signatures(requests)
    # Raise if a web/api request results in multiple, identical calls.
    NPlusOne.check! if Rails.test?

    return requests if requests.empty?
    maybe_valid = requests.dup

    # Weed out requests without a signature.
    maybe_valid.select! do |req|
      if req[:signature].nil?
        req[:reason] = UNSIGNED
        req[:valid] = false
      elsif klass = [SMIME, GPG].find { |k| k.signature?(req[:signature]) }
        req[:class] = klass
      else
        req[:reason] = UNKNOWN_SIG_TYPE
        req[:valid] = false
      end
    end

    # Load users from emails.
    emails = maybe_valid.map { |r| r[:email] }.uniq.compact
    users = User.find_by_emails(emails)

    # Weed out requests with missing user or unverified email.
    maybe_valid.select! do |req|
      email = req[:email].to_s.downcase
      user = users[email]

      if user.nil? || user.spammy?
        req[:reason] = NO_USER
        req[:valid] = false
      else
        req[:user] = user

        if user.is_a?(Bot) || user.user_email_state == "verified"
          true
        elsif !GitHub.email_verification_enabled?
          true # Email verification isn't require on Enterprise or Cloud.
        else
          req[:reason] = UNVERIFIED_EMAIL
          req[:valid] = false
        end
      end
    end

    # Group requests by class and do actual verification.
    maybe_valid.group_by do |req|
      req[:class]
    end.each do |klass, reqs|
      klass.verify_signatures(reqs)
    end

    requests.group_by do |req|
      req[:reason]
    end.each do |reason, reqs|
      GitHub.dogstats.count("git_signing.verification", reqs.size, tags: ["reason:#{reason}"])
    end

    requests
  end

  extend self
end

require_dependency "git_signing/gpg"
require_dependency "git_signing/smime"
require_dependency "git_signing/verifiable"
require_dependency "git_signing/error"
require_dependency "git_signing/n_plus_one"
