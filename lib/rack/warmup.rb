# rubocop:disable Style/FrozenStringLiteralComment

# Backport rack 1.6.0's Rack::Builder#warmup
#   https://github.com/rack/rack/commit/f791197f501776480a67211afbca0b32c628b2c9
#   https://github.com/rack/rack/commit/b51f3036fbdb911eac6b5cf97ebf9388fb1f14a9
module Rack
  class Builder
    def initialize(default_app = nil, &block)
      @use, @map, @run, @warmup = [], nil, default_app, nil
      instance_eval(&block) if block_given?
    end

    def warmup(prc = nil, &block)
      @warmup = prc || block
    end

    def generate_map(default_app, mapping)
      mapped = default_app ? {"/" => default_app} : {}
      mapping.each { |r, b| mapped[r] = self.class.new(default_app, &b).to_app }
      URLMap.new(mapped)
    end

    def to_app
      app = @map ? generate_map(@run, @map) : @run
      fail "missing run or map statement" unless app
      app = @use.reverse.inject(app) { |a, e| e[a] }
      @warmup.call(app) if @warmup
      app
    end
  end unless Rack::Builder.instance_methods.include?(:warmup)
end
