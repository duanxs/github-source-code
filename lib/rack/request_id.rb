# rubocop:disable Style/FrozenStringLiteralComment

require "securerandom"

module Rack
  # Middleware that generates a UUID4 for every request if a request id doesn't
  # already exist in the environment. The purpose is to use this id for
  # logging in order to follow a request through the entire stack. Ideally the
  # request id is generated before this point, if not we fill it in.
  #
  # Backported/customized from https://github.com/rails/rails/blob/master/actionpack/lib/action_dispatch/middleware/request_id.rb
  class RequestId
    GITHUB_REQUEST_ID = "HTTP_X_GITHUB_REQUEST_ID".freeze

    def self.current
      if env = GitHub::RecordRackEnvMiddleware.last_env
        get(env)
      end
    end

    # Gets the request ID from the environment.
    #
    # env - The request Environment.
    #
    # Returns the ID String.
    def self.get(env)
      env[GITHUB_REQUEST_ID]
    end

    def initialize(app)
      @app = app
    end

    def call(env)
      request_id = external_request_id(env) || internal_request_id
      env[GITHUB_REQUEST_ID] = request_id

      if span = env["rack.span"]
        span.set_tag("guid:github_request_id", request_id)
      end

      @app.call(env).tap do |_status, headers, _body|
        headers["X-GitHub-Request-Id"] = request_id
      end
    end

    private

    def external_request_id(env)
      request_id = env[GITHUB_REQUEST_ID]
    end

    def internal_request_id
      SecureRandom.uuid
    end
  end
end
