# rubocop:disable Style/FrozenStringLiteralComment

# Responsible for scheduling jobs with a TimerDaemon.
#
# Given a `GitHub::Jobs::Job` class (or an object that responds to
# `schedule_options` and `enabled`), schedules the job to run at the configured
# interval.
class JobScheduler
  # TimerDaemon instance
  attr_reader :timer_daemon

  # Public: Requires a `TimerDaemon` object to schedule jobs with.
  def initialize(timer_daemon)
    @timer_daemon = timer_daemon
  end

  # Public: Schedules the given job with the TimerDaemon instance.
  #
  # job - the job class to be scheduled.
  # enqueue_to_hydro - optional feature flag to indicate jobs should be
  #                    published to hydro. Defaults to false.
  #
  # Returns nothing.
  def schedule(job, enqueue_to_hydro: false)
    unless job < ActiveJob::Base
      raise TypeError, "JobScheduler#schedule requires ActiveJob class, but was #{job}"
    end

    return unless job.enabled?
    return unless schedule = job.schedule_options

    timer_daemon.schedule job.name, job.schedule_options[:interval], scope: job.schedule_options[:scope] do |timestamp|
      Failbot.push job: job.name
      run_timer job, enqueue_to_hydro: enqueue_to_hydro
    end
  rescue => boom
    Failbot.report(boom, timer: job.name, note: "#{job.name} not scheduled")
    raise if Rails.env.test?
  end

  # Internal: Performs the job or enqueues the job to be run asynchronously (out
  # of band from the TimerDaemon).
  #
  # Returns nothing.
  def run_timer(job, enqueue_to_hydro: false)
    timer_daemon.log(scheduler: "queued", job_name: job.name)

    if job < ActiveJob::Base
      job.set(enqueue_to_hydro: enqueue_to_hydro).perform_later
    else
      raise TypeError, "JobScheduler#run_timer requires ActiveJob class, but was #{job}"
    end
  end
end
