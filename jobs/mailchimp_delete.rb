# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Deletes an email address from our MailChimp list.
    #
    # Because a call to the MailChimp API could fail for a number
    # of different reasons, this should be isolated in a background job.
    class MailchimpDelete < Job
      def self.active_job_class
        ::MailchimpDeleteJob
      end

      areas_of_responsibility :unassigned

      def self.queue
        :mailchimp
      end

      def self.perform(*args)
        job = new(*args)
        job.perform
        job
      end

      # Options:
      #
      # email_address - String
      # user_id - Integer
      #
      def initialize(email_address, user_id, options = {})
        @email_address = email_address
        @user_id       = user_id
        @options       = options.with_indifferent_access

        Failbot.push(
          job: self.class.name,
          user_id: user_id,
        )
      end

      def perform
        GitHub.dogstats.increment "mailchimp", tags: ["job:delete_email"]

        email = UserEmail.new(email: @email_address, user_id: @user_id)

        with_mailchimp_retries do
          mailchimp = GitHub::Mailchimp.new(email)
          GitHub::Mailchimp.list_ids.each do |list_id|
            mailchimp.ensure_subscriber_exists(list_id) do
              mailchimp.delete(list_id: list_id)
            end
          end
        end
      end

      private

      # Private: When MailChimp API calls fail due to 5xx server errors,
      # retry the call up to 25 times. Otherwise, raise an exception
      # and fail the job.
      def with_mailchimp_retries(&block)
        yield
      rescue GitHub::Mailchimp::Error => boom
        if GitHub::Mailchimp::ServerErrorStatuses.include?(boom.status_code)
          GitHub.dogstats.increment "mailchimp", tags: ["job:delete_email", "type:retry"]

          LegacyApplicationJob.retry_later(self.class.active_job_class, [@email_address, @user_id, @options], retries: @options["retries"])
        else
          raise
        end
      end
    end
  end
end
