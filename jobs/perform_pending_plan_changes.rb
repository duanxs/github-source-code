# frozen_string_literal: true

module GitHub
  module Jobs
    class PerformPendingPlanChanges < Job
      def self.active_job_class
        ::PerformPendingPlanChangesJob
      end

      areas_of_responsibility :gitcoin

      schedule interval: 1.hour

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.queue
        :billing
      end

      def self.perform
        ::Billing::PendingPlanChange.incomplete.scheduled_for(GitHub::Billing.today).find_each do |change|
          RunPendingPlanChangeJob.perform_later(change)
        end
      end
    end
  end
end
