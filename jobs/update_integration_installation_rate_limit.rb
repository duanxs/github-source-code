# frozen_string_literal: true

module GitHub
  module Jobs
    class UpdateIntegrationInstallationRateLimit < Job
      def self.active_job_class
        ::UpdateIntegrationInstallationRateLimitJob
      end

      areas_of_responsibility :platform

      def self.queue
        :update_integration_installation_rate_limit
      end

      def self.enqueue(installation_id)
        UpdateIntegrationInstallationRateLimitJob.perform_later(installation_id)
      end

      def self.perform(installation_id)
        new(installation_id).perform
      end

      def initialize(installation_id)
        @integration_installation = IntegrationInstallation.where(id: installation_id).first
      end

      def perform
        return unless @integration_installation.present?
        @integration_installation.recalculate_rate_limit
      end
    end
  end
end
