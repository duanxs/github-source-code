# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RepositoryFsck < Job
      def self.active_job_class
        ::RepositoryFsckJob
      end

      areas_of_responsibility :git, :repositories

      extend GitHub::CacheLock

      def self.perform(repo_id)
        repository = Repository.find_by_id(repo_id)
        cache_lock("repository-fsck:#{repo_id}", 1.hour) do
          # TODO: GitRPC per call timeout 2000.seconds
          repository.fsck!
        end
      end

      def self.queue
        :repository_fsck
      end
    end
  end
end
