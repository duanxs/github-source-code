# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class HookDeliveryRaceConditionCheck < Job
      def self.active_job_class
        ::HookDeliveryRaceConditionCheckJob
      end

      areas_of_responsibility :webhook

      def self.queue
        :hook_delivery_race_condition_check
      end

      def self.extract_model_and_id_from_exception(record_not_found_error)
        if /\ACouldn't find ([\w:]+) with '?ID'?=(\d+)\z/i =~ record_not_found_error.message
          [$1, $2.to_i]
        end
      end

      def self.queue_from_error(record_not_found_error, *delivery_event_args)
        model_name, id = extract_model_and_id_from_exception(record_not_found_error)

        # Only retry cases where we were explicitly finding by ID.
        raise record_not_found_error unless model_name && id

        HookDeliveryRaceConditionCheckJob.set(wait: 30.seconds).perform_later(model_name, id, delivery_event_args)
      end

      def self.perform(model_name, id, delivery_event_args)
        model_class = model_name.constantize
        return unless model_class.exists?(id)

        # Record now exists in the database. This is likely a symptom of a race-condition
        # between the delivery job being picked up and the record being committed to the DB.
        #
        # This is a bug so we log event details and report it to our stats client
        event_type = delivery_event_args.first
        event = Hook::Event.for_event_type(event_type, delivery_event_args[1])
        action = event.respond_to?(:action) ? event.action : nil
        tags = GitHub::TaggingHelper.create_hook_event_tags(event_type, action)
        GitHub.dogstats.increment("hooks.delivery_race_condition", tags: tags)

        GitHub::Logger.log(
          error: "hook_delivery_race_condition",
          model_name: model_name,
          id: id,
          event_type: event_type,
          action: action
        )
        DeliverHookEventJob.perform_later(*delivery_event_args)
      end
    end
  end
end
