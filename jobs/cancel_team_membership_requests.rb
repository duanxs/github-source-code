# frozen_string_literal: true

module GitHub
  module Jobs
    class CancelTeamMembershipRequests < Job
      def self.active_job_class
        ::CancelTeamMembershipRequestsJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :cancel_team_membership_requests
      end

      def self.perform_with_retry(organization_id, user_id, opts = {})
        new(organization_id, user_id).perform
      end

      def initialize(organization_id, user_id)
        @organization_id = organization_id
        @user_id         = user_id
      end

      def perform
        return unless org = Organization.find(@organization_id)
        return unless user = User.find(@user_id)

        org.cancel_team_membership_requests_for(user)
      end
    end
  end
end
