# frozen_string_literal: true

module GitHub
  module Jobs
    class GitbackupsSweeper < Job
      def self.active_job_class
        ::GitbackupsSweeperJob
      end

      areas_of_responsibility :backups

      schedule interval: 4.minutes

      BACKFILL_JOB_THRESHOLD = 1000

      def self.enabled?
        GitHub.realtime_backups_enabled?
      end

      def self.queue_length
        Resque.size(GitHub::Jobs::RepositoryBackupNgBackfill.queue)
      end

      def self.perform
        Failbot.push app: "gitbackups"

        # The queue can get pretty large as we give backfill the lowest
        # priority. Do not bother doing anything if we're above the threshold.
        return if queue_length > BACKFILL_JOB_THRESHOLD

        SlowQueryLogger.disabled do
          Backups::Maintenance.run_sweeper_ng
        end
      end

      def self.queue
        :gitbackups_sweeper
      end
    end
  end
end
