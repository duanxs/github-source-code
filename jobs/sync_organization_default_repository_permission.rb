# frozen_string_literal: true

module GitHub
  module Jobs
    class SyncOrganizationDefaultRepositoryPermission < Job
      def self.active_job_class
        ::SyncOrganizationDefaultRepositoryPermissionJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :sync_organization_default_repository_permission
      end

      def self.status(target)
        JobStatus.find(job_id(target))
      end

      def self.enqueue(target, actor:)
        JobStatus.create(id: job_id(target))

        SyncOrganizationDefaultRepositoryPermissionJob.perform_later(target.id, target.class.name, actor.id)
      end

      def self.job_id(target)
        "sync-organization-default-repository-permissions-#{target.class.name.downcase}_#{target.id}"
      end

      def self.perform_with_retry(target_id, target_name, actor_id, opts = {})
        actor = User.find(actor_id)
        target_type = target_name.present? ? target_name.constantize : ::Organization
        target = target_type.find(target_id)

        # the JobStatus won't exist for pre-queued upgrades from GHE < 2.15.0.
        # add a backup to create a job status if it doesn't exist.
        status = status(target) || JobStatus.create(id: job_id(target))
        status.track do
          target.sync_default_repository_permission!(actor: actor)
        end
      end
    end
  end
end
