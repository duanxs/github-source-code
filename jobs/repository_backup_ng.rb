# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RepositoryBackupNg < Job
      def self.active_job_class
        ::RepositoryBackupNgJob
      end

      areas_of_responsibility :backups

      include GitHub::CacheLock
      extend GitHub::HashLock

      class InvalidRepositoryType < StandardError; end
      class RepositoryBackupFailed < StandardError; end

      def self.queue
        :gitbackups_perform
      end

      # For HashLock
      def self.lock(*args)
        "#{args[1]}-#{args[0]}"
      end

      CONCURRENT_BACKUPS_PER_FS = 25

      attr_reader :repository, :repo_id, :type, :wiki, :opts

      def self.perform(*args)
        # Clear the lock now that we're running to allow a new push to enqueue
        # while we're waiting for the backup script to run. The HashLock module
        # itself will unlock as well after we're done, but we're not worried
        # since at worst that will allow an extra copy of the job to be
        # enqueued, which is suboptimal but it does not hurt.
        clear_lock(*args)
        # Jobs that are enqueued via ActiveJob rather than resque use a different lock set and
        # calling GitHub::Jobs::RepositoryBackupNg#clear_lock won't clear the ActiveJob lock.
        active_job_class.new(*args).clear_lock
        new(*args).perform
      end

      def initialize(repo_id, type, opts = {})
        @repo_id = repo_id
        @type    = type.to_s
        @wiki    = (@type == "wiki")
        @opts    = opts.with_indifferent_access
        @repository = ActiveRecord::Base.connected_to(role: :reading) do
          case @type
          when "repository", "wiki"
            Repository.find_by_id(repo_id)
          when "gist"
            if repo_id.is_a? Numeric
              Gist.find_by_id(repo_id)
            else
              Gist.find_by_repo_name(repo_id)
            end
          else
            raise InvalidRepositoryType, type
          end
        end
      end

      def perform
        Failbot.push app: "gitbackups"

        # We enqueue gists with a nil ID due to the way we write to them before
        # they exist in the database. Just ignore those.
        return if repo_id.nil?

        # If we've archived the repository, there's nothing to do, just return.
        if repository.nil?
          return if archived?

          # At this point it's mostly spam anon gists at a high rate, let's not bother haystack
          GitHub.dogstats.increment("gitbackups.repository-not-found", {tags: ["type:#{type}"]})
          return
        end

        return unless repository.exists_on_disk?
        return if wiki && !repository.unsullied_wiki.exist?

        setup_backup_context
        if cache_lock_obtain(cache_lock_key, 1.hour)
          begin
            restraint.lock!(restraint_key, CONCURRENT_BACKUPS_PER_FS, 1.hour) do
              result = if wiki
                repository.backup_wiki_ng!
              else
                repository.backup_ng!
              end

              # If backups aren't enabled but we got called, or if the
              # repository has disappeared between getting enqueued and us
              # running, consider it a failure and move on.
              if result.nil?
                GitHub.dogstats.increment("gitbackups.failure", {tags: ["type:#{type}"]})
                return
              end

              if result[:ok]
                GitHub.dogstats.increment("gitbackups.success", {tags: ["type:#{type}"]})
              else
                out, err = result[:out], result[:err]
                Failbot.push stdout: out, stderr: err

                GitHub.dogstats.increment("gitbackups.failure", {tags: ["type:#{type}"]})

                if result[:status] == 2
                  # git-backup returns 2 when when it notices that an on-disk checksum does
                  # not match the repository state. We want an accurate checksum so we enqueue
                  # a fix so we can try later.
                  if repository.is_a?(Gist)
                    SpokesRecomputeGistChecksumsJob.perform_later(repository.id, :vote)
                  else
                    SpokesRecomputeChecksumsJob.perform_later(repository.id, :vote, wiki)
                  end
                  GitHub.dogstats.increment("gitbackups.mismatched_checksum", {tags: ["type:#{type}"]})
                  requeue("mismatched-checksum")
                elsif result[:status] == 5
                  # git-backup detected that we've lost a race to perform a
                  # backup. If the other concurrency controls don't stop us from
                  # performing concurrent backups on a repository, we can detect
                  # it when we're about to commit the incremental. We can ignore
                  # it as the backup is as recent as the one we were trying to
                  # make.
                  GitHub.dogstats.increment("gitbackups.concurrent-backup", {tags: ["type:#{type}"]})
                elsif result[:status] == 6
                  # git-backup has asked to try again later because some service
                  # is unavailable. No need to be noisy about it.
                  requeue("unavailable")
                else
                  requeue("failure")
                  raise RepositoryBackupFailed
                end
              end
            end
          rescue GitHub::Restraint::UnableToLock
            requeue("restraint")
          ensure
            cache_lock_release(cache_lock_key)
          end # begin
        else # cache_lock_obtain
          requeue("cache-lock")
        end
      rescue GitHub::DGit::UnroutedError
        # When a repo record has been destroyed, the fetched record is still
        # available, but Repository#network relationship fails. Die
        # gracefully.
        return
      end

      def setup_backup_context
        ActiveRecord::Base.connected_to(role: :reading) do
          case type
          when "repository", "wiki"
            Failbot.push alternates: repository.shared_storage_enabled?,
                         networked: repository.network.shared_storage_enabled?,
                         backup_type: type,
                         spec: repository.dgit_spec(wiki: type == "wiki")
          when "gist"
            Failbot.push owner: repository.user_param,
                         backup_type: type,
                         spec: repository.dgit_spec
          else
            raise InvalidRepositoryType, type
          end
        end
      end

      def archived?
        r = ActiveRecord::Base.connected_to(role: :reading) do
          case type
          when "repository", "wiki"
            Archived::Repository.find_by_id(repo_id)
          when "gist"
            Archived::Gist.find_by_id(repo_id)
          else
            raise InvalidRepositoryType, type
          end
        end

        return !r.nil?
      end

      def at_limit?
        RateLimiter.at_limit?(limit_key, check_without_increment: true, max_tries: limit_tries, ttl: limit_ttl)
      end

      def limit_key
        "#{type}-gitbackups:v1:failures:#{repository.id}"
      end

      def limit_tries
        3
      end

      def limit_ttl
        4.hours.to_i
      end

      def cache_lock_key
        "#{type}-gitbackups:#{repository.id}"
      end

      def restraint
        @restraint ||= GitHub::Restraint.new
      end

      def restraint_key
        "backups_#{repository.route}"
      end

      def requeue(reason)
        GitHub.dogstats.increment("gitbackups.requeue", {tags: ["reason:#{reason}", "type:#{type}"]})
        guid = self.class.lock(repo_id, type)
        LegacyApplicationJob.retry_later(self.class.active_job_class, [repo_id, type, opts], retries: opts["retries"], guid: guid)
      end
    end
  end
end
