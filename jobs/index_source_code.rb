# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class IndexSourceCode < Job
      def self.active_job_class
        ::IndexSourceCodeJob
      end

      # Update the search index with the information from `before` and `after`
      # commit SHAs from a push event. Files will be added, updated, or
      # removed from the search index.
      #
      def self.perform(repo_id, before, after, ref, *args)
        return unless ::Search::ClusterStatus.code_search_indexing_enabled?

        repository = Repository.find_by_id(repo_id)
        return unless repository

        searchable = repository.code_is_searchable?

        Elastomer.each_writable_index(Elastomer::Indexes::CodeSearch) do |config|
          cs = Elastomer::Indexes::CodeSearch.new(config.name, config.cluster)
          head_ref, head = cs.indexed_head(repository.id)

          # remove all code from the search index if the master branch has
          # changed or if the repository should not be shown in the search
          # results
          if head != repository.default_branch or (head_ref and !searchable)
            cs.remove_code(repository.id) if head_ref
          end
        end
        return unless searchable

        hydrated_at = Time.now.utc

        # NOTE: This should hit the (repository_id, after) index
        push = Push.where(repository_id: repo_id, before: before, after: after, ref: ref).first
        pushed_at = push.present? ? push.created_at.utc.to_f : nil

        # if the after SHA is null, that means we are deleting this branch
        if GitHub::NULL_OID == after
          RemoveFromSearchIndexJob.perform_later("code", repository.id, {pushed_at: pushed_at})
        else
          Search.add_to_search_index("code", repository.id, {head_commit_sha: after, pushed_at: pushed_at})
        end

        nil
      rescue StandardError => err
        Failbot.report(err.with_redacting!)
      end

      # Public: The queue from which this job processor will request work.
      #
      # Returns the queue name as a Symbol
      #
      def self.queue
        :index_high
      end

    end  # IndexSourceCode
  end  # Jobs
end  # GitHub
