# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class LdapUserSync < Job
      def self.active_job_class
        ::LdapUserSyncJob
      end

      extend GitHub::HashLock

      schedule interval: GitHub.ldap_user_sync_interval.hours

      # This value is the amount of time that the lock will be kept
      # whether the job completes or not. It is set to a very long
      # value (2 days) to assure that jobs don't stack up under normal
      # circumstances.

      def lock_timeout
        48.hours + 1.minute
      end

      def self.queue
        :ldap_user_sync
      end

      # Public: Returns true for an Enterprise instance with LDAP sync enabled.
      def self.enabled?
        GitHub.enterprise? && GitHub.ldap_sync_enabled?
      end

      # Public: Performs the LDAP sync for the given User, or for all Users.
      #
      # Returns nothing.
      def self.perform(user_id = nil)
        return unless enabled?
        new(user_id).perform
      end

      # Returns a BatchEnumerator
      def initialize(user_id = nil)
        @iterator =
          if user_id
            User.find(user_id)
          else
            User.where("type = 'User' and id != ?", [User.ghost.id]).in_batches(of: 1000)
          end
      end

      attr_reader :iterator

      # Public: Sync user given by `user_id` or all users.
      #
      # Emits `ldap_user_sync.perform` event with:
      #  :count - number of users synced
      def perform
        GitHub.instrument "ldap_user_sync.perform" do |payload|
          payload[:count] = 0
          sync = GitHub::LDAP::UserSync.new

          if iterator.instance_of? User
            sync.perform(iterator)
            payload[:count] += 1
          else
            iterator.each_record do |user|
              sync.perform(user)
              payload[:count] += 1
            end
          end
        end
      end
    end
  end
end
