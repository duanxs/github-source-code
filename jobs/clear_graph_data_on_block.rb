# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ClearGraphDataOnBlock < Job
      def self.active_job_class
        ::ClearGraphDataOnBlockJob
      end

      areas_of_responsibility :platform

      def self.queue
        :clear_graph_data_on_block
      end

      def self.perform(blocker_id, blockee_id)
        @blocker = User.find(blocker_id)
        @blockee = User.find(blockee_id)

        # Get all the repos by the blocker that the blockee has contributed to
        # clear all their graph data
        @blocker.repositories.find_each do |repo|
          if repo.contributor? @blockee
            GitHub::RepoGraph::GRAPH_NAMES.each do |graph_name|
              GitHub::RepoGraph.clear_cache(repo, graph_name)
            end
          end
        end
      end
    end
  end
end
