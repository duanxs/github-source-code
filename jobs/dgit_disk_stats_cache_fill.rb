# frozen_string_literal: true

module GitHub
  module Jobs
    # Cache DFS disk stats.
    # This is used by dgit's `pick_hosts`, which happens for all new
    # repositories. Loading disk stats from the fileservers adds ~700ms
    # to new repo and gist creation times.
    class DgitDiskStatsCacheFill < Job
      def self.active_job_class
        ::SpokesDiskStatsCacheFillJob
      end

      areas_of_responsibility :dgit

      def self.queue
        :dgit_disk_stats
      end

      def self.perform(queued_at)
        Failbot.push app: "github-dgit-debug"
        now = Time.now.to_i
        stale = now - GitHub.dgit_disk_stats_cache_ttl
        if queued_at > stale
          GitHub::DGit.fill_disk_stats_cache
        end
      end
    end
  end
end
