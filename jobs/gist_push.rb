# rubocop:disable Style/FrozenStringLiteralComment

require "failbot"

module GitHub
  module Jobs
    # Enqueued immediately by the git post-receive hook after a user pushes
    # to a gist repository. All refs pushed are provided in the payload.
    #
    # This job currently just updates the gist record's timestamp so that ref
    # caches are invalidated.
    #
    #   path      - The full path to the repository that was pushed to on disk.
    #   pusher    - The String name of the user who performed the push; or, the
    #               "<user>/<repo>" when push was performed over ssh with deploy
    #               key authentication.
    #   refs      - An Array of [ref, before, after] tuples, one for each
    #               ref that was pushed.
    #   proto     - The protocol used to push to the repository. Either 'ssh' or
    #               'http'.
    #
    class GistPush < Job
      def self.active_job_class
        ::GistPushJob
      end

      areas_of_responsibility :gist

      def self.queue
        :gist_push
      end

      def self.perform(*args)
        job = new(*args)
        job.perform
        job
      end

      attr_reader :path
      attr_reader :pusher
      attr_reader :refs
      attr_reader :proto

      def initialize(path, pusher, refs, proto = nil)
        @path   = path
        @pusher = pusher
        @refs   = refs
        @proto  = proto
      end

      # Returns the Gist associated with the push
      def gist
        return @gist if defined?(@gist)
        @gist = Gist.with_path(path)
      end

      # Do the dirt.
      def perform
        Failbot.push(
          job: self.class.name,
          user: gist&.user_param,
          user_id: gist&.user_id,
          gist_id: gist&.id,
          path: path,
          refs: refs.inspect,
          pusher: pusher,
          proto: proto,
        )

        # gist could have been deleted soon after creation
        if gist.nil?
          repo_name = File.basename(path, ".git")
          archived_gist = Archived::Gist.find_by_repo_name(repo_name)
          if archived_gist.nil?
            fail "gist doesn't exist"
          else
            return
          end
        end

        gist.increment_push_counts
        gist.update_pushed_at Time.now

        gist.async_backup
        gist.synchronize_search_index
      end
    end
  end
end
