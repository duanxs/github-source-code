# frozen_string_literal: true

module GitHub
  module Jobs
    class MirrorScheduler < Job
      def self.active_job_class
        ::MirrorSchedulerJob
      end

      areas_of_responsibility :background_jobs

      schedule interval: 1.minute

      BATCH_SIZE = 10

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.queue
        :mirrors
      end

      def self.perform
        Mirror.schedule_pending(BATCH_SIZE)
      end
    end
  end
end
