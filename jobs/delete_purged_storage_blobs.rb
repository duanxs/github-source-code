# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class DeletePurgedStorageBlobs < Job
      def self.active_job_class
        ::DeletePurgedStorageBlobsJob
      end

      areas_of_responsibility :data_infrastructure

      extend GitHub::HashLock

      schedule interval: 1.hour

      class << self
        attr_accessor :purge_batch_size
      end
      self.purge_batch_size = 100

      def self.queue
        :storage_cluster
      end

      def self.enabled?
        GitHub.storage_cluster_enabled?
      end

      def self.purge_batch
        ::Storage::Purge.where("purge_at <= ?", Time.now).limit(purge_batch_size)
      end

      def self.perform
        return if GitHub::Enterprise.backup_in_progress?

        loop do
          to_purge = purge_batch
          expected = to_purge.count
          purged = GitHub::Storage::Destroyer.perform_purge(to_purge)
          break if purge_batch.count == 0
        end
      end
    end
  end
end
