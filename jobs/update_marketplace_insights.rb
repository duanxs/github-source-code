# frozen_string_literal: true

module GitHub
  module Jobs
    class UpdateMarketplaceInsights < Job
      def self.active_job_class
        ::UpdateMarketplaceInsightsJob
      end

      areas_of_responsibility :marketplace, :background_jobs

      schedule interval: 24.hours

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.queue
        :site_stats
      end

      # Queue an analytics update
      def self.enqueue(date, metric_type)
        UpdateMarketplaceInsightsJob.perform_later(date, metric_type)
      end

      def self.perform(date = nil, metric_types = nil)
        # Metric param may have been converted to a string in the queue
        metric_types = Array(metric_types).map(&:to_sym) unless metric_types.nil?

        update_marketplace_listing_insights(date, metric_types)
      end

      def self.update_marketplace_listing_insights(date, metric_types)
        # If not performing for a specific date, go back to previous days to pick up
        # recurring transactions that have settled since the last run.
        backfill_recurring = date.nil?

        date ||= Date.yesterday

        # Traffic metrics may not yet be available when this job runs, so will be updated
        # separately.
        metric_types ||= Marketplace::ListingInsight::ALL_METRIC_TYPES - [:traffic]

        Marketplace::Listing.publicly_listed.find_each do |listing|
          listing.update_insights_for(date, metric_types)

          if backfill_recurring
            Range.new(date - 3.days, date - 1.day).each do |backfill_date|
              existing_insights = listing.insights.find_by(recorded_on: backfill_date)
              existing_insights&.update_metrics!(:transaction)
            end
          end
        end

        insight_tags = metric_types.map { |type| "#{type}:true" }
        GitHub.dogstats.increment("marketplace.insights.updated", tags: insight_tags)
      end
    end
  end
end
