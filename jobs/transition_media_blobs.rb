# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class TransitionMediaBlobs < Job
      def self.active_job_class
        ::TransitionMediaBlobsJob
      end

      areas_of_responsibility :lfs

      extend GitHub::HashLock

      def self.queue
        :lfs
      end

      def self.lock(options)
        options["id"].to_i
      end

      class RetryError < StandardError; end
      retry_on_error RetryError

      def self.perform_with_retry(options)
        finished = false
        id = options["id"]
        return unless transition = Media::Transition.find_by_id(id.to_i)
        transition.perform
        finished = true
      rescue ActiveRecord::RecordInvalid
        # In all likelihood, we have a blob that lacks an asset and this
        # operation is never going to complete, no matter how many times we
        # retry. Throw an exception and don't requeue.
        raise
      rescue StandardError => err
        if err.is_a?(Media::Blob::CopyError)
          Failbot.report_user_error(err)
        end
        raise RetryError.new
      ensure
        state = finished ? :ok : :error
        GitHub.dogstats.increment("lfs.transition_job", tags: ["state:#{state}"])
      end
    end
  end
end
