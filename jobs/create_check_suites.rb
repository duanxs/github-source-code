# frozen_string_literal: true

module GitHub
  module Jobs
    class CreateCheckSuites < Job
      def self.active_job_class
        ::CreateCheckSuitesJob
      end

      areas_of_responsibility :checks

      def self.queue
        :create_check_suites
      end

      def self.enqueue(push_id:)
        CreateCheckSuitesJob.perform_later(push_id)
      end

      def self.perform_with_retry(push_id, opts = {})
        new(push_id).perform
      end

      def initialize(push_id)
        @push_id = push_id
      end

      def perform
        push = Push.find_by(id: @push_id)
        return unless push

        repository = Repository.find_by(id: push.repository_id)
        return unless repository

        installations_with_access = IntegrationInstallation.with_resources_on(
          subject: repository,
          resources: "checks",
          min_action: :write,
        )

        installations_with_access.each do |installation|
          if push.request_checks? || repository.auto_trigger_checks_for?(
            app_id: installation.integration_id)
            create_check_suite repository, push, installation, {}
          end
        end
      end

      def create_check_suite(repository, push, installation, request_opts)
        attrs = {
          github_app_id: installation.integration_id,
          head_sha: push.after,
          repository_id: repository.id,
        }

        check_suite = CheckSuite.find_by(attrs)

        return if check_suite

        attrs.merge!(
          push_id: push.id,
          creator_id: push.pusher_id,
          head_branch: push.branch_name,
        )

        check_suite = CheckSuite.create!(attrs)
        check_suite.request(**request_opts)
      end
    end
  end
end
