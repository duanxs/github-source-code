# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Take an array of AS:N notifications, convert them to octolytics events,
    # and sends them to the collector via the analytics gem.
    class PublishOctolyticsEvents < Job
      def self.active_job_class
        ::PublishOctolyticsEventsJob
      end

      areas_of_responsibility :analytics

      def self.queue
        :analytics
      end

      # Collector only allows lowercase alphanumerics, dash, and underscore
      # in event types. Downcase and translate everything else to dash.
      def self.transform_name_to_event_type(name)
        name.downcase.gsub(/[^a-z0-9_-]/, "-")
      end

      # Converts an AS:N notification to an Octolytics event. The format of
      # Octolytics events is described in
      # https://github.com/github/data-science/blob/7152c19303e36dd728026857837419cb80a1a96a/docs/AnalyticsPipeline.md.
      # If the payload is a hash containing either of the keys "dimensions" or
      # "measures", those are extracted into the dimensions and measures hashes
      # of the generated octolytics event. All other content is added to the
      # context hash.
      #
      # Arguments are the same order and type as the AS:N subscriber block
      # arguments.
      #
      # Returns the formatted octolytics event as a Hash.
      def self.notification_to_octolytics_event(name, start, ending, transaction_id, payload)
        payload.stringify_keys!

        {
          event_type: transform_name_to_event_type(name),
          timestamp: (start.is_a?(Time) ? start : Time.parse(start)).to_i,
          dimensions: payload.delete("dimensions") || {},
          measures: payload.delete("measures") || {},
          context: payload, # this key must be after the deletes above
        }
      end

      # Send events to Octolytics.
      #
      # events_hash - A hash containing a single key "events" with an array of
      #               event arg arrays from GitHub.subscribe blocks:
      #               [name, start, ending, transaction_id, payload]. Start must
      #               be a string parseable by Time.parse. The ending and
      #               transaction_id values are not used. All Hash keys must be
      #               strings.
      #
      # Examples
      #
      # perform({"events" => [
      #   ["user.vote", "2017-03-09T10:50:32Z", nil, nil, {"user_id" => 680}],
      # ]})
      def self.perform(events_hash)
        events = events_hash.with_indifferent_access["events"].map do |event_args|
          notification_to_octolytics_event(*event_args)
        end
        GitHub.analytics.record(events)
      end
    end
  end
end
