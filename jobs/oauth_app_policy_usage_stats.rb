# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Track GitHub.com usage and update statsd/graphite
    class OauthAppPolicyUsageStats < Job
      def self.active_job_class
        ::OauthAppPolicyUsageStatsJob
      end

      areas_of_responsibility :ecosystem_apps

      schedule interval: 12.hours

      # Run on the low priority queue which runs on the worker instances
      def self.queue
        :oauth_app_policy_usage_stats
      end

      # Don't schedule this job under GHE.
      def self.enabled?
        !GitHub.enterprise?
      end

      # Return the total number of orgs with Org Application Policies enabled
      def self.orgs_restricting_apps
        Organization.where(restrict_oauth_applications: true).count
      end

      # Query site usage stats and update statsd
      def self.perform
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub.dogstats.gauge "usage.organizations.restrict_oauth_applications", orgs_restricting_apps
          nil
        end
      end
    end
  end
end
