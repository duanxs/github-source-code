# frozen_string_literal: true

module GitHub
  module Jobs
    class MaintainTrackingRef < Job
      def self.active_job_class
        ::MaintainTrackingRefJob
      end

      retry_on_error GitHub::DGit::ThreepcError, GitHub::DGit::ThreepcBusyError, GitHub::DGit::ThreepcFailedToLock

      def self.perform_with_retry(pull_request_id, actor_id, options = {})
        pull = PullRequest.find(pull_request_id)
        actor = User.find_by_id(actor_id)

        Failbot.push(repo_id: pull.repository_id, pull_request_id: pull.id)

        pull.maintain_tracking_ref_with_retries(actor, priority: :low)
      end

      def self.queue
        :maintain_tracking_ref
      end
    end
  end
end
