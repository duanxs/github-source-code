# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ToggleRepoVisibility < Job
      def self.active_job_class
        ::ToggleRepoVisibilityJob
      end

      areas_of_responsibility :repo_networks

      REPO_STARGAZERS_BATCH_SIZE = 1000

      def self.perform(repo_id, detach, options = {})
        new(repo_id, detach, options).perform
      end

      def initialize(repo_id, detach, options = {})
        @repo = Repository.find(repo_id)
        @detach = detach
        @options = options.with_indifferent_access
      end

      def perform
        drop_public_events_and_watchers
        extract_forks
        toggle_permission
        scan_for_tokens
        remove_invalid_internal_forks
      rescue Freno::Throttler::CircuitOpen, Freno::Throttler::WaitedTooLong
        LegacyApplicationJob.retry_later(self.class.active_job_class, [@repo.id, @detach, @options], retries: @options["retries"])
      ensure
        @repo.unlock_excluding_descendants!
      end

      def drop_public_events_and_watchers
        if @repo.private?
          @repo.drop_events

          stargazer_ids = Star.where(starrable_id: @repo.id, starrable_type: "Repository").pluck(:user_id)

          stargazer_ids.each_slice(REPO_STARGAZERS_BATCH_SIZE) do |stargazer_ids_slice|
            stargazers = User.with_ids(stargazer_ids_slice)

            Promise.all(stargazers.map { |user|
              @repo.async_readable_by?(user).then do |readable|
                user.unstar(@repo) unless readable
              end
            }).sync
          end
        end
      end

      # Perform on-disk operations necessary when a repository's visibility
      # changes.
      def toggle_permission
        GitHub::ToggleRepoPermission.perform(@repo, @detach)
      end

      # Extract the repository forks into a new network. Forks and their
      # descendants should already be locked.
      def extract_forks
        return unless @repo.public? and @repo.network_root?
        @repo.forks.select(&:private?).each do |fork|
          ::RepositoryNetworkOperationsJob.perform_now(:extract, fork)
        end
      end

      # Ensure scan suitably when repo visibility changes.
      def scan_for_tokens
        return unless @repo.exists_on_disk?

        # If the repo visibility is toggled from public to private, explicitly enable secret scanning to maintain continuity.
        if @repo.private? || GitHub.enterprise?
          @repo.enable_token_scanning(actor: @repo.owner)
        end

        if @repo.scan_for_tokens?
          # This block runs if repo is public or if the old email token scanning flag is enabled
          # Soon the above scan_for_tokens method will only be used for the public path
          # The old email private token scanning flag will be deprecated
          RepositoryTokenScanningJob.perform_later(@repo.id)
        end
        @repo.ensure_current_token_scan_status_entry!
      end

      def remove_invalid_internal_forks
        return unless @repo.private_network_root? && @options["old_visibility"] == "internal"
        RemoveInvalidUserForksJob.perform_later(network_id: @repo.network_id)
      end

      def self.queue
        :critical
      end
    end
  end
end
