# frozen_string_literal: true

module GitHub
  module Jobs
    class UninstallIntegrationInstallation < Job
      def self.active_job_class
        ::UninstallIntegrationInstallationJob
      end

      areas_of_responsibility :ecosystem_apps

      def self.queue
        :uninstall_integration_installation
      end

      def self.perform(*args)
        new(*args).perform
      end

      def initialize(actor_id, installation_id)
        @actor_id        = actor_id
        @installation_id = installation_id
      end

      def perform
        actor = User.find_by(id: @actor_id) || User.ghost

        if (installation = IntegrationInstallation.find_by(id: @installation_id))
          installation.uninstall(actor: actor)
        end
      end
    end
  end
end
