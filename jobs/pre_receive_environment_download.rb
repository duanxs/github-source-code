# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::Jobs::PreReceiveEnvironmentDownload < GitHub::Jobs::Job
  def self.active_job_class
    ::PreReceiveEnvironmentDownloadJob
  end

  areas_of_responsibility :enterprise_only

  class DownloadException < Exception
  end

  def self.perform(env_id)
    hook_env = PreReceiveEnvironment.find(env_id)
    update_script = Rails.production? ?
        "/usr/local/share/enterprise/ghe-hook-env-update" :
        "#{Rails.root}/script/ghe-hook-env-update"

    hook_env.start_download
    begin
      checksum = run([update_script, env_id, hook_env.image_url])
      hook_env.download_succeeded checksum
    rescue DownloadException => e
      hook_env.download_failed e.message
    end
  end

  def self.run(command)
    process = POSIX::Spawn::Child.new(*(command))

    if !process.status.success?
      Failbot.push(
          command: command.inspect,
          stdout: process.out,
          stderr: process.err,
          status: process.status.exitstatus.inspect,
      )
      raise DownloadException, process.err
    else
      return process.out.chomp
    end
  end

  def self.queue
    :pre_receive_environment_download
  end
end
