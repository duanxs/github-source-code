# frozen_string_literal: true

require "github/hash_lock"

module GitHub
  module Jobs
    class TransformUserIntoOrg < Job
      def self.active_job_class
        ::TransformUserIntoOrgJob
      end

      areas_of_responsibility :orgs

      extend GitHub::HashLock

      def self.queue
        :transform_user_into_org
      end

      def self.perform(user_id, owner_id, options = {})
        return unless user  = User.find_by_id(user_id)
        return unless owner = User.find_by_id(owner_id)
        Failbot.push(user.event_context)

        Organization.transform!(user, owner, options)
      end
    end
  end
end
