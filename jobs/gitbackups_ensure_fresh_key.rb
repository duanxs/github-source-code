# frozen_string_literal: true

module GitHub
  module Jobs
    class GitbackupsEnsureFreshKey < Job
      def self.active_job_class
        ::GitbackupsEnsureFreshKeyJob
      end

      areas_of_responsibility :backups

      schedule interval: 1.hour

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.perform
        Failbot.push app: "gitbackups"

        Backups.ensure_fresh_key
      end

      def self.queue
        :gitbackups_encryption
      end
    end
  end
end
