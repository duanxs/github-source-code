# frozen_string_literal: true

module GitHub
  module Jobs
    # StoragePurgeObject takes the oid of an overreplicated object and removes
    # it from enough nodes to meet the GitHub.storage_replica_count quota.
    #
    # This job gets queued from StorageClusterMaintenanceSchedulerJob.perform_now
    class StoragePurgeObject < Job
      def self.active_job_class
        ::StoragePurgeObjectJob
      end

      areas_of_responsibility :data_infrastructure
      extend GitHub::HashLock

      def self.queue
        :storage_cluster
      end

      def self.lock(oid_or_hash)
        GitHub::Jobs::StorageReplicateObject.convert_to_hash_args(oid_or_hash)
      end

      def self.perform(oid_or_hash)
        args = GitHub::Jobs::StorageReplicateObject.convert_to_hash_args(oid_or_hash)
        oid = args["oid"]
        if args["non_voting"]
          purge(oid,
            hosts: GitHub::Storage::Allocator.non_voting_hosts_for_oid(oid, args["datacenter"]),
            limit: GitHub::Storage::Allocator.non_voting_replica_count(args["datacenter"]))
        else
          purge(oid,
            hosts: GitHub::Storage::Allocator.cluster_hosts_for_oid(oid),
            limit: GitHub.storage_replica_count)
        end
      end

      def self.purge(oid, hosts:, limit:)
        return if hosts.size <= limit

        purge_from = hosts.sample(hosts.size - limit)
        purge_from.each do |host|
          ok, body = GitHub::Storage::Client.delete(host, [oid])
          if ok
            GitHub::Storage::Creator.delete_replica_from_host(host, oid)
          end
        end
      end
    end
  end
end
