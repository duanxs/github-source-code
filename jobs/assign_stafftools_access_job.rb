# frozen_string_literal: true
class GitHub::Jobs::AssignStafftoolsAccessJob < GitHub::Jobs::Job
  def self.active_job_class
    ::AssignStafftoolsAccessJob
  end

  areas_of_responsibility :stafftools

  SECURITY_USER = "thechickeneater"

  def self.attempt_to_enqueue(desired_state)
      current_users = ::User.where(gh_role: "staff").pluck(:login)
      users_to_add, users_to_remove = user_changes(desired_state, current_users)
      unpermitted_users = invalid_users_in_job_check(users_to_add)

      return { errors: unpermitted_users } if unpermitted_users.any?

      return {
        job: ::AssignStafftoolsAccessJob.perform_later(users_to_add, users_to_remove),
        users_added_count: users_to_add.length,
        users_removed_count: users_to_remove.length,
      }
  end

  def self.perform(users_to_add, users_to_remove)
    ::User.with_logins(users_to_add).find_each(batch_size: 50) do |user|
      grant_stafftools_access(user)
    end

    ::User.with_logins(users_to_remove).find_each(batch_size: 50) do |user|
      revoke_stafftools_access(user)
    end
  end

  def self.grant_stafftools_access(user)
    user.grant_site_admin_access("Added via api")
    GitHub.dogstats.increment("entitlements.api.grant_stafftools_access")
  end

  def self.revoke_stafftools_access(user)
    user.revoke_privileged_access("Removed via api")
    GitHub.dogstats.increment("entitlements.api.revoke_stafftools_access")
  end

  def self.queue
    :assign_stafftools_access
  end

  def self.invalid_users_in_job_check(users_to_add)
    # We currently validate for this, however the validation is never reached because the
    # grant_site_admin_access function short circuits the staff assignment so we do
    # not get the error on the model. This means we have validation messages sprinkled
    # here and in the stafftools user controller, it would be better IMO to just use the
    # built in activerecord validation so this is all in one place.

    invalid_users = []
    ::User.with_logins(users_to_add).find_each(batch_size: 50) do |user|
      invalid_users << user unless user.can_become_staff?
    end
    invalid_users
  end
  private_class_method :invalid_users_in_job_check

  def self.user_changes(new_users, current_users)
    # LDAP is case insensitive, so we may encounter casing issues, shove everything to downcase.
    new_users = new_users.map(&:downcase)
    users_to_add = new_users - current_users.map(&:downcase)
    users_to_remove = current_users.reject { |u| new_users.include?(u.downcase) }
    users_to_add, user_to_remove = adjust_for_hardcoded_users(users_to_remove, users_to_add)
    return users_to_add, users_to_remove
  end
  private_class_method :user_changes

  def self.adjust_for_hardcoded_users(users_to_remove, users_to_add)
    security_user = "thechickeneater"
    users_to_remove.delete(SECURITY_USER)
    users_to_add.delete(SECURITY_USER)
    return users_to_add, users_to_remove
  end
  private_class_method :adjust_for_hardcoded_users

  def self.promised_state(current_users, users_to_remove, users_to_add)
    new_users = current_users - users_to_remove + users_to_add + [SECURITY_USER]
    ::User.includes(:stafftools_roles).with_logins(new_users)
  end
  private_class_method :promised_state
end
