# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs

    # Destroy dependent records for a model in the background.
    class DestroyDependentRecords < Job
      def self.active_job_class
        ::DestroyDependentRecordsJob
      end

      class DestroyDependentRecordsError < RuntimeError; end

      areas_of_responsibility :background_jobs

      SELECT_BATCH_SIZE = 100

      def self.queue
        :background_destroy
      end

      # Perform a throttled destroy of the dependent records for a model.
      #
      # model_name       - The String class name of the parent model, e.g. Repository
      # model_id         - The Integer primary key id of the parent model. The model
      #                    will have been deleted already, but not these
      #                    dependent records.
      # association_name - The String name of the dependent association which needs
      #                    to be cleared, e.g. "commit_contributions"
      # opts             - Options for the destroy strategy. Currently only the :dependent key is
      #                    supported, and we only respect a value of :nullify for that key, in which
      #                    case we nullify the dependent's foreign key rather than destroying the
      #                    dependent altogether.
      #
      def self.perform_with_retry(model_name, model_id, association_name, opts = {})
        Failbot.push \
          model_name: model_name,
          model_id: model_id,
          association_name: association_name

        model = model_name.constantize

        # Bail out if this record has been restored prior to the job running.
        # Affects repository records that have been restored while jobs are
        # backed up. See https://github.com/github/github/issues/74145.
        ActiveRecord::Base.connected_to(role: :writing, prevent_writes: true) do
          record = model.find_by_id(model_id)
          next if record.nil?
          next if !record.respond_to?(:deleted?)
          next if record.deleted?

          error = DestroyDependentRecordsError.new("Dependents parent record is not deleted")
          Failbot.report(error)
          return
        end

        # For some security, use reflection to retrieve the table and column
        # names rather than relying on user input in the job arguments.
        association = model.reflect_on_association(association_name.to_sym)
        dependent   = association.klass
        table       = GitHub::SQL::LITERAL association.klass.table_name
        foreign_key = GitHub::SQL::LITERAL association.foreign_key
        type_column = GitHub::SQL::LITERAL association.type # may be nil
        type_value  = model.name
        destroy_strategy = opts.with_indifferent_access["dependent"]

        tags = [
          "model:#{model_name.underscore.dasherize}",
          "association:#{association_name.to_s.dasherize}",
          destroy_strategy && "dependent:#{destroy_strategy}",
        ].compact

        GitHub.dogstats.increment("destroy_dependent_records", tags: tags)

        records_destroyed = 0
        last_seen = 0
        loop do
          ids = ActiveRecord::Base.connected_to(role: :reading) do
            sql = dependent.github_sql.new \
              table: table,
              foreign_key: foreign_key,
              type_column: type_column,
              type_value: type_value,
              model_id: model_id,
              last_seen: last_seen,
              count: SELECT_BATCH_SIZE
            sql.add <<-SQL
              SELECT id FROM :table
              WHERE :foreign_key = :model_id
              AND id > :last_seen
            SQL

            if !association.type.blank? # we've got a polymorphic association
              sql.add <<-SQL
                AND :type_column = :type_value
              SQL
            end

            sql.add <<-SQL
              ORDER BY id
              LIMIT :count
            SQL
            sql.values
          end

          break if ids.empty?
          last_seen = ids.last

          dependent.where(id: ids).each do |record|
            record.throttle do
              record.transaction do
                dependent.connection.execute("SELECT 1 /* cross-schema-domain-transaction-exempted */")
                if destroy_strategy.to_s == BackgroundDependentDeletes::DEPENDENT_NULLIFY_OPTION
                  record.update({ association.foreign_key => nil })
                else
                  record.destroy
                end
                records_destroyed += 1
              end
            end
          end
        end

        GitHub.dogstats.count("destroy_dependent_records.records_destroyed", records_destroyed, tags: tags)
      end
    end
  end
end
