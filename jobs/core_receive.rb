# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class CoreReceiveError < StandardError; end

    class CoreReceive
      include Push::CommitsHelper

      def self.queue
        :core_receive
      end

      def self.perform(message)
        job = new(message)

        if job.valid?
          context = {
            job: self.name,
            user: job.user.to_s,
            repo: job.repository&.readonly_name_with_owner,
            before: job.before,
            after: job.after,
            ref: job.ref,
            pusher: job.pusher.to_s,
          }
          Failbot.push(context) { job.perform }
        end
      end

      attr_reader :message
      attr_reader :before, :after, :ref
      attr_reader :user, :repository
      attr_reader :pusher

      def initialize(message)
        @message = message.symbolize_keys!

        # Now do the bulk of actual initialization work,
        # setting the relevant attributes for the new instance
        prepare_message
      end

      def pusher_id
        @pusher.id if @pusher
      end

      def stale?
        user.nil? || repository.nil?
      end

      def valid?
        !stale?
      end

      # The type of event. Receive jobs take different actions depending
      # on what kind of event we are dealing with.
      #
      # Returns a symbol.
      def event
        case
        when created?; :create
        when deleted?; :delete
        else :push
        end
      end

      # Wrap a task with Failbot to ensure failures are reported.
      def perform_task(method, *args, &block)
        name = self.class.name.split("::").last.underscore # post_receive, wiki_receive, etc.

        GitHub.dogstats.time("core_receive", tags: ["name:#{name}", "method:#{method}"]) do
          capture_failures do
            send(method, *args, &block)
          end
        end
      end

      # Do work, sending failures to Failbot.
      def capture_failures(data = {}, &block)
        yield
      rescue Object => boom
        GitHub::Logger.log_exception({job: self.class.name}, boom)
        Failbot.report(CoreReceiveError.new)
      end

      # Set the appropriate instance variables, including a repository object if
      # possible.
      #
      # These are used extensively in the post_receive job, which inherits from this class.
      def prepare_message
        @before, @after, @ref = @message[:before], @message[:after], @message[:ref]

        if @user = User.find_by_login(@message[:user])
          @repository = user.repositories.find_by_name(@message[:repo])
        end

        if (@pusher = User.find_by_login(@message[:pusher]))
          if @pusher.is_a?(Bot) && @repository.present?
            @pusher.installation = @pusher.integration.installations.with_repository(@repository).first
          end
        end
      end
    end
  end
end
