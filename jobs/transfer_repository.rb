# frozen_string_literal: true

module GitHub
  module Jobs
    class TransferRepository < Job
      def self.active_job_class
        ::TransferRepositoryJob
      end

      areas_of_responsibility :repositories

      def self.queue
        :transfer_repository
      end

      def self.perform_with_retry(repository_id, actor_id, new_owner_id, team_ids = [], options = {})
        notify_target = options.with_indifferent_access[:notify_target] || false

        Failbot.push(
          job: self.class.name,
          actor_id: actor_id,
          repo_id: repository_id,
          new_owner_id: new_owner_id,
          team_ids: team_ids.join(","),
        )

        repository = Repository.find(repository_id)
        old_nwo = repository.nwo
        old_owner = User.find(repository.owner.id)
        new_owner = User.find(new_owner_id)
        actor = User.find(actor_id)

        GitHub.stratocaster.disable do
          target_teams = new_owner.teams.where(id: team_ids)
          transfer_successful = throttle_on_abilities_cluster do
            repository.transfer_ownership_to(new_owner, actor: actor, target_teams: target_teams)
          end

          if transfer_successful
            # fire repo added to installation webhook
            repository.reload.instrument_repo_added_to_installations_across_all_repositories
            # safe to trigger an update to the search index now
            repository.instrument_search_transfer_ownership_to(old_owner)

            if notify_target
              AccountMailer.immediate_repository_transfer(repository, actor, new_owner, old_nwo).deliver_now
            end
          end
        end
      end

      # Transferring repos generates a lot of ability grants which can cause replication delay.
      # Ability::Grant throttles on Mysql1 so let's wait until that is in a good state before beginning the transfer.
      def self.throttle_on_abilities_cluster(&block)
        Ability.throttle(&block)
      end
    end
  end
end
