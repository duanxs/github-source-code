# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class MigrationDestroyFile < Job
      def self.active_job_class
        ::MigrationDestroyFileJob
      end

      areas_of_responsibility :migration

      def self.queue
        :migration_destroy_file
      end

      # Queue a migration for archive deletion
      def self.enqueue(migration)
        MigrationDestroyFileJob.perform_later("migration_id" => migration.id)
      end

      # Public: Delete the archive for the migration.
      #
      # migration_id - The id of a Migration.
      def self.perform(options)
        migration_id = options.fetch("migration_id")
        Failbot.push migration_id: migration_id
        migration = ::Migration.find(migration_id)
        migration.destroy_file
      end
    end
  end
end
