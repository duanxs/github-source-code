# frozen_string_literal: true
require "failbot"

module GitHub
  module Jobs
    class SyncRegistryUsage < Job
      def self.active_job_class
        ::SyncRegistryUsageJob
      end

      GIGABYTE = (1024 ** 3).to_f

      minutes = ENV["REGISTRY_SYNC_INTERVAL"].to_i.abs
      schedule interval: (minutes.zero? ? 60 : minutes).minutes

      def self.queue
        :aws_log_scanning
      end

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.perform
        return unless marker = Asset::SyncStatus.get(:registry)
        logset = GitHub.dogstats.time("s3_usage.timing", dogstats_options) do
          self.new.perform(marker)
        end

        Asset::SyncStatus.set(:registry, logset.last_log_key) unless logset.last_log_key.blank?
      end

      def self.log_scanner
        scanner = Asset::LogScanner.new(GitHub.s3_production_data_client, "github-production-logging-b9ae0f", "registry-package-file/")
        scanner.minimum_log_parts = 13
        scanner
      end

      def perform(marker)
        logset = self.class.log_scanner.find(marker)
        stats_gauge "queried_logs", logset.size
        return logset if logset.size.zero?

        diff_sec = Time.now - self.class.log_scanner.marker_to_time(marker)
        stats_gauge "marker_offset", (diff_sec/60).round(1)

        logset_entries = parse_logset(logset)

        repo_activity = aggregate_logset_by_repository(logset_entries)
        owner_activity = aggregate_repository_activity_by_owner(repo_activity)
        save_activity(owner_activity)

        package_file_activity = aggregate_logset_by_package_file(logset_entries)
        package_activity = aggregate_key_activity_by_package(package_file_activity)
        save_package_activity(package_activity)

        logset
      end

      def save_activity(activity)
        ActiveRecord::Base.transaction do
          activity.each do |owner_id, owner_activity|
            owner_activity.each do |started_at, total|
              Asset::Activity.track(:registry, owner_id, 0, started_at,
                up: total[:up],
                down: total[:down],
                source_files: total[:files]
              )
            end
          end
        end
      end

      # Takes a multi-dimensional hash from #aggregate_logset_by_repository and
      # combines activities, looks up necessary database ids, and returns a
      # multi-dimensional hash for easy saving
      #
      # {
      #   owner_id => {
      #     hour_aligned_timestamp => {
      #       up: Float, down: Float, files: [String]
      #     }
      #   }
      # }
      #
      def aggregate_repository_activity_by_owner(repo_activity)
        owner_activity = {}

        repo_activity.keys.in_groups_of(300) do |repo_ids|
          repo_ids.compact!
          repos = ActiveRecord::Base.connected_to(role: :reading) do
            Repository.
              where(id: repo_ids).
              includes(:owner).
              all
          end

          repos.each do |repo|
            next unless owner = repo.owner
            next unless by_repo = repo_activity[repo.id]
            by_owner = owner_activity[owner.id] ||= {}

            by_repo.each do |hour_aligned_time, activities|
              seen_files = ActiveRecord::Base.connected_to(role: :reading) do
                Asset::Activity.seen_source_files(:registry, owner.id, 0, hour_aligned_time)
              end
              rolled_up = (by_owner[hour_aligned_time] ||= { up: 0.0, down: 0.0, files: [] })
              log_already_collided = false

              activities.each do |activity|
                if seen_files.include?(activity[:file])
                  unless log_already_collided
                    stats_increment "id_collision"
                    log_already_collided = true
                  end

                  next
                end

                rolled_up[:up] += activity[:up]
                rolled_up[:down] += activity[:down]
                rolled_up[:files] << activity[:file]
              end
            end
          end
        end

        owner_activity
      end

      # Rolls up an Asset::LogSet into a multi-dimensional hash of all activities grouped
      # into by repository and hour aligned time stamp for later saving.
      #
      # logset_entries  - An array of parsed logset entries.
      #
      # {
      #   repository_id => {
      #     hour_aligned_timestamp => [ {
      #       up: Float, down: Float, file: String
      #     } ]
      #   }
      # }
      def aggregate_logset_by_repository(logset_entries)
        logset_entries.each_with_object({}) do |(log_date, log, item), activity|
          by_repo = activity[item[:repo_id]] ||= {}
          by_time = by_repo[item[:hour_aligned_time]] ||= []
          by_time << {
            up: item[:bandwidth_up].to_f,
            down: item[:bandwidth_down].to_f,
            file: log.key,
          }
        end
      end

      def parse_logset(logset)
        entries = []

        logset.each do |log_date, log, item|
          next unless item = parse_line(item, log_date)

          entries << [log_date, log, item]
        end

        entries
      ensure
        stats_gauge "lines", logset.line_count
        stats_gauge "parsed_logs", logset.log_count
      end

      def parse_line(item, log_date = nil)
        return unless item

        line = item[:raw]

        return unless line.key

        query_params = item[:query]
        method = item[:method]
        parsed_info = item.dup

        parsed_info[:key] = line.key
        parsed_info[:repo_id] = key_to_repo_id(line.key)
        parsed_info[:package_file_guid] = key_to_package_file_guid(line.key)

        bytes_sent = line.bytes_sent.to_f
        object_size = line.object_size.to_f

        case method
        when "GET"
          parsed_info[:bandwidth_down] = bytes_sent / GIGABYTE
        when "POST"
          parsed_info[:bandwidth_up] = object_size / GIGABYTE
        else
          return
        end

        parsed_info
      end

      # Internal: Rolls up an Asset::LogSet into a multi-dimensional hash of all activities grouped
      # into by package file and hour aligned time stamp for later saving.
      #
      # logset_entries  - An array of parsed logset entries.
      #
      # Example:
      #
      #   aggregate_logset_by_package_file(logset)
      #   #=> {
      #     package_file_guid => {
      #       hour_aligned_timestamp => [ {
      #         up: Float, down: Float, file: String
      #       } ]
      #     }
      #   }
      #
      # Returns a Hash.
      def aggregate_logset_by_package_file(logset_entries)
        logset_entries.each_with_object({}) do |(log_date, log, item), activity|
          by_package_file = activity[item[:package_file_guid]] ||= {}
          by_time = by_package_file[item[:hour_aligned_time]] ||= []
          by_time << {
            up: item[:bandwidth_up].to_f,
            down: item[:bandwidth_down].to_f,
            file: log.key,
          }
        end
      end

      # Internal: Takes a multi-dimensional hash from #aggregate_logset_by_package_file
      # and rolls it up by package.
      #
      # package_file_activity     - A Hash in the format returned by aggregate_logset_by_package_file.
      #
      # Example:
      #
      #   aggregate_key_activity_by_package( aggregate_logset_by_package_file(logset) )
      #   #=> {
      #     <#Package id:1> => {
      #       hour_aligned_timestamp => {
      #         up: Float,
      #         down: Float,
      #         files: [String, String]
      #       },
      #       hour_aligned_timestamp => {
      #         up: Float,
      #         down: Float,
      #         files: [String, String]
      #       }
      #     },
      #     <#Package id:2> => {
      #       hour_aligned_timestamp => {
      #         up: Float,
      #         down: Float,
      #         files: [String, String]
      #       }
      #     }
      #   }
      #
      #
      # Returns a Hash.
      def aggregate_key_activity_by_package(package_file_activity)
        package_activity = {}

        package_file_activity.keys.in_groups_of(300) do |file_guids|
          file_guids.compact!
          files = Registry::File.where(guid: file_guids).includes(package_version: :package)

          files.each do |file|
            next unless package = file.package_version&.package
            next unless by_file = package_file_activity[file.guid]
            by_package = package_activity[package] ||= {}

            by_file.each do |hour_aligned_time, activities|
              seen_files = seen_source_files_for_package(package, hour_aligned_time)
              rolled_up = (by_package[hour_aligned_time] ||= { up: 0.0, down: 0.0, files: [] })
              log_already_collided = false

              activities.each do |activity|
                if seen_files.include?(activity[:file])
                  log_already_collided = true
                  next
                end

                rolled_up[:up] += activity[:up]
                rolled_up[:down] += activity[:down]
                rolled_up[:files] << activity[:file]
              end
            end
          end
        end

        package_activity
      end

      # Internal: Looks up the log files we've already scanned for a RegistryPackage containing
      # activity at the specified time.
      #
      # Memoizes the result since we may need to look the up multiple times per aggregaction.
      #
      # package       - The Registry::Package.
      # started_at    - A Time indicating the hour aligned start time for the activity.
      #
      # Returns an Array of log filenames.
      def seen_source_files_for_package(package, started_at)
        @seen_memo ||= {}

        cache_key = [package.id, started_at].join("-")
        @seen_memo[cache_key] ||= Registry::PackageActivity.seen_source_files(package.id, started_at)
      end

      # Internal: Saves Registry::PackageActivity records given an aggreagated list.
      #
      # activity    - A Hash of aggregated activities grouped by Registry::Package
      #               and activity timestamp. See #aggregate_key_activity_by_package
      #               for exact format.
      #
      # Returns nothing.
      def save_package_activity(activity)
        Registry::PackageActivity.transaction do
          activity.each do |package, package_activity|
            package_activity.each do |started_at, total|
              Registry::PackageActivity.track(package, started_at,
                up: total[:up],
                down: total[:down],
                source_files: total[:files]
              )
            end
          end
        end
      end

      def key_to_repo_id(key)
        key.split("/")[0].to_i
      end

      def key_to_package_file_guid(key)
        key.split("/")[1]
      end

      def self.dogstats_options
        { tags: ["type:registry"] }
      end

      def stats_increment(suffix)
        GitHub.dogstats.increment("s3_usage.#{suffix}", self.class.dogstats_options)
      end

      def stats_gauge(suffix, value)
        GitHub.dogstats.gauge("s3_usage.#{suffix}", value, self.class.dogstats_options)
      end
    end
  end
end
