# frozen_string_literal: true

module GitHub
  module Jobs
    class UpdateOrganizationTeamPrivacy < Job
      def self.active_job_class
        ::UpdateOrganizationTeamPrivacyJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :update_organization_team_privacy
      end

      def self.perform_with_retry(organization_id, privacy, opts = {})
        new(organization_id, privacy).perform
      end

      def initialize(organization_id, privacy)
        @organization_id = organization_id
        @privacy         = privacy.to_sym
      end

      def perform
        organization.update_team_privacy!(@privacy)
      end

      private

      def organization
        @organization ||= Organization.find(@organization_id)
      end
    end
  end
end
