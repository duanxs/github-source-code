# frozen_string_literal: true

module GitHub
  module Jobs
    # This job will remove all repo keys created by a given OauthApplication in
    # repositories for which the given User or Organization sets the access
    # policy.
    class RemoveRepoKeysForPolicymakerAndApp < Job
      extend Scientist
      extend GitHub::HashLock

      REPOSITORY_BATCH_SIZE = 100

      def self.active_job_class
        ::RemoveRepoKeysForPolicymakerAndAppJob
      end

      areas_of_responsibility :platform

      # Public: The queue from which this job processor will request work.
      #
      # Returns the queue name as a Symbol
      #
      def self.queue
        :remove_repo_keys_for_policymaker_and_app
      end

      # Public: Remove all public keys in repositories for which the given User
      # or Organization sets the access policy.
      #
      # policymaker_id - Integer ID for User or Organization policymaker
      # app_id         - Integer ID for the OauthApplication
      def self.perform(policymaker_id, app_id)
        policymaker = User.find_by_id(policymaker_id)
        oauth_app   = OauthApplication.find_by_id(app_id)

        return unless policymaker && oauth_app

        public_keys_to_destroy = []
        PublicKey.repository_ids_for_policymaker(policymaker).each_slice(REPOSITORY_BATCH_SIZE) do |repo_ids|
          public_keys_to_destroy.concat(PublicKey.where(repository_id: repo_ids, oauth_application_id: oauth_app.id))
        end

        keys_count = public_keys_to_destroy.count

        # do not send instrumentation notification if nothing to destroy
        return if keys_count < 1

        public_keys_to_destroy.each { |key| key.destroy }

        policymaker.instrument :revoke_repo_keys_for_policymaker_and_app,
          application_id: app_id,
          application_name: oauth_app.try(:name),
          keys_revoked: keys_count
      end
    end
  end
end
