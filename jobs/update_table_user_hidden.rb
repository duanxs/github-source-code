# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs

    # This job updates the user_hidden for a user's content in the given tables
    # based on the user's spammy state.
    class UpdateTableUserHidden < Job
      def self.active_job_class
        ::UpdateTableUserHiddenJob
      end

      areas_of_responsibility :user_profile

      include GitHub::CacheLock
      class CouldNotObtainLock < StandardError; end

      attr_reader :user_id
      attr_reader :tables
      attr_reader :options # the options passed in as arguments

      def self.queue
        :update_table_user_hidden
      end

      # Runs the job for a given user
      #
      # user_id - The User ID for the user whose content we're updating
      # tables - The tables containing the content we're updating
      # options - A Hash of job options (used for retries)
      def self.perform(*args)
        new(*args).perform
      end

      # Creates a new instance of the job
      #
      # user_id - The User ID for the user whose content we're updating
      # tables - The table or tables containing the content we're updating
      # options - A Hash of job options (used for retries)
      def initialize(user_id, tables, options = {})
        @user_id = user_id
        @tables = Array.wrap(tables)
        @options = options.with_indifferent_access
      end

      # Updates the content tables, ensuring we're only running a single job
      # at a time per user.
      def perform
        completed_tables = []

        return unless user
        raise CouldNotObtainLock unless obtain_lock

        user_hidden = user.content_hidden?

        tables.each do |table|
          Spam::UpdateUserHidden.update_table(table, user_hidden, user_id)
          completed_tables << table
        end

        ToggleHiddenUserInNotificationsJob.perform_later(user.id, user_hidden ? "hide" : "unhide")
        ToggleIssueNotificationsForSpammyUsersJob.perform_later(user.id, nil)
        user.calculate_followerings_count
        CalculateStarsCountJob.perform_later(user.id)
        ClearGraphDataOnSpammyJob.perform_later(user.id)

        if user_hidden
          PurgeSpammyFromSearchIndexJob.perform_later(user.id)
          DelistSpammyActionFromMarketplaceJob.perform_later(user: user)
          RecalculateUserDiscussionsJob.perform_later(user) unless GitHub.enterprise?
        else
          RestoreUserFromSpammyJob.perform_later(user.id)
        end
      rescue GitHub::Jobs::UpdateTableUserHidden::CouldNotObtainLock
        requeue(tables: tables - completed_tables, dogstats_tags: ["error:could_not_obtain_lock"])
      rescue Freno::Throttler::CircuitOpen, Freno::Throttler::WaitedTooLong => error
        requeue(tables: tables - completed_tables, dogstats_tags: ["error:replication_throttler"])
      ensure
        release_lock
      end

      def requeue(tables:, dogstats_tags:)
        LegacyApplicationJob.retry_later(self.class.active_job_class, [user_id, tables, options], retries: options["retries"])
        GitHub.dogstats.increment("data.table_user_hidden_requeued", tags: dogstats_tags)

        self
      end

      def obtain_lock
        cache_lock_obtain(cache_lock_key, 5.minutes)
      end

      def release_lock
        cache_lock_release(cache_lock_key)
      end

      private

      def cache_lock_key
        "update_table_user_hidden_#{user_id}"
      end

      def user
        @user ||= User.find_by_id(user_id)
      end
    end
  end
end
