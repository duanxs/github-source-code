# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class QueueMediaTransitionJobs < Job
      def self.active_job_class
        ::QueueMediaTransitionJobsJob
      end

      areas_of_responsibility :lfs

      schedule interval: 10.minutes

      def self.queue
        :lfs
      end

      def self.enabled?
        true
      end

      def self.perform
        new.tap(&:perform)
      end

      def perform
        each_transition &:async_perform
      end

      def each_transition(&block)
        Media::Transition.operations.each_value do |op|
          each_operation_transition(op, &block)
        end
      end

      def each_operation_transition(operation)
        last_id = 0

        loop do
          transitions = Media::Transition.where("operation = ? AND id > ?", operation, last_id).order("id ASC").limit(500)

          return if transitions.blank?

          requeuable = transitions.reject(&:earlier_transition?)
          requeuable.sort_by!(&:id)
          requeuable.each { |t| yield t }
          last_id = transitions.map(&:id).max
        end
      end
    end
  end
end
