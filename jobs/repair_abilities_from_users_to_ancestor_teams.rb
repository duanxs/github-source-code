# frozen_string_literal: true
require "github/jobs/retryable_job"

module GitHub
  module Jobs
    class RepairAbilitiesFromUsersToAncestorTeams < Job
      def self.active_job_class
        ::RepairAbilitiesFromUsersToAncestorTeamsJob
      end


      areas_of_responsibility :platform

      extend GitHub::HashLock
      include GitHub::Jobs::RetryableJob

      def self.queue
        :repair_abilities_from_users_to_ancestor_teams
      end

      # Only a single unique instance of the job can be concurrently running
      # per organization
      def self.lock(org_id, options = {})
        "#{name}::#{org_id}"
      end

      def self.perform_with_retry(org_id, options = {})
        Team::ParentChange::RepairAbilitiesOperation.new(org_id, options).execute
      end
    end
  end
end
