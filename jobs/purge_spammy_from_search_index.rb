# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs

    # This job will purge all records of a user from the search index if that
    # user is flagged as "spammy". Those records include:
    #
    # * user
    # * repositories
    # * source code
    # * commits
    # * issues
    # * issue comments
    # * pull requests
    # * pull request comments
    # * milestones
    # * gists
    # * wikis
    #
    class PurgeSpammyFromSearchIndex < Job
      def self.active_job_class
        ::PurgeSpammyFromSearchIndexJob
      end

      areas_of_responsibility :search

      extend GitHub::HashLock

      # Execute the user by executing this job. Pass in the user ID.
      #
      # user_id - The Integer ID of the user to purge.
      #
      def self.perform(user_id)
        user = User.find_by_id(user_id)
        return unless user and user.spammy?

        new(user).purge_all
      end

      def self.queue
        :index_high
      end

      attr_reader :user

      def initialize(user)
        Failbot.push user_id: user.id, user: user.login
        @user = user
      end

      def purge_all
        purge_user
        purge_issues_and_milestones
        purge_pull_requests
        purge_repositories
        purge_code
        purge_commits
        purge_gists
        purge_wikis
      end

      # Remove the given user from the search index.
      def purge_user
        purge_from_each_writable_index(Elastomer::Indexes::Users)
      end

      # Remove all issues and milestones authored by the user.
      def purge_issues_and_milestones
        purge_from_each_writable_index(Elastomer::Indexes::Issues)
      end

      # Remove all pull requests authored by the user.
      def purge_pull_requests
        purge_from_each_writable_index(Elastomer::Indexes::PullRequests)
      end

      # Remove all repositories owned by the user.
      def purge_repositories
        purge_from_each_writable_index(Elastomer::Indexes::Repos)
      end

      # Remove all source code for the user.
      def purge_code
        purge_from_each_writable_index(Elastomer::Indexes::CodeSearch)
      end

      # Remove all commits for the user.
      def purge_commits
        purge_from_each_writable_index(Elastomer::Indexes::Commits)
      end

      # Remove all gists from the search index for the given user.
      def purge_gists
        purge_from_each_writable_index(Elastomer::Indexes::Gists)
      end

      # Remove all wikis from the search index for the given user.
      def purge_wikis
        purge_from_each_writable_index(Elastomer::Indexes::Wikis)
      end

      def purge_from_each_writable_index(index_class)
        Elastomer.each_writable_index(index_class) do |config|
          begin
            index = index_class.new(config.name, config.cluster)
            index.purge_user(user)
          rescue StandardError => boom
            Failbot.report boom
          end
        end
      end
    end

  end  # Jobs
end  # GitHub
