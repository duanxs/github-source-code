# frozen_string_literal: true

module GitHub
  module Jobs
    class InstallAutomaticIntegrations < Job
      class Result
        # action: one of "installed" or "repositories_added"
        def self.success(job, action, installation, repositories, options = {})
          result = new(:success, installation: installation, repositories: repositories)
          result.report(job, action, installation.target, installation.integration, options)
          result
        end

        def self.failed(job, reason, target, integration, options = {})
          result = new(:error, reason: reason, exception: options[:exception])
          result.report(job, reason, target, integration, options)
          result
        end

        def self.already_installed(job, status, installation, options = {})
          result = new(:success, installation: installation, already_installed: true)
          result.report(job, status, installation.target, installation.integration, options)
          result
        end

        attr_reader :installation, :repositories, :job_status, :reason, :exception

        def initialize(status, installation: nil, repositories: nil, reason: nil, exception: nil, already_installed: false)
          # Note: consider persisting JobStatus if/when we start persisting Result
          @job_status        = JobStatus.new(state: status.to_s)
          @installation      = installation
          @repositories      = repositories
          @reason            = reason
          @exception         = exception
          @already_installed = already_installed
        end

        delegate :success?, :error?, to: :job_status
        alias :failed? :error?

        def already_installed?
          !!@already_installed
        end

        def report(job, status, target, integration, options = {})
          return unless job_status.present?

          tags = options.fetch(:tags, [])
          if options[:reason].present?
            tags << "reason:#{options[:reason]}"
          end

          if options[:installation_type].present?
            tags << "installation_type:#{options[:installation_type]}"
          end

          tags.concat(["installation_queue:#{job.queue}", "integration:#{integration.slug}", "owner:#{integration.owner.login}"])
          GitHub.dogstats.increment("jobs.install_automatic_integrations.#{status}", tags: tags.flatten)

          if options[:exception].present?
            options[:exception] = options[:exception].to_s
          end

          GitHub::Logger.log(
            {
              job: job.name.demodulize,
              status: status.to_s,
              installation_queue: job.queue.to_s,
              integration: integration.slug,
              owner: integration.owner.login,
              target: target.login,
            }.merge(options),
          )
        end
      end
    end
  end
end
