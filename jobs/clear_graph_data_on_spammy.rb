# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ClearGraphDataOnSpammy < Job
      def self.active_job_class
        ::ClearGraphDataOnSpammyJob
      end

      areas_of_responsibility :platform

      def self.queue
        :clear_graph_data_on_spammy
      end

      def self.perform(user_id)
        ActiveRecord::Base.connected_to(role: :reading) do
          repo_ids = User.find_by_id(user_id).try(:unfiltered_contributed_repository_ids) || []

          Repository.where(id: repo_ids).find_each do |repo|
            GitHub::RepoGraph::GRAPH_NAMES.each do |graph_name|
              GitHub::RepoGraph.clear_cache(repo, graph_name)
            end
          end
        end
      end
    end
  end
end
