# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class LdapTeamSync < Job
      def self.active_job_class
        ::LdapTeamSyncJob
      end

      extend GitHub::HashLock

      schedule interval: GitHub.ldap_team_sync_interval.hours

      # This value is the amount of time that the lock will be kept
      # whether the job completes or not. It is set to a very long
      # value (2 days) to assure that jobs don't stack up under normal
      # circumstances.
      def lock_timeout
        48.hours + 1.minute
      end

      def self.queue
        :ldap_team_sync
      end

      # Public: Returns true for an Enterprise instance with LDAP configured.
      def self.enabled?
        GitHub.enterprise? && GitHub.ldap_sync_enabled?
      end

      def self.perform(team_id = nil)
        return unless enabled?
        new(team_id).perform
      end

      def initialize(team_id = nil)
        @sync = GitHub::LDAP::TeamSync.new

        @iterator =
          if team_id
            LdapMapping.where(subject_type: "Team", subject_id: team_id)
          else
            # Find LdapMapping records with unique Distinguished Name (DN)
            # values. Wrap the ActiveRecord scope with an `Enumerator` object
            # so when `each` is called it'll perform finds in batches.
            LdapMapping.where(subject_type: "Team").
              select("DISTINCT(dn)").select(:id).enum_for(:find_each)
          end
      end

      # Public: Sync given team by `team_id` or all teams if no id specified.
      #
      # NOTE: Updates all Teams that map to the same DN.
      #
      # Emits `ldap_team_sync.perform` event with:
      #  :count - number of teams synced
      def perform
        GitHub.instrument "ldap_team_sync.perform", count: 0 do |payload|
          @iterator.group_by(&:dn).each_key do |dn|
            @sync.perform(dn)
            payload[:count] += 1
          end
        end
      end
    end
  end
end
