# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    # Timer that schedules maintenance jobs for DGit gist replicas.
    # See DGit::Maintenance for different types of maintenance tasks.
    class DgitGistMaintenanceScheduler < Job
      def self.active_job_class
        ::SpokesGistMaintenanceSchedulerJob
      end

      areas_of_responsibility :dgit
      extend GitHub::HashLock
      def self.lock_timeout; 10 * 60; end

      schedule interval: 2.minutes

      def self.enabled?; true; end

      # Run every interval, schedules jobs_per_interval maintenance jobs to run.
      def self.perform(gist_id = nil)
        Failbot.push app: "github-dgit"
        SlowQueryLogger.disabled do
          start = Time.now
          result = GitHub.dogstats.time "dgit.gist-maintenance-queries" do
            ActiveRecord::Base.connected_to(role: :reading) do
              DGit::Maintenance.run_gist_stats_and_maintenance(only_gist_id: gist_id)
            end
          end
          GitHub.stats.timing("dgit.gist-maintenance-queries", Time.now-start) if GitHub.enterprise?
          result
        end
      end

      # The queue the scheduler runs on. The actual maintenance jobs don't run
      # on this queue, they're enqueued on the fs maintenance queues.
      def self.queue
        :dgit_schedulers
      end
    end
  end
end
