# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class UserSuspendDormant < Job
      def self.active_job_class
        ::UserSuspendDormantJob
      end

      areas_of_responsibility :enterprise_only

      def self.queue
        :user_suspend_dormant
      end

      def self.perform(threshold)
        return unless users = User.dormant_users(threshold)

        users.each { |u| UserSuspendJob.perform_later(u.id, "Bulk dormant user suspension") }
      end
    end
  end
end
