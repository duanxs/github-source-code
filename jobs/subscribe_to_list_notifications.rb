# rubocop:disable Style/FrozenStringLiteralComment

# Public: Use this job to call GitHub.newsies.subscribe_to_list asynchronously.
#
# Example:
#
#  > user = User.find_by_login("jonmagic")
#  > repo = Repository.with_name_with_owner("github/linguist")
#  > response = GitHub.newsies.subscribe_to_list(user, repo)
#  > if response.failed?
#  >   SubscribeToListNotificationsJob.enqueue(user, repo)
#  > end
#
module GitHub
  module Jobs
    class SubscribeToListNotifications < Job
      def self.active_job_class
        ::SubscribeToListNotificationsJob
      end

      areas_of_responsibility :notifications

      def self.queue
        :notifications
      end

      def self.enqueue(user, repository)
        SubscribeToListNotificationsJob.perform_later(
          { "user_id" => user.id, "repository_id" => repository.id }
        )
      end

      def self.perform_with_retry(options)
        # Support both string and symbol keys from legacy RockQueue.
        options.deep_stringify_keys!

        user_id = options.fetch("user_id")
        repository_id = options.fetch("repository_id")

        user, repository = nil
        ActiveRecord::Base.connected_to(role: :reading) do
          user = User.find_by_id(user_id)
          repository = Repository.find_by_id(repository_id)
        end

        return unless user.present? && repository.present?

        response = Newsies::ListSubscription.throttle { GitHub.newsies.subscribe_to_list(user, repository) }
        if response.failed?
          retries = options["retries"].to_i
          LegacyApplicationJob.retry_later(self.active_job_class, [options], retries: retries)
        end
      end
    end
  end
end
