# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Make sure that environment changes are making it to resque workers.
    #
    # To queue up one of these:
    #
    #     TestEnvJob.perform_later('PATH', 'RBENV_VERSION')
    class TestEnvJob
      def self.queue
        :test
      end

      def self.perform(*vars)
        puts vars.map { |name| "[#$$] #{name}=#{ENV[name]}" }
      end
    end
  end
end
