# rubocop:disable Style/FrozenStringLiteralComment

# Send org survey responses to Octolytics
module GitHub
  module Jobs
    class OrgSurveyAnswersToOctolytics < Job
      def self.active_job_class
        ::OrgSurveyAnswersToOctolyticsJob
      end

      include FactsFromSurveyAnswers

      areas_of_responsibility :analytics

      def self.queue
        :analytics
      end

      def self.perform(creator_id, org_id, octolytics: GitHub.analytics)
        return unless creator = User.find_by_id(creator_id)
        return unless org     = Organization.find_by_id(org_id)

        survey = Survey.find_by_slug!("org_creation")

        new(
          creator: creator,
          org: org,
          survey: survey,
          octolytics: octolytics
        ).perform
      end

      def initialize(creator:, org:, survey:, octolytics:)
        @creator    = creator
        @org        = org
        @survey     = survey
        @octolytics = octolytics
      end

      def perform
        octolytics.record!(analytics_events) if analytics_events.any?
      end

      private

      attr_reader :creator, :org, :survey, :octolytics

      def analytics_events
        @analytics_events ||= facts.map do |fact|
          {
            event_type: "org_creation_survey_response",
            dimensions: {
              creator_id: creator.id,
              creator_country: GitHub::Location.look_up(creator.last_ip),
              org_id: org.id,
              question: fact.dimension,
              answer: fact.value,
            },
            timestamp: fact.recorded_at.to_i,
          }
        end
      end

      def survey_answers
        survey.answers.for(org)
      end
    end
  end
end
