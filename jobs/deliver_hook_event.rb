# rubocop:disable Style/FrozenStringLiteralComment
require "github/jobs/retryable_job"

module GitHub
  module Jobs
    class DeliverHookEvent < Job
      def self.active_job_class
        ::DeliverHookEventJob
      end

      areas_of_responsibility :webhook

      include GitHub::Jobs::RetryableJob
      extend Hookshot::DeliverJobLogger

      def self.queue
        :deliver_hook_event
      end

      def self.perform_with_retry(event_type, event_attributes = {})
        # round-trip through JSON to get string keys only
        event_attributes = GitHub::JSON.parse(GitHub::JSON.encode(event_attributes))

        job_start = Time.now
        event = Hook::Event.for_event_type(event_type, event_attributes)

        action = event.respond_to?(:action) ? event.action : nil
        tags = GitHub::TaggingHelper.create_hook_event_tags(event_type, action)

        # This is a specific timer to instrument how long after queueing the
        # job is picked up
        record_time_enqueued_metrics(job_start, event_attributes["queued_at"], tags)

        result = nil
        GitHub.tracer.with_span("job.deliver_hook_event.deliver") do |span|
          span.set_tag("component", "jobs")
          span.set_tag("hook.event", GitHub::TaggingHelper.hook_event_tag_value(event_type, action))
          span.set_tag("hook.event_type", GitHub::TaggingHelper.hook_event_type_tag_value(event_type))
          result = event.deliver
        end
        record_mysql_metrics(tags)

        result
      rescue ActiveRecord::RecordNotFound => e
        # Queue a job to check later if the record was deleted or just
        # hasn't been committed yet. Deleted records can be ignored
        # but we need to know if there is a transactional race
        # condition.
        Failbot.report_user_error(e, hook_event: event_type, event_attributes: event_attributes)
        GitHub::Jobs::HookDeliveryRaceConditionCheck.queue_from_error(e, event_type, event.attributes)
      rescue ::GitRPC::InvalidRepository
        # The repository has gone away before we could send the event. As we
        # needed repository data to fulfill it, we skip delivering it.
        GitHub.dogstats.increment("hooks.deliveries_repository_gone")
        Failbot.report_user_error(e, hook_event: event_type, event_attributes: event_attributes)
      rescue Hookshot::PayloadTooLarge => e
        log_params = {
          error: e,
          target: (event.target_repository || event.target_organization),
        }

        log_payload_too_large_error(**log_params)
        Failbot.report e
      rescue => e
        if event
          @retryable_args = [event_type, event.attributes]
        end
        raise e
      end

      def self.record_mysql_metrics(tags)
        GitHub::MysqlQueryCounter.counts.each do |db_host, counts|
          counts ||= {}
          tags_with_host = tags + ["rpc_host:#{db_host}"]
          GitHub.dogstats.count("job.deliver_hook_event.rpc.mysql.count.reads", counts[:read].to_i, tags: tags_with_host)
          GitHub.dogstats.count("job.deliver_hook_event.rpc.mysql.count.writes", counts[:write].to_i, tags: tags_with_host)
        end
      end
      private_class_method :record_mysql_metrics

      def self.record_time_enqueued_metrics(start, queued_at, tags)
        return unless queued_at
        queued_at = queued_at.kind_of?(String) ? Time.parse(queued_at) : Time.at(queued_at)
        hook_delay_ms = (start - queued_at) * 1_000
        GitHub.dogstats.timing("job.deliver_hook_event.time_enqueued".freeze, hook_delay_ms, tags: tags)
      rescue TypeError, ArgumentError => e
        Failbot.report(e)
      end
      private_class_method :record_time_enqueued_metrics
    end
  end
end
