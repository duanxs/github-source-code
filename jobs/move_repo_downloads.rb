# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class MoveRepoDownloads < Job
      def self.active_job_class
        ::MoveRepoDownloadsJob
      end

      areas_of_responsibility :downloads

      def self.queue
        :move_repo_downloads
      end

      # Moves a Repositories downloads by configuring a dup of the
      # Download Adapter.
      #
      #   # change owners
      #   perform(@repo.id, {:repo_owner => 'old-user'}, {:repo_owner => 'new-user'})
      #
      #   # rename the repo
      #   perform(@repo.id, {:repo_name => 'old-repo'}, {:repo_name => 'new-repo'})
      #
      # repo_id     - Integer of the Repository owns the Downloads that are
      #               moving.
      # old_options - Hash of keys and values representing the
      #               Download::S3Adapter properties to change for the current
      #               location of the download.
      # new_options - Hash of keys and values representing the
      #               Download::S3Adapter properties to change for the pending
      #               location of the download.
      #
      # Returns nothing.
      def self.perform(repo_id, old_options, new_options)
        return unless repo = Repository.find_by_id(repo_id)

        repo.downloads.each do |download|
          move_download(download, old_options, new_options)
        end
      end

      def self.move_download(download, old_options, new_options)
        old_adapter = configured_adapter download, old_options
        new_adapter = configured_adapter download, new_options

        Download.move old_adapter, new_adapter
      end

      def self.configured_adapter(download, options)
        adapter = download.download_adapter.dup
        options.each do |key, value|
          adapter.send "#{key}=", value
        end
        adapter
      end
    end
  end
end
