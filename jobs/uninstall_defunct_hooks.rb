# frozen_string_literal: true

module GitHub
  module Jobs

    # This job finds and deletes installations for defunct hooks.
    class UninstallDefunctHooks < Job
      def self.active_job_class
        ::UninstallDefunctHooksJob
      end

      areas_of_responsibility :webhook

      SELECT_BATCH_SIZE = 10000
      DELETE_BATCH_SIZE = 500

      def self.queue
        :uninstall_defunct_hooks
      end

      # Runs the job
      #
      def self.perform(*args)
        new(*args).perform
      end

      # Creates a new instance of the job
      def initialize(options = {})
      end

      # Iterates over and deletes defunct hooks.
      def perform
        loop do
          last_seen = 0
          ids = ActiveRecord::Base.connected_to(role: :reading) do
            sql = Hook.github_sql.new \
              names: active_services,
              last_seen: last_seen,
              count: SELECT_BATCH_SIZE
            sql.add <<-SQL
              SELECT id FROM hooks
              WHERE name NOT IN :names
              AND id > :last_seen
              LIMIT :count
            SQL
            sql.values
          end

          break if ids.empty?
          last_seen = ids.last

          ids.each_slice(DELETE_BATCH_SIZE) do |slice|
            Hook.throttle do
              delete = Hook.github_sql.run <<-SQL, ids: slice
                DELETE FROM hooks WHERE id IN :ids
              SQL
            end
          end
        end
      end

      def active_services
        return @active_services if defined?(@active_services)

        @active_services = Hook::Service.list.keys.map(&:to_s)
        @active_services << "web"

        @active_services
      end
    end
  end
end
