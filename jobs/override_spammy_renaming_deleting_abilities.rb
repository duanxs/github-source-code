# rubocop:disable Style/FrozenStringLiteralComment

# Finds all the spammy orgs that a spammy user owns and Sets
# an override to allow them to delete their own orgs
module GitHub
  module Jobs
    class OverrideSpammyRenamingDeletingAbilities < Job
      def self.active_job_class
        ::OverrideSpammyRenamingDeletingAbilitiesJob
      end

      areas_of_responsibility :stafftools

      def self.queue
        :spam
      end

      def self.perform(actor, user, toggle_type)
        user = User.find_by_id(user)
        actor = User.find_by_id(actor)
        kv_key = "user_id.#{user.id}.#{toggle_type}_spammy_orgs_overridden"

        return unless orgs = ActiveRecord::Base.connected_to(role: :reading) { user.owned_organizations }

        if GitHub.kv.exists(kv_key).value!
          orgs.each do |org|
            begin
              if org.spammy?
                instance = ::Stafftools::SpammyRenameDeleteOverride.new(actor: actor, user: org, toggle_type: toggle_type)
                instance.delete_override!
              end
            rescue ActiveRecord::RecordNotFound => boom
              Failbot.report(boom)
              next
            end
          end
          GitHub.kv.del(kv_key)
        else
          orgs.each do |org|
            begin
              if org.spammy?
                instance = ::Stafftools::SpammyRenameDeleteOverride.new(actor: actor, user: org, toggle_type: toggle_type)
                instance.enable_override!
              end
            rescue ActiveRecord::RecordNotFound => boom
              Failbot.report(boom)
              next
            end
          end
          GitHub.kv.set(kv_key, Time.zone.now.to_s)
        end
      end

      def self.enqueue(user, toggle_type)
        OverrideSpammyRenamingDeletingAbilitiesJob.perform_later(user, toggle_type)
      end
    end
  end
end
