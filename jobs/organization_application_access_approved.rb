# frozen_string_literal: true

module GitHub
  module Jobs
    class OrganizationApplicationAccessApproved < Job
      def self.active_job_class
        ::OrganizationApplicationAccessApprovedJob
      end

      areas_of_responsibility :platform, :orgs

      BATCH_SIZE = 1000

      attr_reader :approval, :approver

      def self.queue
        :organization_application_access_approved
      end

      def self.perform(*args)
        new(*args).perform
      end

      def initialize(approval_id, approver_id)
        @approval = OauthApplicationApproval.find_by_id(approval_id)
        @approver = User.find_by_id(approver_id)
      end

      def perform
        return unless approval

        recipient_ids.each_slice(BATCH_SIZE) do |recipient_ids_slice|
          recipients = organization.people.with_ids(recipient_ids_slice)

          recipients.each do |recipient|
            mail = OrganizationMailer.application_access_approved \
            recipient: recipient, organization: organization,
            application: application

            mail.deliver_now
          end
        end
      end

      private

      def application
        @approval.application
      end

      def organization
        @approval.organization
      end

      def recipient_ids
        org_people_with_oauth_access_ids = []
        org_people_ids = organization.people.pluck(:id)

        org_people_ids.each_slice(BATCH_SIZE) do |org_people_ids_slice|
          org_people_with_oauth_access_ids.concat(
              OauthAccess.where(
                  user_id: org_people_ids_slice,
                  application_id: application.id,
              ).pluck(:user_id),
          )
        end

        org_people_with_oauth_access_ids.uniq - [approver.id]
      end
    end
  end
end
