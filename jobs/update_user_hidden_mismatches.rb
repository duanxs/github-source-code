# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class UpdateUserHiddenMismatches < Job
      def self.active_job_class
        ::UpdateUserHiddenMismatchesJob
      end

      areas_of_responsibility :platform

      if GitHub.spamminess_check_enabled?
        schedule interval: 5.minutes
      end

      def self.queue
        :spam
      end

      # Don't schedule this job when spammy checks aren't enabled.
      def self.enabled?
        GitHub.spamminess_check_enabled?
      end

      UPDATED_SINCE_TIME_SPAN = 30.minutes

      # Public: Perform the job to update the `*.user_hidden` column from `users.spammy`.
      #
      # Returns nothing.
      def self.perform
        user_ids = Spam::UpdateUserHidden.user_ids_for_mismatches(
          updated_since: UPDATED_SINCE_TIME_SPAN.ago,
        )

        user_ids.each do |user_id|
          UpdateTableUserHiddenJob.perform_later(user_id, Spam::Spammable.tables_classes_including.keys)
        end
      end
    end
  end
end
