# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class PostDelayedAudit < Job
      def self.active_job_class
        ::PostDelayedAuditJob
      end

      # Elastomer::Client::Error: Exceptions can be generated from Elasticsearch
      # and should be retried.
      retry_on_error Elastomer::Client::Error, SystemCallError, Audit::SplunkHEC::Logger::ResponseError

      def self.queue
        :audit_logs
      end

      # We need to massage the method signature for perform for
      # GitHub::Jobs::RetryErrors to be able to keep track of the number of
      # retries. The payload argument is a Hash and RetryErrors mistakes it for
      # a place to keep track of retries.
      def self.perform(action, payload, options = {})
        super(action, payload, options)
      end

      def self.perform_with_retry(action, payload, options = {})
        new(action, payload, options).perform
      end

      attr_reader :action
      attr_reader :payload
      attr_reader :options

      def initialize(action, payload, options = {})
        @action  = action
        @payload = payload
        @options = options

        # The default behavior in production is to log to Failbot and continue.
        # We want to raise so the retry behavior can take over and manage
        # execution.
        audit.on_error do |error|
          raise error
        end
      end

      def perform
        audit.log_payload(action, payload.dup)
      end

      private

      def audit
        @audit ||= GitHub.audit
      end
    end
  end
end
