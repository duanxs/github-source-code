# frozen_string_literal: true

require "github/hash_lock"
require "github/dgit/cold_storage_sql"

module GitHub
  module Jobs
    class DgitMoveNetworkFromColdStorage < Job
      def self.active_job_class
        ::SpokesMoveNetworkFromColdStorageJob
      end

      areas_of_responsibility :dgit
      extend GitHub::HashLock

      def self.queue
        :dgit_repairs
      end

      def self.perform(network_id)
        Failbot.push(
          app: "github-dgit",
          job: self.class.name,
          spec: "network/#{network_id}",
          network: network_id,
        )

        GitHub::Logger.log(
          job: "DgitMoveNetworkFromColdStorage",
          at: "start",
          spec: "network/#{network_id}",
          network: network_id,
        )

        # Mark this network as no longer in cold storage
        # Maintenance for it will be held off for 2hr (FIXME) to ensure
        # adequate time for the move from cold storage to be performed
        # by placer
        if !GitHub::DGit::ColdStorage.mark_network_not_cold(network_id)
          Failbot.report GitHub::DGit::ColdStateUnchangedError.new("Could not mark network as no longer cold")

          # No point proceeding if DB can't be updated as maintenance
          # sweeper would simply undo any changes we might try here
          return
        end

        GitHub.dogstats.increment("dgit.actions.cold-storage.from")
      end
    end
  end
end
