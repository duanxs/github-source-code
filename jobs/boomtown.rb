# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # This job is for site admin testing of error processing for background jobs.
    # See also SiteController#boomtown
    class Boomtown < Job
      def self.active_job_class
        ::BoomtownJob
      end

      areas_of_responsibility :background_jobs

      def self.queue
        :boomtown
      end

      def self.perform(options = {})
        if options[:cause]
          begin
            raise "the underlying cause (from a job)"
          rescue
            raise "the outer wrapper (from a job)"
          end
        else
          raise "BOOM! (from a job)"
        end
      end
    end
  end
end
