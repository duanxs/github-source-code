# rubocop:disable Style/FrozenStringLiteralComment

require "set"

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"
require "github/dgit/routing"

module GitHub
  module Jobs
    class DgitRepairNetworkReplica < Job
      def self.active_job_class
        ::SpokesRepairNetworkReplicaJob
      end

      areas_of_responsibility :dgit

      # Don't sync locks.
      EXCLUDE_RSYNC = [
        "/*/dgit-state.*",
        "/packed-refs.new",
        "*.lock",
        "/*/.backup_lock",
        "/*/objects/info/alternates+",
      ]

      # When syncing data with remote replicas, we need to account for reduced TCP throughput.
      #
      # Given 30 MiB/sec throughput, GitRPCs default 30 minute timeout would only allow us to copy ~50 GiB of data,
      # ignoring time spent on the sender side checksumming data.
      #
      # Bumping the timeout to two hours gives us an upper limit of 200 GiB, which - in combination with repo quotas of
      # 100 GiB - should serve us for some time.
      RSYNC_TIMEOUT = 2.hours

      # Try this many times -- repairing a network can fail if it
      # changes while the repair is taking place.  If after several
      # attempts, one or more repos in the network replica don't match
      # their expected checksums, trigger a destroy job.
      REPAIR_ATTEMPTS = 4

      def self.force_checksum_recomputation?
        false
      end

      def self.perform(network_id, host)
        Failbot.push app: "github-dgit",
                     spec: "network/#{network_id}"

        GitHub::Logger.log_context(job: name.demodulize, spec: "network/#{network_id}") do
          GitHub::Logger.log(method: "perform!", host: host) do
            GitHub.dogstats.time("dgit.actions.repair-network", tags: ["server:#{host}"]) do
              perform!(network_id, host, GitHub::DGit::FAILED, nil)
            end
          end
        end
      rescue Freno::Throttler::Error
        # Ignore freno errors and rely on retries from our own maintenance scheduler
      end

      def self.network_replica_for_host(network_id, host)
        # Retry getting a network replica a few times, to make sure
        # the database has caught up.
        replica = nil
        (0..3).each do
          replica = GitHub::DGit::Routing.network_replica_for_host(network_id, host)
          break if replica
          GitHub.dogstats.increment("repair_network_replica.wait_for_replication.retry")
          sleep 5
        end
        raise GitHub::DGit::ReplicaRepairError, "Network #{network_id} on #{host} not found" unless replica
        replica
      end

      def self.perform!(network_id, host, prior_state, after_rsync = nil)
        replica = network_replica_for_host(network_id, host)
        storage_path = GitHub::Routing.nw_storage_path(network_id: network_id)
        rpc = replica.to_route(storage_path).build_maint_rpc

        if replica.state != prior_state
          GitHub::Logger.log message: "Repair of #{network_id} failed: expected state "\
                       "#{GitHub::DGit::STATES[prior_state]} but got "\
                       "#{GitHub::DGit::STATES[replica.state]}"
          return
        end

        delegate = GitHub::DGit::Delegate::Network.new(network_id, storage_path)
        begin
          read_route = delegate.get_read_routes.first
          read_route_resolved = read_route.resolved_host
          read_route_path = read_route.path
        rescue GitHub::DGit::UnroutedError => e
          GitHub::Logger.log_exception({method: "get_read_routes"}, e)
          GitHub::Logger.log message: "Repair of #{network_id} failed: unrouted (no available read replica)"
          return
        end

        # bail if we're somehow repairing from the same host
        if read_route.original_host == host
          raise GitHub::DGit::ReplicaRepairError, "Cannot repair network #{network_id} on #{host} from itself"
        end

        # Only report this to statsd if we're performing a real
        # repair (which includes restoring a backup).  Don't tell
        # statsd about repairs that are due to replica creation and
        # replica migration (i.e., disk rebalancing).
        tell_statsd = (prior_state == GitHub::DGit::FAILED)
        GitHub.stats.increment "dgit.#{host}.actions.repair-network" if tell_statsd && GitHub.enterprise?
        GitHub.dogstats.increment "dgit", tags: ["host:#{host}", "action:repair_network"]
        start = Time.now
        GitHub::Logger.log message: "DGit repairing network replica #{network_id} on #{host} from source host #{read_route.original_host}",
            at: "start",
            source_host: read_route.original_host

        # Set the network replica to REPAIRING
        if !GitHub::DGit::Maintenance::set_network_state(network_id, host, GitHub::DGit::REPAIRING, prior_state)
          GitHub::Logger.log message: "Repair of #{network_id} failed: could not change state "\
                       "from #{GitHub::DGit::STATES[prior_state]} to REPAIRING"
          return
        end

        GitHub::DGit::Maintenance.backup_before_repair_with_rpc(rpc, self) unless prior_state == GitHub::DGit::CREATING

        (1..REPAIR_ATTEMPTS).each do |repair_attempt|
          begin
            # hash repo_id -> type -> update time/checksum to examine after the rsync
            initial_checksums = GitHub::DGit::Maintenance.checksums_for_network(network_id)
            if initial_checksums.empty?
              GitHub::Logger.log message: "Repair of network #{network_id} skipped: network is empty"
              GitHub::DGit::Maintenance.set_network_state(
                network_id, host, GitHub::DGit::ACTIVE, GitHub::DGit::NOT_DORMANT)
              return
            end
            rsync_url = if Rails.development? || Rails.test?
              read_route_path + "/"
            else
              "git@#{read_route_resolved}:#{read_route_path}/"
            end

            begin
              rpc.ensure_dir(rpc.backend.path)
            rescue GitRPC::CommandFailed => e
              Failbot.push mkdir_result: e
              raise GitHub::DGit::ReplicaRepairError, "Could not mkdir: #{e}"
            end

            begin
              noop, res = attempt_rsync(
                rpc, rsync_url,
                checksum: repair_attempt == 1,
                abbreviate_output: repair_attempt == 1,
                gitmon_models: repair_attempt == 1
              )
            rescue GitRPC::Timeout => e
              # A client-side timeout may leave rsync running. We don't know
              # what state this replica is in and it will be changing. Best we
              # can do leave it "REPAIRING" and try again after the sweeper picks
              # up that this repair has been running too long and destroy it.
              GitHub::Logger.log_exception({method: "attempt_rsync"}, e)
              raise GitHub::DGit::ReplicaRepairFatalError, "Timeout waiting for rsync"
            end

            unless noop
              next
            end

            after_rsync.call if after_rsync

            if force_checksum_recomputation?
              forks = GitHub::DGit::Util.repo_ids_for(network_id, include_deleted: false)
            else
              # Determine which forks might have changed during the
              # rsync based on whether any replicas' checksums and
              # updated_at times have changed since before the rsync
              # started. For any that haven't changed, write the
              # checksum of the existing replicas to the DB. For any
              # that have changed, write the repo_id to `forks`.
              current_checksums = GitHub::DGit::Maintenance.checksums_for_network(network_id)
              updated_checksums = Hash.new

              forks = Set.new
              dgit_states = Set.new
              rpc.forks_with_file("dgit-state").each do |dir|
                if dir =~ /\A(\d+)(\.wiki)?\.git\z/
                  dgit_states.add [$1.to_i, $2.nil? ? GitHub::DGit::RepoType::REPO : GitHub::DGit::RepoType::WIKI]
                end
              end

              current_checksums.each do |(repo_id, repo_type), current_replicas|
                # if we didn't see the dgit-state in the source, we cannot
                # do the quick update, do a full checksum recomputation
                checksum = dgit_states.include?([repo_id, repo_type]) &&
                           known_checksum(host, repo_id, repo_type,
                                          initial_checksums[[repo_id, repo_type]],
                                          current_replicas)
                if checksum
                  updated_checksums[[repo_id, repo_type]] = checksum
                else
                  forks.add(repo_id)
                end
              end
              unless updated_checksums.empty?
                GitHub::DGit::Delegate.update_checksums_for_host(network_id, host, updated_checksums)
              end
            end

            bad_checksums = 0
            GitHub::Logger.log message: "Recomputing checksums on #{forks.size} forks"
            forks.each_with_index do |repo_id, idx|
              if idx % 100 == 0
                GitHub::Logger.log message: "[#{idx+1}/#{forks.size}] #{repo_id}"
              end

              bad_checksums += update_checksums_for_network_replica_repair(host, network_id, repo_id)
            end

            GitHub::Logger.log at: "recompute", bad_checksums: bad_checksums

            if bad_checksums < 1000 && bad_checksums <= forks.size/3
              GitHub::Logger.log message: "DGit completed repair for network #{network_id} on #{host} with #{bad_checksums} bad checksums remaining",
                  at: "finish",
                  elapsed: Time.now-start,
                  result: "ACTIVE"
              return GitHub::DGit::Maintenance.set_network_state(
                network_id, host, GitHub::DGit::ACTIVE, GitHub::DGit::NOT_DORMANT)
            end

          rescue GitHub::DGit::ReplicaRepairError => e
            Failbot.report(e)
          end
        end
        GitHub::Logger.log message: "Repair unsuccessful after #{REPAIR_ATTEMPTS} attempts",
            at: "finish",
            elapsed: Time.now-start,
            attempts: REPAIR_ATTEMPTS,
            result: "FAILED"

        # Don't destroy the network.  Try again in two minutes.
        # FIXME: this will retry indefinitely.
        GitHub::DGit::Maintenance.set_network_state(
          network_id, host, GitHub::DGit::FAILED, GitHub::DGit::NOT_DORMANT)
        nil
      end

      def self.queue
        :dgit_repairs
      end

      def self.attempt_rsync(rpc, rsync_url, checksum:, abbreviate_output:, gitmon_models:)
        additional_exclude = []
        if !gitmon_models
            additional_exclude << "/gitmon.model"
            additional_exclude << "/*/gitmon.model"
        end
        noop0, res0 = rsync(
          rpc, rsync_url, ".", EXCLUDE_RSYNC + ["/network.git"] + additional_exclude,
          checksum: checksum, abbreviate_output: abbreviate_output
        )

        noop1, res1 = rsync(
          rpc, "#{rsync_url}/network.git/", "network.git/",
          EXCLUDE_RSYNC.map { |path| path.sub(/\A\/\*/, "") } + additional_exclude,
          checksum: checksum, abbreviate_output: abbreviate_output
        )

        [noop0 && noop1, [res0, res1]]
      end
      private_class_method :attempt_rsync

      def self.rsync(rpc, src, dst, exclude, checksum:, abbreviate_output:)
        rsync_opts = {archive: true, verbose: true, hard_links: true,
                      delete: true, stats: true, exclude: exclude}

        # Use --checksum to find files with matching mtime and size.
        # Only necessary on the first pass -- passes 2+ only exist to
        # pick up files that have changed, which will have new mtimes.
        rsync_opts[:checksum] = true if checksum

        GitHub::Logger.log message: "rsync beginning: #{src}"
        res = ::GitRPC.with_timeout(RSYNC_TIMEOUT) do
          rpc.rsync(src, dst, rsync_opts)
        end
        if !res["ok"]
          if GitHub::DGit::RETRYABLE_RSYNC_RESULTS.include?(res["status"])
            GitHub::Logger.log message: "rsync failed with retryable result #{res["status"]}"
            return [true, res]
          else
            Failbot.push rsync_result: res
            raise GitHub::DGit::ReplicaRepairError, "Could not rsync: #{res["err"]}"
          end
        end

        if abbreviate_output
          GitHub::Logger.log stderr: res["err"], message: "rsync succeeded: #{res["out"].lines.size} lines of output."
        else
          GitHub::Logger.log stdout: res["out"], stderr: res["err"], message: "rsync succeeded."
        end

        # rsync transfers 0 files and 0 bytes when it's done
        noop   = (res["out"] =~ /Number of (?:regular )?files transferred: 0/)
        noop &&= (res["out"] =~ /Total transferred file size: 0 bytes/)
        # rsync isn't done if it created or deleted any files
        noop &&= (res["out"] !~ /Number of created files: [^0]/)
        noop &&= (res["out"] !~ /^deleting\s/)

        [noop, res]
      end
      private_class_method :rsync

      # If the checksum for the specified [repo_id, repo_type] is
      # valid and unanimous on the other replicas, and hasn't changed
      # since initial_replicas, return it. Otherwise, return nil.
      # Note: `initial_replicas` and `current_replicas` are maps
      #
      #     {host => {checksum: checksum, updated_at: time}}
      def self.known_checksum(host, repo_id, repo_type, initial_replicas, current_replicas)
        other_replicas = current_replicas.except(host)

        # ensure that fork update times and checksums did not change
        # during the rsync
        return if other_replicas != initial_replicas.except(host)

        # ensure that all checksums are identical and valid (ie, not "creating")
        uniq_checksums = other_replicas.collect { |k, v| v[:checksum] }.uniq
        return if uniq_checksums.length != 1

        checksum = uniq_checksums.first
        return if checksum == "creating"

        checksum
      end
      private_class_method :known_checksum

      # Recompute and update the checksums for the specified
      # repository and its wiki (if present). Return the number of
      # checksums that still have to be considered bad.
      def self.update_checksums_for_network_replica_repair(host, network_id, repo_id)
        repo = Repository.find_by_id(repo_id)
        if !repo
          # skip just-now-deleted repos, but count them as problems
          GitHub::Logger.log repo_id: repo_id, not_exists: true
          return 1
        end

        checksum = GitHub::DGit::Routing.repo_checksum(network_id, repo_id)
        if checksum == "creating" || checksum == "" || checksum.nil?
          # If this repo hasn't even been created then there is nothing to
          # repair. Counting these as bad_checksums would likely cause our network
          # replica to be stuck in a unrepairable state. This condition should be
          # fixed up by DgitRepairDoaRepo job.
          GitHub::Logger.log repo_spec: "#{network_id}/#{repo_id}", doa: true
          return 0
        end

        # Count up how many bad checksums we see, this can be more than 1
        # because of wikis.
        bad_checksums = 0

        # Can't just use `read_route.host`, because we need to bless a
        # repo with a healthy checksum.
        repo_read_host = GitHub::DGit::Routing.hosts_for_repo(repo_id).first
        if !repo_read_host
          GitHub::Logger.log repo_spec: repo.dgit_spec, unrouted: true
          bad_checksums += 1
        end

        if repo.unsullied_wiki.exist?
          # Because the best wiki replica might be on a different host to the
          # best repo replica, make a separate decision for any wiki that needs
          # repairing.  Don't simply reuse `repo_read_host`.
          wiki_read_host = GitHub::DGit::Routing.hosts_for_repo(repo_id, true).first
          if !wiki_read_host
            GitHub::Logger.log repo_spec: repo.dgit_spec(wiki: true), unrouted: true
            bad_checksums += 1
          end
        else
          wiki_read_host = nil
        end

        begin
          GitHub::DGit::DB.for_network_id(network_id).throttle do
            if repo_read_host
              if !attempt_recompute_checksums(network_id, repo, repo_read_host, host, :repo)
                bad_checksums += 1
              end
            end

            if wiki_read_host
              if !attempt_recompute_checksums(network_id, repo, wiki_read_host, host, :wiki)
                bad_checksums += 1
              end
            end
          end
        rescue Freno::Throttler::ClientError => e
          GitHub::Logger.log_exception(
            {message: "failed to recompute checksum due to freno failure", repo_spec: repo.dgit_spec, freno_failed: true}, e)

          # Presumably the checksum was not updated so assume it's still bad.
          bad_checksums += 1
        end

        bad_checksums
      end
      private_class_method :update_checksums_for_network_replica_repair

      def self.attempt_recompute_checksums(network_id, repo, good_host, repair_host, repo_type)
        begin
          host_checksums = GitHub::DGit::Maintenance.recompute_checksums(
            repo, good_host, is_wiki: repo_type == :wiki)
          if host_checksums[:repo] != host_checksums[repair_host]
            GitHub::Logger.log message: "#{repo.dgit_spec(wiki: repo_type == :wiki)} has mismatched checksum on #{repair_host}: want "\
                         "#{host_checksums[:repo]} but got #{host_checksums[repair_host]}"
            return false
          end
        rescue GitHub::DGit::Error => e
          Failbot.report(e, spec: repo.dgit_spec(wiki: repo_type == :wiki), app: "github-dgit-debug")
          GitHub::Logger.log_exception({method: "recompute_checksums"}, e)
          return false
        end
        return true
      end
      private_class_method :attempt_recompute_checksums
    end
  end
end
