# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RemoveForksForInaccessibleRepositories < Job
      def self.active_job_class
        ::RemoveForksForInaccessibleRepositoriesJob
      end

      areas_of_responsibility :repo_networks

      def self.queue
        :sync_organization_default_repository_permission
      end

      def self.perform(repository_ids, user_ids)
        # This job is called in one of two ways:
        # 1. checking forks within a network of repos for a specific user (i.e. a user removed from a team)
        # 2. checking a repo for a list of users (i.e. a repo removed from a team)
        start = Time.current
        reparented_repos = []
        if user_ids.size < repository_ids.size
          reparented_repos = remove_forks_by_user(user_ids, repository_ids)
        else
          reparented_repos = remove_forks_by_repository(repository_ids, user_ids)
        end
        remove_reparented_forks_without_access(reparented_repos)
      ensure
        GitHub.dogstats.timing_since("job.remove_forks_for_inaccessible_repositories.time", start)
      end

      # Check one (or a few) users with forks of many given repositories to
      # make sure they still have access to them. If not, remove the fork.
      def self.remove_forks_by_user(user_ids, repository_ids)
        reparented_repos = []
        User.where(id: user_ids).each do |user|
          # Find the forks of any of the given repositories owned by the user,
          # and see if they still have access to the parent.
          Repository.private_scope.where(parent_id: repository_ids).owned_by(user).includes(:parent).each do |user_fork|
            parent = user_fork.parent
            if !parent.pullable_by? user
              reparented_repos += parent.throttle { parent.remove_user_fork(user_fork) }
              GitHub.dogstats.increment("org.repo.archive_fork")
            end
          end
        end
        reparented_repos.uniq
      end

      # Check forks of one (or a few) repos to make sure many users retain access to them.
      def self.remove_forks_by_repository(repository_ids, user_ids)
        reparented_repos = []
        # Iterate each of the given repos, and for each find any forks owned by
        # the given users and check only those user-owned forks to see if the
        # user retains access to the parent repo.
        Repository.private_scope.where(id: repository_ids).each do |parent_repo|
          reparented_repos += parent_repo.throttle { parent_repo.remove_inaccessible_forks_for(user_ids) }
        end
        reparented_repos.uniq
      end

      # When a fork is removed, its children have their parent set to the parent of the removed fork.
      # Here we recursively look at those reparented forks to determine if their owners have access to
      # the new parent. If they don't, we again remove the fork and re-evaluate newly reparented children.
      def self.remove_reparented_forks_without_access(reparented_repos)
        return if reparented_repos.nil? || reparented_repos.length == 0
        new_reparented_repos = []
        reparented_repos.each do |repo|
          new_reparented_repos += repo.throttle { repo.parent.remove_user_fork(repo) } if repo.should_remove_for_inacessible_parent?
        end
        remove_reparented_forks_without_access(new_reparented_repos.uniq)
      end
    end
  end
end
