# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    class RepositoryPurge < Job
      def self.active_job_class
        ::RepositoryPurgeJob
      end

      include Scientist
      extend GitHub::HashLock

      areas_of_responsibility :repositories

      class Error < StandardError; end

      CACHE_EXPIRES = 1.hour

      def self.queue
        :repository_purge
      end

      def self.perform(repo_id)
        new(repo_id).perform
      end

      def initialize(repo_id)
        @repo_id = repo_id.to_i
      end

      def perform
        @archived = Archived::Repository.find_by_id(@repo_id)

        Failbot.push repo_id: @archived&.id

        raise Error, "Invalid deleted repository." if @archived.nil?

        self.status = :purging
        @archived.purge
        self.status = :ok

        true
      rescue => boom
        self.status = :failed
        raise
      end

      def status=(value)
        GitHub.kv.set(status_key, value.to_s, expires: CACHE_EXPIRES.from_now)
      rescue GitHub::KV::UnavailableError
        # Noop if KV is unavailable. We don't want to hold up performing the
        # rest of the job.
      end

      # Public: Gets the status for this job.
      #
      # Return a Symbol status for Rails, like :ok.
      def status
         res = GitHub.kv.get(status_key).value! || default_status
         res.to_sym
      end

      private

      # Infer a status based on locks
      #
      # Return a status symbol
      def default_status
        return :accepted if self.class.locked? @repo_id
        :pending
      end

      def status_key
        "repo-purge:#{@repo_id}:status"
      end
    end
  end
end
