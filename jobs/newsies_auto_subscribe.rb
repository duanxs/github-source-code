# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    # Emails users about auto-subscribed repositories on a regular interval
    class NewsiesAutoSubscribe < Job
      # The maximum number of repositories we will list in a single email
      # Also limits the number of writes per throttle { ... } block
      BATCH_SIZE = 100

      def self.active_job_class
        ::NewsiesAutoSubscribeJob
      end

      areas_of_responsibility :notifications

      extend GitHub::HashLock

      schedule interval: 5.minutes

      def self.queue
        :newsies_auto_subscribe
      end

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.perform
        new.perform
      end

      def perform
        users = 0
        mailing = 0
        time = Time.now

        GitHub.newsies.notify_auto_subscribed do |settings, repositories|
          unless settings.auto_subscribe?
            mark_repositories_as_notified(settings.user, repositories)
            next
          end

          users += 1
          mailing_start = Time.now

          repositories
            .group_by { |repo| settings.email(repo.organization || :global).address }
            .each do |email_address, repos|
              notify_user_of_repositories(settings.user, email_address, repos)
            end

          mailing += Time.now - mailing_start
        end
      rescue Freno::Throttler::Error
        GitHub.dogstats.increment("newsies.auto_subscribe_job.throttled")
      ensure
        total = Time.now - time
        ms = ((total - mailing) * 1000).round
        mailing_ms = (mailing * 1000).round
        GitHub.dogstats.timing("newsies.auto_subscribe_job.query", ms)
        GitHub.dogstats.timing("newsies.auto_subscribe_job.deliver", mailing_ms)
        GitHub.dogstats.count("newsies.auto_subscribe_job.users", users)
      end

      private

      # Private: mark repositories as notified and send the email
      def notify_user_of_repositories(user, email_address, repositories)
        # Work in batches to ensure we don't have large amounts of unthrottled writes
        # This also limits how many repositories are listed per-email
        repositories.each_slice(BATCH_SIZE) do |repos_slice|
          # we try to mark lists as notified before sending email, so that if the
          # throttle fails the database will not be incorrect
          Newsies::ListSubscription.throttle do
            response = GitHub.newsies.mark_lists_as_notified(user, repos_slice)

            # fail here if newsies is unavailable to prevent further delivery
            raise response.error if response.failed?
          end

          NewsiesMailer.auto_subscribe(user, email_address, repos_slice).deliver_now
        end
      end

      # Private: just mark the repositories as notified without delivering the email
      def mark_repositories_as_notified(user, repositories)
        repositories.each_slice(BATCH_SIZE) do |repos_slice|
          Newsies::ListSubscription.throttle do
            response = GitHub.newsies.mark_lists_as_notified(user, repos_slice)

            # fail here if newsies is unavailable to prevent further delivery
            raise response.error if response.failed?
          end
        end
      end
    end
  end
end
