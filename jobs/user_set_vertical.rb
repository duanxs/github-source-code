# frozen_string_literal: true

module GitHub
  module Jobs
    class UserSetVertical < Job
      def self.active_job_class
        ::UserSetVerticalJob
      end

      areas_of_responsibility :analytics

      def self.queue
        :analytics
      end

      def self.perform(user_id)
        user = User.find_by_id(user_id)
        user.set_vertical! if user
      end
    end
  end
end
