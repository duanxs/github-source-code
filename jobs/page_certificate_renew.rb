# frozen_string_literal: true

module GitHub::Jobs
  class PageCertificateRenew < Job
    def self.active_job_class
      ::PageCertificateRenewJob
    end

    areas_of_responsibility :pages

    extend GitHub::HashLock

    # We're allowed 200 outstanding authorizations by LE. Stay under that.
    FUTURE_EXPIRY_BATCH_SIZE = 50
    # Expired domains should be rare – usually due to one-off incidents (e.g.
    # https://github.com/github/pages/issues/2145) – so we allocate only a small
    # rate for them.
    EXPIRED_BATCH_SIZE = 2

    schedule interval: 30.seconds

    def self.queue
      "pages_https"
    end

    def self.enabled?
      GitHub.pages_custom_domain_https_enabled?
    end

    def self.perform
      Failbot.push(app: "pages-certificates")

      ActiveRecord::Base.connected_to(role: :reading) do
        future_expiry_scope = Page::Certificate.where(<<-SQL, Page::Certificate::RENEWAL_WINDOW.from_now)
          expires_at BETWEEN NOW() AND ?
        SQL
        # Enqueue a PageCertificateWorkJob job for each cert needing renewal.
        future_expiry_scope.order(Arel.sql("RAND()")).limit(FUTURE_EXPIRY_BATCH_SIZE).each(&:resume_flow)

        # Check for domains that are approved but expired, which means we failed
        # to renew their certs in time.
        expired_domain_scope = Page::Certificate.where(<<-SQL, Page::Certificate.states[:approved])
          state = ? AND expires_at <= NOW()
        SQL
        # Enqueue a PageCertificateWorkJob job for each cert needing renewal.
        expired_domain_scope.order(Arel.sql("RAND()")).limit(EXPIRED_BATCH_SIZE).each(&:resume_flow)
      end
    end
  end
end
