# frozen_string_literal: true

module GitHub
  module Jobs
    class DeleteDependentAbilities < Job
      def self.active_job_class
        ::DeleteDependentAbilitiesJob
      end

      def self.queue
        :delete_dependent_abilities
      end

      # Perform a throttled deletion of abilities referencing any of the
      # specified abilities through the indirect grant ancestry information in
      # parent_id and/or grandparent_id
      #
      # ids - IDs of the abilities whose dependent abilities we want to delete.
      #
      def self.perform_with_retry(ids, options = {})
        new(ids).perform
      end

      attr_reader :ids

      def initialize(ids)
        @ids = ids
      end

      def perform
        Ability.delete_dependent_abilities_for!(ids)
      end
    end
  end
end
