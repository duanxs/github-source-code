# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class WikiMaintenance < Job
      def self.active_job_class
        ::WikiMaintenanceJob
      end

      areas_of_responsibility :git

      def self.queue
        :wiki_maintenance
      end

      def self.log(entity, data, &blk)
        GitHub::Logger.log_context(job: active_job_class.to_s, spec: entity.dgit_spec) do
          GitHub::Logger.log(data, &blk)
        end
      end

      def self.perform(wiki_id)
        Failbot.push wiki: wiki_id

        return unless wiki = RepositoryWiki.find_by_id(wiki_id)

        Failbot.push spec: wiki.dgit_spec


        log(wiki, method: "perform") do
          log(wiki,
              message: "starting git maintenance",
              status: wiki.maintenance_status,
              maintenance_elapsed: Time.now - (wiki.last_maintenance_at || wiki.created_at),
              maintenance_pushes: wiki.pushed_count_since_maintenance || 0)

          GitHub.dogstats.time("git_maintenance.perform", tags: ["type:wiki"]) do
            perform! wiki
          ensure
            GitHub.dogstats.increment("git_maintenance", tags: ["type:wiki", "result:#{wiki.maintenance_status}"])
          end
        ensure
          log(wiki,
              at: "exit",
              message: "exiting git maintenance",
              status: wiki.maintenance_status)
        end

        nil
      end

      # Run maintenance for the given wiki repository.
      def self.perform!(wiki)
        # if a backup-utils backup is in progress, delay the sync operation by
        # requeuing after a short sleep period.
        if GitHub::Enterprise.backup_in_progress?
          clear_lock(wiki.id)
          wiki.schedule_maintenance
          sleep 1
          return
        end

        wiki.perform_maintenance
      end

      # All WikiMaintenance jobs are locked by their wiki id. No attempt
      # will be made to enqueue a job that's already running and the lock is
      # checked before running the job.
      extend GitHub::HashLock
    end
  end
end
