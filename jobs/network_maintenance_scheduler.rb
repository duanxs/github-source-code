# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Timer that schedules maintenance jobs for individual networks. See
    # RepositoryNetwork::Maintenance for scheduling logic and different types of
    # maintenance tasks.
    class NetworkMaintenanceScheduler < Job
      def self.active_job_class
        ::NetworkMaintenanceSchedulerJob
      end

      areas_of_responsibility :git

      schedule interval: 10.minutes

      # This job is scheduled in all environments: github.com and GHE.
      def self.enabled?
        true
      end

      # The number of maintenance jobs to enqueue at each interval.
      def self.jobs_per_interval
        1000
      end

      # Minimum length of time between maintenance runs for any single
      # network in seconds. Networks that have received maintenance
      # within this time period will not be scheduled unless they've received
      # more than 50 pushes.
      def self.min_age
        1.week
      end

      # Do not schedule more than this many maintenance jobs on a single host.
      # Once the limit is reached, networks on that host are excluded from scheduling
      # queries.
      def self.host_threshold
        1000
      end

      # Run every interval, schedules jobs_per_interval maintenance jobs to run.
      def self.perform
        SlowQueryLogger.disabled do
          networks = RepositoryNetwork.schedule_maintenance(jobs_per_interval, host_threshold, min_age)

          unless networks.empty?
            most_stale  = networks.min_by { |net| net.last_maintenance_at || net.created_at }
            lag_time = Time.now - (most_stale.last_maintenance_at || most_stale.created_at)
            GitHub.dogstats.histogram("network_maintenance_scheduler.lag", lag_time)

            most_active = networks.max_by { |net| net.pushed_count_since_maintenance }
            GitHub.dogstats.histogram("network_maintenance_scheduler.most-active", most_active.pushed_count_since_maintenance)
          end

          RepositoryNetwork.maintenance_counts.each do |status, network_count|
            GitHub.dogstats.gauge("git_maintenance.count", network_count, tags: ["type:network", "status:#{status}"])
          end

          networks
        end
      end

      # The queue the scheduler runs on. The actual maintenance jobs don't run
      # on this queue, they're enqueued on the fs maintenance queues.
      def self.queue
        :network_maintenance_scheduler
      end
    end
  end
end
