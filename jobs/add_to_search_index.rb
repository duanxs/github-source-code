# rubocop:disable Style/FrozenStringLiteralComment
module GitHub
  module Jobs

    # This job will add a document to the ElasticSearch index. The document is
    # constructed using an 'adapter' based on the document type and the ID of
    # the document in the MySQL database. An adapter is created that knows how
    # to read all the pertinent information from the MySQL database and then
    # format that information into an indexable document. That document is
    # then stored in ElasticSearch.
    #
    class AddToSearchIndex < Job
      include GitHub::ServiceMapping

      def self.active_job_class
        ::AddToSearchIndexJob
      end

      class PerformStats
        def initialize(submitted_at, resolved_service)
          @tags = { catalog_service: resolved_service }
          @millis_since_submitted = ((Time.now - Timestamp.to_time(submitted_at)) * 1_000).to_i
        end

        def applied_readonly_static
          @tags[:readonly] = :static
        end

        def applied_readonly_dynamic
          @tags[:readonly] = :dynamic
        end

        def applied_retry(reason)
          @tags[:retried] = reason
        end

        def report
          tags = @tags.map { |tag, value| "#{tag}:#{value}" }
          GitHub.dogstats.histogram("add_to_search_index.perform.millis_since_submitted", @millis_since_submitted, tags: tags)
        end
      end

      class HeartbeatStats
        def initialize(write_timestamp, resolved_service)
          @resolved_service = resolved_service
          @write_timestamp  = write_timestamp.to_f
        end

        def report(read_timestamp)
          return unless @write_timestamp > 0
          diff = read_timestamp.to_f - @write_timestamp
          if diff > 0
            freshness_tag = "up_to_date:true"
          else
            freshness_tag = "up_to_date:false"
          end

          resolved_service_tag = "catalog_service:#{@resolved_service}"

          GitHub.dogstats.histogram "add_to_search_index.replica_heartbeat_check",
            diff.abs * 1000, tags: [freshness_tag, resolved_service_tag]
        end
      end

      # How many concurrent jobs with the same key are allowed. Since this is
      # indexing content, only one at a time per document, please:
      RUNNING_JOBS_PER_KEY = 1

      # How long a running (or crashed) job is allowed to hold onto a lock, in
      # seconds, before it expires and another one takes over.
      JOB_LOCK_TTL = 30.minutes

      # The list of document types that should always use a read-replica when
      # making database requests.
      USE_READONLY = Set.new(%w[code commit wiki])

      # How long to wait for replication delay before requeuing a job to retry
      # later, in seconds. This is based on the 95th percentile for indexing job
      # runs (~1sec), and the goal is to balance job throughput when waiting for
      # replication delay.
      MAX_REPLICATION_DELAY_WAIT = 2.0

      # Public: Perform the work of generating the document and indexing it
      # into ElasticSearch.
      #
      # type - The adapter name as a String
      # id   - The numeric ID of the database record
      # opts - An options hash, which may include the following:
      #        "purge"            - set to true to remove any existing data from
      #                             the index before reindexing it
      #        "index", "cluster" - passed on to Elastomer
      #
      # Returns the Hash response from the ElasticSearch server or `nil` if the
      # document could not be stored.
      #
      def self.perform(type, id, opts = {})
        new(type, id, opts).perform
      end

      # Construct a GUID String for the given job arguments. The GUID string
      # excludes transient values passed into the `options` hash - "retries",
      # "guid", "heartbeat_ts", "request_id".
      #
      # type - The adapter name as a String
      # id   - The numeric ID of the database record
      # opts - An options hash
      #
      # Returns a GUID String for this job.
      def self.guid(type, id, opts = {})
        ary = [self.name, type, id, opts.with_indifferent_access.except("heartbeat_ts", "submitted_at", "retries", "guid", "request_id")]
        Digest::SHA1.hexdigest(ary.to_json)
      end

      attr_reader :type    # what kind of record is being indexed
      attr_reader :id      # the id of the record being indexed
      attr_reader :options # the options passed in as arguments

      def initialize(type, id, opts = {})
        @type    = type
        @id      = id
        @options = opts.with_indifferent_access
      end

      def perform
        stats = nil
        # Don't try and perform the job if elasticsearch is being ignored.
        # Otherwise allow the job to proceed, whether elasticsearch is allowed
        # or will raise.
        return nil if GitHub.elasticsearch_access_ignored?

        if type == "code" && !Search::ClusterStatus.code_search_indexing_enabled?
          return nil
        end

        stats = PerformStats.new(submitted_at, logical_service)

        # Only one job for this record is allowed to run at a time:
        key = "#{type}-#{id}"
        restraint.lock!(key, RUNNING_JOBS_PER_KEY, JOB_LOCK_TTL) do
          if USE_READONLY.include?(type)
            ActiveRecord::Base.connected_to(role: :reading) { reindex }
            stats.applied_readonly_static
          else
            WaitForReplication.new(
              submitted_at,
              store_name: adapter.mysql_cluster,
              max_wait_seconds: MAX_REPLICATION_DELAY_WAIT,
            ).wait!

            read_heartbeat = ActiveRecord::Base.connected_to(role: :reading) { Search.current_heartbeat_timestamp }
            HeartbeatStats.new(heartbeat_ts, logical_service).report(read_heartbeat)
            ActiveRecord::Base.connected_to(role: :reading) { reindex }
            stats.applied_readonly_dynamic
          end
        end
      rescue Elastomer::Client::Error => err
        if err.retry?
          report_retry(err)
          retry_later
        else
          raise err
        end
      rescue GitRPC::ObjectMissing => err
        report_retry(err)
        options["purge"] = true
        retry_later
      rescue WaitForReplication::DataUnavailable
        retry_later
        stats.applied_retry(:data_unavailable)
      rescue GitHub::Restraint::UnableToLock
        retry_later
        stats.applied_retry(:unable_to_lock)
      rescue Freno::Error
        retry_later
        stats.applied_retry(:freno_error)
      ensure
        stats.try(:report)
      end

      # Public: not wired up with GitHub::ServiceMapping, used directly (for now)
      def logical_service
        if type.downcase == "code"
          "#{::GitHub::ServiceMapping::SERVICE_PREFIX}/code_search_indexing"
        else
          "#{::GitHub::ServiceMapping::SERVICE_PREFIX}/search_muddle"
        end
      end

      # Internal: reindex the document
      def reindex
        if options["purge"] && adapter.respond_to?(:delete_query)
          Elastomer.remove_from_search_index(adapter, options)
        end

        hydrated_at = Time.now.utc
        Elastomer.add_to_search_index(adapter, options)
        if options["pushed_at"].present?
          job = self.class.name.demodulize.underscore.downcase
          tags = ["job:#{job}", "catalog_service:#{logical_service}"]
          GitHub.dogstats.timing_since("search.indexing.updated_at_to_indexed_at", options["pushed_at"], tags: tags)
        end

      rescue Elastomer::ModelMissing
        nil # these things happen
      end

      # Internal: retries the job later
      def retry_later
        LegacyApplicationJob.retry_later(
          self.class.active_job_class, [type, id, options],
          {retries: retries, guid: guid}
        )
      end

      # Internal: the elastomer adapter for the record
      def adapter
        unless defined? @adapter
          adapter_class = Elastomer.env.lookup_adapter(type)
          @adapter = adapter_class.create(id, options)
        end
        @adapter
      end

      # Returns the number of retries that have been attempted for this job.
      def retries
        options["retries"]
      end

      # Returns the GUID String for this particular job instance.
      def guid
        options["guid"] ||= self.class.guid(type, id, options)
      end

      # Returns the time when this job was submitted from the application.
      def submitted_at
        options["submitted_at"] ||= Timestamp.from_time(Time.now)
      end

      # Return the heartbeat value as of when this job was first enqueued
      def heartbeat_ts
        options["heartbeat_ts"]
      end

      # Internal: a concurrency restraint using resque redis
      def restraint
        @restraint ||= GitHub::Restraint.new
      end

      def report_retry(error)
        job = self.class.name.demodulize.underscore.downcase
        error_name = error.class.name.demodulize.underscore

        tags = %W[job:#{job} type:#{type} error:#{error_name} catalog_service:#{logical_service}]
        GitHub.dogstats.count("search.indexing.retries", 1, tags: tags)
      end

      # Public: The queue from which this job processor will request work.
      #
      # Returns the queue name as a Symbol
      #
      def self.queue
        :index_high
      end
    end
  end
end
