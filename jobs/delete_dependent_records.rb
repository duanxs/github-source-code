# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs

    # Delete dependent records for a model in the background.
    class DeleteDependentRecords < Job
      def self.active_job_class
        ::DeleteDependentRecordsJob
      end

      areas_of_responsibility :background_jobs

      SELECT_BATCH_SIZE = 10000
      DELETE_BATCH_SIZE = 100

      def self.queue
        :background_destroy
      end

      # Perform a throttled deletion of the dependent records for a model.
      #
      # model_name       - The String class name of the parent model, e.g. Repository
      # model_id         - The Integer primary key id of the parent model. The model
      #                    will have been deleted already, but not these
      #                    dependent records.
      # association_name - The String name of the dependent association which needs
      #                    to be cleared, e.g. "commit_contributions"
      #
      def self.perform_with_retry(model_name, model_id, association_name, options = {})
        new(model_name, model_id, association_name).perform
      end

      def self.audit_key(model_name, model_id, association_name)
        "#{model_name.downcase}.#{association_name}.#{model_id}"
      end

      attr_reader :model_name, :model_id, :association_name

      def initialize(model_name, model_id, association_name)
        @model_name = model_name
        @model_id = model_id
        @association_name = association_name
      end

      def perform
        Failbot.push \
          model_name: model_name,
          model_id: model_id,
          association_name: association_name

        # For some security, use reflection to retrieve the table and column
        # names rather than relying on user input in the job arguments.
        model       = model_name.constantize
        association = model.reflect_on_association(association_name.to_sym)
        table       = GitHub::SQL::LITERAL association.klass.table_name
        foreign_key = GitHub::SQL::LITERAL association.foreign_key
        type_column = GitHub::SQL::LITERAL association.type # may be nil
        type_value  = model.name

        tags = ["model:#{model_name.underscore.dasherize}", "association:#{association_name.to_s.dasherize}"]

        rows_deleted = 0

        last_seen = 0
        loop do
          ids = ActiveRecord::Base.connected_to(role: :reading) do
            sql = association.klass.github_sql.new \
              table: table,
              foreign_key: foreign_key,
              type_column: type_column,
              type_value: type_value,
              model_id: model_id,
              last_seen: last_seen,
              count: SELECT_BATCH_SIZE
            sql.add <<-SQL
              SELECT id FROM :table
              WHERE :foreign_key = :model_id
              AND id > :last_seen
            SQL

            if !association.type.blank? # we've got a polymorphic association
              sql.add <<-SQL
                AND :type_column = :type_value
              SQL
            end

            sql.add <<-SQL
              ORDER BY id
              LIMIT :count
            SQL
            sql.values
          end

          break if ids.empty?
          last_seen = ids.last

          ids.each_slice(DELETE_BATCH_SIZE) do |slice|
            klass = association.klass
            klass.throttle do
              sql = klass.github_sql.new
              delete = sql.run <<-SQL, table: table, ids: slice
                DELETE FROM :table WHERE id IN :ids
              SQL
              rows_deleted += delete.affected_rows

              # If we are archiving a `Storage::Uploadable` model, we dereference it so that if
              # a purge job needs to be scheduled, it can be. Since the models are not `#destroy`'d, the
              # registered `after_destroy` callbacks are not called, this does nearly equivalent work.
              klass = association.klass
              if klass.try(:act_on_storage_uploadable?)
                slice.each do |id|
                  GitHub::Storage::Destroyer.dereference_raw(association.klass, id, purge_at: klass.purge_at)
                end
              end
            end
          end
        end

        GitHub.dogstats.histogram("delete_dependent_records", rows_deleted, tags: tags)
      end
    end
  end
end
