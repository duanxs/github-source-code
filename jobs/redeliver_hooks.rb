# frozen_string_literal: true

module GitHub
  module Jobs
    class RedeliverHooks < Job
      def self.active_job_class
        ::RedeliverHooksJob
      end

      areas_of_responsibility :webhook

      extend Hookshot::DeliverJobLogger

      def self.queue
        :redeliver_hooks
      end

      def self.perform(delivery_guid, hook_id)
        hook = Hook.find(hook_id)

        Hook::DeliverySystem.redeliver(delivery_guid, hook)
      rescue Hookshot::PayloadTooLarge => e
        log_params = {
          error: e,
          target: hook.installation_target,
          parent: hook.hookshot_parent_id,
          guid: delivery_guid,
          hook_ids: hook_id,
        }

        log_payload_too_large_error(**log_params)
        Failbot.report e
      end
    end
  end
end
