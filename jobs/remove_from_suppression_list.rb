# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RemoveFromSuppressionList < Job
      def self.active_job_class
        ::RemoveFromSuppressionListJob
      end

      areas_of_responsibility :unassigned

      def self.queue
        :remove_from_suppression_list
      end

      # Remove the user from the suppression list.
      #
      # - Resubscribe them to the MailChimp list, as being
      # removed from the suppression list means the user
      # is eligible again to receive marketing material.
      #
      # user_id - The integer user id.
      #
      # Returns nothing.
      def self.perform(user_id)
        user = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }
        return unless user
        Failbot.push(user: user.login)

        GitHub.dogstats.time "suppression_list", tags: ["action:remove_user", "via:job"] do
          SuppressionList.remove_user(user)
        end

        email = user.primary_user_email
        MailchimpSubscribeJob.perform_later(email.id) if GitHub.mailchimp_enabled?
      end
    end
  end
end
