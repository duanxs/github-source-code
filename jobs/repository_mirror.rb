# rubocop:disable Style/FrozenStringLiteralComment

# Fetches or updates a repository from its mirror. The repository must
# have a valid mirror record.
module GitHub
  module Jobs
    class RepositoryMirror < Job
      def self.active_job_class
        ::RepositoryMirrorJob
      end

      areas_of_responsibility :repositories, :stafftools

      # Exception raised when the job fails due to git command failure.
      class Failed < StandardError
      end

      def self.queue
        :repository_mirror
      end

      def self.perform(repository_id)
        self.set_status_in_cache("mirror-timestamp:#{repository_id}", Time.now.to_s)
        repo = Repository.find(repository_id)
        Failbot.push repo_id: repository_id
        repo.mirror.perform!
        self.set_status_in_cache("mirror-result:#{repository_id}", "success")
      rescue GitRPC::CommandFailed => boom
        self.set_status_in_cache("mirror-result:#{repository_id}", "failed")
        Failbot.push(app: "github-user",
                     detail: boom.message)
        raise RepositoryMirror::Failed, "Repository mirror failed due to GitRPC error"
      rescue Repository::CommandFailed => boom
        self.set_status_in_cache("mirror-result:#{repository_id}", "failed")
        Failbot.push(app: "github-user",
                     command: boom.command,
                     stderr: boom.err,
                     exit_status: boom.exitstatus)
        raise RepositoryMirror::Failed, "Repository mirror failed due to command failure"
      end

      def self.set_status_in_cache(key, value)
        GitHub.kv.set(key, value)
      rescue GitHub::KV::UnavailableError
        # Noop if KV is unavailable. We don't want to hold up performing the
        # rest of the job.
      end
    end
  end
end
