# frozen_string_literal: true

module GitHub
  module Jobs
    class StorageClusterMaintenanceScheduler < Job
      def self.active_job_class
        ::StorageClusterMaintenanceSchedulerJob
      end

      areas_of_responsibility :data_infrastructure

      schedule interval: 2.minutes
      extend GitHub::HashLock

      def self.enabled?
        GitHub.storage_cluster_enabled?
      end

      def self.queue
        :storage_cluster
      end

      def self.perform
        SlowQueryLogger.disabled do
          backing_up = GitHub::Enterprise.backup_in_progress?
          GitHub::Storage::ReplicaVerifier.perform unless backing_up

          GitHub::Storage::ClusterRepair.perform_voting(backing_up: backing_up)
          datacenters = GitHub::Storage::Allocator.get_non_voting_datacenters
          datacenters.each do |dc|
            GitHub::Storage::ClusterRepair.perform_non_voting(dc, backing_up: backing_up)
          end

          unless backing_up
            GitHub::Storage::Rebalancer.perform
            datacenters.each do |dc|
              GitHub::Storage::Rebalancer.perform_non_voting(dc)
            end
          end
        end
      end
    end
  end
end
