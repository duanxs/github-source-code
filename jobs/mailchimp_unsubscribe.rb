# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Unsubscribes an email address from our MailChimp list.
    #
    # Because a call to the MailChimp API could fail for a number
    # of different reasons, this should be isolated in a background job.
    class MailchimpUnsubscribe < Job
      def self.active_job_class
        ::MailchimpUnsubscribeJob
      end

      areas_of_responsibility :unassigned

      def self.queue
        :mailchimp
      end

      def self.perform(*args)
        job = new(*args)
        job.perform
        job
      end

      # Options:
      #
      # user_id       - User ID
      # email_address - String
      #
      def initialize(user_id, email_address, options = {})
        @user_id       = user_id
        @user          = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }
        @email_address = email_address
        @email         = ActiveRecord::Base.connected_to(role: :reading) { UserEmail.find_by_email(email_address) }
        @options       = options.with_indifferent_access

        Failbot.push(
          job: self.class.name,
          user: @user.try(:login),
          user_id: user_id,
          email_id: @email.try(:id),
        )
      end

      def perform
        GitHub.dogstats.increment "mailchimp", tags: ["job:unsubscribe"]
        return unless @user

        # Use the found UserEmail record or instantiate a ghost email in
        # the case where the email address was already deleted.
        email = @email || UserEmail.new(email: @email_address, user: @user)

        return if email.stealth?

        with_mailchimp_retries do
          mailchimp = GitHub::Mailchimp.new(email)
          GitHub::Mailchimp.list_ids.each do |list_id|
            mailchimp.ensure_subscriber_exists(list_id) do
              mailchimp.unsubscribe(list_id: list_id)
            end
          end
        end
      end

      private

      # Private: When MailChimp API calls fail due to 5xx server errors,
      # retry the call up to 25 times. Otherwise, raise an exception
      # and fail the job.
      def with_mailchimp_retries(&block)
        yield
      rescue GitHub::Mailchimp::Error => boom
        if GitHub::Mailchimp::ServerErrorStatuses.include?(boom.status_code)
          GitHub.dogstats.increment "mailchimp", tags: ["job:unsubscribe", "type:retry"]

          LegacyApplicationJob.retry_later(self.class.active_job_class, [@user_id, @email_address, @options], retries: @options["retries"])
        else
          raise
        end
      end
    end
  end
end
