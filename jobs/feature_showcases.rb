# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # FeatureShowcases will update the featured showcases section weekly
    class FeatureShowcases < Job
      def self.active_job_class
        ::FeatureShowcasesJob
      end

      areas_of_responsibility :explore

      schedule interval: 1.day

      def self.queue
        :feature_showcases
      end

      # Don't schedule this job under GHE.
      def self.enabled?
        !GitHub.enterprise?
      end

      # Internal: Put another job on the Resque queue.
      #
      # Returns this job instance.
      def requeue
        FeatureShowcasesJob.perform_later
        self
      end

      def self.perform
        today = Time.now
        return unless today.monday?

        # clear current featured showcases
        Showcase::Collection.where(featured: false).update_all(featured: true)

        # feature all 9 in the group
        Showcase::Collection.where(id: self.featured_ids).update_all(featured: true)
      end

      def self.featured_ids
        # grab all the new showcases in the past 2 weeks
        featured_ids = Showcase::Collection.published.where("created_at > ?", 2.weeks.ago).limit(9).map(&:id)

        # count how many more we need to get to 9
        remainder = 9 - featured_ids.count

        # grab the remainder by random showcases
        unless remainder == 0
          conditions = remainder == 9 ? "" : ["id not in (?)", featured_ids]

          featured_ids.concat(Showcase::Collection.published.where(conditions).order(Arel.sql("RAND()")).limit(remainder).map(&:id))
        end
      end
    end
  end
end
