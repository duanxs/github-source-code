# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class PostReceive < CoreReceive
      def self.active_job_class
        ::PostReceiveJob
      end

      def self.queue
        :post_receive
      end

      def pages_preview_build?
        ref_is_branch? &&
          repository.page&.preview_deployments_enabled? &&
          repository.page&.deployment_enabled_for?(short_ref)
      end

      def short_ref
        ref.to_s.sub("refs/heads/", "")
      end

      def pages_branch?
        ref == "refs/heads/#{repository.pages_branch}"
      end

      def default_branch?
        ref == "refs/heads/#{repository.default_branch}"
      end

      def valid?
        super && (branch_or_tag? || is_note?)
      end

      def perform
        @triggered_at = Time.now

        if !after.kind_of?(String)
          raise TypeError, "after must be a SHA1, not #{after.inspect}"
        end

        # The repository might get deleted while this job is scheduled
        # or it might disappear while we're running. Stop a loop of
        # failures if we're deleting the repo.
        return if (repository.deleted? || !repository.exists_on_disk?)

        perform_task :collect_stats

        return if is_note?

        # The bulk of the job work, with some variation depending
        # on event type
        perform_task "record_#{event}"
        perform_task :update_model
        perform_task :update_websocket
      end

      def record_create
        record_push
        instrument_creation
        perform_task :restore_pull_request_head_refs
      end

      def record_push
        if ref_is_tag?
          instrument_push
          return
        end

        save_push_record(pusher, repository, before, after, ref)
      end

      def record_delete
        repository.ref_deleted(ref, pusher_id)
        instrument_delete
        instrument_push
        perform_task :close_pull_requests

        save_push_record(pusher, repository, before, after, ref, false)
      end

      # Internal: actually save the push record
      def save_push_record(pusher, repository, before, after, ref, perform_tasks = true)
        push = Push.new(
          pusher: pusher,
          repository: repository,
          before: before,
          after: after,
          ref: ref,
        )
        push.commits = commits_pushed unless large_push?
        perform_basic_push_tasks if perform_tasks

        # after_commit hooks on the Push model will also save commit_contribution
        # data, send notifications, and instrument with "push.create" additional push stats
        push.save
      end

      # Record stats about this action.
      #
      #   git.push.{note,tag,branch,other}.{push,create,delete,force_push}
      #   git.push.{commits,new_commits}
      #
      # Returns nothing.
      def collect_stats
        # Determine the type of stat we're dealing with
        type = if ref_is_tag?
          "tag"
        elsif ref_is_branch?
          "branch"
        elsif is_note?
          "note"
        else
          "other"
        end

        # Special-case override the event type name (e.g., 'push')
        # in cases of a force push or large pushes
        name = if large_push?
          "large"
        elsif event == :push && forced?
          "force_push"
        else
          event.to_s
        end

        # Send the actual stats
        GitHub.dogstats.increment("git", tags: ["action:push", "type:#{type}", "name:#{name}"])
        GitHub.dogstats.histogram("git.commits", commits_pushed_count, tags: ["action:push", "type:#{type}", "name:#{name}"])

        # Count the number of times we would potentially run the
        # LFS tracking job in https://github.com/github/github/pull/53625
        # This will set expectations for the incoming volume of load.
        if Media::Blob.exists?(repository_network_id: repository.network_id)
          GitHub.dogstats.increment("lfs", tags: ["action:push", "type:#{type}", "name:#{name}"])
          GitHub.dogstats.histogram("lfs.commits", commits_pushed_count, tags: ["action:push", "type:#{type}", "name:#{name}"])
        end
      end

      # Perform a series of tasks after push.
      def perform_basic_push_tasks
        instrument_push
        perform_task :publish_pages
        perform_task :update_pull_requests

        if default_branch?
          perform_task :publish_repository_changed_for_code_search
          perform_task :index_readme
          perform_task :index_source_code
          perform_task :index_commits
          perform_task :reset_mobile_status
          perform_task :reset_docker_file_status
          perform_task :update_repository_actions
        end

        if large_push?
          perform_task :schedule_maintenance
        else
          perform_task :integrate_issues, commits_pushed
          perform_task :notify_mentioned, commits_pushed
          perform_task :update_repository_workflows

          if GitHub.octolytics_enabled?
            perform_task(:send_octolytics_post_receives, commits_pushed)
          end
        end
      end

      def publish_pages
        return unless pages_branch? || pages_preview_build?
        return if deleted?
        publisher = pusher || repository.owner
        repository.rebuild_pages(publisher, git_ref_name: short_ref)
      rescue ::Page::PageBuildFailed => boom
        PagesMailer.build_failure(publisher, repository, boom.message).deliver_now
      end

      def update_model
        if ref_is_branch?
          notify_ref_socket_subscribers
        elsif ref_is_tag?
          update_tag
        end
      end

      def notify_ref_socket_subscribers
        repository.refs.build(ref, after).
          notify_socket_subscribers(before: before,
                                    after: after,
                                    pusher: pusher.try(:login),
                                    action: event,
                                    timestamp: @triggered_at)
      end

      # Update Release if its tag is destroyed.  Placeholder for future
      # integration with a Tag model.
      def update_tag
        return unless @repository
        return unless rel = @repository.releases.find_by_tag_name(tag_name)
        rel.state = :draft if deleted?
        rel.save
      end

      def update_websocket
        return unless pusher.present?

        data = {
          timestamp: @triggered_at,
          reason: "repository '#{repository.name_with_owner}' was pushed to by '#{pusher}'",
        }

        channel = GitHub::WebSocket::Channels.post_receive(repository, pusher)
        GitHub::WebSocket.notify_repository_channel(repository, channel, data)
      end

      def notify_mentioned(commits)
        commits.each do |commit|
          CommitMention.process(repository, commit)
        end
      end

      def integrate_issues(commits)
        commits.each do |commit|
          close_or_reference_issue(commit)
        end
      end

      def commit_is_year_older?(issue, commit)
        seconds = issue.created_at - commit.created_at
        seconds > 1.year
      end

      def close_or_reference_issue(commit)
        commit.context_user(pusher)
        commit.issue_references.each do |reference|
          issue = reference.issue

          # The pusher is the only actor we can trust to reference issues.
          # We used to allow the commit author/committer but those can be
          # trivially spoofed.
          next if pusher.nil?

          # ignore commits that are a year older than the referenced issue
          next if commit_is_year_older?(issue, commit)

          # Locked issues shouldn't be spammed by commit references, unless they are closing the issue
          next if issue.locked? && !reference.close?

          # closing references only count when they're for issues the
          # pusher has write access to (and not on a parent of this repo)
          # and the commit is on the default or gh-pages branch.
          #
          # otherwise, these are just considered normal references.
          if (default_branch? || pages_branch?) &&
             !issue.closed? && reference.close? &&
             issue.repository.pushable_by?(pusher) &&
             !repository.parents.include?(issue.repository)
            issue.close(pusher, commit: { id: commit.oid, repository: repository })
          elsif issue.readable_by?(pusher)
            issue.reference_from_commit(pusher, commit.oid, repository)
          end
        end
      end

      def update_pull_requests
        return if created?

        PullRequestSynchronizationJob.perform_later(repository, ref, pusher,
          forced: forced?,
          before: before,
          after: after
        )
      end

      def close_pull_requests
        PullRequest.after_branch_delete(repository, ref, pusher, before)
      end

      def restore_pull_request_head_refs
        PullRequest.mark_head_ref_as(:restored, repository, ref, after, pusher)
      end

      # Iterate through the list of commits looking for modifications to the
      # repository README file. If this path is included in any of the
      # commits then enqueue an indexing job for the repository. We only care
      # about the README on the master branch.
      #
      # Returns nil.
      #
      def index_readme
        readme_path = repository.preferred_readme ? repository.preferred_readme.name : nil
        return if readme_path.blank?

        if repository.rpc.file_changed?(readme_path, before, after)
          Search.add_to_search_index("repository", repository.id)
        end

        nil
      rescue StandardError => err
        Failbot.report(err)
      end

      # Update the search index with the information from this push event.
      # Files will be added, updated, or removed from the search index.
      #
      # Returns nil.
      #
      def index_source_code
        IndexSourceCodeJob.perform_later(repository.id, before, after, ref)
        nil
      rescue StandardError => err
        Failbot.report(err)
      end

      # Update the search index with the information from this push event.
      # Commits will be added or removed from the search index.
      #
      # Returns nil.
      #
      def index_commits
        Search.add_to_search_index("commit", repository.id)
        nil
      end

      # Schedule network maintenance on large pushes (exceeding
      # Push::CommitsHelper::LARGE_PUSH_THRESHOLD)
      #
      # Pushes containing a large number of commits can make the repository
      # inaccessible without triggering a maintenance job via other heuristics.
      # This ensures they are processed, thus avoiding service failures.
      #
      # Note that maintenance jobs are scheduled only when a repository is
      # not already undergoing maintenance, and is also not scheduled to undergo
      # maintenance. This prevents our largest repositories from repeatedly
      # enqueueing expensive maintenance tasks.
      def schedule_maintenance
        tags = []
        if repository.network.maintenance_pending?
          tags << "pending:#{repository.network.maintenance_status}"
        else
          tags << "scheduled:true"
          repository.network.schedule_maintenance
        end

        GitHub.dogstats.increment("git.post_receive.maintenance", tags: tags)
      end

      # Reset the repository's mobile status if it exists, as pushing code updates
      # may change that status.
      #
      # The mobile status will only be used if the repo doesn't already have CI,
      # so don't bother with the potentially expensive detection if CI is already
      # running.
      #
      # Returns nil.
      def reset_mobile_status
        if repository.has_ci?
          repository.clear_mobile_status
        else
          repository.set_mobile_status
        end
      end

      # Check if workflows have been modified
      #
      # Returns nil.
      def update_repository_workflows
        repository.update_repository_workflows(ref, before, after)
      end

      # Updates the action exposed by this repository.
      #
      # Returns nil.
      def update_repository_actions
        repository.update_repository_actions
      end

      # Reset the repository's Dockerfile status if it exists, as pushing code updates
      # may change that status.
      #
      # The Dockerfile status will only be used if the repo doesn't already have CI,
      # does not bother to run if CI is already running.
      #
      # Returns nil.
      def reset_docker_file_status
        if repository.has_ci?
          repository.clear_docker_file_status
        else
          repository.set_docker_file_status
        end
      end

      def send_octolytics_post_receives(commits)
        events = commits.map do |commit|
          event = commit.octolytics_event_payload.merge({
            event_type: "post_receive",
            timestamp: @triggered_at,
          })

          transcoder = Stratocaster::Octolytics::Utf8Transcoder.new(event)
          event = transcoder.transcode
          event[:dimensions][:import_transcoded_to_utf8] = "true" if transcoder.transcoded_to_utf8

          policy = Stratocaster::Octolytics::StringLengthPolicy.new(event)
          event = policy.enforce
          event[:dimensions][:import_string_truncated] = "true" if policy.truncated

          event
        end

        GitHub.analytics.record(events) unless events.empty?
      rescue GitRPC::InvalidRepository
        # The repo disappeared (was deleted) immediately after being pushed to,
        # or created. Don't report partial data for the push.
      end

      # hooks + stratocaster listens for this. Performed on create
      def instrument_creation
        GitHub.instrument "post_receive.refs.create", event_payload
      end

      # hooks + stratocaster listens for this. Performed on delete
      def instrument_delete
        GitHub.instrument "post_receive.refs.destroy", event_payload
      end

      # hook only event. Performed on create + delete
      def instrument_push
        event = Hook::Event::PushEvent.new(event_payload)
        delivery_system = Hook::DeliverySystem.new(event)

        delivery_system.generate_push_event_hookshot_payloads
        delivery_system.deliver_push_event_later

        if repository.push_notifications_active?
          # We wait 10 seconds to give async spam checks time to finish https://github.com/github/notifications/issues/296
          DeliverRepositoryPushNotificationJob
            .set(wait: GitHub::SpamChecker::DELAY_FOR_EXTERNAL_CHECKS)
            .perform_later(
              repository_id: repository.id,
              ref: ref,
              before: before,
              after: after,
              pusher_id: pusher&.id,
            )
        end
      end

      def event_payload
        {
          repo: repository,
          ref: ref,
          before: before,
          after: after,
          pusher: pusher,
          triggered_at: @triggered_at,
        }
      end

      # Publishes a message to Hydro for consumption by the Geyser code search
      # indexing system.
      #
      # Hydro publishing is asynchronous, so this should be fine to do here
      # rather than in a separate Resque job.
      def publish_repository_changed_for_code_search
        payload = {
          change: :PUSHED,
          repository: repository,
          ref: ref,
          updated_at: @triggered_at.utc,
        }
        GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
          GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:pushed"])
      end
    end
  end
end
