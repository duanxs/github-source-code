# frozen_string_literal: true

# Recalculates the cached number of stars for each user that
# starred the specific repository.
#
# Scenarios when this could run:
# - When a repository is deleted
# - When a repository is publicized or privatized
#
class GitHub::Jobs::CalculateUserStarsCount < GitHub::Jobs::Job
  def self.active_job_class
    ::CalculateUserStarsCountJob
  end

  areas_of_responsibility :user_profile

  BATCH_SIZE = 300

  def self.queue
    :stars
  end

  def self.perform_with_retry(repository_id, options = {})
    ActiveRecord::Base.connected_to(role: :reading) do
      user_ids = Star.repositories
        .where(starrable_id: repository_id)
        .pluck(:user_id)

      user_ids.each_slice(BATCH_SIZE) do |batch|
        users = User.where(id: batch)

        ApplicationRecord::Domain::KeyValues.throttle do
          users.each do |user|
            ActiveRecord::Base.connected_to(role: :writing) do
              user.update_starred_public_repositories_count!
              user.update_starred_private_repositories_count!
            end
          end
        end
      end
    end
  end
end
