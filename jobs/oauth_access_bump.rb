# frozen_string_literal: true

module GitHub
  module Jobs
    class OauthAccessBump < Job
      def self.active_job_class
        ::OauthAccessBumpJob
      end

      areas_of_responsibility :ecosystem_apps

      def self.queue
        :oauth_access_bumps
      end

      def self.perform(id, unix_timestamp)
        time = Time.at(unix_timestamp)
        now = Time.now.to_i
        valid_until = unix_timestamp + OauthAccess::ACCESS_THROTTLING
        ttl = valid_until - now

        # Only set the memcached lock if the ttl would be greater than 0. 0 is
        # default ttl and means forever, which we don't want. Less than zero
        # means the key would be of no use.
        if ttl > 0
          return unless GitHub.cache.add(write_prevention_cache_key(id), time, ttl)
        end

        ActiveRecord::Base.connected_to(role: :reading) do
          if oauth_access = OauthAccess.find_by_id(id)
            oauth_access.bump!(time)
          end
        end
      end

      def self.write_prevention_cache_key(id)
        "github:jobs:oauth_access_bump:#{id}"
      end
    end
  end
end
