# rubocop:disable Style/FrozenStringLiteralComment

require "failbot"

module GitHub
  module Jobs

    # Enqueued from Gist when a user clicks "Report as abuse" button on a gist.
    #
    # Initially we're just going to re-check the Gist content via the
    # normal gist spam checker.
    class GistFlaggedByUser < Job
      def self.active_job_class
        ::GistFlaggedByUserJob
      end

      areas_of_responsibility :gist

      def self.queue
        :gist_flagged_by_user
      end

      attr_reader :gist
      attr_reader :gist_id
      attr_reader :user
      attr_reader :user_id

      def initialize(gist_id, user_id)
        @gist_id = gist_id
        @gist = Gist.find_by_id(gist_id.to_i)
        @user_id = user_id
        @user = User.find_by_id(user_id.to_i)
      end

      def perform
        Failbot.push(
          job: self.class.name,
          name: gist&.user&.login,
          gist_id: gist_id,
          user: @user&.login,
        )

        fail "gist #{gist_id} doesn't exist" if gist.nil?

        # Staff reports get their own reporting, below
        unless user && user.site_admin?
          # Note the button push stat, even if we can't go any further below.
          GitHub.dogstats.increment "spam.flagged", tags: ["spam_target:gist", "flag_type:by_user"]

          # Make a note that it happened, even if we don't take any action
          GitHub.audit.log "spam.gist_flagged_by_user",
            gist: gist,
            actor: user.login,
            actor_id: user.id
        end

        # Not much to do right now about anonymous gists
        return unless gist.user

        # If they're already spammy, then our work here is done.
        return if gist.user.spammy?

        # If User is reporting himself, let's just move silently on
        return if user_id == gist.user_id

        # If a Hubber hit the button, just mark the gist spammy and log it.
        if user && user.site_admin?
          reason = "Gist #{gist.id} flagged via 'Report as abuse' button"
          gist.user.mark_as_spammy(reason: reason, actor: user)
          GitHub.dogstats.increment "spam.flagged", tags: ["spam_target:gist", "flag_type:by_staff"]

          GitHub.audit.log "spam.gist_flagged_by_staff",
            gist: gist,
            actor: user.login,
            actor_id: user.id,
            reason: reason

          # If a Hubber is manually nailing a spammer's gist, toss his IP
          # on the naughty list for a while.
          Spam::ip_blacklist(gist.user.last_ip)

          # Add this gist to the SpamCorpus and train the Bayes filter
          # with the new info.
          if (sc = Spam.train :spam, gist, user: user)
            sdj_classifier = Spam::Classifier.new
            sdj_classifier.train(sc)
          end
        elsif gist.respond_to?(:check_for_spam)
          gist.check_for_spam
        else
          # In case the old Gist#check_for_spam gets temporarily ripped out
          # by someone eagerly trying to delete too much code in a post-Gist2
          # frenzy. If so, do it manually.  Hey, frenzies happen.

          if (reason = GitHub::SpamChecker.test_gist(gist))
            GitHub.dogstats.increment "spam.flagged", tags: ["spam_target:gist"]
            gist.user.safer_mark_as_spammy(reason: reason)
          end
        end

        # Queue the reported user for review. If it's spammy, there are
        # probably more where it crawled from.
        GitHub::SpamChecker.notify("Gist %d (%s) reported for spam by %s" % [gist.id, gist.url, user.login])
        GlobalInstrumenter.instrument(
          "add_account_to_spamurai_queue",
          {
            account_global_relay_id: gist.user.global_relay_id,
            additional_context: "RESQUE",
            origin: :RESQUE,
            queue_global_relay_id: SpamQueue::POSSIBLE_SPAMMER_QUEUE_GLOBAL_RELAY_ID,
          },
        )
      end

      def self.perform(*args)
        job = new(*args)
        job.perform
        job
      end
    end
  end
end
