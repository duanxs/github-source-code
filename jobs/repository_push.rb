# rubocop:disable Style/FrozenStringLiteralComment

require "failbot"
require "resque_timer"
require "github/dgit/cold_storage_sql"

module GitHub
  module Jobs

    # Enqueued immediately after a user pushes to a repository by the
    # git post-receive hook.
    # See: lib/git-core/template_shadow/hooks/post-receive.rb
    # All refs pushed are provided in the payload. This job runs the PostReceive
    # job once for each ref.
    #
    #   path      - The full path to the repository that was pushed to on disk.
    #   pusher    - The String name of the user who performed the push, either
    #               directly or via a repository deploy key that they verified.
    #   refs      - An Array of [ref, before, after] tuples, one for each
    #               ref that was pushed.
    #   proto     - The protocol used to push to the repository. Either 'ssh',
    #               'http', 'mirror', 'wiki', or 'merge-button'.
    #
    class RepositoryPush < Job
      class RepositoryNotFound < StandardError; end

      areas_of_responsibility :repositories, :code_collab, :pull_requests

      def self.active_job_class
        ::RepositoryPushJob
      end

      extend Resque::Plugins::Timer

      retry_on_error RepositoryNotFound

      def self.queue
        :repository_push
      end

      def self.perform_with_retry(*args)
        args.extract_options! # drop 'retries' and other metadata if any
        new(*args).perform
      end

      attr_reader :path
      attr_reader :refs
      attr_reader :proto

      def initialize(path, pusher, refs, proto = nil)
        @path   = path
        @pusher = pusher
        @refs   = refs
        @proto  = proto
      end

      # Returns the Repository object associated with the push.
      def repository
        @repository ||= ActiveRecord::Base.connected_to(role: :reading) do
          Repository.with_path(path).tap do |repo|
            repo.network if repo # preload network from readonly replica
          end
        end
      end

      # Determine if the push was to a Wiki repository.
      def wiki?
        path.end_with?(".wiki.git")
      end

      # The user that performed the push.
      #
      # Returns the String username of the user that performed the push
      # directly or via a repository deploy key that they verified.
      def pusher
        @pusher
      end

      # The logical repository name. This accounts for Wiki repositories.
      def repo_name
        if wiki?
          repository.name + ".wiki"
        else
          repository.name
        end
      end

      # Do the dirt.
      def perform
        build_failbot_context do |context|
          raise RepositoryNotFound unless repository

          context.merge!(
            repo_id: repository.id,
            user_id: repository.owner&.id,
          )
        end

        # The repository might get deleted while this job is scheduled
        # or it might disappear while we're running. Stop a loop of
        # failures if we're deleting the repo.
        return if (repository.deleted? || !repository.exists_on_disk?)

        if wiki?
          perform_for_wiki
        elsif svn_mapping_push?
          perform_for_svn_mapping
        else
          perform_for_repo
        end

        if GitHub.flipper[:dgit_thaws].enabled?(repository) && GitHub::DGit::ColdStorage.is_network_cold?(repository.network.id)
          SpokesMoveNetworkFromColdStorageJob.perform_later(repository.network.id)
        end

      rescue GitRPC::InvalidRepository
        # The repo disappeared and we lost a race, let it go, let it go
      end

      def perform_for_wiki
        repository.bump_wiki_caches

        repository_wiki = repository.repository_wiki
        repository_wiki.pushed_count += 1
        repository_wiki.pushed_count_since_maintenance += 1
        repository_wiki.save!

        repository.async_backup_wiki
        run_receive_jobs GitHub::Jobs::WikiReceive
      end

      def perform_for_svn_mapping
        repository.async_backup
        update_network_push_stats
      end

      def perform_for_repo
        repository.update_pushed_at(Time.now)

        repository.async_backup
        update_network_push_stats

        repository.adjust_default_branch

        # run this before kicking off other stuff,
        # so that the default_branch is up to date.
        run_receive_jobs GitHub::Jobs::PostReceive

        update_disk_usage
        update_language

        # Synchronize shared storage, and/or generate incremental commit-graphs.
        repository.synchronize_shared_storage

        update_pre_receive_checkout
      end

      def svn_mapping_push?
        refs.all? { |ref, _, _| ref.start_with?("refs/__gh__/svn/") }
      end

      # Record the network level pushed_at and pushed_count attributes.
      def update_network_push_stats
        repository.network.pushed_at = Time.now
        repository.network.unpacked_size_in_mb = get_unpacked_size
        repository.network.save!
        repository.network.increment_pushed_counts
      end

      def get_unpacked_size
        repository.rpc.get_unpacked_size
      end

      def run_receive_jobs(klass)
        tags, branches = ordered_refs.partition do |(ref, before, after)|
          ref =~ %r|^refs/tags|
        end

        GitHub.dogstats.histogram("repository_push.tags", tags.size)
        GitHub.dogstats.histogram("repository_push.branches", branches.size)

        # Ignore tags if the push has more than 3 tags.
        processed_refs = tags.size > 3 ? branches : ordered_refs
        processed_refs.each_with_index do |(ref, before, after), index|
          begin
            klass.perform(
              user: repository.owner.to_s,
              repo: repo_name,
              before: before,
              after: after,
              ref: ref,
              pusher: pusher,
            )
          rescue Object => boom
            Failbot.report(boom)
          end
        end
      end

      # Sort the refs list by branch modifications followed by branch deletions.
      # This helps to ensure that events like merging a pull request are
      # processed before events like a pull request's branch being deleted.
      #
      # Returns an array of [ref, before, after] tuples - one for each ref that
      # was included in the push.
      def ordered_refs
        @ordered_refs ||= refs.sort { |a, b| b[2] <=> a[2] }
      end

      def update_language
        return unless refs.any? { |ref, _, _| ref == "refs/heads/#{repository.default_branch}" }
        repository.enqueue_analyze_language_breakdown
      end

      # Update the current disk usage for this repository
      #
      # We're using the `enqueue_once_per_interval` helper from RockQueue to
      # prevent recalculating the disk usage too many times during many frequent
      # pushes. We will only calculate disk usage _once_ for all the pushes
      # after an hour has passed.
      def update_disk_usage
        RepositoryDiskUsageJob.enqueue_once_per_interval([repository.id], interval: 60 * 60)
      end

      def update_pre_receive_checkout
        ActiveRecord::Base.connected_to(role: :reading) do
          if hook = PreReceiveHook.find_by_repository_id(repository.id)
            PreReceiveRepositoryUpdateJob.perform_later(repository.id, hook.repository_url)
          end
        end
      end

      def build_failbot_context(&block)
        context = {
          job: self.class.name,
          refs: refs.inspect,
          pusher: pusher,
          wiki: wiki?,
        }

        yield context
      ensure
        Failbot.push(context)
      end
    end
  end
end
