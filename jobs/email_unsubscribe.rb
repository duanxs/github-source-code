# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class EmailUnsubscribe < Email
      def self.active_job_class
        ::EmailUnsubscribeJob
      end

      areas_of_responsibility :notifications, :email

      def self.queue
        :email_unsubscribe
      end

      attr_reader :mail, :sender, :target, :error

      def self.perform_with_retry(mail, options = {})
        job = new(mail)
        job.perform
        job
      end

      def perform
        return if @mail.blank?
        load_sender_and_target

        unsub_method_name = "unsub_from_#{@target.class.name.underscore}"

        respond_to?(unsub_method_name) || raise(EmailError, :unable_to_reply)

        send(unsub_method_name)
      rescue EmailError => err
        @error = err # see log_result_in_syslog
      rescue Object => err
        @error = err
        raise
      ensure
        log_result_in_syslog
      end

      def unsub_from_commit_comment
        Newsies::ThreadSubscription.throttle { GitHub.newsies.ignore_thread(@sender, @target.repository, @target.commit) }
      end

      def unsub_from_issue(issue = @target)
        Newsies::ThreadSubscription.throttle { GitHub.newsies.ignore_thread(@sender, issue.repository, issue) }
      end

      def unsub_from_pull_request_review_comment
        unsub_from_issue @target.pull_request.issue
      end
    end
  end
end
