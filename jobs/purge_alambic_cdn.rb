# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    class PurgeAlambicCdn < Job
      def self.active_job_class
        ::PurgeAlambicCdnJob
      end

      areas_of_responsibility :data_infrastructure

      extend GitHub::HashLock

      def self.queue
        :cdn
      end

      # Public: Purges a given surrogate key.  Optionally waits for replication
      # to the readonly database before purging.
      #
      # options - Hash
      #           :key       - String surrogate key to purge.
      #           :state     - String state name: "exists" or "deleted"
      #           :model     - String class name of the record to check.
      #           :id        - Integer ID of the record to check.
      #           :signature - String unique signature of the record.
      #           :retries   - Integer number of times this job has been retried.
      #
      # Returns nothing.
      def self.perform(options = {})
        options = options.with_indifferent_access
        keys = Array(options["keys"])
        keys.sort!

        AssetUploadable::Cdn.purge(*keys)
      rescue Faraday::Error => err
        retries = options["retries"].to_i

        LegacyApplicationJob.retry_later(self.active_job_class, [options], retries: retries)

        if err.is_a?(AssetUploadable::Cdn::Error)
          Failbot.report_user_error err,
            retries: retries,
            keys: keys
          return
        end

        raise err
      end

      def self.lock(options = {})
        keys = Array(options["keys"])
        keys.sort!
        "#{keys.join(",")}:#{options["retries"].to_i}"
      end

      def self.lock_timeout
        300
      end
    end
  end
end
