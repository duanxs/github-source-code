# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class GistMaintenance < Job
      def self.active_job_class
        ::GistMaintenanceJob
      end

      areas_of_responsibility :git

      def self.queue
        :gist_maintenance
      end

      def self.log(entity, data, &blk)
        GitHub::Logger.log_context(job: active_job_class.to_s, spec: entity.dgit_spec) do
          GitHub::Logger.log(data, &blk)
        end
      end

      def self.perform(gist_id)
        Failbot.push spec: "gist/#{gist_id}"

        return unless gist = Gist.find_by_id(gist_id)

        log(gist, method: "perform") do
          log(gist,
              message: "starting git maintenance",
              status: gist.maintenance_status,
              maintenance_elapsed: Time.now - (gist.last_maintenance_at || gist.created_at),
              maintenance_pushes: gist.pushed_count_since_maintenance || 0)

          GitHub.dogstats.time("git_maintenance.perform", tags: ["type:gist"]) do
            perform! gist
          ensure
            GitHub.dogstats.increment("git_maintenance", tags: ["type:gist", "result:#{gist.maintenance_status}"])
          end
        ensure
          log(gist,
              at: "exit",
              message: "exiting git maintenance",
              status: gist.maintenance_status)
        end

        nil
      end

      # Run maintenance for the given gist repository.
      def self.perform!(gist)
        # if a backup-utils backup is in progress, delay the sync operation by
        # requeuing after a short sleep period.
        if GitHub::Enterprise.backup_in_progress?
          clear_lock(gist.id)
          gist.schedule_maintenance
          sleep 1
          return
        end

        gist.perform_maintenance
      end

      # All GistMaintenance jobs are locked by their gist id. No attempt
      # will be made to enqueue a job that's already running and the lock is
      # checked before running the job.
      extend GitHub::HashLock
    end
  end
end
