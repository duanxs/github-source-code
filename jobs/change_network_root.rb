# frozen_string_literal: true

module GitHub
  module Jobs
    class ChangeNetworkRoot < Job
      def self.active_job_class
        ::ChangeNetworkRootJob
      end

      areas_of_responsibility :stafftools, :repo_networks

      def self.queue
        :change_network_root
      end

      def self.perform(repo_id)
        new(repo_id).perform
      end

      def initialize(repo_id)
        @repo_id = repo_id
      end

      def perform
        return unless repo = Repository.find_by_id(@repo_id)

        Failbot.push repo_id: repo.id

        repo.make_network_root!

        repo.repository_and_descendants.each do |repository|
          repository.reindex_issues(true)
          repository.reindex_pull_requests(true)
        end

        # Reindex code, commits, and backfill contributions now that the
        # repository is the root of the network
        repo.reindex_code
        repo.reindex_commits
        CommitContribution.backfill!(repo)
      end
    end
  end
end
