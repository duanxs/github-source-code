# frozen_string_literal: true

require_relative "install_automatic_integrations/result"

module GitHub
  module Jobs
    class InstallAutomaticIntegrations < Job
      def self.active_job_class
        ::InstallAutomaticIntegrationsJob
      end

      areas_of_responsibility :ecosystem_apps

      extend GitHub::HashLock

      INSTALL_ON_ALL_REPOS = [] # Passing an empty array to `Integration#install_on` is equivalent to "install on all repos"
      SILENT_ERRORS = %i(spammy_actor spammy_target)

      AppInstallError = Class.new(RuntimeError)
      AppUpdateError  = Class.new(RuntimeError)

      retry_on_error AppInstallError, AppUpdateError, ActiveRecord::RecordNotFound, ActiveRecord::RecordNotUnique

      def self.queue
        :install_automatic_integrations
      end

      # Public: install the GitHub App on the Target via the given Trigger
      #
      # target_id       Integer: the ID of the Organization or User on which to
      #                 install the App.
      # integration_id  Integer: the ID of the GitHub App to install.
      # repo_ids        Array of Integers: a list of repositories, owned by the
      #                 target, on which to give the App permission. Supplying
      #                 nil or an empty array will install the App on *all*
      #                 repositories.
      # options         Hash: arbitrary values that will be passed to the
      #                 trigger class after the App has been installed.
      def self.perform_with_retry(target_id, integration_id, trigger_id, repo_ids = INSTALL_ON_ALL_REPOS, options = {})
        options = options.stringify_keys
        enqueued_timestamp = options.fetch("enqueued_timestamp", Time.now.to_i)

        trigger = IntegrationInstallTrigger.find(trigger_id)
        integration = Integration.find(integration_id)
        target   = User.find(target_id)

        if IntegrationInstallTrigger.latest(integration: integration, install_type: trigger.install_type)&.deactivated?
          return Result.failed(self, :deactivated, target, integration)
        end

        installer = target.organization? ? target.admins.first : target
        repo_ids  = Array(repo_ids)

        unless should_install?(trigger, integration, target, repo_ids, options)
          return Result.failed(self, :canceled, target, integration)
        end

        result = if (installation = integration.installations_on(target).first)
          add_repositories_to_existing_installation(installation, repo_ids, installer)
        else
          create_new_installation(integration, target, repo_ids, installer, trigger_id)
        end

        if result.failed?
          # only raise exceptions when running asynchronously
          raise result.exception if result.exception.present? && !GitHub.foreground?
          return result
        end

        unless result.already_installed?
          # consider passing the result here if/when we start persisting it
          trigger.after_integration_installed(
            integration: integration,
            installation: result.installation,
            target:  target,
            installer: installer,
            repositories: result.repositories,
            options: options,
          )
        end

        mean_time_to_install_ms = (Time.now.to_i - enqueued_timestamp) * 1_000
        track_time_to_install(integration, mean_time_to_install_ms)

        result
      end

      def self.lock(target_id, integration_id, trigger_id, repo_ids = INSTALL_ON_ALL_REPOS, options = {})
        [target_id, integration_id, trigger_id, repo_ids].join(":")
      end

      # Public: Add repositories to an installation.
      #
      # Returns a Boolean, (IntegrationInstallation/nil), and an Array
      def self.add_repositories_to_existing_installation(installation, repo_ids, editor)
        integration = installation.integration

        if installation.installed_on_all_repositories?
          return Result.already_installed(self, :installed_on_all_repositories, installation)
        end

        editor_result = if repo_ids != INSTALL_ON_ALL_REPOS
          repo_ids_to_install = (repo_ids - installation.repository_ids)

          # If there aren't any repositories to add, exit early.
          if repo_ids_to_install.empty?
            return Result.already_installed(self, :already_installed, installation, installation_type: "selected")
          end

          repositories = Repository.find(repo_ids_to_install)
          IntegrationInstallation::Editor.append(installation, repositories: repositories, editor: editor)
        else
          repositories = INSTALL_ON_ALL_REPOS
          IntegrationInstallation::Editor.perform(installation, repositories: INSTALL_ON_ALL_REPOS, editor: editor)
        end

        unless editor_result.success?
          exception = AppUpdateError.new(editor_result.error)
          return Result.failed(
            self,
            :failed_to_update,
            installation.target,
            installation.integration,
            exception: exception,
          )
        end

        installation_type = repo_ids == INSTALL_ON_ALL_REPOS ? "all" : "selected"

        Result.success(
          self,
          :repositories_added,
          editor_result.installation,
          repositories,
          installation_type: installation_type,
        )
      end

      # Public: Create a new IntegrationInstallation and install a set of repositories.
      #
      # Returns a Boolean, IntegrationInstallation, and an Array
      def self.create_new_installation(integration, target, repo_ids, installer, trigger_id)
        repositories = Repository.find(repo_ids)

        install_result = integration.install_on(target, repositories: repositories, installer: installer, trigger_id: trigger_id)

        if install_result.success?
          Result.success(self, :installed, install_result.installation, repositories)
        else
          exception = nil
          unless SILENT_ERRORS.include?(install_result.reason)
            exception = AppInstallError.new(install_result.error)
          end
          Result.failed(
            self,
            :failed_to_create,
            target,
            integration,
            reason: install_result.reason,
            exception: exception,
          )
        end
      end

      # Private: Run before installation checks.
      #
      # This callback is optional, and can be used to signal cancelations.
      # See https://github.com/github/github/pull/125211/files#r328366552 for context
      def self.should_install?(trigger, integration, target, repo_ids, options)
        result = trigger.should_install?(
          integration: integration,
          target: target,
          repositories: Repository.find(repo_ids),
          options: options,
        )
        return true if result.nil?
        !!result
      end
      private_class_method :should_install?

      def self.track_time_to_install(integration, mean_time_to_install_ms)
        tags = ["installation_queue:#{self.queue}", "integration:#{integration.slug}", "owner:#{integration.owner.login}"]

        GitHub.dogstats.timing(
          "jobs.install_automatic_integrations.mean_time_to_install",
          mean_time_to_install_ms,
          tags: tags,
        )

        GitHub.dogstats.gauge(
          "jobs.install_automatic_integrations.fixed_time_to_install",
          mean_time_to_install_ms,
          tags: tags,
        )
      end
      private_class_method :track_time_to_install
    end
  end
end
