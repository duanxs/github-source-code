# frozen_string_literal: true

module GitHub
  module Jobs
    class PublicKeyAccess < Job
      def self.active_job_class
        ::PublicKeyAccessJob
      end

      def self.queue
        :public_key_accesses
      end

      def self.perform(id, unix_timestamp)
        time = Time.at(unix_timestamp)
        now = Time.now.to_i
        valid_until = unix_timestamp + PublicKey::ACCESS_THROTTLING
        ttl = valid_until - now

        # Only set the memcached lock if the ttl would be greater than 0. 0 is
        # default ttl and means forever, which we don't want. Less than zero
        # means the key would be of no use.
        if ttl > 0
          return unless GitHub.cache.add(write_prevention_cache_key(id), time, ttl)
        end

        ActiveRecord::Base.connected_to(role: :reading) do
          if public_key = PublicKey.find_by_id(id)
            public_key.access!(time)
          end
        end
      end

      def self.write_prevention_cache_key(id)
        "github:jobs:public_key_access:#{id}"
      end
    end
  end
end
