# frozen_string_literal: true

require "github/hash_lock"

module GitHub
  module Jobs

    # This job will restore all records of a user to the search index after the
    # spammy flag has been cleared. Those records include:
    #
    # * user
    # * repositories
    # * source code
    # * commits
    # * issues
    # * issue comments
    # * milestones
    # * gists
    # * wikis
    #
    class RestoreUserFromSpammy < Job
      def self.active_job_class
        ::RestoreUserFromSpammyJob
      end

      extend GitHub::HashLock

      # Restore the user by executing this job. Pass in the user ID.
      #
      # user_id - The Integer ID of the user to restore.
      #
      def self.perform(user_id)
        user = User.find_by_id(user_id)
        return unless user and !user.spammy?

        new(user).restore_all
      end

      def self.queue
        :index_high
      end

      attr_reader :user

      def initialize(user)
        Failbot.push user_id: user.id, user: user.login
        @user = user
      end

      def restore_all
        restore_user
        restore_issues_and_milestones
        restore_pull_requests
        restore_repositories
        restore_code
        restore_commits
        restore_gists
        restore_wikis
      end

      # Restore the given user to the search index.
      def restore_user
        restore_to_each_writable_index(Elastomer::Indexes::Users)
      end

      # Restore all issues and milestones to the search index.
      def restore_issues_and_milestones
        restore_to_each_writable_index(Elastomer::Indexes::Issues)
      end

      # Restore all pull requests to the search index.
      def restore_pull_requests
        restore_to_each_writable_index(Elastomer::Indexes::PullRequests)
      end

      # Restore all repositories to the search index.
      def restore_repositories
        restore_to_each_writable_index(Elastomer::Indexes::Repos)
      end

      # Restore all source code to the search index.
      def restore_code
        restore_to_each_writable_index(Elastomer::Indexes::CodeSearch)
      end

      # Restore all commits to the search index.
      def restore_commits
        restore_to_each_writable_index(Elastomer::Indexes::Commits)
      end

      # Restore all gists to the search index for the given user.
      def restore_gists
        restore_to_each_writable_index(Elastomer::Indexes::Gists)
      end

      # Restore all wikis to the search index.
      def restore_wikis
        restore_to_each_writable_index(Elastomer::Indexes::Wikis)
      end

      def restore_to_each_writable_index(index_class)
        index_name = index_class.index_name
        GitHub.dogstats.time("restore_user_index.time", tags: ["index:" + index_name]) do
          Elastomer.each_writable_index(index_class) do |config|
            begin
              index = index_class.new(config.name, config.cluster)
              index.restore_user(user)
            rescue StandardError => boom
              Failbot.report boom
            end
          end
        end
      end
    end

  end  # Jobs
end  # GitHub
