# frozen_string_literal: true

module GitHub
  module Jobs
    class MarketplaceRetargeting < Job
      def self.active_job_class
        ::MarketplaceRetargetingJob
      end

      areas_of_responsibility :marketplace, :background_jobs

      schedule interval: 1.hour

      BATCH_SIZE = 1_000
      STALE_THRESHOLD = 3.days
      NEWSLETTER = "marketplace_orders".freeze

      # See https://github.com/github/marketplace/issues/591#issuecomment-417507240
      OPTIN_REQUIRED_COUNTRIES = %w(CA AT BE BG HR CY CZ DK EE FI FR DE GR HU IE IT LV LT LU MT NL PL PT RO SK SI ES SE UK)

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.queue
        :mailers
      end

      def self.perform
        ActiveRecord::Base.connected_to(role: :reading) do
          record_retargeting_metrics

          last_user_id = 0

          while (preview_users = get_preview_users(last_user_id)).any?
            user_countries = user_countries(preview_users.map(&:id))

            preview_users.each do |user|
              subscription = user.newsletter_subscriptions.find_by(name: NEWSLETTER)
              previews = self.targetable_previews(user).select { |preview| preview.account.present? }
              next if previews.none?

              if eligible_for_email_retargeting?(user: user, subscription: subscription, countries: user_countries[user.id], previews: previews)
                send_retargeting_email(user, subscription, previews)
                GitHub.dogstats.increment("marketplace.retargeting.deliveries")
                GitHub.analytics.record([event_payload(user, previews)])
              else
                reset_notice_dismissal(user, previews)
                GitHub.dogstats.increment("marketplace.retargeting.notices_triggered")
              end
            end

            last_user_id = preview_users.last.id
          end
        end
      end

      def self.reset_notice_dismissal(user, previews)
        ActiveRecord::Base.connected_to(role: :writing) do
          user.reset_notice("marketplace_retargeting")
          previews.each { |preview| preview.update!(retargeting_notice_triggered_at: Time.current) }
        end
      end

      def self.record_retargeting_metrics
        1.upto(3) do |age|
          duration = age.days
          previews = Marketplace::OrderPreview.where(email_notification_sent_at: nil).where("viewed_at <= ?", duration.ago)
          previews = previews.where("viewed_at > ?", (age + 1).days.ago) unless duration == STALE_THRESHOLD
          GitHub.dogstats.count("marketplace.order_previews.stale", previews.count, tags: ["days:#{age}"])
        end
      end

      def self.get_preview_users(last_user_id)
        User.joins(:marketplace_order_previews).includes(:primary_user_email).
          joins("JOIN marketplace_listings ON marketplace_order_previews.marketplace_listing_id=marketplace_listings.id").
          annotate("cross-schema-domain-query-exempted").
          where("users.id > ?", last_user_id).
          where(users: { spammy: false, suspended_at: nil }).
          where(marketplace_listings: { state: ::Marketplace::Listing.publicly_listed_state_values }).
          where(marketplace_order_previews: { email_notification_sent_at: nil }).
          where("viewed_at <= ?", STALE_THRESHOLD.ago).
          order(:id).distinct.first(BATCH_SIZE)
      end

      def self.targetable_previews(user)
        user.marketplace_order_previews.targetable.includes(:listing, :listing_plan, :account).
          where("viewed_at <= ?", STALE_THRESHOLD.ago)
      end

      def self.user_countries(user_ids)
        # Build a list of countries from which this batch of users has viewed pages in the past week. We'll use this to determine
        # their country of residence to know whether we're allowed to email them without opt-in.
        page_view_query = %Q(
          SELECT DISTINCT user_id, ip_country_code
            FROM hive.github_dotcom.logged_in_page_views_rollup
            WHERE day BETWEEN timestamp '#{7.days.ago.to_s(:db)}' AND timestamp '#{Time.current.to_s(:db)}'
            AND user_id IN (#{user_ids.join(", ")})
        )
        _, rows = GitHub.presto.run(page_view_query)
        rows.each_with_object(Hash.new { |h, k| h[k] = [] }) { |(user, country), hash| hash[user] << country }
      end

      def self.eligible_for_email_retargeting?(user:, subscription:, countries:, previews:)
        marketing_optin = NewsletterPreference.marketing?(user: user)
        subscribed = subscription.nil? || subscription.active?

        # Users residing in Canada/EU may not be emailed without optin
        if !marketing_optin
          marketing_ok = (countries & OPTIN_REQUIRED_COUNTRIES).empty?
        else
          marketing_ok = true
        end

        undeliverable_reasons = []

        unless user.wants_email?
          undeliverable_reasons << "reason:global_optout"
        end

        unless user.email
          undeliverable_reasons << "reason:no_email"
        end

        unless marketing_ok
          undeliverable_reasons << "reason:no_optin"
        end

        unless subscribed
          undeliverable_reasons << "reason:unsubscribed"
        end

        if previews.none? { |preview| preview.listing_plan&.paid? }
          undeliverable_reasons << "reason:no_previews_for_paid_plans"
        end

        if undeliverable_reasons.any?
          undeliverable_reasons.each do |reason|
            GitHub.dogstats.increment("marketplace.retargeting.undeliverable", tags: [reason])
          end

          return false
        end

        GitHub.dogstats.increment("marketplace.retargeting.deliverable")
        true
      end

      def self.send_retargeting_email(user, subscription, previews)
        ActiveRecord::Base.connected_to(role: :writing) do
          unsubscribe_token = NewsletterSubscription.get_unsubscribe_token(user, NEWSLETTER)

          if subscription.nil?
            user.newsletter_subscriptions.create(
              name: NEWSLETTER, kind: "notification", auto_subscribed: true, subscribed_at: Time.zone.now, active: true,
            )
          end

          MarketplaceMailer.retarget(user, previews, unsubscribe_token).deliver_now

          previews.each { |preview| preview.update!(email_notification_sent_at: Time.zone.now) }
        end
      end

      def self.event_payload(user, previews)
        {
          event_type: "marketplace-retargeting-email-sent",
          dimensions: {
            user_id: user.id,
          },
          context: {
            order_previews: previews.map do |preview|
              {
                account_id: preview.account_id,
                marketplace_listing_id: preview.marketplace_listing_id,
                marketplace_listing_plan_id: preview.marketplace_listing_plan_id,
              }
            end,
          },
        }
      end
    end
  end
end
