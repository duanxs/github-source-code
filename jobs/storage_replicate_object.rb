# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # StorageReplicateObject takes the oid of an underreplicated object and
    # ensures it is replicated on GitHub.storage_replica_count nodes.
    #
    # This job gets queued from StorageClusterMaintenanceSchedulerJob.perform_now
    class StorageReplicateObject < Job
      def self.active_job_class
        ::StorageReplicateObjectJob
      end

      class Error < StandardError
      end

      areas_of_responsibility :data_infrastructure
      extend GitHub::HashLock

      def self.queue
        :storage_cluster
      end

      def self.lock(oid_or_hash)
        convert_to_hash_args(oid_or_hash)
      end

      def self.perform(oid_or_hash)
        args = convert_to_hash_args(oid_or_hash)
        oid = args["oid"]
        if args["non_voting"]
          replicate(oid, non_voting: true,
            limit: GitHub::Storage::Allocator.non_voting_replica_count(args["datacenter"]),
            replicas: GitHub::Storage::Allocator.query_least_loaded_non_voting_hosts(args["datacenter"]))
        else
          replicate(oid, non_voting: false,
            limit: GitHub.storage_replica_count,
            replicas: GitHub::Storage::Allocator.query_least_loaded_voting_hosts)
        end
      end

      def self.replicate(oid, limit:, non_voting:, replicas:)
        hosts = GitHub::Storage::Allocator.hosts_for_oid(oid)
        total_replicas = replicas.size
        replicas -= hosts
        current_replicas = total_replicas - replicas.size
        return if current_replicas >= limit

        send_to = replicas.sample(limit - current_replicas)
        ok, body = GitHub::Storage::Client.replicate_to(hosts.first, [{
          oid: oid,
          fileservers: send_to.map { |host| GitHub.storage_replicate_fmt % host },
        }])

        if !ok
          GitHub::Logger.log(method: __method__,
                             host: hosts.first,
                             body: body)
          raise Error, "Server error on replicate_to"
        end

        failed = []
        received = []
        Array(body["objects"]).each do |obj|
          Array(obj["success"]).each do |raw_url|
            received << URI.parse(raw_url).host
          end

          # fallback due to json structag typo
          # https://github.com/github/alambic/blob/2b1f21bc5c61613e3b3b231b3a8badcdf0b55e09/cluster/cluster.go#L592
          if errors = obj["errors"] || obj["Errors"]
            errors.each do |node, err|
              failed << "#{node}: #{err}"
            end
          end
        end

        if received.present?
          GitHub::Storage::Creator.create_replicas_for_oid(oid, received)
        end

        if failed.present?
          GitHub::Logger.log(method: __method__,
                             oid: oid,
                             failed: failed)
          raise Error, "Error replicating #{oid}"
        end
      end

      def self.convert_to_hash_args(oid_or_hash)
        args = case oid_or_hash
        when String then {"oid" => oid_or_hash, "non_voting" => false}
        when Hash then oid_or_hash.stringify_keys
        else
          raise ArgumentError, "expected String OID or hash args, got: #{oid_or_hash.inspect}"
        end
      end
    end
  end
end
