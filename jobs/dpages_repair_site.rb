# frozen_string_literal: true

require "github/hash_lock"
require "github/timeout_and_measure"
require "github/pages/management/delegate"
require "github/pages/management/repair"

module GitHub
  module Jobs
    # DpagesRepairSite repairs a single site's voting or non-voting replicas
    # To enqueue, pass an array of arguments including [page_id, page_deployment_id, voting].
    # page_deployment_id may be nil for legacy deployments.
    class DpagesRepairSite < Job
      def self.active_job_class
        ::DpagesRepairSiteJob
      end

      areas_of_responsibility :pages

      class RepairFailed < RuntimeError
        def areas_of_responsibility
          [:pages]
        end
      end

      extend GitHub::HashLock
      include GitHub::TimeoutAndMeasure

      # The queue the repairs run on. The scheduler jobs don't run on
      # this queue, they're enqueued on the :dpages_maintenance_scheduler queue.
      def self.queue
        :dpages_maintenance
      end

      # Perform the evacuation for the single site on the single host.
      def self.perform(*args)
        new(args, delegate: GitHub::Pages::Management::Delegate.new(logger: GitHub::Logger)).perform
      end

      def initialize(args, delegate: nil)
        @delegate = delegate || GitHub::Pages::Management::Delegate.new
        @args = args
      end

      def validate_args!
        # args must contain [page_id, page_deployment_id, voting]
        if !@args.is_a?(Array) || @args.size != 3
          raise ArgumentError, "invalid arguments: #{@args.inspect}"
        end

        page_id, page_deployment_id, voting = *@args
        unless page_id.is_a?(Integer)
          raise ArgumentError, "page_id must be an integer, but was #{page_id.class}: #{page_id.inspect}"
        end

        unless page_deployment_id.nil? || page_deployment_id.is_a?(Integer)
          raise ArgumentError, "page_deployment_id must be nil or an integer, but was #{page_deployment_id.class}: #{page_deployment_id.inspect}"
        end

        unless voting == true || voting == false
          raise ArgumentError, "voting must be true or false, but was #{voting.class}: #{voting.inspect}"
        end

        true
      end

      # Limit the execution time of this job so we don't have dangling repairs.
      # If it times out, then fine: the scheduler will enqueue it again.
      def max_execution_time
        10.minutes
      end

      # Perform the evacuation, limited to max_execution_time, with logs and stats along the way.
      def perform
        validate_args!

        timeout_and_measure(max_execution_time.to_i, "pages.dpages_maintenance.work") do
          command = GitHub::Pages::Management::Repair.new(delegate: @delegate)

          page_id, page_deployment_id, voting = *@args
          Failbot.push(app: "pages", page_id: page_id, page_deployment_id: page_deployment_id, voting: voting)
          @delegate.log "Repairing page_id=#{page_id} page_deployment_id=#{page_deployment_id.inspect} voting=#{voting}"

          result = begin
            command.repair_single_site(page_id: page_id, page_deployment_id: page_deployment_id, voting: voting)
          rescue GitHub::Pages::Management::ExecutionError => e
            Failbot.push(logged: @delegate.logged)
            raise e
          end

          if result.nil?
            @delegate.log "Nothing to repair for page_id=#{page_id} page_deployment_id=#{page_deployment_id.inspect} voting=#{voting}"
            break false
          end

          unless result
            @delegate.log "Failed to repair page_id=#{page_id} page_deployment_id=#{page_deployment_id.inspect} voting=#{voting}"
            Failbot.push(logged: @delegate.logged)
            raise RepairFailed, "Repair failed"
          end

          true
        end
      end
    end
  end
end
