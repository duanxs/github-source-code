# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"
require "github/timeout_and_measure"
require "github/pages/management/delegate"
require "github/pages/management/repair"

module GitHub
  module Jobs
    # Timer that schedules repair jobs for DPages sites.
    class DpagesMaintenanceScheduler < Job
      def self.active_job_class
        ::DpagesMaintenanceSchedulerJob
      end

      include GitHub::TimeoutAndMeasure

      areas_of_responsibility :pages

      schedule interval: GitHub.dpages_maintenance_scheduler_schedule_interval

      # Perform the evacuation for the single site on the single host.
      def self.perform(*args)
        new(args, delegate: GitHub::Pages::Management::Delegate.new(logger: GitHub::Logger),
        ).perform
      end

      attr_reader :delegate, :args

      def initialize(args, delegate: nil)
        @delegate = delegate || GitHub::Pages::Management::Delegate.new
        @args = args
      end

      # Run every interval, schedules jobs_per_interval maintenance jobs to run.
      def perform
        timeout_and_measure(GitHub.dpages_maintenance_scheduler_maximum_execution_time.to_i, "pages.dpages_maintenance.scheduler") do
          ActiveRecord::Base.connected_to(role: :reading) do
            repairable_unhealthy_pages_replica_counts do |repl|
              DpagesRepairSiteJob.perform_later(repl.page_id, repl.page_deployment_id, repl.voting)
            end
          end
        end
      end

      def repairable_unhealthy_pages_replica_counts
        GitHub::Pages::Management::Repair.new(delegate: delegate)
          .repairable_unhealthy_pages_replica_counts(limit: GitHub.dpages_maintenance_scheduler_batch_size) do |repl|
          yield repl
        end
      end

      # The queue the scheduler runs on. The actual maintenance jobs don't run
      # on this queue, they're enqueued on the :dpages_maintenance queue.
      def self.queue
        :dpages_maintenance_scheduler
      end

      # Repairs are a concern of both .com and Enterprise
      def self.enabled?
        true
      end

      # Only one may run at any given time. Locked based on the uniqueness of args.
      extend GitHub::HashLock
    end
  end
end
