# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Timer that schedules maintenance jobs for individual wikis. See
    # RepositoryWiki::Maintenance for scheduling logic and different types of
    # maintenance tasks.
    class WikiMaintenanceScheduler < Job
      def self.active_job_class
        ::WikiMaintenanceSchedulerJob
      end

      areas_of_responsibility :wikis

      schedule interval: 10.minutes

      # This job is scheduled in GHE and github.com environments.
      def self.enabled?
        true
      end

      # The number of maintenance jobs to enqueue at each interval.
      def self.jobs_per_interval
        1000
      end

      # Minimum length of time between maintenance runs for any single
      # wiki in seconds. Wikis that have received maintenance
      # within this time period will not be scheduled unless they've received
      # more than 50 pushes.
      def self.min_age
        1.week
      end

      # Run every interval, schedules jobs_per_interval maintenance jobs to run.
      def self.perform
        SlowQueryLogger.disabled do
          wikis = RepositoryWiki.schedule_maintenance(jobs_per_interval, min_age)
          return if wikis.empty?

          most_stale  = wikis.min_by { |wiki| wiki.last_maintenance_at || wiki.created_at }
          lag_time = Time.now - (most_stale.last_maintenance_at || most_stale.created_at)
          GitHub.dogstats.gauge("wiki.maint.lag", lag_time)

          most_active = wikis.max_by { |wiki| wiki.pushed_count_since_maintenance }
          GitHub.dogstats.gauge("wiki.maint.most-active", most_active.pushed_count_since_maintenance)

          wikis
        end
      end

      # The queue the scheduler runs on. The actual maintenance jobs don't run
      # on this queue, they're enqueued on the fs maintenance queues.
      def self.queue
        :wiki_maintenance_scheduler
      end
    end
  end
end
