# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    class DgitDestroyGistReplica < Job
      def self.active_job_class
        ::SpokesDestroyGistReplicaJob
      end

      areas_of_responsibility :dgit

      def self.perform(gist_id, host, inhibit_repairs = false)
        Failbot.push app: "github-dgit",
                     spec: "gist/#{gist_id}"

        GitHub::Logger.log_context(job: name.demodulize, spec: "gist/#{gist_id}") do
          GitHub::Logger.log(method: "perform!", host: host, inhibit_repairs: inhibit_repairs) do
            GitHub.dogstats.time("dgit.actions.destroy-gist", tags: ["server:#{host}"]) do
              perform!(gist_id, host, inhibit_repairs: inhibit_repairs)
            end
          end
        end
      rescue Freno::Throttler::Error
        # Ignore freno errors and rely on retries from our own maintenance scheduler
      rescue GitHub::DGit::ReplicaDestroyNotFoundError
        # Ignore. This is possible due to races.
      end

      def self.perform!(gist_id, host, via: nil, inhibit_repairs: false)
        Failbot.push(via: via) unless via.nil?

        gist = Gist.find_by_id(gist_id)
        raise GitHub::DGit::ReplicaDestroyNotFoundError, "There is no gist #{gist_id}" unless gist

        all_replicas = GitHub::DGit::Routing.all_gist_replicas(gist_id)
        Failbot.push delegate_replicas: all_replicas.inspect

        replica = all_replicas.find { |r| r.host == host }
        raise GitHub::DGit::ReplicaDestroyNotFoundError, "Gist #{gist_id} on #{host} not found" unless replica

        return unless GitHub::DGit::Maintenance.set_gist_state(gist_id, host, GitHub::DGit::DESTROYING)

        GitHub.stats.increment "dgit.#{host}.actions.destroy-gist" if GitHub.enterprise?
        GitHub.dogstats.increment "dgit", tags: ["host:#{host}", "action:destroy", "type:gist"]

        GitHub::DGit::Maintenance::delete_gist_from_disk_on_replica(
          gist_id, replica, gist.original_shard_path)

        # Order for these next two steps is important.
        # - Deleting the gist_replicas row must be last, because its absence
        #   is what indicates that the destroy succeeded.  If it's still there as
        #   state=DESTROYING, then this operation will get retried as needed.
        # - Schedule the creation event before deleting the gist_replicas row,
        #   so that the creation event can avoid the host with to-be-deleted
        #   replica when picking a host on which to place the to-be-created
        #   replica.

        begin
          # Schedule creation of a replacement replica, if needed.
          remaining_replicas = GitHub::DGit::Routing.all_gist_replicas(gist_id).select(&:voting?).reject(&:destroying?)
          if remaining_replicas.size < GitHub.dgit_copies
            GitHub::DGit::Maintenance.create_gist_replica(gist_id) unless inhibit_repairs
          end
        ensure
          # Delete the row from gist_replicas.
          GitHub::DGit::DB.for_gist_id(gist_id).throttle do
            GitHub::DGit::Maintenance.delete_gist_replica_for_host(gist_id, host)
          end
        end

        GitHub::DGit::Maintenance.rebalance_gist_read_weight(gist_id)
      end

      def self.queue
        :dgit_repairs
      end
    end
  end
end
