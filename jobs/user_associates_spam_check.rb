# rubocop:disable Style/FrozenStringLiteralComment

require "failbot"

module GitHub
  module Jobs

    # Look more closely at other User accounts sharing an IP address and
    # other characteristics. Done here out-of-band because these checks
    # tend to be more expensive than we should do during a request cycle.
    class UserAssociatesSpamCheck < Job
      def self.active_job_class
        ::UserAssociatesSpamCheckJob
      end

      def self.queue
        :spam
      end

      attr_reader :user
      attr_reader :user_id

      def initialize(user_id)
        @user_id = user_id.to_i
        @user = User.find_by_id(user_id)
      end

      def perform
        # No such User, so let's bail
        return if user.nil?

        Failbot.push(
          job: self.class.name,
          user: user.login,
          user_id: user_id,
        )

        # No need to check if they're already flagged or can't be flagged
        return if user.spammy || !user.can_be_flagged?

        if (reason = GitHub::SpamChecker.test_user_associates(user))
          GitHub::SpamChecker.notify("User '#{user.login}' (#{user.id} - #{user.email}) is guilty by association: #{reason}")
          user.safer_mark_as_spammy(reason: reason)
        end
      end

      def self.perform(*args)
        job = new(*args)

        begin
          original_component = GitHub.component
          GitHub.component = :resque_user_associates_spam_check

          job.perform
        ensure
          GitHub.component = original_component
        end

        job
      end
    end
  end
end
