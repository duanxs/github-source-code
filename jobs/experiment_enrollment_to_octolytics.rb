# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ExperimentEnrollmentToOctolytics < Job
      def self.active_job_class
        ::ExperimentEnrollmentToOctolyticsJob
      end

      areas_of_responsibility :analytics

      extend GitHub::HashLock

      schedule interval: 5.minutes

      def self.queue
        :analytics
      end

      # Don't schedule this job under GHE.
      def self.enabled?
        !GitHub.enterprise?
      end

      # This job should always be serial, regardless of arguments
      def self.lock(*args)
        "key"
      end

      # This job is only meant for github.com.
      def self.before_enqueue_with_enterprise_check(*)
        GitHub.octolytics_enabled? && GitHub.user_experiments_enabled?
      end

      def self.perform(octolytics: GitHub.analytics, iterator: default_iterator)
        return unless GitHub.octolytics_enabled? && GitHub.user_experiments_enabled?

        GitHub.dogstats.time "experiment_enrollment_to_octolytics.perform" do
          new(octolytics, iterator).perform
        end
      end

      def self.default_iterator
        UserExperimentEnrollment.select(
          :id,
          :user_experiment_id,
          :subject_id,
          :subject_type,
          :subgroup,
          :created_at
        ).where("subgroup IS NOT NULL")
      end

      private_class_method :default_iterator

      def initialize(octolytics, iterator)
        @octolytics = octolytics
        @iterator   = iterator

        @experiments = {}
      end

      def perform
        begin
          ActiveRecord::Base.connected_to(role: :reading) do
            # Transform attributes from the model (which has some join
            # columns from the user table) into the dimensions of an
            # Octolytics event. The event has empty measures and context,
            # and the attached timestamp is of the user's enrollment time
            # in the experiment.
            first_unsubmitted_id = last_sumbitted_id.to_i + 1
            @iterator.find_in_batches(start: first_unsubmitted_id, batch_size: 200) do |models|
              batch = models.map { |enrollment|
                {
                  event_type: "user_experiment_enrollment",
                  measures: {},
                  context: {},
                  dimensions: {
                    subject_id:   subject_id(enrollment),
                    subject_type: enrollment.subject_type,
                    arm:          enrollment.subgroup,
                    experiment:   experiment_name(enrollment.user_experiment_id),
                  },
                  timestamp: enrollment.created_at.to_i,
                }
              }

              submit(batch) unless batch.empty?
              update_last_submitted_id(models.last.id)
            end
          end
        end
      end

      KV_KEY = "octolytics_experiment_enrollment:ModelIterator:UserExperimentEnrollment:current"
      def update_last_submitted_id(id)
        ActiveRecord::Base.connected_to(role: :writing) do
          GitHub.kv.set(KV_KEY, id.to_s, expires: 90.days.from_now)
        end
      end

      def last_sumbitted_id
        ActiveRecord::Base.connected_to(role: :writing) do
          value = GitHub.kv.get(KV_KEY).value!
          value ? value.to_i : 0
        end
      end

      # Submit the given set of events to the collector.
      def submit(batch)
        GitHub.dogstats.time "user_experiment_enrollment.octolytics.submit" do
          @octolytics.record(batch)
        end
      end

      # A small cache of experiment id to name, looked up lazily from the db.
      def experiment_name(user_experiment_id)
        @experiments[user_experiment_id] ||= UserExperiment.select("slug").find(user_experiment_id).slug
      end

      # Private: Extract a subject ID from an enrollment.
      # Site visitors have an alternative identifier that is recognized by
      # Octolytics.
      #
      # Return an Integer to Octolytics ID string.
      def subject_id(enrollment)
        if enrollment.subject_is_a?(Analytics::Visitor)
          enrollment.subject.unversioned_octolytics_id
        else
          enrollment.subject_id
        end
      end
    end
  end
end
