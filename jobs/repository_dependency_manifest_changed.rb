# frozen_string_literal: true

# Does three things:
#   1. Pushes stats about manifest file changes.
#   2. Updates the persisted manifest files for the repository.
#   3. Updates the dependency graph API with changed manifests.
module GitHub
  module Jobs
    class RepositoryDependencyManifestChanged < Job
      def self.active_job_class
        ::RepositoryDependencyManifestChangedJob
      end

      areas_of_responsibility :dependabot

      def self.queue
        :repository_dependencies
      end

      def self.perform_with_retry(push_id, options = {})
        options = options.with_indifferent_access
        push = Push.find_by_id(push_id)
        return unless push&.repository.present?
        return unless push&.on_default_branch?

        repository = push.repository
        return unless repository&.dependency_graph_enabled?

        changed_files = push.changed_files(decompose_renames: true)
        return unless changed_files&.any?

        modified_files = {}
        removed_files = []

        changed_files.each do |file|
          next unless repository.manifest_detector.recognized_path?(path: file.path)

          if file.deletion?
            removed_files << file.path
          else
            manifest_descriptor = repository.dependency_manifest_descriptor_for(file.path, file.oid)
            if manifest_descriptor.nil?
              removed_files << file.path
            else
              next if modified_files.length >= repository.max_manifest_files
              modified_files[file.path] = manifest_descriptor.serialize
            end
          end
        end

        return unless modified_files.any? || removed_files.any?

        DependencyManifestFile.record_repository_manifest_changed_stats(
          repository: repository,
          paths: (removed_files | modified_files.keys),
        )

        repository.update_dependency_manifests(
          modified: modified_files,
          removed: removed_files,
        )

        manifest_blobs = repository.dependency_manifest_blobs(at_paths: modified_files.keys)
        manifest_blobs ||= []

        removed_blobs = removed_files.map do |path|
          DependencyManifestFile::ManifestBlob.new(
            path: path,
            content: "",
            oid: GitHub::NULL_OID,
            repository_id: repository.id,
          )
        end

        update_dependency_manifests(repository, removed_blobs, manifest_blobs, push, options: options)

        Dependabot.dependency_graph_initialized(repository: repository)
      end

      def self.update_dependency_manifests(repository, removed_blobs, manifest_blobs, push, options: {})
        if GitHub.enterprise? || Rails.env.development?
          update_dependency_manifests_mutation(push: push,
            repository: repository,
            removed_files: removed_blobs,
            manifest_blobs: manifest_blobs,
            options: options)
        else
          enqueue_hydro_events(push: push,
            repository: repository,
            removed_files: removed_blobs,
            manifest_blobs: manifest_blobs)
        end
      end

      def self.enqueue_hydro_events(push: , repository: , removed_files: , manifest_blobs:)
        manifest_blobs.each do |blob|
          next if blob.content.empty?
          GlobalInstrumenter.instrument "update_manifest.repository", {
            repository_id: repository.id,
            owner_id: repository.owner_id,
            repository_private: repository.private?,
            repository_fork: repository.fork?,
            repository_nwo: repository.name_with_owner,
            repository_stargazer_count: repository.stargazer_count,
            manifest_file: {
              filename: blob.filename,
              path: blob.path,
              content: blob.content,
              git_ref: push.after,
              pushed_at: push.created_at.to_i,
            },
          }
        end

        # Push messages for each deleted manifest file.
        removed_files.each do |blob|
          GlobalInstrumenter.instrument "delete_manifest.repository", {
            repository_id: repository.id,
            repository_private: repository.private?,
            repository_fork: repository.fork?,
            manifest_file: {
              filename: blob.filename,
              path: blob.path,
              git_ref: push.after,
              pushed_at: push.created_at.to_i,
            },
          }
        end
      end

      def self.update_dependency_manifests_mutation(push: , repository: , removed_files: , manifest_blobs:, options:)
        # If all of our removed_files are a ManifestBlob, we want to merge them into the manifest_blobs Array.
        # This is to preserve behavior that was previously implemented before our method was called, however given our
        # divergent code paths we now need to implement it here.
        #
        # NOTE: `&.all?` works here due to the safe navigation operator, we'll get nil if removed_files isn't an Array.
        if removed_files&.all? { |i| i.is_a?(DependencyManifestFile::ManifestBlob) }
          manifest_blobs += removed_files
        end

        manifest_blobs.each do |blob|
          mutation = DependencyGraph::UpdateManifestsMutation.new(
            input: {
              repositoryId: repository.id,
              ownerId: repository.owner_id,
              repositoryPrivate: repository.private?,
              repositoryNwo: repository.name_with_owner,
              repositoryStargazerCount: repository.stargazer_count,
              manifestFiles: [
                {
                  filename: blob.filename,
                  path: blob.path,
                  encoded: blob.encoded,
                  gitRef: push.after,
                  pushedAt: push.created_at.iso8601,
                },
              ],
            },
          )
          result = mutation.execute
          failed = false
          result.value do
            Failbot.report(result.error, app: "github-dependency-graph", repo_id: repository.id, push_id: push.id)
            failed = true
          end
          if failed
            LegacyApplicationJob.retry_later(self.active_job_class, [push.id, options], {retries: options["retries"]})
            break
          end
        end
      end
    end
  end
end
