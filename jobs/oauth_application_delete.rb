# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class OauthApplicationDelete < Job
      def self.active_job_class
        ::OauthApplicationDeleteJob
      end

      areas_of_responsibility :platform

      def self.queue
        :oauth_application_delete
      end

      # Public: Delete an OauthApplication.
      #
      # oauth_application_id - The ID for the OauthApplication.
      def self.perform(oauth_application_id)
        app = OauthApplication.find_by_id(oauth_application_id)
        return if app.nil?

        GitHub.dogstats.time("oauth_application", tags: ["action:delete", "via:job"]) { app.destroy }
      end

      # All OauthApplicationDelete jobs are locked by their
      # oauth_application_id. No attempt will be made to enqueue a job that's
      # already running, and the lock is checked before running the job.
      extend GitHub::HashLock
      def self.lock(oauth_application_id)
        oauth_application_id
      end
    end
  end
end
