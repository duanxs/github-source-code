# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Jobs
  class SynchronizePlanSubscription < AbstractBraintreeJob
    def self.active_job_class
      ::SynchronizePlanSubscriptionJob
    end

    areas_of_responsibility :gitcoin

    def self.queue
      :synchronize_plan_subscription
    end

    def self.perform(*args); new(*args).perform; end

    def perform
      perform_with_retry do
        plan_subscription = ::Billing::PlanSubscription.find_by_user_id(user_id)
        plan_subscription&.synchronize_with_lock(retries: retries)
      end
    end

    private

    attr_reader :retry_args, :user_id

    def initialize(options)
      options = options.with_indifferent_access
      @user_id = options["user_id"]
      @retry_args = [options]
      @retry_options = { retries: options["retries"] }

      if GitHub.zuorest_client
        GitHub.zuorest_client.timeout = 60
        GitHub.zuorest_client.open_timeout = 60
      end
    end

    def retries
      @retry_options[:retries].to_i
    end
  end
end
