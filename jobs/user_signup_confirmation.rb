# frozen_string_literal: true

module GitHub
  module Jobs
    # Sends a one-time signup confirmation email to users that
    # have elected a transactional email preference.
    #
    # This job is queued after a user verifies their email address.
    class UserSignupConfirmation < Job
      def self.active_job_class
        ::UserSignupConfirmationJob
      end

      areas_of_responsibility :user_growth

      def self.queue
        :user_signup
      end

      def self.perform(email_id)
        job = new(email_id)
        job.perform
        job
      end

      def initialize(email_id)
        @email_id = email_id
        @email    = UserEmail.find_by_id(email_id)
        @user     = @email.try(:user)

        Failbot.push(
          job: self.class.name,
          user: @user.try(:login),
          email_id: @email_id,
        )
      end

      def perform
        return unless @email && @user

        if should_receive_welcome_email?
          AccountMailer.welcome(@email).deliver_now
          Onboarding.for(@user).welcomed_via_email!
        end
      end

      private

      # Private: Should the user receive the welcome email?
      # Returns a Boolean.
      def should_receive_welcome_email?
        return false if NewsletterPreference.marketing?(user: @user)
        recent_signup? && !already_welcomed?
      end

      # Private: Whether or not a user already received the welcome email or series.
      # Returns a Boolean.
      def already_welcomed?
        onboarding = Onboarding.for(@user)
        onboarding.welcomed_via_email? || onboarding.enrolled_in_welcome_series?
      end

      # Private: Did the user sign up recently?
      # Returns a Boolean.
      def recent_signup?
        @user.created_at > 30.days.ago
      end
    end
  end
end
