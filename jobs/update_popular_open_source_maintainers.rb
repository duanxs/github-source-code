# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class UpdatePopularOpenSourceMaintainers < Job
      def self.active_job_class
        ::UpdatePopularOpenSourceMaintainersJob
      end

      areas_of_responsibility :opensource

      schedule interval: 24.hours

      def self.queue
        :open_source_maintainer_contributions
      end

      # Don't schedule this job on GitHub Enterprise where it's not useful.
      def self.enabled?
        !GitHub.enterprise?
      end

      if Rails.env.development?
        PUSHED_AT_DAYS = 365*10
        STAR_COUNT = 1
        MINIMUM_CONTRIBUTION_COUNT = 1
        OLDEST_CONTRIBUTION = 10.years
      else
        PUSHED_AT_DAYS = 60
        STAR_COUNT = 1000
        MINIMUM_CONTRIBUTION_COUNT = 12
        OLDEST_CONTRIBUTION = 6.months
      end

      CACHE_KEY = "opensource:maintainer_contributions".freeze

      def self.maintainer_contributions
        maintainer_contributions = GitHub.kv.get(CACHE_KEY).value!
        return unless maintainer_contributions
        return unless maintainer_contributions.length > 2
        JSON.parse(maintainer_contributions)
      end

      def self.maintainer_contributions_and_criteria
        maintainers_members = []
        if maintainers_repo = Repository.with_name_with_owner("maintainers/discussions")
          maintainers_members = maintainers_repo.actor_ids(type: User, min_action: :read)
        end

        {
          pushed_at_days: PUSHED_AT_DAYS,
          star_count: STAR_COUNT,
          minimum_contribution_count: MINIMUM_CONTRIBUTION_COUNT,
          oldest_contribution_date: OLDEST_CONTRIBUTION.ago.to_date,
          maintainer_contributions: maintainer_contributions,
          maintainers_members: maintainers_members,
        }
      end

      # Public: query the most active maintainers on GitHub and save into
      # transient Redis.
      #
      # Returns nothing.
      def self.perform
        GitHub.dogstats.time("job.update_popular_open_source_maintainers.time") do
          ActiveRecord::Base.connected_to(role: :reading) do
            popular_repos = Repository.where(public: true)
                                      .where("primary_language_name_id IS NOT NULL")
                                      .joins(:repository_license)
                                      .where("pushed_at > #{PUSHED_AT_DAYS.days.ago}")
                                      .where("watcher_count >= #{STAR_COUNT}")
                                      .order(:watcher_count)
                                      .select("repositories.id")

            popular_repos_ids_committer_ids = {}
            popular_repos.each do |repo|
              maintainer_ids = repo.actor_ids(type: User, min_action: :write)
              next if maintainer_ids.empty?

              popular_repos_ids_committer_ids[repo.id] = maintainer_ids
            end

            contribution_date_range = (OLDEST_CONTRIBUTION.ago.to_date)..Date.today
            maintainer_contributions = {}

            popular_repos_ids_committer_ids.each do |repo_id, user_ids|
              user_ids.each do |user_id|
                contribution_count =
                  CommitContribution.for_repository(repo_id)
                                    .for_user(user_id)
                                    .where(committed_date: contribution_date_range)
                                    .pluck(:commit_count)
                                    .sum
                contribution_count +=
                  Issue.for_repository(repo_id)
                       .for_user(user_id)
                       .where(created_at: contribution_date_range)
                       .count

                next if contribution_count < MINIMUM_CONTRIBUTION_COUNT

                user_hash = maintainer_contributions[user_id]
                user_hash ||= {
                  login: User.where(id: user_id).pluck(:login).first,
                  contribution_count: 0,
                }
                user_hash[:contribution_count] += contribution_count
                maintainer_contributions[user_id] = user_hash
              end
            end

            maintainer_contributions =
              maintainer_contributions.sort_by do |_, user_hash|
                -user_hash[:contribution_count]
              end

            GitHub.kv.set(CACHE_KEY, maintainer_contributions.to_json)
          end
        end
      end
    end
  end
end
