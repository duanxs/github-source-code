# rubocop:disable Style/FrozenStringLiteralComment

require "github/timer"
require "github/pages/management/delegate"
require "github/pages/management/evacuate"

module GitHub
  module Jobs
    # Timer that schedules evacuation jobs for DPages sites.
    class DpagesEvacuationScheduler < Job
      def self.active_job_class
        ::DpagesEvacuationSchedulerJob
      end

      areas_of_responsibility :pages

      # Run for at most two minutes so the user doesn't need to intervene to stop
      # the job manually.
      def self.max_execution_time
        2.minutes
      end
      extend GitHub::TimeoutAndMeasure

      schedule interval: (max_execution_time + 1.minute)

      # Evacuations are a concern of .com and Enterprise.
      def self.enabled?
        true
      end

      # Run every interval, schedules jobs_per_interval maintenance jobs to run.
      def self.perform(*args)
        sites_enqueued = 0

        timeout_and_measure(max_execution_time.to_i, "pages.dpages_evacuations.scheduler") do
          ActiveRecord::Base.connected_to(role: :reading) do
            evacuating_hosts = GitHub::Pages::Management::Evacuate.evacuating_hosts

            evacuating_hosts.each do |host|
              command = GitHub::Pages::Management::Evacuate.new(
                host: host,
                delegate: GitHub::Pages::Management::Delegate.new(logger: GitHub::Logger),
              )
              command.sites_to_evacuate(limit: GitHub.dpages_evacuations_scheduler_batch_size).each do |(page_id, page_deployment_id)|
                break if sites_enqueued >= GitHub.dpages_evacuations_scheduler_batch_size

                DpagesEvacuateSiteJob.perform_later(page_id, page_deployment_id, host)
                sites_enqueued += 1
              end
            end
          end
        end
      end

      # The queue the scheduler runs on. The actual evacuation jobs don't run
      # on this queue, they're enqueued on the :dpages_evacuations queue.
      def self.queue
        :dpages_evacuations_scheduler
      end

      # Only one of these jobs may run at once.
      extend GitHub::HashLock
    end
  end
end
