# frozen_string_literal: true

module GitHub
  module Jobs
    class DestroyMergeRefs < Job
      def self.active_job_class
        ::DestroyMergeRefsJob
      end

      areas_of_responsibility :code_collab, :pull_requests

      retry_on_error GitHub::DGit::ThreepcFailedToLock

      def self.perform_with_retry(pull_request_id, options = {})
        pull = PullRequest.find(pull_request_id)

        Failbot.push(repo_id: pull.repository_id, pull_request_id: pull.id)

        pull.destroy_merge_refs
      end

      def self.queue
        :maintain_tracking_ref
      end
    end
  end
end
