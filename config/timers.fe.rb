# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/redis"
require File.expand_path("../basic", __FILE__)
require "github"
require "github/config/stats"
require "github/config/resque"
require "github/config/active_job"
require "resque/buffered_resque"
require "timed_resque"
require "timed_resque/buffered_timed_resque"
require "job_scheduler"
require "active_support/core_ext/numeric/time"
require "github/timeout_and_measure"
require "github/hash_lock"
require "github/restraint"
require "global_instrumenter"

require "github/config/active_job"
require "active_job/queue_adapters/buffered_resque_adapter"

require "#{Rails.root}/app/jobs/perform_ofac_downgrades_job"
require "#{Rails.root}/app/jobs/billing/azure/monthly_extract_job"
require "#{Rails.root}/app/jobs/billing/azure/quarterly_extract_job"
require "#{Rails.root}/app/jobs/billing/azure/monthly_sales_extract_job"
require "#{Rails.root}/app/jobs/billing/munich_sync_credit_balances_job"
require "#{Rails.root}/app/jobs/billing/schedule_zuora_usage_file_uploads_job"
require "#{Rails.root}/app/jobs/billing/shared_storage/start_aggregation_job"
require "#{Rails.root}/app/jobs/scheduled_two_factor_recovery_request_notifier_job"
require "#{Rails.root}/app/jobs/two_factor_recovery_request_cleanup_job.rb"
require "#{Rails.root}/app/jobs/trade_controls/perform_organization_enforcement_job.rb"
require "#{Rails.root}/app/jobs/trade_controls/perform_past_organization_enforcement_job.rb"
require "#{Rails.root}/app/jobs/billing/check_payouts_ledger_discrepancies_job"
require "#{Rails.root}/app/jobs/billing/check_unprocessed_webhooks_job"
require "#{Rails.root}/app/jobs/billing/check_unsubmitted_metered_line_items_job"
require "#{Rails.root}/app/jobs/billing/synchronize_apple_iap_subscription_job"
require "#{Rails.root}/app/jobs/billing/check_business_organization_transition_failures_job"

# configure ActiveJob
ActiveJob::Base.queue_adapter = :buffered_resque

# configure daemon
daemon = TimerDaemon.instance
daemon.err = GitHub::Logger.method(:log)
daemon.redis = GitHub.resque_redis
scheduler = JobScheduler.new(daemon)

ScheduledFeTimerError = Class.new(StandardError)

# report exceptions to Haystack
daemon.error do |boom, timer|
  Failbot.report(ScheduledFeTimerError.new("timer #{timer.name} failed"), timer: timer.name)
end

# test timer
daemon.schedule "test", 30.seconds do
  daemon.log({ test: "hello from #$$" })
end

daemon.schedule "dgit-disk-stats-timer", GitHub.dgit_disk_stats_interval do
  SpokesDiskStatsCacheFillJob.set(enqueue_to_hydro: false).perform_later(Time.now.to_i)
end

[
  # Billing
  PerformPendingPlanChangesJob,
  BillingCouponReminderJob,
  BillingDeleteObsoleteCouponExpirationNoticesJob,
  BillingExpiringCardRemindersJob,
  BillingUpdateExchangeRatesJob,
  BillingUpdateTransactionsWithPendingStatusJob,
  BillingRunStartJob,
  BraintreeTransactionsCsvJob,
  EnsureRecentlyUpdatedPayoutsLedgersInBalanceJob,
  ExpireCouponsJob,
  BillingFreeTrialReminderJob,
  PerformOFACDowngradesJob,
  RetrieveFailedZuoraWebhooksJob,
  YearlyCycleNoticeStartJob,
  ZuoraUsageUploadStatusJob,
  Billing::SharedStorage::StartAggregationJob,
  Billing::Azure::MonthlyExtractJob,
  Billing::Azure::QuarterlyExtractJob,
  Billing::Azure::MonthlySalesExtractJob,
  TradeControls::PerformOrganizationEnforcementJob,
  TradeControls::PerformPastOrganizationEnforcementJob,
  Billing::CheckPayoutsLedgerDiscrepanciesJob,
  Billing::CheckUnprocessedWebhooksJob,
  Billing::CheckUnsubmittedMeteredLineItemsJob,
  Billing::MunichSyncCreditBalancesJob,
  Billing::ScheduleZuoraUsageFileUploadsJob,
  Billing::SynchronizeAppleIapSubscriptionJob,
  Billing::CheckBusinessOrganizationTransitionFailuresJob,

  # Pages
  DeleteDependentPagesReplicasSchedulerJob,
  DpagesEvacuationSchedulerJob,
  DpagesMaintenanceSchedulerJob,
  PageCertificateRenewJob,
  PageCertificateRemoveExpiredJob,

  # Explore
  FeatureShowcasesJob,

  # Cached Topics stats
  CalculateTopicAppliedCountsJob,

  NewsletterDeliveryJob,
  MailchimpDataQualitySyncJob,

  OauthAppPolicyUsageStatsJob,

  ExperimentEnrollmentToOctolyticsJob,

  SpokesMaintenanceSchedulerJob,
  NetworkMaintenanceSchedulerJob,
  SpokesGistMaintenanceSchedulerJob,
  SpokesEvacuateNetworksJob,
  SpokesEvacuateNetworksNonvotingJob,
  SpokesEvacuateGistsJob,
  SpokesEvacuateGistsNonvotingJob,
  QueueMediaTransitionJobsJob,

  ClearSoftBouncesJob,

  WikiMaintenanceSchedulerJob,

  GistMaintenanceSchedulerJob,

  NewsiesAutoSubscribeJob,  # Iterates through all users in `notification_subscriptions`
  RemoveExpiredOauthJob,
  RemoveStaleOauthJob,
  RemoveStalePublicKeysJob,
  RemoveStaleAuthenticationRecordsJob,
  RemoveStaleAuthenticatedDevicesJob,

  OtpSmsTimingCleanupJob,

  # grab nexmo sms provider account balance
  NexmoBalanceStatsJob,

  SyncAssetUsageJob,
  SyncRegistryUsageJob,

  # deleting stuff
  PurgeStaleUploadManifestFileBlobsJob,
  MigrationEnqueueDestroyFileJobsJob,
  PurgeArchivedAssetsJob,
  PurgeRestorablesJob,
  RemoveStaleRepositoryContributionGraphStatusesJob,
  RepositoryPurgeArchivedJob,
  GistPurgeArchivedJob,
  DestroyDeletedPackageVersionsJob,
  PurgeFlaggedUploadsJob,

  # archive
  ScheduledArchiveDanglingForksJob,

  # stats
  RetryJobsJob,

  # spam
  UpdateUserHiddenMismatchesJob,

  # Update total counts for users, repos, issues for site marketing site
  UpdateSiteStatsJob,

  # prodsec
  DeleteUnconfirmedAccountRecoveryTokensJob,
  QintelImportNewFiles,
  ScheduledTwoFactorRecoveryRequestNotifierJob,
  TwoFactorRecoveryRequestCleanupJob,
  AuthenticatedAfterTwoFactorRecoveryRequestJob,
  RollEarthsmokeKeys,

  # Git Backups
  GitbackupsSchedulerJob,
  GitbackupsSweeperJob,
  GitbackupsEnsureFreshKeyJob,

  # Spokes Cold Storage
  SpokesColdStorageSweeperJob,

  # open source
  UpdatePopularOpenSourceMaintainersJob,

  # Marketplace
  UpdateMarketplaceInsightsJob,
  MarketplaceRetargetingJob,
  MarketplaceTrendingAppsJob,
  MarketplaceBlogSyncJob,
  SyncMarketplaceSearchIndexJob,

  MirrorSchedulerJob,
  DeleteRepositoryRetryJob,

  VulnerabilitySyncWithDotcom,

  # token scanning
  TokenScanningSchedulerJob,

  # code scanning
  StaleCodeScanningCheckRunsScheduledJob,

  ExampleScheduledJob,

  # GitHub Apps
  CategorizeIntegrationsJob,
  DeleteOldIntegrationManifestJob,

  EnqueueAqueductTestJob,

  ScienceEventCleanupJob,

  # ce-extensibility
  DeleteOutdatedInteractiveComponentsJob,
  LegacyStaleChecksScheduledJob,
  StaleCheckJob,

  # Sponsors
  RefreshSponsorsMembershipsCriteriaJob,
  EnableSponsorsPayoutsJob,
  UpdateSponsorsActivityMetricsJob,
  AutoAcceptSponsorsApplicationsJob,
  PopulateSponsorsFraudReviewsJob,

  EnqueueUpcomingRemindersJob,

  # Invalidate expired invitations
  InvalidateExpiredInvitesJob,
  ExpireBusinessAdminInvitationsJob,

  # Codespaces
  CodespacesFetchBillingStorageAccountNamesJob

].each do |job|
  scheduler.schedule job, enqueue_to_hydro: false
end
