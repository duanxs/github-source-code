# frozen_string_literal: true

class Trilogy
  class EstablishConnectionSubscriber
    def initialize(notifier: ActiveSupport::Notifications, stats: GitHub.dogstats)
      @notifier = notifier
      @stats = stats
    end

    def call(name, started_at, finished_at, id, payload = {})
      stats.timing "rpc.mysql.configure_connection.time",
                   TimeSpan.new(started_at, finished_at).duration,
                   tags: ["rpc_host:#{payload[:connected_host]}"]
    end

    def subscribe
      notifier.subscribe TrilogyAdapter::Events::ESTABLISH_CONNECTION, self
    end

    private

    attr_reader :notifier, :stats
  end
end

Trilogy::EstablishConnectionSubscriber.new.subscribe
