# frozen_string_literal: true

module Trilogy::WriteAfterReadToWriteSubscriber
  class << self
    def call(name, start, finish, id, payload = {})
      Failbot.push(write_sql: payload[:sql], write_backtrace: caller)
    end

    def subscribe
      ActiveSupport::Notifications.subscribe(
        TrilogyAdapter::Events::WRITE_AFTER_READ_TO_WRITE,
        self,
      )
    end
  end
end

Trilogy::WriteAfterReadToWriteSubscriber.subscribe
