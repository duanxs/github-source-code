# frozen_string_literal: true

module Trilogy::QueryDeadlockSubscriber
  class << self
    def call(name, start, finish, id, payload = {})
      GitHub.dogstats.increment("rpc.mysql.retry.deadlock.count", tags: [
        "exception:#{payload[:error].class.name}",
      ])
    end

    def subscribe
      ActiveSupport::Notifications.subscribe(TrilogyAdapter::Events::QUERY_DEADLOCK, self)
    end
  end
end

Trilogy::QueryDeadlockSubscriber.subscribe
