# frozen_string_literal: true

# Configure authentication rate limits.
if GitHub.lockouts_enabled? && AuthenticationLimit.instances.empty?
  # A few people mistyping passwords from the same IP.
  AuthenticationLimit.new name: :short_ip,
                          max_tries: 10,
                          ttl: 5.minutes,
                          data_key: :ip

  # Probably a malicious IP trying to brute-force multiple accounts.
  AuthenticationLimit.new name: :long_ip,
                          max_tries: 200,
                          ttl: 12.hours,
                          ban_ttl: 1.week,
                          data_key: :ip

  if GitHub.web_ip_lockouts_enabled?
    # A few people mistyping passwords from the same IP (web only).
    AuthenticationLimit.new name: :short_web_ip,
                            max_tries: 10,
                            ttl: 5.minutes,
                            data_key: :web_ip

    # Probably a malicious IP trying to brute-force multiple accounts (web only).
    AuthenticationLimit.new name: :long_web_ip,
                            max_tries: 200,
                            ttl: 12.hours,
                            ban_ttl: 1.week,
                            data_key: :web_ip
  end

  # Password mistyped or misconfigured script.
  AuthenticationLimit.new name: :short_login,
                          max_tries: 5,
                          ttl: 5.minutes,
                          ban_ttl: 10.minutes,
                          data_key: :login

  # Probably brute force attack. Possibly misconfigured script.
  AuthenticationLimit.new name: :long_login,
                          max_tries: 100,
                          ttl: 12.hours,
                          ban_ttl: 1.week,
                          data_key: :login

  # The user mistypes a few OTPs.
  AuthenticationLimit.new name: :short_2fa_login,
                          max_tries: 5,
                          ttl: 10.minutes,
                          data_key: :two_factor_login

  # Semi-permanant. The password has been compromised.
  AuthenticationLimit.new name: :long_2fa_login,
                          max_tries: 200,
                          ttl: 1.month,
                          ban_ttl: 12.months,
                          data_key: :two_factor_login

  AuthenticationLimit.new name: :short_verified_device_challenge,
                          max_tries: 5,
                          ttl: 10.minutes,
                          data_key: :verified_device_login

  AuthenticationLimit.new name: :long_verified_device_challenge,
                          max_tries: 200,
                          ttl: 1.month,
                          ban_ttl: 12.months,
                          data_key: :verified_device_login

AuthenticationLimit.new name: :short_authenticated_device_creation,
                          max_tries: 10,
                          ttl: 60.minutes,
                          data_key: :device_creation_login

AuthenticationLimit.new name: :long_authenticated_device_creation,
                          max_tries: 50,
                          ttl: 1.day,
                          ban_ttl: 1.week,
                          data_key: :device_creation_login

AuthenticationLimit.new name: :short_known_device_use,
                          max_tries: 10,
                          ttl: 60.minutes,
                          data_key: :device_use_login

AuthenticationLimit.new name: :long_known_device_use,
                          max_tries: 50,
                          ttl: 1.day,
                          ban_ttl: 1.week,
                          data_key: :device_use_login
end
