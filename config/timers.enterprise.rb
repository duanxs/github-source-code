# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/redis"
require File.expand_path("../basic", __FILE__)
require "github"
require "github/config/stats"
require "github/config/resque"
require "github/config/active_job"
require "resque/buffered_resque"
require "timed_resque"
require "timed_resque/buffered_timed_resque"
require "job_scheduler"
require "active_support/core_ext/numeric/time"
require "github/timeout_and_measure"
require "github/hash_lock"
require "github/config/active_record"
require "github/pages/allocator"
require "github/sql"
require "github/dgit"
require "raindrops"
require "global_instrumenter"

require "github/config/active_job"
require "active_job/queue_adapters/buffered_resque_adapter"

# configure ActiveJob
ActiveJob::Base.queue_adapter = :buffered_resque

# configure daemon
daemon = TimerDaemon.instance
daemon.redis = GitHub.resque_redis
scheduler = JobScheduler.new(daemon)

# report exceptions to Haystack
daemon.error do |boom, timer|
  Failbot.report(boom, timer: timer.name)
end

# test timer
daemon.schedule "test", 30.seconds do
  daemon.log({ test: "hello from #$$" })
end

if GitHub.cluster_web_server?
  daemon.schedule "dgit-liveness-check", 10.seconds, scope: :host do
    GitHub::DGit.liveness_check
  end
end

if GitHub.cluster_pages_server? || GitHub.single_instance?
  daemon.schedule "pages-partition-usage", 1.minute, scope: :host do
    GitHub::Pages::Allocator.update_disk_usage!(hostname: GitHub.local_pages_host_name)
  end

  daemon.schedule "pages-garbage-collect", 1.hour, scope: :host do
    GitHub::Pages::GarbageCollector.run!
  end
end

[
  CalculateTopicAppliedCountsJob,
  FeatureShowcasesJob,
  ClearSoftBouncesJob,
  SpokesMaintenanceSchedulerJob,
  SpokesGistMaintenanceSchedulerJob,
  SpokesEvacuateNetworksJob,
  SpokesEvacuateNetworksNonvotingJob,
  SpokesEvacuateGistsJob,
  SpokesEvacuateGistsNonvotingJob,
  ExpireCouponsJob,

  GistMaintenanceSchedulerJob,

  MigrationEnqueueDestroyFileJobsJob,
  NetworkMaintenanceSchedulerJob,
  NewsiesAutoSubscribeJob,
  OauthAppPolicyUsageStatsJob,
  OtpSmsTimingCleanupJob,
  QueueMediaTransitionJobsJob,
  RepositoryPurgeArchivedJob,
  RetryJobsJob,
  RemoveExpiredOauthJob,
  SyncAssetUsageJob,

  # Storage archiving and purging
  PurgeRestorablesJob,
  PurgeStaleUploadManifestFileBlobsJob,
  PurgeStaleRenderBlobsJob,
  DeletePurgedStorageBlobsJob,
  UpdateStoragePartitionsJob,
  StorageClusterMaintenanceSchedulerJob,

  WikiMaintenanceSchedulerJob,
  DeleteRepositoryRetryJob,

  # Gitbackups maintenance
  GitbackupsWalJob,

  # LDAP sync
  LdapTeamSyncJob,
  LdapUserSyncJob,

  # SAML
  SamlSessionRevokeJob,

  # GitHubConnect
  GitHubConnectPushNewContributionsJob,
  UpdateConnectInstallationInfo,
  VulnerabilitySyncWithDotcom,
  UploadEnterpriseServerUserAccountsJob,

  # Pages
  DpagesEvacuationSchedulerJob,
  DpagesMaintenanceSchedulerJob,
  DeleteDependentPagesReplicasSchedulerJob,

  # code scanning
  StaleCodeScanningCheckRunsScheduledJob,

  DeleteOldIntegrationManifestJob,
  QueueCollectMetricsJob,

  # ce-extensibility
  DeleteOutdatedInteractiveComponentsJob,
  LegacyStaleChecksScheduledJob,
  StaleCheckJob,

  # Invalidate expired invitations
  InvalidateExpiredInvitesJob,
  ExpireBusinessAdminInvitationsJob,

  # token scanning
  TokenScanningSchedulerJob,
].each do |job|
  scheduler.schedule job
end
