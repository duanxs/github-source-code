# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl

  define_access :get_repo,
                :list_dependency_graphs,
                :list_events,
                :list_watchers,
                :list_stargazers,
                :list_languages,
                :list_tags,
                :list_forks,
                :list_contributors,
                :list_repo_issue_events,
                :get_dependency_graph,
                :get_license,
                :get_repo_stats,
                :git_poll do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :repo_metadata_reader
  end

  define_access :v4_get_repo do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :v4_private_repo_metadata_reader
  end

  define_access :v4_list_private_repos do |access|
    access.allow :v4_private_repo_lister
  end

  define_access :list_branches,
                :compare_commits,
                :get_blob,
                :get_commit,
                :get_diff,
                :get_push,
                :list_commits,
                :list_issue_templates,
                :list_issue_comment_templates,
                :list_refs,
                :list_repo_contact_links,
                :get_ref,
                :get_tag,
                :get_tree,
                :get_readme,
                :get_code_of_conduct,
                :get_funding,
                :get_archive_link,
                :get_temp_clone_token do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :repo_contents_reader
  end

  define_access :get_contents do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :repo_contents_reader
    access.allow :repo_file_reader
  end

  define_access :view_community_profile do |access|
    access.allow :everyone
  end

  define_access :report_content do |access|
    access.ensure_context :resource
    access.ensure_context :repo
    access.allow :repo_contents_reader
  end

  define_access :follow_repo_redirect do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :repo_redirect_follower
    access.allow :installed_integration
  end

  define_access :list_network_events do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
  end

  define_access :read_user_repo_subscription do |access|
    access.ensure_context :user, :resource
    access.allow :user_repository_watching_reader
  end

  define_access :write_user_repo_subscription do |access|
    access.ensure_context :user, :resource
    access.allow :user_repository_watching_writer
  end

  define_access :pull do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :repo_contents_reader
  end

  define_access :pull_storage do |access|
    access.ensure_context :resource
    access.allow :repo_contents_reader
  end

  define_access :create_fork do |access|
    access.ensure_context :user, :resource
    access.allow :repo_forker
  end

  define_access :get_star do |access|
    access.ensure_context :user, :resource
    access.allow :user_repository_starring_reader
  end

  define_access :star, :unstar do |access|
    access.ensure_context :user, :resource
    access.allow :user_repository_starring_writer
  end

  define_access :create_repo_for_org do |access|
    access.ensure_context :resource
    access.allow :org_repo_creator
  end

  define_access :create_private_repo_for_org do |access|
    access.ensure_context :resource
    access.allow :org_private_repo_creator
  end

  define_access :list_watched, :list_starred, :list_repos do |access|
    access.allow(:everyone) { |context| context[:user] }
  end

  define_access :list_associated_public_org_owned_repos do |access|
    access.allow :associated_public_org_owned_repo_lister
  end

  define_access :list_private_repos do |access|
    access.allow :private_repo_lister
  end

  define_access :list_all_repos do |access|
    access.allow :site_admin
  end

  define_access :list_private_stars, :list_private_watched_repos do |access|
    access.allow :private_repo_lister do |context|
      user, target = extract(context, :user, :resource)

      user && target && user.id == target.id
    end
  end

  define_access :read_vulnerability_alerts do |access|
    access.allow :repo_vulnerability_alert_reader
  end

  define_access :admin_vulnerability_alerts do |access|
    access.allow :repo_administration_writer
  end

  define_access :admin_secret_scanning do |access|
    access.allow :repo_administration_writer
  end

  define_access :admin_automated_security_fixes do |access|
    access.allow :repo_administration_writer
  end

  define_access :read_branch_protection do |access|
    access.allow :repo_administration_reader
  end

  define_access :update_branch_protection do |access|
    access.allow :repo_administration_writer
  end

  define_access :create_repo do |access|
    access.allow :repo_creator
  end

  define_access :create_private_repo do |access|
    access.allow :private_repo_creator
  end

  define_access :delete_repo do |access|
    access.ensure_context :resource
    access.allow :repo_deleter
  end

  define_access :list_repo_keys, :get_repo_key do |access|
    access.ensure_context :resource
    access.allow :repo_administration_reader
  end

  define_access :transfer_repo do |access|
    access.ensure_context :resource
    access.allow :repo_transferer
  end

  define_access :edit_repo do |access|
    access.ensure_context :resource
    access.allow :repo_administration_writer
  end

  define_access :manage_topics do |access|
    access.ensure_context :resource
    access.allow :topics_manager
  end

  define_access :list_repo_teams do |access|
    access.ensure_context :resource
    access.allow :repo_administration_reader
  end

  define_access :add_repo_key, :manage_repo_key do |access|
    access.allow :repo_administration_writer
  end

  define_access :add_collaborator do |access|
    access.ensure_context :resource, :collab
    access.allow :repo_administration_writer
  end

  define_access :remove_collaborator do |access|
    access.ensure_context :resource, :collab
    access.allow :repo_member_remover
  end

  define_access :list_collaborators do |access|
    access.ensure_context :resource
    access.allow :repo_resources_writer
    access.allow :installed_integration
  end

  define_access :see_user_permission do |access|
    access.ensure_context :resource
    access.allow :repo_resources_writer
    access.allow :installed_integration
  end

  define_access :list_repository_invitations do |access|
    access.ensure_context :resource
    access.allow :repo_invitations_reader
  end

  define_access :admin_repository_invitations do |access|
    access.ensure_context :resource
    access.allow :repo_invitations_writer
  end

  define_access :push do |access|
    access.ensure_context :resource
    access.allow :repo_contents_writer
  end

  define_access :create_repo_dispatch do |access|
    access.ensure_context :repo
    access.allow :repo_contents_writer
  end

  define_access :create_blob,
                :create_commit,
                :create_tag,
                :create_tree do |access|
    access.ensure_context :resource
    access.allow :repo_contents_writer
  end

  define_access :create_ref,
                :update_ref,
                :delete_ref do |access|
    access.ensure_context :resource
    access.allow :repo_contents_writer
    access.allow :repo_packages_writer
  end

  define_access :read_pages do |access|
    access.ensure_context :resource
    access.allow :repo_pages_reader
  end

  define_access :build_pages do |access|
    access.ensure_context :resource
    access.allow :repo_pages_writer
  end

  define_access :write_pages do |access|
    access.ensure_context :resource
    access.allow :repo_pages_writer
  end

  define_access :merge_branch do |access|
    access.ensure_context :resource
    access.allow :repo_contents_writer
  end

  define_access :update_file, :delete_file  do |access|
    access.ensure_context :resource
    access.allow :repo_file_writer
  end

  define_access :write_file do |access|
    access.ensure_context :resource
    access.allow :repo_file_writer
  end

  define_access :repo_access do |access|
    access.ensure_context :resource
    access.allow :repo_access_admin
  end

  define_access :repo_traffic do |access|
    access.ensure_context :resource

    access.allow(:repo_administration_reader) do |context|
      user = extract(context, :user)
      actor = user.try(:installation) || user

      actor && actor.can_have_granular_permissions?
    end

    access.allow(:repo_contents_writer) do |context|
      user = extract(context, :user)
      actor = user.try(:installation) || user

      actor && !actor.can_have_granular_permissions?
    end
  end

  define_access :read_repository_interaction_limits do |access|
    access.ensure_context :resource
    access.allow :repo_administration_reader
  end

  define_access :set_repository_interaction_limits do |access|
    access.ensure_context :resource

    access.allow :repo_interaction_limiter
    access.allow :site_admin
  end

  define_access :minimize_repo_comment do |access|
    access.allow :repo_comment_minimizer
  end

  define_access :notify_channel do |access|
    access.ensure_context :resource

    access.allow :presence_participant
  end

  define_access :get_status_check_rollup do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :check_run_reader
    access.allow :status_reader
  end

  define_access :upload_code_scanning_analysis_read do |access|
    access.ensure_context :resource
    access.allow :security_events_reader
  end

  define_access :read_code_scanning do |access|
    access.ensure_context :resource
    access.allow :security_events_reader
  end

  define_access :write_code_scanning do |access|
    access.ensure_context :resource
    access.allow :security_events_writer
  end

  define_access :create_check_run_for_container_scan do |access|
    access.ensure_context :resource
    access.allow :check_run_writer
    access.allow :repo_contents_writer
  end
end
