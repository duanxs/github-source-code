# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :write_codespace do |access|
    access.ensure_context :user, :resource
    access.allow :codespace_writer
  end

  define_access :read_codespaces do |access|
    access.ensure_context :user, :resource
    access.allow :codespace_reader
  end

  define_access :list_codespaces do |access|
    access.ensure_context :user, :resource
    access.allow :codespace_lister
  end
end
