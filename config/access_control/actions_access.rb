# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :resolve_actions do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :actions_reader
  end

  define_access :read_actions do |access|
    access.ensure_context :resource
    access.allow(:everyone) { |context| context[:resource].public? }
    access.allow :actions_reader
  end

  define_access :read_actions_downloads do |access|
    # We do not allow :everyone to protect from scanning logs/artifacts for tokens
    access.ensure_context :resource
    access.allow :actions_reader
  end

  define_access :write_actions do |access|
    access.ensure_context :resource
    access.allow :actions_writer
  end

  define_access :write_actions_secrets do |access|
    access.ensure_context :resource
    access.allow :actions_secrets_writer
  end

  define_access :read_actions_secrets do |access|
    access.ensure_context :resource
    access.allow :actions_secrets_reader
  end

  define_access :write_org_actions_secrets do |access|
    access.ensure_context :resource
    access.allow :org_actions_secrets_writer
  end

  define_access :read_org_actions_secrets do |access|
    access.ensure_context :resource
    access.allow :org_actions_secrets_reader
  end

  define_access :write_org_self_hosted_runners do |access|
    access.ensure_context :resource
    access.allow :org_self_hosted_runners_writer
  end

  define_access :read_org_self_hosted_runners do |access|
    access.ensure_context :resource
    access.allow :org_self_hosted_runners_reader
  end

  define_access :write_enterprise_self_hosted_runners do |access|
    access.ensure_context :resource
    access.allow :business_self_hosted_runners_writer
  end

  define_access :read_enterprise_self_hosted_runners do |access|
    access.ensure_context :resource
    access.allow :business_self_hosted_runners_writer
  end

  define_access :write_admin_actions_repo do |access|
    access.ensure_context :resource
    access.allow :repo_administration_writer
  end

  define_access :read_admin_actions do |access|
    access.ensure_context :resource
    access.allow :repo_administration_reader
  end

  define_access :register_actions_runner_org do |access|
    access.ensure_context :resource, :user
    access.allow :org_self_hosted_runners_writer
  end

  define_access :register_actions_runner_business do |access|
    access.ensure_context :resource, :user
    access.allow :business_self_hosted_runners_writer
  end

  define_access :register_actions_runner_repo do |access|
    access.ensure_context :resource, :user
    access.allow :repo_administration_writer
  end
end
