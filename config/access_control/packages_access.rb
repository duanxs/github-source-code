# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :get_package do |access|
    access.ensure_context :repo
    access.allow(:everyone) { |context| context[:repo].public? }
    access.allow :repo_packages_reader
  end

  define_access :create_package do |access|
    access.ensure_context :repo
    access.allow :repo_packages_writer
  end

  define_access :list_packages do |access|
    access.ensure_context :repo
    access.allow(:everyone) { |context| context[:repo].public? }
    access.allow :repo_packages_reader
  end

  define_access :delete_private_repository_packages do |access|
    access.ensure_context :repo
    access.allow :private_repository_package_deleter
  end

  define_access :authenticate_for_registry do |access|
    access.ensure_context :user
    access.allow :authenticated_user_using_personal_access_token
  end
end
