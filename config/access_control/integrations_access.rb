# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl

  define_access :list_installation_repositories,
                :revoke_installation_token do |access|
    access.ensure_context :resource, :installation
    access.allow :integration_installation_self
  end

  define_access :list_installation_repositories_for_user do |access|
    access.ensure_context :installation
    access.allow :user_installation_repos_lister
  end

  define_access :installation_repo_admin do |access|
    access.ensure_context :installation, :resource
    access.allow :user_repo_installation_writer
  end
end
