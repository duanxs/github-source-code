# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :import_issue do |access|
    access.ensure_context :repo
    access.allow :repo_administration_writer
  end

  define_access :migration_export do |access|
    access.ensure_context :user, :resource
    access.allow(:v3_org_admin)  { |context| context[:resource].organization? }
    access.allow(:user_exporter) { |context| context[:resource].user? }
  end

  define_access :migration_import do |access|
    access.allow :v3_org_admin
  end

  define_access :write_migration do |access|
    access.allow :org_admin
  end

  define_access :migration_repo_unlock do |access|
    access.ensure_context :repo
    access.allow :repo_administration_writer
  end
end
