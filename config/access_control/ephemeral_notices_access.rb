# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :create_ephemeral_notice, :delete_ephemeral_notice do |access|
    access.ensure_context :resource, :user
    access.allow :ephemeral_notice_accessor do |context|
      context[:user].can_have_granular_permissions?
    end
  end
end
