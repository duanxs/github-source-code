# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :read_sponsors_listing do |access|
    access.ensure_context :sponsors_listing

    access.allow(:everyone) { |context| context[:sponsors_listing].approved? }
    access.allow :sponsors_listing_writer
  end

  define_access :admin_sponsors_listing do |access|
    access.ensure_context :sponsors_listing
    access.allow :sponsors_listing_writer
  end

  define_access :read_sponsorship do |access|
    access.allow :everyone
  end
end
