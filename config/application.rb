# rubocop:disable Style/FrozenStringLiteralComment

require File.expand_path("../boot", __FILE__)

require "rails"
require "trilogy_adapter"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "will_paginate/railtie"
require "view_component/engine"
require "rails/test_unit/railtie" if Rails.env.test?
require "inline_svg"
require_relative "initializer_instrumentation"
require_relative "../lib/api_schema"
require_relative "../lib/github/middleware/database_selection"
require "active_job/queue_adapters/aqueduct_adapter"

module GitHub
  class Application < Rails::Application
    include RailsApplicationInstrumentation

    # Override Rails' default eager load, so we can exclude certain paths using config.eager_ignore_paths.
    def eager_load!
      config.eager_load_paths.each do |load_path|
        matcher = /\A#{Regexp.escape(load_path.to_s)}\/(.*)\.rb\Z/
        Dir.glob("#{load_path}/**/*.rb").sort.each do |file|
          next if config.eager_ignore_paths.any? { |p| file.start_with?(p.to_s) }
          require_dependency file.sub(matcher, '\1')
        end
      end

      # Special case for API's load path
      matcher = /\A#{Regexp.escape(Rails.root.to_s)}\/app\/(.*)\.rb\Z/
      Dir.glob("#{Rails.root}/app/api/**/*.rb").sort.each do |file|
        require_dependency file.sub(matcher, '\1')
      end

      Views.eager_load! if Rails.application.config.cache_classes
    end

    # Rails 6+ has a new autoloader called Zeitwerk. We don't want
    # to yet start using the new autoloader so we set it to `:classic`
    # for Rails 6 and up.
    if ENV["ZEITWERK"] == "1"
      config.autoloader = :zeitwerk
      Rails.autoloaders.log! if ENV["ZEITWERK_VERBOSE"] == "1"
      Rails.autoloaders.each do |autoloader|
        autoloader.ignore Rails.root.join("lib/git-core")
        autoloader.ignore Rails.root.join("lib/github/config")
        autoloader.ignore Rails.root.join("lib/github/dgit")
        autoloader.ignore Rails.root.join("lib/github/pi_link_header.rb")
        autoloader.ignore Rails.root.join("lib/github/pi_link_collection.rb")
        autoloader.ignore Rails.root.join("lib/hydro")
        autoloader.ignore Rails.root.join("app/views")
        autoloader.ignore Rails.root.join("app/assets")
        autoloader.inflector = GitHub::ZeitwerkInflector.new
      end
    else
      config.autoloader = :classic
    end

    config.autoload_paths = [
      Rails.root.join("app/view_models"),
      Rails.root.join("app/components"),
      Rails.root.join("app/controllers"),
      Rails.root.join("app/helpers"),
      Rails.root.join("app/mailers"),
      Rails.root.join("app/validators"),
      Rails.root.join("app/models"),
      Rails.root.join("app/models/concerns"),
      Rails.root.join("app/sanitizers"),
      Rails.root.join("app/jobs"),
      Rails.root.join("lib"),
      Rails.root.join("app"),
    ]

    config.eager_load_paths = config.autoload_paths - [Rails.root.join("app")] + [
      Rails.root.join("jobs"),
    ]

    config.eager_ignore_paths = [
      # These are conditionally loaded based on the environment
      Rails.root.join("lib/github/config/environments").to_s,
      # These are used as git hooks
      Rails.root.join("lib/git-core").to_s,
      # These are run to transition data
      Rails.root.join("lib/github/transitions").to_s,
      # These are used to run audit log transitions
      Rails.root.join("lib/audit/transitions").to_s,
    ]

    config.paths["vendor"].skip_load_path!

    config.watchable_dirs["app/views"] = [:erb]

    # Use memcache as rails cache store
    require "github/config/memcache"
    GitHub.set_up_rails_cache_store

    config.after_initialize do
      # Preload
      if config.eager_load && !ENV["GITAUTH"] && GitHub.role != :fsworker && GitHub.role != :resque
        GitHub::Markup.preload!
        begin
          # TODO: Remove this after project munich ships
          # it's only needed for the feature flag check in Plan.all
          if FlipperGate.table_exists?
            GitHub::Plan.all
          end
        rescue ActiveRecord::NoDatabaseError
          # No database created yet
        end

        # Codeowners
        GitHub.codeowners
        GitHub.areas_of_responsibility
        ApplicationController.descendants.each(&:codeowners)

        # SERVICEOWNERS
        GitHub.serviceowners

        # ActiveRecord
        ActiveRecord::Base.descendants.each do |model|
          next if model._internal?

          table_exists = model.table_exists?
          next unless table_exists

          model.define_attribute_methods
        rescue ActiveRecord::NoDatabaseError
          # Likely no database
        end
      end

      # Ensure we use GitHub::JSON instead of ActiveSupport::JSON
      require "github/json/active_support_patch"
    end

    # The schema cache for github/github is loaded by Trilogy on the connection.
    # Turning off this initializer skips the schema cache loader from Rails. If we don't
    # turn this initializer off then Rails will try to load a schema cache for `mysql1_primary`
    # but be unable to because we use a custom JSON schema cache.
    config.active_record.use_schema_cache_dump = false

    # adjust the logging level to whatever's set up on GitHub::Config. This is
    # usually :info (1) in production environments and :debug (0) elsewhere.
    config.log_level = GitHub.rails_log_level

    config.active_job.queue_adapter = :aqueduct
    config.active_job.return_false_on_aborted_enqueue = true
    # Rails 6.1 introduces "jitter" to ActiveJob retry timing.
    # We set to 0.0 to remove any randomness in retries
    config.active_job.retry_jitter = 0.0

    # unicorn is a multi-process / single-threaded server. we don't need an
    # explicit mutex to guarantee only one app request runs at a time.
    config.middleware.delete(Rack::Lock)

    # Middleware for choosing which database connection to use.
    config.middleware.use GitHub::Middleware::DatabaseSelection

    # This changes the trusted lookup method because we know that for both
    # .com and enterprise we never have private space ip's in this list. This
    # makes sure that we actually select those private ip's since we want
    # those in an enterprise context.
    Rack::Request.ip_filter = lambda { |ip|
      /\A127\.0\.0\.1\z|\A::1\z|\Alocalhost\z/i.match(ip)
    }

    # We have our own request id generation, so disable the standard Rails one
    config.middleware.delete(ActionDispatch::RequestId)

    if GitHub.request_limiting_enabled?
      config.middleware.insert_after ActionDispatch::Session::CookieStore,
        GitHub::Limiters::AppMiddleware,
        # Limit per-browser-session requests similarly to the API. Instead of 5k
        # requests per hour, allow 6k per hour but in two-minute increments of
        # 200 requests to allow fast recovery.
        #
        # Inserted after the session cookie store since it relies on
        # rack.session to operate.
        #
        # This request limiter was added specifically to help address
        # https://github.com/github/core-app/issues/192.
        GitHub::Limiters::CountByBrowserSession.new(limit: 300, ttl: 120),
        GitHub::Limiters::SearchCountAnonymousByIp.new(limit: 100, ttl: 3600), # 100 per hour per IP
        GitHub::Limiters::SearchCountAnonymousByIp.new(limit: 10,  ttl: 60),   # 10 per minute per IP
        GitHub::Limiters::SearchCountAnonymous.new(ttl: 60)                    # global limits
    end

    # Disable rack-cache. We don't use machine-level page/response caching.
    config.action_dispatch.rack_cache =  nil

    config.action_dispatch.default_headers.clear

    config.action_dispatch.use_authenticated_cookie_encryption = true
    config.action_dispatch.use_cookies_with_metadata = true
    config.active_support.use_authenticated_message_encryption = true

    # We don't support old browsers that need this
    config.action_view.default_enforce_utf8 = false

    config.active_record.default_timezone = :local

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = "UTC"

    config.action_view.field_error_proc = proc do |html_tag, instance|
      "<div class=\"field-with-errors\">#{html_tag}</div>".html_safe # rubocop:disable Rails/OutputSafety
    end

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += GitHub.filtered_params
    config.filter_parameters << GitHub.filtered_params_proc

    if GitHub.enterprise?
      # enable external auth provider when configured
      GitHub.auth.add_middleware(config.middleware)
    end

    # Disable a deprecation warning that dumps to STDERR:
    I18n.enforce_available_locales = true

    # As of Rails 3.2, an accurate `tld_length` is required for `:subdomain`
    # routing conditions to work for domains with more than one `.`  (ex.
    # A.B.C.D.xip.io is used in GitHub Enterprise integration tests). Also, we
    # only set `tld_length` if `host_name` is set, since some enterprise rake
    # tasks load `config/application` before we have a valid host name.
    config.action_dispatch.tld_length = GitHub.host_name.count(".") if GitHub.host_name

    config.active_record.schema_format = :sql

    # Enterprise instances might be running behind a proxy so we disable checking
    # of the `Origin` header.
    config.action_controller.forgery_protection_origin_check = GitHub.origin_verification_enabled?

    # Enable per-form CSRF tokens.
    config.action_controller.per_form_csrf_tokens = true
    config.action_view.embed_authenticity_token_in_remote_forms = true

    # Override to remove eager_load_paths.
    #
    # Due to the non-railsy naming conventions of `app/api` (classes are in the
    # `API` namespace and also in a directory named `api`) and `jobs` (classes
    # are in the `GitHub::Jobs` namespace), those two directories cause
    # autoloading issues when they are in the autoload paths. They need to be in
    # `#eager_load_paths` so they are required in production.
    #
    # cc @bkeepers if you have questions about this horribleness
    def _all_autoload_paths
      @_all_autoload_paths ||= (config.autoload_paths + config.autoload_once_paths).uniq
    end

    # Override to add eager_load_paths back
    # Rails 6 RC1 added `config.add_autoload_paths_to_load_path` which will
    # prevent /app directories from being added to $LOAD_PATH. This is only
    # useful with Zeitwerk. See documentation for more information.
    def _all_load_paths(add_autoload_paths_to_load_path)
      @_all_load_paths ||= begin
        load_paths  = config.paths.load_paths
        load_paths += _all_autoload_paths if add_autoload_paths_to_load_path
        load_paths += config.eager_load_paths
        load_paths.uniq
      end
    end
  end
end

require "adaptive_cards/components"
