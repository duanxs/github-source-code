# rubocop:disable Style/FrozenStringLiteralComment

require "timer_daemon"

require "github/config/active_record"
require "github/config/redis"
require "github/pages/allocator"
require "github/sql"

# configure daemon
daemon = TimerDaemon.instance
daemon.err = GitHub::Logger.method(:log)
daemon.redis = GitHub.resque_redis

ScheduledPagesDfsTimerError = Class.new(StandardError)

# report exceptions to Haystack
daemon.error do |boom, timer|
  Failbot.report(ScheduledPagesDfsTimerError.new("timer #{timer.name} failed"), timer: timer.name)
end

# As dotcom moved to v3 naming, we needed the be able to use the FQDN
# of the host. For backwards-compatibility, we should continue using
# the shortname in Enterprise.
fqdn = Socket.gethostname
hostname = GitHub.enterprise? ? fqdn.split(".").first : fqdn

daemon.schedule "pages-partition-usage", 1.minute, scope: :host do
  GitHub::Pages::Allocator.update_disk_usage!(hostname: hostname)
end

daemon.schedule "pages-garbage-collect", 1.hour, scope: :host do
  GitHub::Pages::GarbageCollector.run!
end
