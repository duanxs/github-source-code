# rubocop:disable Style/FrozenStringLiteralComment

ActiveSupport::Inflector.inflections do |inflect|
  inflect.acronym "GitHub"
  inflect.acronym "DGit"
  inflect.acronym "SCIM"
  inflect.acronym "UUID"
  inflect.acronym "OFAC"
  inflect.acronym "NumFOCUS"

  inflect.irregular "has", "have"
  inflect.irregular "is", "are"
  inflect.irregular "was", "were"
end
