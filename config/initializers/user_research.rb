# rubocop:disable Style/FrozenStringLiteralComment

require "verdict"

# Tell verdict to look in lib/github/user_research/experiments for
# experiment definitions.
Verdict.default_logger = Rails.logger
Verdict.directory = Rails.root.join("lib", "github", "user_research", "experiments")
