# rubocop:disable Style/FrozenStringLiteralComment
if Rails.test?
  # Print a warning and (optionally) raise if anyone uses the
  # `raw` helper to mark a string `html_safe`.  Using `raw` can lead to
  # insecure HTML markup, as it makes a string `html_safe`, usually without
  # sufficient surrounding context for a code review to assess the safety of the
  # resulting HTML.
  #
  # Error class for when `raw` method is called
  class DeprecatedRawUse < RuntimeError; end;

  # This overrides the `raw` method to send a warning and optionally raise
  # when called
  #
  # Alternatives:
  #   * Use Rails helpers (`content_tag`, `link_to`, etc) when possible.
  #   * Use `html_safe` in a ViewModel with sufficient context/comments to provide
  #     a justification for why marking the string `html_safe` is safe.
  #   * Follow the guidance in https://github.com/github/security-docs/blob/master/standards/Secure%20Coding%20Principles.md#server-side-html-generation
  module OutputSafetyHelperWithGitHubDeprecation
    def raw(stringish)
      callstack = caller
      if github_whitelisted_raw_caller?(callstack)
        return super(stringish)
      end
      boom = DeprecatedRawUse.new("deprecated raw usage from #{callstack[0]}")
      boom.set_backtrace(callstack)
      Failbot.report(boom)
      raise boom if Rails.env.test?
      super(stringish)
    end

    GITHUB_RAW_WHITELIST = Regexp.union(
      %r{app/views/mailers},
    )

    def github_whitelisted_raw_caller?(callstack)
      callstack[0] =~ GITHUB_RAW_WHITELIST
    end
  end

  ActionView::Helpers::OutputSafetyHelper.send(:prepend, OutputSafetyHelperWithGitHubDeprecation)
end
