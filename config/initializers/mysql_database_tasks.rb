# rubocop:disable Style/FrozenStringLiteralComment

ActiveRecord::Tasks::DatabaseTasks.register_task("trilogy", "ActiveRecord::Tasks::MySQLDatabaseTasks")

module ActiveRecord
  module Tasks
    class MySQLDatabaseTasks
      # Override the backport of structure_dump and add the
      # --skip-comments and --single-transaction arguments to the mysqldump
      def structure_dump(filename, extra_flags)
        args = prepare_command_options
        args.concat(["--result-file", "#{filename}"])
        args.concat(["--no-data"])
        args.concat(["--skip-comments"])
        args.concat(["--single-transaction"])
        args.concat(["--set-gtid-purged=OFF"])
        args.concat(["--routines"])
        if GitHub.rails_6_0?
          args.concat(["#{configuration['database']}"])
        else
          args.concat(["#{configuration_hash[:database]}"])
        end
        args.unshift(*extra_flags) if extra_flags

        run_cmd("mysqldump", args, "dumping")
      end
    end

    if GitHub.rails_6_0?
      module DatabaseTasks
        # Backport Rails 6.1 definition of truncate_tables
        def truncate_tables(db_config)
          # rubocop:disable GitHub/DoNotCallMethodsOnActiveRecordBase
          ActiveRecord::Base.establish_connection(db_config)

          connection = ActiveRecord::Base.connection
          connection.truncate_tables(*connection.tables)
        end
        private :truncate_tables
      end
    end
  end
end
