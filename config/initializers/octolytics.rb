# rubocop:disable Style/FrozenStringLiteralComment

require "octolytics"

class Octolytics::Error
  def failbot_context
    { app: "github-octolytics" }
  end
end
