# frozen_string_literal: true

require "flipper"
require "flipper/groups"

# See lib/flipper/groups.rb for list of groups
Flipper::Groups.register_groups

module Flipper
  # TODO: push this into Flipper itself
  class Type
    def to_s
      value.to_s
    end
  end
end

require "flipper/employee_mode"
