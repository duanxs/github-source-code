# frozen_string_literal: true

module Transferable
  def transfer_to(model)
    reflection = proxy_association.reflection

    if reflection.type
      update_all(reflection.type => model.class.to_s, reflection.foreign_key => model.id)
    else
      update_all(reflection.foreign_key => model.id)
    end
  end
end

ActiveRecord::Associations::CollectionProxy.include Transferable
