# frozen_string_literal: true

# Open up Scientist and hack it to return our own GitHub Experiment.
#
# See GitHub::Experiment
Scientist::Experiment.module_eval do
  def self.new(name)
    GitHub::Experiment.new(name)
  end
end

ActionController::Base.module_eval do
  include Scientist
end

ActiveRecord::Base.module_eval do
  include Scientist
  extend Scientist
end

if Rails.env.test?
  GitHub::Experiment.raise_on_mismatches = true
  GitHub::Experiment.raise_on_internal_errors = true
elsif Rails.env.development?
  GitHub::Experiment.raise_on_mismatches = false
  GitHub::Experiment.raise_on_internal_errors = true
end
