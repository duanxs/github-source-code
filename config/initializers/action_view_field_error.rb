# rubocop:disable Style/FrozenStringLiteralComment

ActionView::Base.field_error_proc = proc do |html_tag, instance|
  "<div class=\"field-with-errors\">#{html_tag}</div>".html_safe # rubocop:disable Rails/OutputSafety
end
