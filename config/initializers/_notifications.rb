# frozen_string_literal: true

# Require our custom ActiveSupport::Notifications config as early
# as possible so that we specify our notifier before any code starts
# subscribing to events.
require "github/config/notifications"
