# frozen_string_literal: true

require "eloqua"
Eloqua.cache_provider = Rails.cache
