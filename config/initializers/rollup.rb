# rubocop:disable Style/FrozenStringLiteralComment

require "rollup"

# Adds these to the default frame blacklist in Rollup
blacklist = [
  "THE WIRE",
  "app/models/git/ref.rb",
  "app/models/repository/commits_dependency.rb",
  "app/models/repository/refs_dependency.rb",
  "app/models/repository/spawn_dependency.rb",
  "bin/ernicorn",
  "config/ernicorn.rb",
  "config/initializers/unsafe_raw_sql.rb",
  "config/initializers/cache_notification_info.rb",
  "config/instrumentation",
  "lib/github/active_record_enumerable_protection.rb",
  "lib/github/association_instrumenter.rb",
  "lib/github/cache",
  "lib/github/config/mysql.rb",
  "lib/github/config/notifications.rb",
  "lib/github/experiment.rb",
  "lib/github/notifications/fanout_with_exception_handling.rb",
  "lib/github/request_timer.rb",
  "lib/github/ds.rb",
  "lib/github/sql/batched.rb",
  "lib/github/sql/batched_between.rb",
  "vendor/",
]
Rollup.set_custom_blacklist(blacklist)
