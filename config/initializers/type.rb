# frozen_string_literal: true

require "utc_timestamp"
ActiveRecord::Type.register(:utc_timestamp, UtcTimestamp)
