# frozen_string_literal: true

require "braintree"

if GitHub.billing_enabled?
  ::Braintree::Configuration.environment = GitHub.braintree_environment
  ::Braintree::Configuration.merchant_id = GitHub.braintree_merchant_id
  ::Braintree::Configuration.public_key  = GitHub.braintree_public_key
  ::Braintree::Configuration.private_key = GitHub.braintree_private_key

  if Rails.development? || Rails.test?
    logger       = ::Logger.new(STDOUT)

    logger.level = if ENV["BRAINTREE_DEBUG"].present?
      ::Logger::DEBUG
    elsif Rails.development?
      ::Logger::INFO
    else
      ::Logger::FATAL
    end

    ::Braintree::Configuration.logger = logger
  end
end
