# rubocop:disable Style/FrozenStringLiteralComment

# ruby stdlib
require "digest/sha1"
require "digest/md5"
require "base64"
require "yaml"
require "ostruct"
require "resolv"

# bring in github lib
require "github"

# bring in all github server connections
require "github/config/actions_runner_ips"
require "github/config/mysql"
require "github/config/memcache"
require "github/config/redis"
require "github/config/kv"
require "github/config/stats"
require "github/config/graphite"
require "github/config/analytics"
require "github/config/munger"

# load this early, as many subsequent things depend on it
require "posix/spawn"

# bring in library configs
require "github/config/context"
require "github/config/resque"
require "github/config/resque-server"
require "github/config/s3"
require "github/config/services"
require "github/config/gitrpc"
require "github/config/octocat"
require "github/config/flipper"
require "github/config/dogapi"
require "github/config/presto"
require "github/config/aqueduct"
require "github/optimizely/config"

# other gems
require "redcloth"
require "version_sorter"
require "will_paginate/array"
require "net/https"
require "color"
require "color_ext"
require "rexml/document"
require "yajl"
require "mustache"
require "oauth2"
require "securerandom"
require "sanitize"
require "addressable/uri"
require "wikicloth"
require "email_reply_parser"
require "sinatra/base"
require "money"
require "money/bank/open_exchange_rates_bank"
require "stratocaster"
require "browser"
require "bcrypt"
require "charlock_holmes"
require "charlock_holmes/string"
require "rinku"
require "rails_rinku"
require "array_ext"
require "string_ext"
require "time_ext"
require "msgpack_ext"
require "stable_sorter"
require "elastomer"
require "premailer"
require "simple_uuid"
require "rqrcode"
require "prawn"
require "prawn-svg"
require "twilio-ruby"
require "terminal-notifier"
require "geoip2_compat"
require "octolytics"
require "prose_diff"
require "will_paginate"
require "geo_pattern"
require "diff/lcs"
require "github-pages-health-check"
require "coconductor"
require "licensee"
require "coconductor"
require "u2f"
require "phonelib"
require "lru_redux"
require "ipaddress"
require "secure_headers"
require "octicons_helper"
require "commonmarker"
require "acme-client"
require "earthsmoke"
require "hydro"
require "public_suffix"
require "homograph_detector"
require "webauthn"
require "aqueduct"
require "ssh_data"
require "ed25519"
require "eventer/client"
require "driftwood/client"
require "dynsampler"
require "codeowners"

# github libs
require "github/commit_message"
require "github/egress"
require "github/hash_lock"
require "github/packers"
require "github/job_stats"
require "linguist"
require "github/markup"
require "github/billing/zuora_dependency"
require "github/plan"
require "newsies"
require "scientist"
require "search"
require "gpg_verify"
require "pgp_armor"
require "authorization"
require "instrumentation/model"
require "dumpable"
require "method_timing"
require "global_instrumenter"
require "github/html_safe_string"
require "audit/operation_types"

# github rack overrides
require "rack/octotracer"
require "rack/warmup"
require "promise"

# Disable nagle on excon connections.
# Currently these are used by the gpgverify and search client libraries.
require "excon"
Excon.defaults[:tcp_nodelay] = true

# charliesome's favorite module
require "active_support/concern"

# Don't parse JSON requests by default.  See #21699
ActionDispatch::Request.parameter_parsers.delete Mime[:json].symbol

# platform and graphql
require "graphql/client"

require "github/erb_implementation"
ActionView::Template::Handlers::ERB.erb_implementation = GitHub::ErbImplementation

ActiveSupport::XmlMini.backend = "Nokogiri"

require "last_modified_calculation"
ActiveRecord::Base.send :include, LastModifiedCalculation

require "github/active_record_enumerable_protection"
GitHub::ActiveRecordEnumerableProtection.initialize
require "github/active_record_readonly_mode"
GitHub::ActiveRecordReadonlyMode.initialize

begin
  require "rblineprof"
  require "stackprof"
rescue LoadError
  # it's not the end of the world if we can't load up some profilers
end

require "slow_query_logger"
