# frozen_string_literal: true

# gRPC is a janky FFI extension and the underlying library blows up if you call
# into it, fork, and then call into it again. Since we're always forking in CI,
# this code monkey patches the gRPC Ruby library to raise an error if called
# into before forking. This is easier to track down than the gRPC errors, which
# are raised in the fork, not the parent.

if Rails.env.test?
  require_relative "../../test/test_helpers/test_env"
  if TestEnv.github_ci?
    require "grpc"

    module NoGrpcBeforeFork
      extend self

      Error = Class.new(StandardError)

      PARENT_PID = Process.pid

      def only_in_fork!
        return if in_fork?
        raise Error, "Don't call into gRPC before forking!"
      end

      def in_fork?
        Process.pid != PARENT_PID
      end
    end

    module GRPC
      class ClientStub
        alias orig_initialize initialize

        def initialize(*args, **kwargs)
          NoGrpcBeforeFork.only_in_fork!
          orig_initialize(*args, **kwargs)
        end
      end
    end
  end
end
