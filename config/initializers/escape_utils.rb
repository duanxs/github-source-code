# frozen_string_literal: true

require "escape_utils"
EscapeUtils.html_secure = false
EscapeUtils.html_safe_string_class = ActiveSupport::SafeBuffer

class ERB
  module Util
    def html_escape(s)
      s = s.to_s
      if s.html_safe?
        s
      else
        EscapeUtils.escape_html_as_html_safe(GitHub::Encoding.try_guess_and_transcode(s))
      end
    end

    def force_escape(s)
      EscapeUtils.escape_html_as_html_safe(GitHub::Encoding.try_guess_and_transcode(s))
    end

    remove_method(:h)
    alias h html_escape

    module_function :h

    singleton_class.send(:remove_method, :html_escape)
    module_function :html_escape
  end
end
