# frozen_string_literal: true

# Rails 3 defaults to fully qualified class names in STI columns.
# While this is a good default, it's the opposite of Rails 2's default and we
# have records in production using unqualified class names.
# Override the default until we have a chance to fix those records:
ActiveRecord::Base.store_full_sti_class = false
