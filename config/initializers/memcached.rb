# rubocop:disable Style/FrozenStringLiteralComment

# Bring in base cache configuration
require "github/config/memcache"

# Added to fragment and action cache key prefixes by Rails. useful for
# rev'ing only fragment caches.
ENV["RAILS_CACHE_ID"] = "v8"

# Point the cache logger at AR's if possible
GitHub.cache.logger = ActiveRecord::Base.logger if GitHub.cache.respond_to?(:logger=)

T_5_MINUTES = 300
