# rubocop:disable Style/FrozenStringLiteralComment

# Print a warning and (optionally) raise if anyone uses the
# backtick method to execute code.  Using backticks can lead to
# insecure code, as it invokes a shell, so if user entered input
# ever makes its way into the call, you have a severe security hole.
#
# Error class for when backtick method is called
class InsecureCommandExecution < RuntimeError; end;

# This overrides the backtick method to send a warning and optionally raise
# when called
#
# Alternatives:
#   * Posix::Spawn (https://github.com/rtomayko/posix-spawn)
#   * look for a standard library providing the same function,
#     for example Socket.gethostname instead of `hostname`
#   * IO.popen
class Object
  def `(arg)
    callstack = caller
    if github_whitelisted_backtick_caller?(callstack)
      return super
    end
    boom = InsecureCommandExecution.new("insecure command execution via ` method from #{callstack[0]}")
    boom.set_backtrace(callstack)
    Failbot.report(boom)
    raise boom if Rails.env.test?
    super
  end

  GITHUB_BACKTICK_WHITELIST = Regexp.union(
    %r{config/timers\..+\.rb},
    %r{config/unicorn\.rb},
    %r{lib/active_support/core_ext},
    %r{lib/rack/process_utilization\.rb},
    %r{lib/resque/worker\.rb},
    %r{test/integration/.+},
    %r{test/fast/linting/.+},
    %r{test/test_helpers/.+},
    %r{vendor/.+gems/byebug.+/lib/.+},
    %r{vendor/.+gems/enterprise-crypto.+/lib/.+},
    %r{vendor/.+gems/pry.+/lib/.+},
    %r{vendor/.+gems/dogapi.+/lib/capistrano/datadog.rb},
    %r{vendor/.+gems/dogapi.+/lib/dogapi/common.rb},
    %r{vendor/.+gems/stripe.+/lib/.+},
    %r{minitest/+unit\.rb},
    %r{minitest/+assertions\.rb},
    %r{ruby-progressbar/calculators/length.rb},
    %r{lib/readline_ext.rb},
  )

  def github_whitelisted_backtick_caller?(callstack)
    callstack[0] =~ GITHUB_BACKTICK_WHITELIST
  end
end

# Make sure POSIX::Spawn's own version of backtick doesn't get used anywhere
POSIX::Spawn.class_eval do
  remove_method :`
end
