# frozen_string_literal: true

class ActiveSupport::TimeZone
  # See test/integration/time_zone_test.rb.  Rails 4.1 throws an exception when
  # ambiguous times are used; we just want the first.
  def period_for_local(time, dst = true)
    tzinfo.period_for_local(time, dst) { |results| results.first }
  end
end

Time::DATE_FORMATS[:db] = lambda { |time|
  time = time.dup.utc.to_time.utc

  if !defined?(ActiveRecord::Base.default_timezone) || ActiveRecord::Base.default_timezone == :local
    # our DB is in local time (ugh), so make sure the time object is
    # converted to local time before converting it to a db string
    #
    # also we have to do this ridiculous dance to ensure that we can
    # turn any given DateTime object into something in localtime.
    time = time.getlocal
  end

  time.strftime("%Y-%m-%d %H:%M:%S")
}
