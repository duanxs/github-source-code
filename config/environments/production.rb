# frozen_string_literal: true

GitHub::Application.configure do
  # Settings specified here will take precedence over those in config/environment.rb
  #
  #  #########################################################
  #  #                                                     ##
  #  #   !!!!!!    DO NOT PUT SECRETS IN HERE    !!!!!!    ##
  #  #                                                     ##
  #  #     see docs/enterprise.md for more info.           ##
  #  #                                                     ##
  #  ########################################################
  #
  #

  config.cache_classes = true
  config.eager_load = true

  config.consider_all_requests_local = false
  config.action_controller.perform_caching             = true
  config.action_mailer.raise_delivery_errors           = false
  config.action_mailer.delivery_method                 = :smtp
  config.serve_static_assets                         = false

  # Set the BCrypt computation cost to a reasonable setting
  GitHub.password_cost = 8

  # Ensure default user password is not set
  GitHub.default_password = nil

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  config.action_controller.action_on_unpermitted_parameters = :log

  # Number of voting replicas in production and staging-lab (except on
  # GHES where this is controlled by an environment variable.)
  #
  # N.B. In production the actual replication level is determined
  # by policies in spokesd, but this expected number of replicas
  # puts a floor of (dgit_copies/2)+1 on the quorum size for writes.
  GitHub.dgit_copies = 5 if !GitHub.enterprise?
end
