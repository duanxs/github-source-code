# frozen_string_literal: true

class LightStepObserver
  def initialize(statsd)
    @statsd = statsd
  end

  def call(name, start, ending, transaction_id, payload)
    duration_ms = (ending - start) * 1_000

    if payload[:exception]
      boom = payload[:exception]
      # NOTE: if we wanted to report this error somewhere else, we'd need
      # a threadsafe way to do it. For now, just dogstats.
      tags = []
      tags << "exception_class:#{boom.class.name.underscore}"

      @statsd.timing("lightstep.report.error.time", duration_ms, tags: tags)
    else
      @statsd.timing("lightstep.report.time", duration_ms)
    end
  end
end

# We need our own dogstats because we're operating in the reporting
# thread. It's ok to share just the one per transport though because
# HTTPJSON promises it'll be ok.
GitHub.subscribe("flush.lightstep", LightStepObserver.new(GitHub.new_dogstats))
