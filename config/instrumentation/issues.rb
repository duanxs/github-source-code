# rubocop:disable Style/FrozenStringLiteralComment

GitHub.subscribe "issue.create" do |event, start, ending, transaction_id, payload|
  puts Scrolls::Log.unparse(payload.merge(at: "issue.create")) if Rails.env.development?
  GitHub::Logger.log payload.merge(at: "issue.create")
end
