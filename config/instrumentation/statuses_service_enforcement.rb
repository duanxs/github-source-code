# frozen_string_literal: true

module Statuses
  class UnsafeQueryError < StandardError
    def initialize(query, frame)
      @query = query
      @frame = frame
      super("Unsafe Statuses query `%s` from `%s`. Use Statuses::Service to query for Statuses" % [query, frame])
    end

    def tags
      @tags ||= begin
        ts = []
        if match = @frame.to_s.match(/(.*):(\d+)(?::in `([^']+)')?/) # taken from Rollbar gem
          filename, lineno, method = match[1], match[2], match[3]

          ts.push("file:#{File.basename(filename)}")

          if method
            method = method.sub(/_erb__[_\d]+\z/, "_erb") # scrub generated ERB method names
            ts.push("method:#{method}")
          end
        end
        ts
      end
    end
  end

  class ServiceSubscriber
    WHITELIST = [
      # StatusesService
      "lib/statuses/service.rb",
      "test/lib/statuses/service_test.rb",

      # migrations & transitions
      "db/migrate/",
      "lib/github/transitions/",
      "lib/github/safe_database_migration_helper.rb",

      # tests
      "test/test_helpers/fixtures.rb",
      "test/",

      # safe
      "lib/trilogy_adapter/events/notifier.rb",
      "app/models/application_record.rb", # reload, used in tests
      "lib/github/background_dependent_deletes.rb",
      "jobs/delete_dependent_records.rb",

      # false positives
      # NONE!

      # known offenders...
      "lib/platform/loaders/active_record.rb", # Platform::Loaders::ActiveRecord#fetch
      "app/models/data_quality", # ability orphans scan
    ].freeze

    # http://rubular.com/r/GX2BGie7o3
    UNSAFE_QUERY_PATTERN = /(from|into)\s+`?statuses`?\s*/i
    SHOW_FULL_FIELDS = /\ASHOW (?:FULL )?FIELDS/

    SAFE_COMMENT = "StatusesService".freeze

    def call(event, start, ending, transaction_id, payload)
      query, query_comment = payload[:sql].split("/*", 2)

      if unsafe_query?(query)
        return if query_comment&.include?(SAFE_COMMENT)

        frame = Rollup.first_significant_frame(caller)
        return if whitelisted?(frame)

        error = UnsafeQueryError.new(payload[:sql], frame)
        GitHub.dogstats.increment("statuses_service.unsafe_queries", tags: error.tags)

        if raise_unsafe?
          raise error
        elsif report_unsafe?
          error.set_backtrace(caller)
          Failbot.report!(error, app: "github-statuses-unsafe-queries")
        end
      end
    end

    private

    def unsafe_query?(query)
      query =~ UNSAFE_QUERY_PATTERN && query !~ SHOW_FULL_FIELDS
    end

    def whitelisted?(frame)
      WHITELIST.any? { |sf| frame.include?(sf) }
    end

    def raise_unsafe?
      return if GitHub.enterprise?
      Rails.development? || Rails.test?
    end

    def report_unsafe?
      return if GitHub.enterprise?
      Rails.production?
    end
  end
end

ActiveSupport::Notifications.subscribe "sql.active_record", Statuses::ServiceSubscriber.new
