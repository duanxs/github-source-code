# frozen_string_literal: true

require "active_support/notifications"
require "flipper"
require "flipper/instrumentation/subscriber"
require_relative "../../lib/permissions/custom_role_flag_subscriber"

class FlipperSubscriber
  class << self
    attr_accessor :tested_features
  end

  def self.reset_stats
    self.tested_features = Hash.new
  end
  reset_stats

  # Private: Sample rate for the flipper instrumentation that is higher
  # throughput to reduce the overhead of instrumenting every enabled? check.
  # Examples of higher throughput metrics would be checking if a feature is
  # enabled and getting the gate values for a feature from an adapter (in order
  # to do the enabled check).
  HIGH_THROUGHPUT_SAMPLE_RATE = 0.1

  # Private: Sample rate for the flipper instrumentation that is lower
  # throughput. Examples of lower throughput would be metrics instrumented from
  # enable and disable of feature.
  LOW_THROUGHPUT_SAMPLE_RATE = 1

  def self.sample_rate(operation)
    if operation == :enabled?
      HIGH_THROUGHPUT_SAMPLE_RATE
    else
      LOW_THROUGHPUT_SAMPLE_RATE
    end
  end

  def self.call(name, start, ending, transaction_id, payload)
    feature_name = payload[:feature_name]
    operation = payload[:operation]

    if operation == :enabled?
      tested_features[feature_name.to_sym] ||= payload[:result]
    elsif operation == :enable || operation == :disable
      record_modification(payload)
    end

    # DD doesn't like tags with "?" and common operations are `enabled?` and
    # `disabled?`, so we'll strip the "?"
    formatted_operation = operation.to_s.tr "?", ""

    tags = [
      "operation:#{formatted_operation}",
      "feature:#{feature_name}",
    ]

    # track whether feature was enabled or not so we can show graphs accordingly
    if operation == :enabled?
      tags << (payload[:result] ? "result:true" : "result:false")
    end

    # track which gate was enabled or disabled so we can see which get used
    if operation == :enable || operation == :disable
      tags << "gate:#{payload[:gate_name]}"
    end

    value = GitHub::Dogstats.duration(start, ending)
    GitHub.dogstats.distribution("flipper.dist.time", value, tags: tags)
  end

  def self.record_modification(payload)
    operation = payload[:operation]
    return unless (operation == :enable || operation == :disable)
    payload = payload.dup

    flipper_type = payload.delete(:thing) # this is the flipper type being enabled or disabled
    return unless flipper_type

    # this is the thing the flipper type is wrapping (ie: user, repo, number, etc.)
    subject =
      case flipper_type
      when Flipper::Types::Actor
        # the audit log wants the thing (repo or user), not the flipper type actor
        flipper_type.thing
      else
        flipper_type
      end

    subject_serialized = subject.to_s # make it something serializable for the audit log
    subject_class = subject.class.name

    payload[:subject] = subject_serialized
    payload[:subject_class] = subject_class
    payload[:subject_id] = subject.id if subject.respond_to?(:id)

    GitHub.instrument "feature.#{operation}", payload

    actor = GitHub.context[:actor]
    feature = payload[:feature_name]

    desc = FlipperFeature.get_subject_label(gate_name: payload[:gate_name], subject: subject_serialized, skip_actor_gates: true)
    if desc
      begin
        if ENV["CHATTERBOX_TOKEN"]
          GitHub::Chatterbox.client.say("github-features", "#{actor} #{operation}d the #{feature} feature for #{desc}")
        else
          warn "Chatterbox token not set, skipping chat message." unless Rails.env.test?
        end
      rescue => boom
        context = { chatterbox_response: boom.response } if boom.respond_to?(:response)
        Failbot.report!(boom, context)
      end
    end
  end
end

ActiveSupport::Notifications.subscribe "feature_operation.flipper", FlipperSubscriber
GitHub.subscribe "feature.disable", Permissions::CustomRoleFlagSubscriber

ActiveSupport::Notifications.subscribe "adapter_operation.flipper" do |name, payload_id, started_at, ended_at, payload|
  next unless payload[:gate_name] == :actor

  if payload[:operation] == :enable
    GitHub.dogstats.increment("flipper.gate_count", tags: ["feature:#{payload[:feature_name]}"])
  elsif payload[:operation] == :disable
    GitHub.dogstats.decrement("flipper.gate_count", tags: ["feature:#{payload[:feature_name]}"])
  end
end
