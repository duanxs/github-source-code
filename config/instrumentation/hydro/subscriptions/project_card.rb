# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
    subscribe("browser.project_card_xref_toggle.click") do |payload|
        message = {
          request_context: serializer.request_context(GitHub.context.to_hash),
          actor: serializer.user(User.find_by(id: payload[:user_id])),
          project: serializer.project(Project.find_by(id: payload[:project_id])),
          issue: serializer.issue(Issue.find_by(id: payload[:issue_id])),
          card: serializer.project_card(ProjectCard.find_by(id: payload[:card_id])),
          action: payload[:action],
        }
        publish(message, schema: "github.v1.ProjectCardXrefViewEvent")
    end
end
