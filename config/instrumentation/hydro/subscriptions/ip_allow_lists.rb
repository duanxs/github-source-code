# frozen_string_literal: true

# Hydro event subscriptions related to IP allow lists.
Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("ip_allow_list.enable") do |payload|
    message = {
      owner_type: payload[:owner].is_a?(Business) ? :BUSINESS : :ORG,
      owner_id: payload[:owner].id,
      entries: serializer.ip_allow_list_entries(payload[:entries]),
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.ip_allow_list.v0.Enable")
  end

  subscribe("ip_allow_list.disable") do |payload|
    message = {
      owner_type: payload[:owner].is_a?(Business) ? :BUSINESS : :ORG,
      owner_id: payload[:owner].id,
      entries: serializer.ip_allow_list_entries(payload[:entries]),
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.ip_allow_list.v0.Disable")
  end

  subscribe("ip_allow_list.app_access.enable") do |payload|
    message = {
      owner_type: payload[:owner].is_a?(Business) ? :BUSINESS : :ORG,
      owner_id: payload[:owner].id,
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.ip_allow_list.v0.EnableAppAccess")
  end

  subscribe("ip_allow_list.app_access.disable") do |payload|
    message = {
      owner_type: payload[:owner].is_a?(Business) ? :BUSINESS : :ORG,
      owner_id: payload[:owner].id,
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.ip_allow_list.v0.DisableAppAccess")
  end

  subscribe("ip_allow_list.policy_unsatisfied") do |payload|
    message = {
      owner_type: payload[:owner].is_a?(Business) ? :BUSINESS : :ORG,
      owner_id: payload[:owner].id,
      active_entries_for_owner: serializer.ip_allow_list_entries(payload[:active_entries_for_owner]),
      actor: serializer.user(payload[:actor]),
      integration_actor: serializer.integration(payload[:integration_actor]),
      actor_ip: payload[:actor_ip],
    }
    publish(message, schema: "github.ip_allow_list.v0.PolicyUnsatisfied")
  end

  subscribe("ip_allow_list_entry.create") do |payload|
    message = {
      entry: serializer.ip_allow_list_entry(payload[:entry]),
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.ip_allow_list.v0.IpAllowListEntryCreate")
  end

  subscribe("ip_allow_list_entry.update") do |payload|
    message = {
      entry: serializer.ip_allow_list_entry(payload[:entry]),
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.ip_allow_list.v0.IpAllowListEntryUpdate")
  end

  subscribe("ip_allow_list_entry.destroy") do |payload|
    message = {
      entry: serializer.ip_allow_list_entry(payload[:entry]),
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.ip_allow_list.v0.IpAllowListEntryDestroy")
  end
end
