# frozen_string_literal: true

# These are Hydro event subscriptions related to Advisory Credits.

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("advisory_credit.create") do |payload|
    publish(
      serializer.advisory_credit(
        payload,
        except: [:advisory_credit_created_at, :advisory_credit_current_state],
      ),
      schema: "github.security_advisories.v0.AdvisoryCreditCreate",
    )
  end

  subscribe("advisory_credit.accept") do |payload|
    publish(
      serializer.advisory_credit(
        payload,
        except: [:advisory_credit_current_state],
      ),
      schema: "github.security_advisories.v0.AdvisoryCreditAccept",
    )
  end

  subscribe("advisory_credit.decline") do |payload|
    publish(
      serializer.advisory_credit(
        payload,
        except: [:advisory_credit_current_state],
      ),
      schema: "github.security_advisories.v0.AdvisoryCreditDecline",
    )
  end

  subscribe("advisory_credit.destroy") do |payload|
    publish(
      serializer.advisory_credit(payload),
      schema: "github.security_advisories.v0.AdvisoryCreditDestroy",
    )
  end

  subscribe("advisory_credit.notify") do |payload|
    publish(
      serializer.advisory_credit(
        payload,
        except: [:advisory_credit_current_state]
      ),
      schema: "github.security_advisories.v0.AdvisoryCreditNotify"
    )
  end
end
