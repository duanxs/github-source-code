# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("repository_transfer.immediate") do |payload|
    repository = payload[:repository]
    requester = payload[:requester]
    target = payload[:target]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(repository),
      requester: serializer.user(requester),
      target: serializer.user(target),
      criteria: :IMMEDIATE,
      previous_owner: serializer.user(payload[:previous_owner])
    }

    publish(message, schema: "github.v1.RepositoryTransfer")
  end

  subscribe("repository_transfer.request_start") do |payload|
    repository_transfer = payload[:repository_transfer]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(repository_transfer.repository),
      requester: serializer.user(repository_transfer.requester),
      responder: serializer.user(repository_transfer.responder),
      target: serializer.user(repository_transfer.target),
      state: repository_transfer.state,
      criteria: :REQUEST,
      repository_transfer_id: repository_transfer.id,
      previous_owner: serializer.user(payload[:previous_owner])
    }

    publish(message, schema: "github.v1.RepositoryTransfer")
  end

  subscribe("repository_transfer.request_finish") do |payload|
    repository_transfer = payload[:repository_transfer]

    if repository_transfer.responded?
      message = {
        request_context: serializer.request_context(GitHub.context.to_hash),
        repository: serializer.repository(repository_transfer.repository),
        requester: serializer.user(repository_transfer.requester),
        responder: serializer.user(repository_transfer.responder),
        target: serializer.user(repository_transfer.target),
        state: repository_transfer.state,
        criteria: :REQUEST,
        repository_transfer_id: repository_transfer.id,
        previous_owner: serializer.user(payload[:previous_owner])
      }

      publish(message, schema: "github.v1.RepositoryTransfer")
    end
  end
end
