# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("trade_restriction.flag") do |payload|
    reasons = { ip: "IP_ADDRESS", email: "EMAIL" }
    message = {
      account: serializer.user(payload[:user]),
      reason: reasons[payload[:reason]],
      country: payload[:country],
      region: payload[:region],
      ip_address: payload[:ip],
      email_address: payload[:email],
    }

    publish(message, schema: "github.trade_restrictions.v0.TradeRestrictionFlag")
  end

  subscribe("trade_restriction.unflag") do |payload|
    message = {
      account: serializer.user(payload[:user]),
      actor: serializer.user(payload[:actor]),
      reason: payload[:reason],
    }

    publish(message, schema: "github.trade_restrictions.v0.TradeRestrictionUnflag")
  end

  subscribe("trade_restriction.organization_enforce") do |payload|
    message = {
      organization: serializer.organization(payload[:organization]),
      actor: serializer.user(payload[:actor]),
      restriction_reason: payload[:restriction_reason],
      organization_member_count: payload[:organization_member_count],
      organization_public_repos_count: payload[:organization_public_repos_count],
      organization_private_repos_count: payload[:organization_private_repos_count],
      organization_trade_restricted_member_count:  payload[:organization_trade_restricted_member_count],
      organization_time_since_creation_in_days: payload[:organization_time_since_creation_in_days],
      organization_total_paid_amount_in_cents: payload[:organization_total_paid_amount_in_cents],
      email_address: payload[:email_address],
      percentage_of_trade_restricted_admins: payload[:percentage_of_trade_restricted_admins],
      website_url: payload[:website_url],
      location: payload[:location],
      percentage_of_trade_restricted_billing_managers: payload[:percentage_of_trade_restricted_billing_managers],
      percentage_of_trade_restricted_outside_collaborators: payload[:percentage_of_trade_restricted_outside_collaborators],
      percentage_of_trade_restricted_members: payload[:percentage_of_trade_restricted_members],
    }

    publish(message, schema: "github.trade_restrictions.v0.TradeRestrictionOrganizationEnforce")
  end

  subscribe("trade_restriction.organization_pending") do |payload|
    message = {
      organization: serializer.organization(payload[:organization]),
      pending_reason: payload[:pending_reason],
      organization_member_count: payload[:organization_member_count],
      organization_trade_restricted_member_count: payload[:organization_trade_restricted_member_count],
      organization_public_repos_count:  payload[:organization_public_repos_count],
      organization_private_repos_count: payload[:organization_private_repos_count],
      organization_time_since_creation_in_days: payload[:organization_time_since_creation_in_days],
      organization_total_paid_amount_in_cents: payload[:organization_total_paid_amount_in_cents],
      percentage_of_trade_restricted_outside_collaborators: payload[:percentage_of_trade_restricted_outside_collaborators],
      percentage_of_trade_restricted_members: payload[:percentage_of_trade_restricted_members],
      percentage_of_trade_restricted_admins: payload[:percentage_of_trade_restricted_admins],
      percentage_of_trade_restricted_billing_managers: payload[:percentage_of_trade_restricted_billing_managers],
      enforcement_date: payload[:enforcement_date],
    }

      publish(message, schema: "github.trade_restrictions.v0.TradeRestrictionOrganizationPending")
  end
end
