# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("browser.search_result.click") do |payload|
    message = {
      actor: serializer.user(payload[:client][:user]),
      client_timestamp: payload[:client][:timestamp].try(:to_i),
      server_timestamp: Time.zone.now,
      originating_request_id: payload[:originating_request_id],
      result: payload[:result],
      result_position: payload[:result_position],
      page_number: payload[:page_number],
      per_page: payload[:per_page],
      query: payload[:query],
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
    }

    publish(message, schema: "github.v1.SearchResultClick")
  end

  subscribe("search.execute") do |payload|
    if payload[:query]
      message = {
        actor: serializer.user(payload[:actor]),
        query: payload[:query],
        search_type: serializer.search_type(payload[:search_type]),
        search_context: payload[:search_context],
        results: payload[:results],
        total_results: payload[:total_results],
        search_server_took_ms: payload[:search_server_took_ms],
        search_server_timed_out: payload[:search_server_timed_out],
        max_score: payload[:max_score],
        page_number: payload[:page_number],
        per_page: payload[:per_page],
        request_context: serializer.request_context(GitHub.context.to_hash),
        es_query: payload[:es_query],
        originating_request_id: payload[:originating_request_id],
      }

      message[:search_scope] = payload[:search_scope].to_s if payload[:search_scope]
      message[:variant] = payload[:variant] if payload[:variant]

      publish(message, schema: "github.v1.Search")
    end
  end

  #### RepositoryChanged event subscriptions (non-analytics events) ####

  # Publish a RepositoryChanged message for Geyser code search indexing.
  # This is the default subcription for all event emit types that are
  # eligible for standard feature flag and repo-status publish gating
  subscribe("search_indexing.repository_changed") do |payload|
    next if payload.empty? || payload[:repository].blank?
    next unless GitHub.geyser_ingest_enabled?

    repository = payload[:repository]
    lab_scoped = payload[:lab_scoped].present?
    publisher = Search::Geyser::Publisher.override(payload)

    # bail early if the Repository should not be published
    next unless repository.geyser_indexing_enabled?

    # only publish public repos to lab env
    next if lab_scoped && !repository.public?

    # parameterize the target topic by intended Geyser env (lab or production)
    topic = if lab_scoped
      "github.search.v0.LabRepositoryChanged"
    else
      "github.search.v0.RepositoryChanged"
    end

    message = {
      change: payload.fetch(:change, :CHANGE_UNKNOWN),
      repository: serializer.repository(repository),
      owner_name: repository.owner.name,
      ref: payload[:ref]&.dup&.force_encoding(Encoding::UTF_8),
      updated_at: payload.fetch(:updated_at, Time.now.utc),
      old_owner_id: payload.fetch(:old_owner_id, 0),
      target_collections: payload.fetch(:target_collections, []),
    }

    if GitHub.tracing_enabled? && GitHub.tracer.last_span.present?
      GitHub.tracer.last_span.when_enabled do |span|
        carrier = {}
        GitHub.tracer.inject(span.context, OpenTracing::FORMAT_TEXT_MAP, carrier)
        message[:tracing_carrier] = carrier
      end
    end

    publish(message, schema: "github.search.v0.RepositoryChanged", partition_key: repository.id, topic: topic, publisher: publisher)
  end

  # Publish a RepositoryChanged message when a repo is enabled/disabled for Geyser code search indexing
  subscribe("search_indexing.repository_access_changed") do |payload|
    next if payload.empty? || payload[:repository].blank?
    next unless GitHub.geyser_ingest_enabled?

    repository = payload[:repository]
    lab_scoped = payload[:lab_scoped].present?
    publisher = Search::Geyser::Publisher.override(payload)

    # bail early if the Repository should not be published
    next unless repository.geyser_indexing_mutable_access_enabled?

    # only publish public repos to lab env
    next if lab_scoped && !repository.public?

    # parameterize the target topic by intended Geyser env (lab or production)
    topic = if lab_scoped
      "github.search.v0.LabRepositoryChanged"
    else
      "github.search.v0.RepositoryChanged"
    end

    message = {
      change: payload.fetch(:change, :CHANGE_UNKNOWN),
      repository: serializer.repository(repository),
      owner_name: repository.owner.name,
      ref: payload[:ref]&.dup&.force_encoding(Encoding::UTF_8),
      updated_at: payload.fetch(:updated_at, Time.now.utc),
      old_owner_id: payload.fetch(:old_owner_id, 0),
      target_collections: payload.fetch(:target_collections, []),
    }

    if GitHub.tracing_enabled? && GitHub.tracer.last_span.present?
      GitHub.tracer.last_span.when_enabled do |span|
        carrier = {}
        GitHub.tracer.inject(span.context, OpenTracing::FORMAT_TEXT_MAP, carrier)
        message[:tracing_carrier] = carrier
      end
    end

    publish(message, schema: "github.search.v0.RepositoryChanged", partition_key: repository.id, topic: topic, publisher: publisher)
  end

  # Publish a RepositoryChanged CREATED message for Geyser code search indexing, without typical flag gating
  subscribe("search_indexing.repository_created") do |payload|
    next if payload.empty? || payload[:repository].blank?
    next unless GitHub.geyser_ingest_enabled?

    repository = payload[:repository]
    lab_scoped = payload[:lab_scoped].present?
    publisher = Search::Geyser::Publisher.override(payload)

    # bail early if the owner isn't eligible for repo search
    if !GitHub.flipper[:geyser_index].enabled?(repository.owner) ||
        GitHub.flipper[:geyser_denylist].enabled?(repository.owner)
      next
    end

    # only publish public repos to lab env
    next if lab_scoped && !repository.public?

    # parameterize the target topic by intended Geyser env (lab or production)
    topic = if lab_scoped
      "github.search.v0.LabRepositoryChanged"
    else
      "github.search.v0.RepositoryChanged"
    end

    message = {
      change: payload.fetch(:change, :CHANGE_UNKNOWN),
      repository: serializer.repository(repository),
      owner_name: repository.owner.name,
      ref: payload[:ref]&.dup&.force_encoding(Encoding::UTF_8),
      updated_at: payload.fetch(:updated_at, Time.now.utc),
      old_owner_id: payload.fetch(:old_owner_id, 0),
      target_collections: payload.fetch(:target_collections, []),
    }

    if GitHub.tracing_enabled? && GitHub.tracer.last_span.present?
      GitHub.tracer.last_span.when_enabled do |span|
        carrier = {}
        GitHub.tracer.inject(span.context, OpenTracing::FORMAT_TEXT_MAP, carrier)
        message[:tracing_carrier] = carrier
      end
    end

    publish(message, schema: "github.search.v0.RepositoryChanged", partition_key: repository.id, topic: topic, publisher: publisher)
  end

  # Publish a RepositoryChanged DELETED message without typical gating for Geyser code search
  subscribe("search_indexing.repository_deleted") do |payload|
    next if payload.empty? || (payload[:repository_id].blank? && payload[:repository].blank?)
    next unless GitHub.geyser_ingest_enabled?

    repository = if payload[:repository_id].present?
      # For times when the repo has been removed and we no longer have access to it
      Repository.new(id: payload[:repository_id])
    else
      payload[:repository]
    end

    publisher = Search::Geyser::Publisher.override(payload)

    # parameterize the target topic by intended Geyser env (lab or production)
    topic = if payload[:lab_scoped].present?
      "github.search.v0.LabRepositoryChanged"
    else
      "github.search.v0.RepositoryChanged"
    end

    message = {
      change: payload.fetch(:change, :CHANGE_UNKNOWN),
      repository: serializer.repository(repository),
      ref: payload[:ref]&.dup&.force_encoding(Encoding::UTF_8),
      updated_at: payload.fetch(:updated_at, Time.now.utc),
      old_owner_id: payload.fetch(:old_owner_id, 0),
      target_collections: payload.fetch(:target_collections, []),
    }
    message[:owner_name] = repository.owner.name unless repository.owner.nil?

    if GitHub.tracing_enabled? && GitHub.tracer.last_span.present?
      GitHub.tracer.last_span.when_enabled do |span|
        carrier = {}
        GitHub.tracer.inject(span.context, OpenTracing::FORMAT_TEXT_MAP, carrier)
        message[:tracing_carrier] = carrier
      end
    end

    publish(message, schema: "github.search.v0.RepositoryChanged", partition_key: repository.id, topic: topic, publisher: publisher)
  end
end
