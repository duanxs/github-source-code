# rubocop:disable Style/FrozenStringLiteralComment

GitHub.subscribe "contact_form.blacklisted" do |name, start, ending, transaction_id, payload|
  GitHub.dogstats.increment("contact_form.blacklisted")
end

GitHub.subscribe "contact_form.honeypot" do |name, start, ending, transaction_id, payload|
  GitHub.dogstats.increment("contact_form.honeypot")
end

GitHub.subscribe "contact_form.spam" do |name, start, ending, transaction_id, payload|
  GitHub.dogstats.increment("contact_form.spam")
end

GitHub.subscribe "contact_form.success" do |name, start, ending, transaction_id, payload|
  tags = payload.map { |k, v| "#{k}:#{v}" }
  GitHub.dogstats.increment("contact_form.success", tags: tags)
end

GitHub.subscribe "contact_form.ratelimited" do |name, start, ending, transaction_id, payload|
  GitHub.dogstats.increment("contact_form.ratelimited")
end
