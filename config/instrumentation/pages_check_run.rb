# frozen_string_literal: true

GitHub.subscribe("check_run.rerequest") do |*args|
  event = ActiveSupport::Notifications::Event.new(*args)
  PagesCheckReRun::Service.run(
    event.payload[:check_run_id],
    event.payload[:actor_id],
  )
end
