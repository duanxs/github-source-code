# rubocop:disable Style/FrozenStringLiteralComment

GitHub.subscribe "user_session.create" do |event, start, ending, transaction_id, payload|
  GitHub.dogstats.increment "user_session", tags: ["action:create"]
end
