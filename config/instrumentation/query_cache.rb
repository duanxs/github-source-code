# frozen_string_literal: true

class QueryCacheLogSubscriber < ActiveSupport::LogSubscriber
  class << self
    attr_accessor :track, :cache_hit_count, :cache_hits
  end

  def self.reset_stats
    self.cache_hit_count = 0
    self.cache_hits = []
  end
  reset_stats

  def self.with_track
    original = self.track
    self.track = true
    yield
  ensure
    self.track = original
  end


  def sql(event)
    return unless event.payload[:cached]
    self.class.cache_hit_count += 1

    return unless self.class.track
    self.class.cache_hits << query_for_event(event)
  end

  attach_to :active_record

  private

  def query_for_event(event)
    sql = event.payload[:sql]
    result_count = event.payload[:result_count]

    GitHub::MysqlInstrumenter::Query.new(
      sql: sql,
      result_count: result_count,
      duration: nil,
      connection_url: event.payload[:connection_url],
      connected_host: event.payload[:connected_host],
      on_primary: false,
    )
  end
end
