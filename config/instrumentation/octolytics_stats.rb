# frozen_string_literal: true
# Instrument Octolytics metrics here. Instrument Octolytics events in
# octolytics.rb.

# Measure the middleware's flush time
GitHub.subscribe "octolytics.flush_queue" do |name, start, ending, transaction_id, payload|
  duration_ms = (ending - start) * 1_000
  GitHub.dogstats.timing("octolytics.flush_queue", duration_ms)
end
