# rubocop:disable Style/FrozenStringLiteralComment

module GitRPCInstrumentation
  DEFAULT_SAMPLE_RATE = 1

  SAMPLE_RATES = {
    exists: 0.5,
    online: 0.5,
    spawn_git_ro: 0.5,
  }.freeze

  BERT_SAMPLE_RATE = 0.05

  RPC_STORE_GITRPC = "rpc_store:gitrpc".freeze

  # Utility class to help us generate canonical descriptions for our gitrpc calls.
  class RemoteCallEvent
    attr_reader :name, :type, :argv, :event

    def initialize(*args)
      @event = ActiveSupport::Notifications::Event.new *args

      case @event.payload[:name].to_s
      when "spawn", "spawn_ro"
        @type = "spawn".freeze
        @argv = @event.payload[:args].first
        @name = argv.first
      when "spawn_git", "spawn_git_ro"
        @type = "spawn".freeze
        @name = @event.payload[:args].first
      else
        @type = "call".freeze
        @name = @event.payload[:name]
      end
    end

    def payload
      @event.payload
    end

    def duration
      @event.duration
    end

    def long_name
      @argv || @name
    end

    def rpc_operation
      @event.payload[:name] || "unknown".freeze
    end
  end

  ActiveSupport::Notifications.subscribe "bert_encode.gitrpc" do |*args|
    event = ActiveSupport::Notifications::Event.new *args
    GitHub.dogstats.timing("rpc.bert.encode".freeze, event.duration,
                           sample_rate: BERT_SAMPLE_RATE, tags: [RPC_STORE_GITRPC])
  end

  ActiveSupport::Notifications.subscribe "bert_encode_size.gitrpc" do |*args|
    event = ActiveSupport::Notifications::Event.new *args
    GitHub.dogstats.timing("rpc.bert.encode_size".freeze, event.payload[:size],
                           sample_rate: BERT_SAMPLE_RATE, tags: [RPC_STORE_GITRPC])
  end

  ActiveSupport::Notifications.subscribe "bert_decode.gitrpc" do |*args|
    event = ActiveSupport::Notifications::Event.new *args
    GitHub.dogstats.timing("rpc.bert.decode".freeze, event.duration,
                           sample_rate: BERT_SAMPLE_RATE, tags: [RPC_STORE_GITRPC])
  end

  ActiveSupport::Notifications.subscribe "bert_decode_size.gitrpc" do |*args|
    event = ActiveSupport::Notifications::Event.new *args
    GitHub.dogstats.timing("rpc.bert.decode_size".freeze, event.payload[:size],
                           sample_rate: BERT_SAMPLE_RATE, tags: [RPC_STORE_GITRPC])
  end

  ActiveSupport::Notifications.subscribe "bertrpc_send_message.gitrpc" do |*args|
    event = RemoteCallEvent.new *args

    dog_tags = GitRPCLogSubscriber.stats_tags + ["rpc_operation:#{event.rpc_operation}"]

    # We don't always have a route, but when we do, it would be nice to know
    # what datacenter we're talking to. I think the only case where we don't
    # have a route is when BERTRPC is used directly, like in tests.
    if event.payload[:route]&.respond_to? :datacenter
      dc = event.payload[:route].datacenter
      dog_tags << "destination_dc:#{dc}"
    end

    GitHub.dogstats.timing(
      "rpc.bert.send_message.time".freeze,
      event.duration,
      sample_rate: 0.25,
      tags: dog_tags)

    unless event.payload[:exception].nil?
      error_tags = dog_tags.dup

      exc_name, exc_message = event.payload[:exception]
      case exc_name
      when GitRPC::Failure.name
        error_tags << "error:failure"
      when GitRPC::ObjectMissing.name, GitRPC::InvalidObject.name, GitRPC::NoSuchPath.name, GitRPC::BadRevision.name
        error_tags << "error:not_found"
      when GitRPC::CommandFailed.name
        error_tags << "error:command"
      when GitRPC::CommandBusy.name
        error_tags << "error:busy"
      when BERTRPC::ReadTimeoutError.name, GitRPC::Timeout.name
        error_tags << "error:timeout"
      when BERTRPC::ReadError.name, BERTRPC::ProtocolError.name
        error_tags << "error:network"
      when BERTRPC::ConnectionError.name
        error_tags << "error:connection"
      else
        error_tags << "error:other"
      end

      GitHub.dogstats.increment(
        "rpc.bert.send_message.error".freeze,
        tags: error_tags)
    end
  end

  ActiveSupport::Notifications.subscribe "bertrpc_send_message.gitrpc" do |*args|
    event = RemoteCallEvent.new *args

    # Some tags should be set at span creation time because they may impact
    # properties like span ids depending on the type of tracer.
    tags = {
      "component" => "gitrpc",
      "span.kind" => "client",
    }

    span = GitHub.tracer.start_span(event.rpc_operation, start_time: event.event.time, tags: tags)
    span.when_enabled do
      span.set_tag("gitrpc.type", event.type)

      if event.payload[:route]
        span.set_tag("peer.host", event.payload[:route].host)
        span.set_tag("peer.dc", event.payload[:route].datacenter) if event.payload[:route].respond_to? :datacenter
      end

      if event.payload[:exception]
        exc_name, exc_message = event.payload[:exception]
        # Setting tags in this way seems consistent with hubstep, but it does
        # not match the semantic recommendations from OpenTracing which would
        # be a log event.
        span.set_tag("error", true)
        span.set_tag("error.object", exc_name)
        span.set_tag("error.message", exc_message)
      end
    end

    span.finish(end_time: event.event.end)
  end

  ActiveSupport::Notifications.subscribe "remote_call.gitrpc" do |*args|
    event = RemoteCallEvent.new *args

    remote_call_dogstats(event)
  end

  def self.remote_call_dogstats(call_event)
    sample_rate = SAMPLE_RATES[call_event.rpc_operation] || DEFAULT_SAMPLE_RATE

    dog_tags = GitRPCLogSubscriber.stats_tags + ["rpc_operation:#{call_event.rpc_operation}"]
    GitHub.dogstats.timing(
      "rpc.git.time".freeze,
      call_event.duration,
      tags: dog_tags,
      sample_rate: sample_rate,
    )

    GitHub.dogstats.distribution("rpc.git.dist.time".freeze, call_event.duration, tags: dog_tags)
  end


  class Logging < ActiveSupport::LogSubscriber
    COLUMN_FORMAT = "%s (%3.1fms)".freeze
    UNDERLINE = "\e[2m".freeze

    def remote_call(event)
      write_log(event, "Remote Call".freeze, RED)
    end

    def online_check(event)
      write_log(event, "Online Check".freeze, GREEN)
    end

    attach_to :gitrpc

    private

    # Write a debug message to the Rails log.
    def write_log(event, action_text, color)
      return unless logger.debug?

      log_text = COLUMN_FORMAT % ["GitRPC #{action_text}", event.duration]
      log_text = color(color(log_text, color, true), UNDERLINE, true)
      log_text = "  #{log_text}"

      if event.payload.any?
        payload = event.payload
        info = color(payload[:name], BOLD, true)
        args = payload[:args] || []
        args = decorate_argument_array(args)
        args.map! { |a| color(a.inspect, BOLD, true) }
        info = "#{info} (#{args.join(', ')})" if args.any?
        log_text = "#{log_text}  #{info}"
      end

      debug log_text
    end

    # Format the elements of array, condensing 40 char SHA1 strings to 8
    # characters.
    def decorate_argument_array(array)
      array.map do |a|
        if a.is_a?(String) && a =~ /\A[0-9a-f]{40}\Z/
          "#{a[0, 8]}"
        elsif a.is_a?(Array)
          decorate_argument_array(a)
        else
          a
        end
      end
    end
  end
end

class GitRPCLogSubscriber < ActiveSupport::LogSubscriber
  CallStats = Struct.new(:count, :time)

  class << self
    attr_accessor :rpc_time, :rpc_count, :rpc_calls, :rpc_call_stats, :track, :tags, :stats_tags
  end

  def self.reset_stats
    self.rpc_time = self.rpc_count = 0
    self.rpc_call_stats = Hash.new { |h, k| h[k] = CallStats.new(0, 0) }
    self.rpc_calls = []
    self.tags = []
    self.stats_tags = []
  end
  reset_stats

  def self.with_track(&block)
    prev_track = self.track
    begin
      self.track = true
      block.call
    ensure
      self.track = prev_track
    end
  end

  def remote_call(event)
    if self.class.track
      self.class.rpc_calls << EventTrace.new(event.payload[:name], event.duration, event.payload[:args], tags: self.class.tags.dup)
    end

    call_event = GitRPCInstrumentation::RemoteCallEvent.new(event.name, event.time, event.end, event.transaction_id, event.payload)

    stats = self.class.rpc_call_stats[call_event.long_name]
    stats.time += event.duration / 1000.0
    stats.count += 1
    self.class.rpc_time += event.duration / 1000.0
    self.class.rpc_count += 1
  end

  attach_to :gitrpc
end
