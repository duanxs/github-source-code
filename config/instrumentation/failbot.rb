# rubocop:disable Style/FrozenStringLiteralComment

Failbot.instrumenter = ActiveSupport::Notifications
# Record stats for reported exceptions.
#
# data                   - a Hash of reported exception data
# data["app"]            - the bucket that the exception was reported to (.e.g. "github",
#                         "github-user", etc)
# data["report_status"]  - whether or not the report succeeded (`success` or `error`)
# data["elapsed_ms"]     - Time to send report in milliseconds
# data["exception_type"] - If the status is `failure`, then the exception that was
#                          thrown when the report failed.
GitHub.subscribe "report.failbot" do |name, start, ending, transaction_id, data|
  if GitHub.enterprise?
    GitHub.stats.increment "exception.#{data["app"]}.count"
  else
    tags = [
      "application:#{data["app"]}",
      "status:#{data["report_status"]}",
    ]
    tags << "exception:#{data["exception_type"]}" if data.key?("exception_type")
    GitHub.dogstats.distribution("failbot.report", data["elapsed_ms"], tags: tags)
  end
end
