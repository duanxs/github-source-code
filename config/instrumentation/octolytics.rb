# rubocop:disable Style/FrozenStringLiteralComment

# See docs/octolytics.md to learn how to send your own events to Octolytics.

# These events are enqueued directly into GitHub.analytics_queue, flushed to
# Resque at the end of the request by FlushOctolyticsEventsMiddleware, then
# sent to the analytics pipeline by the PublishOctolyticsEvents Resque job.

# If you add an event here, don't forget to add a test in
# octolytics_instrumentation_test.rb!

# See hydro_stats.rb for instrumenting octolytics/hydro metrics.

events = [
  "advisory_credit.accept",
  "advisory_credit.create",
  "advisory_credit.decline",
  "advisory_credit.destroy",
  "advisory_credit.notify",
  "combined_vulnerability_alert_email.delivery",
  "integration.listing_authorized",
  "integration.listing_install_click",
  "integration.listing_learn_more_click",
  "issue.create",
  "issues.first_time_contributor_banner",
  "marketplace_onboarding.progress",
  "marketplace.new_repo_quick_install",
  "marketplace_app_selector.opened",
  "marketplace_blog_cta.clicked",
  "marketplace_purchase.purchased",
  "marketplace_cta.view",
  "marketplace_listing.state_change",
  "marketplace_order_preview.deleted",
  "pull_requests.first_time_contributor_banner",
  "label.create",
  "news_feed.event",
  "org.delete",
  "org.enable_saml",
  "org.disable_saml",
  "org.update_saml_provider_settings",
  "org.display_commenter_full_name_disabled",
  "org.display_commenter_full_name_enabled",
  "organization_default_label.create",
  "organization_default_label.destroy",
  "organization_default_label.update",
  "oss_survey.qualify",
  "repo.archived",
  "repo.create",
  "repo.destroy",
  "repo.archived",
  "repo.maintainer_label_education",
  "repository_actions.index_page_view",
  "repository_content_analysis.enable",
  "repository_content_analysis.disable",
  "repository_dependency_graph.enable",
  "repository_dependency_graph.disable",
  "repository_secret_scanning.enable",
  "repository_secret_scanning.disable",
  "repository_vulnerability_alert.alert_seen",
  "repository_vulnerability_alert.create",
  "repository_vulnerability_alert.resolve",
  "repository_vulnerability_alert.dismiss",
  "repository_vulnerability_alerts.enable",
  "repository_vulnerability_alerts.digest_sent",
  "repository_vulnerability_alerts.disable",
  "repository_vulnerability_alerts.authorized_users_teams",
  "repo.unarchived",
  "review_request.create",
  "user.delete",
  "user.follow",
  "user_experiment.success",
  "dashboard.discovery_recommendations",
  "vulnerability.withdraw",
]

events.each do |event|
  GitHub.subscribe(event) do |*event_args|
    GitHub.analytics_queue << event_args
  end
end

GitHub.subscribe("stars.star") do |event, start, ending, transaction_id, payload|
  GitHub.analytics_queue << [event, start, ending, transaction_id, payload.slice(:dimensions)]
end
