# rubocop:disable Style/FrozenStringLiteralComment

if GitHub.enterprise?
  GitHub.subscribe "user_session.access" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_active_session(user)
  end

  GitHub.subscribe "blob.crud" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_web_edit(user)
  end

  GitHub.subscribe "branch.create" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_web_edit(user)
  end

  GitHub.subscribe "branch.delete" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_web_edit(user)
  end

  GitHub.subscribe "tag.create" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_web_edit(user)
  end

  GitHub.subscribe "pull_request.create" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_pull_request(user)
  end

  GitHub.subscribe "issue.create" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_issue(user)
  end

  GitHub.subscribe "comment.create" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_comment(user)
  end

  GitHub.subscribe "git.push" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:pusher_id])
    Interaction.track_push(user)
  end

  GitHub.subscribe /wiki\.(create|update|revert|destroy)/ do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:user_id])
    Interaction.track_wiki_edit(user)
  end

  GitHub.subscribe "release.create" do |event, start, ending, transaction_id, payload|
    user = User.find_by_id(payload[:actor_id])
    Interaction.track_release(user)
  end

  GitHub.subscribe "public_key.access" do |event, start, ending, transaction_id, payload|
    GitHub.stats.increment("public_key.access.count")
  end
end
