# frozen_string_literal: true

GitHub.subscribe /optimizely/ do |name, start, ending, transaction_id, payload|
  GitHub.dogstats.increment(name)
end
