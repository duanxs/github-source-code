# rubocop:disable Style/FrozenStringLiteralComment

class Redis::Client
  class << self
    attr_accessor :query_time, :query_count, :queries, :track

    def reset_stats
      self.query_time = self.query_count = 0
      self.queries = []
    end
  end
  reset_stats

  module RedisClientInstrumentation
    # Private: Value between 0 and 1 that is the sample rate for the metrics
    # produced by this class.
    STATS_SAMPLE_RATE = 0.5

    DOT = ".".freeze
    DASH = "-".freeze

    def call(cmd, *args, &block)
      start = Time.now
      name = Array(cmd).first
      GitHub.tracer.with_span("rpc.redis.#{name}", enable: GitHub.verbose_tracing?) do |span|
        span.set_tag("component".freeze, "redis".freeze)
        super(cmd, *args, &block)
      end
    ensure
      duration = (Time.now - start)
      duration_ms = duration * 1_000

      if command_name = Array(cmd).first
        tags = [
          "rpc_operation:#{command_name}",
          "rpc_host:#{host}",
          "rpc_site:#{GitHub.site_from_host(host)}",
        ]

        GitHub.dogstats.timing("rpc.redis.time".freeze, duration_ms, sample_rate: STATS_SAMPLE_RATE, tags: tags)
        GitHub.dogstats.distribution("rpc.redis.dist.time".freeze, duration_ms, tags: tags)
      end
      if self.class.track
        self.class.queries << EventTrace.new(cmd, duration, args)
      end
      Redis::Client.query_time += duration
      Redis::Client.query_count += 1
    end

    def call_pipelined(cmd, *args, &block)
      start = Time.now
      GitHub.tracer.with_span("redis.pipelined".freeze, enable: GitHub.verbose_tracing?) do |span|
        span.set_tag("component".freeze, "redis".freeze)
        super(cmd, *args, &block)
      end
    ensure
      duration = (Time.now - start)
      duration_ms = duration * 1_000
      tags = [
        "rpc_operation:pipelined".freeze,
        "rpc_host:#{host}",
        "rpc_site:#{GitHub.site_from_host(host)}",
      ]

      GitHub.dogstats.timing("rpc.redis.time".freeze, duration_ms, sample_rate: STATS_SAMPLE_RATE, tags: tags)
      GitHub.dogstats.distribution("rpc.redis.dist.time".freeze, duration_ms, tags: tags)

      cmd.each do |command|
        command_name = command[0]

        tags = [
          "rpc_operation:#{command_name}",
          "rpc_host:#{host}",
          "rpc_site:#{GitHub.site_from_host(host)}",
        ]

        GitHub.dogstats.increment("rpc.redis.pipelined".freeze,
          sample_rate: STATS_SAMPLE_RATE,
          tags: tags)
      end

      if self.class.track
        self.class.queries << EventTrace.new("(pipelined)", duration, cmd)
      end
      Redis::Client.query_time += duration
      Redis::Client.query_count += 1
    end
  end

  prepend RedisClientInstrumentation
end
