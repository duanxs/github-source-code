# rubocop:disable Style/FrozenStringLiteralComment

GitHub.subscribe "user_experiment_enrollment.create" do |name, start, ending, transaction_id, payload|
  GitHub.dogstats.increment "user_experiment_enrollment.#{payload[:user_experiment_slug]}.#{payload[:subgroup]}"
  GitHub::Logger.log(payload.merge({category: "experiment", at: :enrollment}))
end

GitHub.subscribe "user_experiment_display.create" do |name, start, ending, transaction_id, payload|
  GitHub.dogstats.increment "user_experiment_display.#{payload[:user_experiment_slug]}.displayed"
end

GitHub.subscribe "user_experiment_display.ignore" do |name, start, ending, transaction_id, payload|
  GitHub.dogstats.increment "user_experiment_display.#{payload[:user_experiment_slug]}.ignored"
end
