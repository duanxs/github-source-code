# frozen_string_literal: true

return unless GitHub.billing_enabled?

TRADE_RESTRICTION_REASONS = %w[
  ip
  email
  organization_member
  organization_outside_collaborator
  organization_admin
  organization_billing_manager
  website_url
]

GlobalInstrumenter.subscribe "user.signup" do |_, _, _, _, payload|
  actor = payload[:actor]
  unless actor.trade_controls_restriction.any?
    enqueue_user_compliance_check(actor, email: payload[:signup_email]&.email)
  end
end

GitHub.subscribe "user.create" do |_, _, _, _, payload|
  actor_id = payload[:actor_id]
  actor = User.find(actor_id)

  unless actor.trade_controls_restriction.any?
    actor_ip = GitHub.context.to_hash[:actor_ip]
    enqueue_user_compliance_check(actor, ip: actor_ip)
  end
end


GlobalInstrumenter.subscribe "user.user_country_change" do |_, _, _, _, payload|
  actor = payload[:actor]
  unless actor.trade_controls_restriction.any?
    actor_ip = GitHub.context.to_hash[:actor_ip]
    enqueue_user_compliance_check(actor, ip: actor_ip)
  end
end

GlobalInstrumenter.subscribe "user.login" do |_, _, _, _, payload|
  actor = payload[:actor]
  unless actor.trade_controls_restriction.any?
    actor_ip = GitHub.context.to_hash[:actor_ip]
    enqueue_user_compliance_check(actor, ip: actor_ip)
  end
end

GlobalInstrumenter.subscribe "user.last_ip_update" do |_, _, _, _, payload|
  actor = payload[:actor]
  unless actor.trade_controls_restriction.any?
    enqueue_user_compliance_check(actor, ip: payload[:current_last_ip])
  end
end

GlobalInstrumenter.subscribe "user.add_email" do |_, _, _, _, payload|
  user = payload[:user]
  unless user.trade_controls_restriction.any?
    enqueue_user_compliance_check(user, email: payload[:added_email]&.email)
  end
end

# Report trade controls events to dogstats
GitHub.subscribe(/^trade_controls_restriction\./) do |event, _, _, _, payload|
  # Keep reason tag to low cardinality
  payload[:reason] = :manual unless TRADE_RESTRICTION_REASONS.include?(payload[:reason].to_s)

  tags = payload.slice(:country_code, :reason).map { |t| t.join(":") }

  GitHub.dogstats.increment(event, tags: tags)
end

GlobalInstrumenter.subscribe "profile.update" do |_, _, _, _, payload|
  account = payload[:current_profile].user
  if account.organization? && payload[:changed_attribute_names].include?("blog")
    enqueue_org_compliance_check(account.id, website_url: account.profile_blog)
  end
end

GlobalInstrumenter.subscribe "profile.update" do |_, _, _, _, payload|
  account = payload[:current_profile].user
  if account.organization? && payload[:changed_attribute_names].include?("email")
    if GitHub.flipper[:email_compliance_refactor].enabled?(account)
      enqueue_org_compliance_check(account.id, reason: :organization_profile_email)
    else
      enqueue_org_compliance_check(account.id, email: account.profile_email)
    end
  end
end

GitHub.subscribe "billing.change_email" do |_, _, _, _, payload|
  if GitHub.flipper[:email_compliance_refactor].enabled?
    enqueue_org_compliance_check(payload[:org_id], reason: :organization_billing_email)
  else
    billing_email = payload[:email]
    enqueue_org_compliance_check(payload[:org_id], email: billing_email)
  end
end

GitHub.subscribe "org.add_billing_manager" do |_, _, _, _, payload|
  enqueue_org_compliance_check(payload[:org_id], reason: :organization_billing_manager)
end

GitHub.subscribe "org.remove_billing_manager" do |_, _, _, _, payload|
  enqueue_org_compliance_check(payload[:org_id], reason: :organization_billing_manager)
end

GlobalInstrumenter.subscribe "org.add_member" do |_, _, _, _, payload|
  if payload[:permission] == :admin
    enqueue_org_compliance_check(payload[:org].id, reason: :organization_admin)
  end
end

GitHub.subscribe "org.update_member" do |_, _, _, _, payload|
  if payload[:permission] == :admin
    enqueue_org_compliance_check(payload[:org_id], reason: :organization_admin)
  end
end

GlobalInstrumenter.subscribe "org.add_member" do |_, _, _, _, payload|
  enqueue_org_compliance_check(payload[:org].id, reason: :organization_member)
end

def enqueue_org_compliance_check(org, **kwargs)
    TradeControls::OrganizationComplianceCheckJob.perform_later(org, **kwargs)
end

def enqueue_user_compliance_check(user, **kwargs)
  TradeControls::ComplianceCheckJob.perform_later(user, **kwargs)
end
