# rubocop:disable Style/FrozenStringLiteralComment

# Reporting timing info for resque related events
require "github/config/notifications"
require "github/config/stats"

GitHub.subscribe "queued.resque" do |_, start, ending, _, payload|
  class_name, queue = payload.values_at(:class, :queue)
  duration_ms = (ending - start) * 1_000
  tags = [
    "class:#{class_name}",
    "queue:#{queue}",
    "backend:#{payload[:backend]}",
  ]
  GitHub.dogstats.timing("job.time_enqueued", duration_ms, { tags: tags })
end

GitHub.subscribe "performed.resque" do |_, start, ending, _, payload|
  class_name, queue, success, db_counts = payload.values_at(:class, :queue, :success, :db_counts)
  duration_ms = (ending - start) * 1_000
  tags = [
    "class:#{class_name}",
    "queue:#{queue}",
    "status:#{success ? "success" : "failed"}",
  ]
  tags_with_backend = tags + ["backend:#{payload[:backend]}"]

  GitHub.dogstats.timing("job.time", duration_ms, { tags: tags_with_backend })

  job_allocations = GC.stat(:total_allocated_objects) - payload[:pre_perform_allocation_count]
  GitHub.dogstats.gauge "job.gc.eden_pages", GC.stat(:heap_eden_pages), tags: tags
  GitHub.dogstats.gauge "job.gc.live_slots", GC.stat(:heap_live_slots), tags: tags
  GitHub.dogstats.gauge "job.gc.free_slots", GC.stat(:heap_free_slots), tags: tags
  GitHub.dogstats.gauge "job.gc.allocations", job_allocations, tags: tags

  db_counts ||= {}
  db_counts.each do |db_host, counts|
    counts ||= {}
    tags_with_host = tags + ["rpc_host:#{db_host}"]

    GitHub.dogstats.count("job.rpc.mysql.count.reads", counts[:read].to_i, tags: tags_with_host)
    GitHub.dogstats.count("job.rpc.mysql.count.writes", counts[:write].to_i, tags: tags_with_host)
  end

  initial_mem_usage, current_mem_usage = GitHub::JobStats.memory_usage_report
  if initial_mem_usage.present? && current_mem_usage.present?
    usage_delta = current_mem_usage.delta(initial_mem_usage)
    GitHub.dogstats.distribution("job.mem.rss_delta", usage_delta.rss, tags: tags)
    GitHub.dogstats.distribution("job.mem.shared_delta", usage_delta.shared, tags: tags)
    GitHub.dogstats.distribution("job.mem.private_delta", usage_delta.priv, tags: tags)
  end
end

if Rails.staging? || GitHub.enterprise?
  GitHub.subscribe /\.resque\Z/ do |name, start, ending, _, payload|
    ms = 1000.0 * (ending - start)
    Rails.logger.info "#{start} [metrics #{name.split('.').reverse.join('.')}]: #{payload.inspect} #{ms}ms"
  end
else
  GitHub.subscribe "queued.resque" do |_, start, ending, _, payload|
    formatted_class = payload[:class].tr("/", "-")
    sec = ending - start

    # We need to maintain old compatibility with graphite keys
    JobReporter.handle_queue(payload.merge(class: formatted_class), sec)
  end

  GitHub.subscribe "performed.resque" do |_, start, ending, _, payload|
    formatted_class = payload[:class].tr("/", "-")
    sec = ending - start
    # We need to maintain old compatibility with graphite keys
    JobReporter.handle_job(payload.merge(class: formatted_class), sec)
  end
end

GitHub.subscribe("enqueue.rock_queue") do |_, _, _, _, payload|
  if payload[:job_class].respond_to?(:active_job_class)
    GitHub::JobStats.record_enqueue(job_class: payload[:job_class].active_job_class)
  end
end

GitHub.subscribe("enqueue.active_job") do |*args|
  event = ActiveSupport::Notifications::Event.new(*args)

  event.payload[:job].tap do |job|
    GitHub.dogstats.increment("active_job.enqueued", tags: job.all_stats_tags)
    GitHub.dogstats.timing("active_job.enqueue.time", event.duration, tags: event.payload[:job].all_stats_tags)
    GitHub.dogstats.distribution("active_job.enqueue.dist.time", event.duration, tags: event.payload[:job].all_stats_tags)
    GitHub::JobStats.record_enqueue(job_class: job.class)
  end
end

GitHub.subscribe("enqueue_at.active_job") do |*args|
  event = ActiveSupport::Notifications::Event.new(*args)

  event.payload[:job].tap do |job|
    GitHub.dogstats.increment("active_job.enqueued_at", tags: job.all_stats_tags)
    GitHub.dogstats.timing("active_job.enqueue.time", event.duration, tags: event.payload[:job].all_stats_tags)
    GitHub.dogstats.distribution("active_job.enqueue.dist.time", event.duration, tags: event.payload[:job].all_stats_tags)
    GitHub::JobStats.record_enqueue(job_class: job.class)
  end
end

GitHub.subscribe("perform_start.active_job") do |_, _, _, _, payload|
  GitHub.dogstats.increment("active_job.performed", tags: payload[:job].all_stats_tags)
end

GitHub.subscribe("perform.active_job") do |*args|
  event = ActiveSupport::Notifications::Event.new(*args)
  GitHub.dogstats.timing("active_job.perform.time", event.duration, tags: event.payload[:job].all_stats_tags)
  GitHub.dogstats.distribution("active_job.perform.dist.time", event.duration, tags: event.payload[:job].all_stats_tags)
end

GitHub.subscribe("error.active_job") do |*, payload|
  payload.values_at(:job, :error).tap do |job, error|
    GitHub.dogstats.increment("active_job.error", tags: job.all_stats_tags(error: error))
  end
end

GitHub.subscribe("discard.active_job") do |*, payload|
  payload.values_at(:job, :error).tap do |job, error|
    GitHub.dogstats.increment("active_job.discard", tags: job.all_stats_tags(error: error))
  end
end

GitHub.subscribe("enqueue_retry.active_job") do |*, payload|
  payload.values_at(:job, :error).tap do |job, error|
    GitHub.dogstats.increment("active_job.retry", tags: job.all_stats_tags(error: error))
  end
end

GitHub.subscribe("retry_stopped.active_job") do |*, payload|
  payload.values_at(:job, :error).tap do |job, error|
    GitHub.dogstats.increment("active_job.stop_retry", tags: job.all_stats_tags(error: error))
  end
end

module JobReporter
  NEWSLETTER_DELIVERY_RUN_JOB_CLASS_NAME = "github-jobs-newsletter_delivery_run".freeze
  REFRESH_SPONSORS_MEMBERSHIPS_CRITERIA_JOB_CLASS_NAME =
    "github-jobs-refresh_sponsors_memberships_criteria_job".freeze

  BACKGROUND_DESTROY_QUEUE_NAME = "background_destroy".freeze
  BACKUP_QUEUE_TYPE = "backup".freeze
  MAINT_QUEUE_TYPE = "maint".freeze
  VERIFY_QUEUE_TYPE = "verify".freeze

  DEFAULT_MAX_JOB_DURATION = 10.minutes
  MAX_JOB_DURATIONS = {
    NEWSLETTER_DELIVERY_RUN_JOB_CLASS_NAME => 30.minutes,
    REFRESH_SPONSORS_MEMBERSHIPS_CRITERIA_JOB_CLASS_NAME => 30.minutes,
  }.freeze

  DEFAULT_MAX_QUEUE_DELAY = 1.minute
  MAX_QUEUE_DELAYS = {
    BACKGROUND_DESTROY_QUEUE_NAME => 10.minutes,
    BACKUP_QUEUE_TYPE => 10.minutes,
    MAINT_QUEUE_TYPE => 10.minutes,
  }.freeze

  class Error < StandardError
  end

  # Special error classes for tracking jobs that are taking a long time to run
  # or are sitting in the queue for a long time
  class LongRun < Error
  end

  class LongDelay < Error
  end

  # Threshold, in seconds, to decide a job has been running long enough to report
  def self.threshold_for_job_run(job)
    MAX_JOB_DURATIONS.fetch(job, DEFAULT_MAX_JOB_DURATION)
  end

  # Threshold, in seconds, to decide a job has been queued long enough to report
  def self.threshold_for_queue_delay(queue)
    queue_type = queue_type(queue)
    MAX_QUEUE_DELAYS.fetch(queue_type, DEFAULT_MAX_QUEUE_DELAY)
  end

  # Some queues come in batches, like fs-related queues where each server gets its own
  def self.queue_type(queue)
    case queue
    when /\Abackup_/ then BACKUP_QUEUE_TYPE
    when /\Agitbackups_/ then BACKUP_QUEUE_TYPE
    when /\Amaint_/  then MAINT_QUEUE_TYPE
    when /\Averify_/ then VERIFY_QUEUE_TYPE
    else queue
    end
  end

  def self.handle_job(payload, sec)
    return if skip_exception_reporting?(payload[:queue])
    return unless threshold = threshold_for_job_run(payload[:class])
    return unless sec >= threshold

    sec = "%.2f" % sec
    e = LongRun.new "#{payload[:class]} took #{sec}s"
    e.set_backtrace(caller)

    # roll up long-running job needles based on job class
    report(e, rollup: payload[:class])
  end

  def self.handle_queue(payload, sec)
    return if skip_exception_reporting?(payload[:queue])
    return unless threshold = threshold_for_queue_delay(payload[:queue])
    return unless sec >= threshold

    sec = "%.2f" % sec
    e = LongDelay.new "#{payload[:class]} queued for #{sec}s on #{payload[:queue]}"
    e.set_backtrace(caller)

    # roll up long-delayed job needles based on queue
    report(e, rollup: queue_type(payload[:queue]))
  end

  # report to Haystack, don't raise
  def self.report(e, rollup:)
    return if ENV.fetch("GH_PERF_NEEDLES_DISABLED", 0) != 0

    Failbot.report!(e,
      app: "github-queue-health",
      rollup: Digest::MD5.hexdigest([e.class.name, rollup].join("|")),
    )
  end

  def self.skip_exception_reporting?(queue_name)
   queue_type = queue_type(queue_name)
   queue_types = [BACKUP_QUEUE_TYPE, MAINT_QUEUE_TYPE]
   queue_types.include?(queue_type)
  end
end
