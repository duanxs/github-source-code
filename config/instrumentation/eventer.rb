# frozen_string_literal: true

module Eventer
  class EventForwarder
    def self.configure(source:, &block)
      instance = new(source)
      instance.instance_exec(&block)
    end

    def initialize(instrumenter)
      @instrumenter = instrumenter
      @role_tag = "role:#{GitHub.role}"
    end

    def publish(payload, options = {})
      result = GitHub.hydro_publisher.publish(payload, **options)
      if !result.success?
        report_error(result.error, options.slice(:schema))
      end
      result
    end

    def build_and_publish(name, owner:, timestamp:)
      Eventer::Messages::IncrementCounter.build(name, owner: owner, timestamp: timestamp).each do |msg|
        publish(*msg)
      end
    end

    def report_error(error, schema:)
      GitHub.dogstats.increment("hydro_client.publish_error", tags: [
        "schema:#{schema}",
        "error:#{error.class.name.underscore}",
      ])

      Failbot.report(error, {
        app: "github-eventer",
        areas_of_responsibility: [:analytics],
        schema: schema,
      })
    end

    class MissingTimeStampError < ArgumentError; end

    def report_missing_timestamp(payload, event_type)
      attrs = {
        app: "github-eventer",
        event_type: event_type,
      }.merge(payload)
      err = MissingTimeStampError.new("Missing timestamp in message")
      err.set_backtrace(caller)
      Failbot.report(err, attrs)
    end

    def subscribe(pattern, &block)
      @instrumenter.subscribe(pattern) do |event, _, _, _, payload|
        if GitHub.hydro_enabled?
          GitHub.dogstats.time("hydro_client.subscriber_time", {
            tags: ["event:#{event}", @role_tag],
            sample_rate: 0.1,
          }) do
            begin
              yield payload, event
            rescue => e
              if Rails.env.production?
                Failbot.report(e, app: "github-eventer")
              else
                puts "Failed to encode eventer message: #{e.message}"
                raise(e)
              end
            end
          end
        end
      end
    end
  end
end

Eventer::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("issue.create") do |payload|
    name = timestamp = nil

    if payload[:issue].pull_request_id.nil?
      name = GitHub::OrgInsights::Metric::ISSUE_CREATED
      timestamp = payload[:issue].contribution_time
    elsif payload[:pull_request]
      name = GitHub::OrgInsights::Metric::PULL_REQUEST_CREATED
      timestamp = payload[:pull_request].contribution_time
    end

    repo = payload[:repository]
    org = repo.organization

    if name.present? && org.present? && org.insights_enabled?
      if timestamp.present?
        build_and_publish(name, owner: [repo, org], timestamp: timestamp)
      else
        report_missing_timestamp(payload, name)
      end
    end
  end

  subscribe("issue.transform_to_pull") do |payload|
    repo = payload[:repository]
    org = repo.organization

    if org.present? && org.insights_enabled?
      name = GitHub::OrgInsights::Metric::PULL_REQUEST_CREATED
      timestamp = payload[:pull_request].contribution_time

      if timestamp.present?
        build_and_publish(name, owner: [repo, org], timestamp: timestamp)
      else
        report_missing_timestamp(payload, name)
      end
    end
  end

  subscribe("issue.events.merged") do |payload|
    repo = payload[:repository]
    org = repo.organization

    if org.present? && org.insights_enabled?
      name = GitHub::OrgInsights::Metric::PULL_REQUEST_MERGED
      timestamp = payload[:pull_request].merged_at

      if timestamp.present?
        build_and_publish(name, owner: [repo, org], timestamp: timestamp)
      else
        report_missing_timestamp(payload, name)
      end
    end
  end

  subscribe("issue.events.closed") do |payload|
    repo = payload[:repository]
    org = repo.organization
    issue = payload[:issue]
    pr = payload[:pull_request]

    if org.present? && org.insights_enabled? && (!pr || !pr.merged?)
      timestamp = issue.closed_at

      name =
        if pr
          GitHub::OrgInsights::Metric::PULL_REQUEST_CLOSED
        else
          GitHub::OrgInsights::Metric::ISSUE_CLOSED
        end

      if timestamp.present?
        build_and_publish(name, owner: [repo, org], timestamp: timestamp)
      else
        report_missing_timestamp(payload, name)
      end
    end
  end

  subscribe("pull_request_review.create") do |payload|
    repo = payload[:repository]
    org = repo.organization
    if org.present? && org.insights_enabled?
      name = GitHub::OrgInsights::Metric::PULL_REQUEST_REVIEWED
      timestamp = payload[:review].created_at

      if timestamp.present?
        build_and_publish(name, owner: [repo, org], timestamp: timestamp)
      else
        report_missing_timestamp(payload, name)
      end
    end
  end

  subscribe("commit.create") do |payload|
    repo = payload[:repository]
    org = repo.organization

    if org.present? && org.insights_enabled?
      name = GitHub::OrgInsights::Metric::COMMIT_CREATED
      timestamp = payload[:contributed_on]

      if timestamp.present?
        build_and_publish(name, owner: [repo, org], timestamp: timestamp)
      else
        report_missing_timestamp(payload, name)
      end
    end
  end

  subscribe("integration_installation.create") do |payload|
    installation = IntegrationInstallation.find(payload[:installation_id])
    name = GitHub::OrgInsights::Metric::INTEGRATION_INSTALLED
    timestamp = installation.created_at

    if timestamp.present?
      build_and_publish(name, owner: [payload[:integration]], timestamp: timestamp)
    else
      report_missing_timestamp(payload, name)
    end
  end

  subscribe("oauth_authorization.create") do |payload|
    oauth_authorization = payload[:oauth_authorization]
    name = GitHub::OrgInsights::Metric::OAUTH_AUTHORIZATION_CREATED
    timestamp = oauth_authorization.created_at

    if timestamp.present?
      build_and_publish(name, owner: [oauth_authorization.application], timestamp: timestamp)
    else
      report_missing_timestamp(payload, name)
    end
  end
end
