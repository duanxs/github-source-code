# rubocop:disable Style/FrozenStringLiteralComment

GitHub.subscribe "call_filter.html_pipeline" do |event, start, ending, transaction_id, payload|
  filter = payload[:filter].split("::").last

  # Republish using a filter-specific event name so that the time spent in this
  # filter will be tracked by GitHub::RequestTimer.
  GitHub.publish("#{filter.underscore}.call_filter.html_pipeline", start, ending, transaction_id, payload)
end
