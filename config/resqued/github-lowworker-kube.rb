# rubocop:disable Style/FrozenStringLiteralComment

# Configuration for lowworker deployment in Kubernetes. Aqueduct only,
# fallback resque workers run outside of Kubernetes on
# github-lowworker hosts. Loads after github-environment.rb.

GitHub.role = :lowworker_kube

# Provide aqueduct.workers.count metrics
before_fork { Resqued::MetricsServer.start }

# Drop a file on disk to indicate that the workers are ready.
after_fork { |worker| Resqued::WorkerReadiness.report_ready }

# Kubernetes only runs Aqueduct workers. Backup resque workers run on
# legacy worker nodes.
worker_pool ENV.fetch("RESQUED_WORKER_POOL_SIZE", 10).to_i, shuffle_queues: true
worker_factory { |queues| Resqued::AqueductWorkerAdapter.new(*queues) }

queue "boomtown"

queue "fix_duplicate_labels"
queue "kubernetes_analytics"
queue "rebalance_milestone"
queue "rebalance_project_column"
queue "saved_reply_instrumentation"
queue "security_analysis_settings"
queue "task_list_instrumentation"
