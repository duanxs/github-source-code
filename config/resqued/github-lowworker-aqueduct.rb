# rubocop:disable Style/FrozenStringLiteralComment

# Configuration github-lowworker hosts. Aqueduct only, fallback resque
# workers are defined in github-lowworker.rb Loads after
# github-environment.rb.

GitHub.role = :lowworker

# The WorkerPoolConfig divides workers between resque and aqueduct depending on
# feature flag rollout. If 25% of jobs are sent to aqueduct via feature flag,
# 25% of the worker pool will be allocated to aqueduct.
worker_pool_config = Resqued::WorkerPoolConfig.new(
  # Which type of worker is this, aqueduct or resque?
  worker_type: Resqued::WorkerPoolConfig::AQUEDUCT_WORKER,
  # The app-role determines the rollout feature flag name.
  app_role: :github_lowworker,
  # The total number of aqueduct and resque workers to run on the host.
  total_pool_size: GitHub.environment.fetch("RESQUED_LOW_WORKERS", 20).to_i + 1,
  # The minimum number of resque workers to run, regardless of pool allocation.
  min_resque_workers: 1,
  # The minimum number of aqueduct workers to run, regardless of pool allocation.
  min_aqueduct_workers: 1,
)

# Apply the worker pool config to this resqued config, which toggles the aqueduct
# resqued adapter when necessary and adds a hook to restart workers if the
# feature flag controlling pool allocation changes.
worker_pool_config.apply(self)

# Configure worker pool sizes for aqueduct and resque.
if worker_pool_config.aqueduct_worker?
  worker_pool worker_pool_config.aqueduct_workers, shuffle_queues: true
else
  worker_pool worker_pool_config.resque_workers, shuffle_queues: true
end

# Added in haste due to an outage 5/1/16
queue "spam"

queue "low"
queue "analytics"
queue "billing"
queue "languages"
queue "mailchimp"
queue "lfs"
queue "repository_disk_usage"
queue "repository_purge"
queue "dgit_repairs"
queue "dgit_disk_stats"
queue "dgit_cold_storage_sweeper"
queue "rebalance_memex_project"
# A collab cluster performance regression and excessive contributions_* job queueing led to an
# incident on 09-24-2019 where slow contributions_* jobs consumed all the lowworker capacity and
# blocked critical lowworker jobs e.g. notifications. We limit the `count` here so that
# slow backfill jobs can only consume a portion of overall worker capacity.
# See https://github.com/github/availability/issues/1155#issuecomment-534632097
queue "contributions_backfill", count: 5
queue "contributions_track_push", count: 5
queue "purge_restorables"
queue "retry_jobs"
queue "cdn"
queue "user_contributions_backfill"
queue "site_stats"
queue "stars"
queue "storage_cluster"
queue "delete_unconfirmed_account_recovery_tokens"
queue "pull_request_review_stats"
queue "toggle_hidden_user_in_notifications"
queue "cancel_team_membership_requests"
queue "change_network_root"
queue "open_source_maintainer_contributions"
queue "aws_log_scanning"
queue "repair_abilities_from_users_to_ancestor_teams"
queue "calculate_stars_count"
queue "slotted_counters"
queue "pages_https"
queue "public_key_accesses"
queue "oauth_authorization_bumps"
queue "oauth_access_bumps"
queue "experiments"
queue "expire_enterprise_sessions"
queue "project_workflows"
queue "project_workflow_resync"
queue "zuora"
queue "archive_dangling_forks"
queue "github_connect"
queue "sendgrid_suppression_removal"
queue "devtools"
queue "event"
queue "remove_expired_oauth"
queue "git_signing"
queue "token_scanning"
queue "token_scanning_scheduler"
queue "token_path_discovery"
queue "token_scanning_config_change"
queue "update_event_feeds", count: 12
queue "background_destroy", count: 3 # let a few workers pitch in on this queue too
queue "process_unfurls"
queue "disable_repository_interaction_limits"
queue "mailers"
queue "assign_stafftools_access"
queue "repository_set_license"
queue "marketplace"
queue "mirrors"
queue "partial_two_factor_authentication_notification"
queue "create_auto_inactive_deployment_statuses"
queue "sample_scheduled_job"
queue "import_issue"
queue "org_insights_backfill"
queue "deliver_repository_push_notifications"
queue "integration_installation_instrumentation"
queue "integration_manifests"
queue "contributions_instrument_push"
queue "aqueduct_test"
queue "enforce_active_user_session_limit"
queue "invalidate_content_attachments_cache"
queue "newsletter_delivery_run"

queue "notifications"
queue "deliver_notifications"
# this queue can have sizeable backlogs, so only allow
# approximately 20 percent of workers to work this queue
queue "deliver_notifications_low", count: 1
queue "notifications_maintenance", count: 1

queue "science_event_cleanup"
queue "sync_enterprise_server_user_accounts"
queue "add_to_suppression_list"
queue "calculate_followerings_count"
queue "calculate_trending_repos"
queue "calculate_trending_users"
queue "chatterbox_notification"
queue "clear_soft_bounces"
queue "core_receive"
queue "delayed"
queue "docusign"
queue "email_reply"
queue "calculate_topic_applied_counts"
queue "email"
queue "email_unsubscribe"
queue "feature_showcases"
queue "gist_flagged_by_user"
queue "gist_maintenance"
queue "gist_maintenance_scheduler"
queue "ignore_user"
queue "migration_destroy_file"
queue "migration_enqueue_destroy_file_jobs"
queue "move_repo_downloads"
queue "repository_access"
queue "network_maintenance"
queue "network_maintenance_scheduler"
queue "newsies_auto_subscribe"
queue "newsletter_delivery"
queue "nexmo_balance_stats"
queue "oauth_app_policy_usage_stats"
queue "organization_application_access_approved"
queue "organization_application_access_requested"
queue "otp_sms_timing_cleanup"
queue "post_mirror_hook"
queue "publish_vulnerability"
queue "post_receive"
queue "pre_receive_environment_delete"
queue "pre_receive_environment_download"
queue "pre_receive_repository_update"
queue "purge_archived_assets"
queue "purge_stale_render_blobs"
queue "rebuild_storage_usage"
queue "remove_from_suppression_list"
queue "repository_fsck"
queue "repository_mirror"
queue "repository_network_operations"
queue "repository_purge_archived"
queue "repository_sync"
queue "sync_asset_usage"
queue "synchronize_plan_subscription"
queue "update_team_sync_for_business_organization"
queue "user_signup_followup"
queue "wiki_maintenance"
queue "wiki_maintenance_scheduler"
queue "stripe"
queue "interactive_components"
queue "stale_check_runs"
queue "sponsors_application_processing"
queue "audit_log_parse_query"
queue "mass_install_automatic_integrations"
queue "qintel_import_new_files"
queue "qintel_import_file"
queue "actions"
queue "metered_billing"
queue "vulnerability_notification", count: 10
queue "business_membership_cleanup"
queue "reminders"
queue "rewrite_mannequin_associations"
queue "convert_labelled_issues_to_discussions"
queue "sponsors_activity"
queue "roll_earthsmoke_keys"
queue "recalculate_user_discussions"
queue "business_user_accounts"
queue "code_scanning"
queue "invalidate_expired_invites"
queue "two_factor_recovery"
queue "trade_controls"
queue "codespaces"
queue "process_mentioned_references"
queue "sponsorships_export"
queue "revoke_oauth_accesses"
queue "complete_sponsors_goal"
queue "scan_uploads"
queue "audit_log_export"
queue "remove_stale_oauth"
queue "remove_stale_public_keys"
queue "remove_stale_authentication_records"
queue "remove_stale_authenticated_devices"
queue "purge_flagged_uploads"

if worker_pool_config.evenly_split?
  worker "background_destroy"
elsif worker_pool_config.aqueduct_worker? && worker_pool_config.aqueduct_majority?
  2.times { worker "background_destroy" }
elsif worker_pool_config.resque_worker? && worker_pool_config.resque_majority?
  2.times { worker "background_destroy" }
end
