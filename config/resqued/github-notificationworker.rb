# rubocop:disable Style/FrozenStringLiteralComment

# Configuration for notificationworker deployment in Kubernetes. Resque
# only. Loads after github-environment.rb.

GitHub.role = ENV.fetch("RESQUED_WORKER_ROLE", "notificationworker").intern

after_fork do |worker|
  # Both `cant_fork` and `graceful_term` must be `true` in order for jobs to
  # handle SIGTERM signals gracefully.
  worker.cant_fork = true
  worker.graceful_term = true
end

pool_size = ENV["RESQUED_WORKER_POOL_SIZE"].to_i
pool_size = 1 if pool_size < 1
worker_pool pool_size, shuffle_queues: true

queue "deliver_mobile_push_notifications"
queue "kubernetes_deliver_notifications"
queue "kubernetes_deliver_notifications_low"
