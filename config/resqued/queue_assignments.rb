# frozen_string_literal: true

# A comprehensive list of which dotcom queues are assigned to worker hosts. Supports the
# resque to aqueduct transition, which ramps up aqueduct job enqueues and worker pool size by
# worker app-role. Feel free to stop by #aqueduct with any questions.
module Resque
  module QueueAssignments
    QUEUES_BY_WORKER = {
      "github-lowworker" => %w{
        actions
        add_to_suppression_list
        analytics
        kubernetes_analytics
        aqueduct_test
        archive_dangling_forks
        assign_stafftools_access
        audit_log_export
        audit_log_parse_query
        aws_log_scanning
        background_destroy
        billing
        boomtown
        business_membership_cleanup
        business_user_accounts
        calculate_followerings_count
        calculate_stars_count
        calculate_topic_applied_counts
        calculate_trending_repos
        calculate_trending_users
        cancel_team_membership_requests
        cdn
        change_network_root
        chatterbox_notification
        clear_soft_bounces
        code_scanning
        codespaces
        complete_sponsors_goal
        contributions_backfill
        contributions_instrument_push
        contributions_track_push
        convert_labelled_issues_to_discussions
        core_receive
        create_auto_inactive_deployment_statuses
        delayed
        delete_unconfirmed_account_recovery_tokens
        deliver_notifications
        deliver_notifications_low
        deliver_repository_push_notifications
        devtools
        dgit_cold_storage_sweeper
        dgit_disk_stats
        dgit_repairs
        disable_repository_interaction_limits
        docusign
        email
        email_reply
        email_unsubscribe
        enforce_active_user_session_limit
        event
        experiments
        expire_enterprise_sessions
        feature_showcases
        fix_duplicate_labels
        gist_flagged_by_user
        gist_maintenance
        gist_maintenance_scheduler
        git_signing
        github_connect
        ignore_user
        import_issue
        integration_installation_instrumentation
        integration_manifests
        interactive_components
        invalidate_content_attachments_cache
        invalidate_expired_invites
        languages
        lfs
        low
        mailchimp
        mailers
        marketplace
        mass_install_automatic_integrations
        metered_billing
        migration_destroy_file
        migration_enqueue_destroy_file_jobs
        mirrors
        move_repo_downloads
        network_maintenance
        network_maintenance_scheduler
        newsies_auto_subscribe
        newsletter_delivery
        newsletter_delivery_run
        nexmo_balance_stats
        notifications
        notifications_maintenance
        oauth_access_bumps
        oauth_app_policy_usage_stats
        oauth_authorization_bumps
        open_source_maintainer_contributions
        org_insights_backfill
        organization_application_access_approved
        organization_application_access_requested
        otp_sms_timing_cleanup
        pages_https
        partial_two_factor_authentication_notification
        post_mirror_hook
        post_receive
        pre_receive_environment_delete
        pre_receive_environment_download
        pre_receive_repository_update
        process_mentioned_references
        process_unfurls
        project_workflow_resync
        project_workflows
        public_key_accesses
        publish_vulnerability
        pull_request_review_stats
        purge_archived_assets
        purge_flagged_uploads
        purge_restorables
        purge_stale_render_blobs
        qintel_import_file
        qintel_import_new_files
        rebalance_memex_project
        rebalance_milestone
        rebalance_project_column
        rebuild_storage_usage
        recalculate_user_discussions
        reminders
        remove_expired_oauth
        remove_from_suppression_list
        remove_stale_authenticated_devices
        remove_stale_authentication_records
        remove_stale_oauth
        remove_stale_public_keys
        repair_abilities_from_users_to_ancestor_teams
        repository_access
        repository_disk_usage
        repository_fsck
        repository_mirror
        repository_network_operations
        repository_purge
        repository_purge_archived
        repository_set_license
        repository_sync
        retry_jobs
        revoke_oauth_accesses
        rewrite_mannequin_associations
        roll_earthsmoke_keys
        sample_scheduled_job
        saved_reply_instrumentation
        scan_uploads
        science_event_cleanup
        security_analysis_settings
        sendgrid_suppression_removal
        site_stats
        slotted_counters
        spam
        sponsors_activity
        sponsors_application_processing
        sponsorships_export
        stale_check_runs
        stars
        storage_cluster
        stripe
        sync_asset_usage
        sync_enterprise_server_user_accounts
        synchronize_plan_subscription
        task_list_instrumentation
        toggle_hidden_user_in_notifications
        token_path_discovery
        token_scanning
        token_scanning_config_change
        token_scanning_scheduler
        trade_controls
        two_factor_recovery
        update_event_feeds
        update_team_sync_for_business_organization
        user_contributions_backfill
        user_signup_followup
        vulnerability_notification
        wiki_maintenance
        wiki_maintenance_scheduler
        zuora
      },
      "github-highworker" => %w{
        archive_project
        archive_project_cards
        cancel_repo_invitation
        clear_abilities
        clear_graph_data_on_block
        clear_graph_data_on_spammy
        clone_template_repo_files
        commit_upload_manifest
        convert_to_outside_collaborator
        create_check_suites
        create_pull_request_merge_commit
        critical
        delete_dependent_abilities
        deliver_integration_update_email
        destroy_integration
        destroy_team
        destroy_team_dependants
        enforce_two_factor_requirement
        gist_push
        handle_team_parent_changes
        high
        import_project
        install_automatic_integrations
        integration_installation_repository_removal
        invite_collaborator_to_repository
        issue_triage
        maintain_tracking_ref
        migrate_legacy_admin_teams
        minimize_all_comments_on_repo
        notify_subscription_status_change
        populate_cloned_project
        pull_request_synchronization
        remove_biz_user_forks
        remove_invalid_user_forks
        remove_noncollab_assignees_from_issue
        remove_oauth_user_tokens
        remove_org_member_data
        remove_org_member_package_access
        remove_outside_collaborator_from_organization
        remove_unlinked_saml_members_from_organization
        remove_user_from_org
        remove_repo_member
        remove_team_from_repo
        repository_add_teams
        repository_push
        request_pr_reviewers
        restore_organization_user
        retry_transfer_issue
        revoke_org_apps_management_grants
        revoke_org_membership_abilties
        saml_session_revoke
        signup_emails
        staff_tools_report
        subscribe_and_notify
        sync_organization_default_repository_permission
        sync_scoped_integration_installations
        synchronize_pull_request
        team_add_forks
        team_member_added_emails
        team_remove_members
        team_update_forked_repository_permissions
        transfer_discussion
        transfer_issue
        transfer_repository
        transform_user_into_org
        uninstall_integration_installation
        unlink_project_repo_links
        update_close_issue_references
        update_integration_installation_rate_limit
        update_member_repo_permissions
        update_invitation_repo_permissions
        update_team_repo_permissions
        update_organization_routing_to_eligible_email
        update_organization_team_privacy
        update_table_user_hidden
        upgrade_integration_installation_version
        user_delete
        user_signup
        user_suspend
        user_suspend_dormant
        wiki_receive
        workspace_repository_abilities
        zendesk
        convert_to_discussion
      },
      "github-backupworker" => %w{
        gitbackups_backfill
        gitbackups_encryption
        gitbackups_perform
        gitbackups_scheduler
        gitbackups_sweeper
      },
      "github-depworker" => %w{
        dependabot
        example
        manifest_backfill
        repository_dependencies
        vulnerability_identification
      },
      "github-graphworker" => %w{
        graphs
        netgraph
      },
      "github-slowworker" => %w{
        archive_restore
        audit_logs
        detect_community_health_files
        dgit_schedulers
        dotcom_importer
        gh_migrator
        index_bulk
        index_high
        index_low
        map_import_records
        oauth_application_delete
        prepare_import_archive
        remove_integration_tokens
        remove_oauth_app_tokens
        remove_repo_keys_for_policymaker_and_app
        toggle_spammy_notifications
      },
      "github-notificationworker" => %w{
        deliver_mobile_push_notifications
        kubernetes_deliver_notifications
        kubernetes_deliver_notifications_low
      },
      "gitbackups-maintenance" => %w{
        gitbackups_maintenance
      },
      "github-hookworker" => %w{
        deliver_hook_event
        hook_delivery_race_condition_check
        post_to_hookshot
        post_hooks_to_aqueduct
        redeliver_hooks
      },
      "github-pagesworker" => %w{
        dpages_evacuations
        dpages_evacuations_scheduler
        dpages_maintenance
        dpages_maintenance_scheduler
        pages-docker
      },
    }

    # For a given queue, look up the assigned worker app-role.
    def self.app_role_for_queue(queue)
      @index ||= build_index

      # Special case for jobs enqueued on staff hosts.
      if staff_host? && !GitHub.dynamic_lab?
        return "github-staff"
      end

      if app_role = @index[queue]
        return app_role
      end

      # Special case for github-dfs queues, which are dynamically generated from
      # the hostname.
      if queue.starts_with?("maint_")
        return "github-dfs"
      end
    end

    # Generate a map of queue names to worker app-roles.
    def self.build_index
      QUEUES_BY_WORKER.flat_map do |app_role, queues|
        queues.map do |queue|
          [queue, app_role]
        end
      end.to_h
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def self.staff_host?
      @staff_host ||= GitHub.staff_host?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  end
end
