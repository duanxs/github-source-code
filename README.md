# GitHub

This is GitHub.com and GitHub Enterprise.

## Documentation

The latest documentation is in the [Engineering Guide](https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/). The docs include information about day-to-day development, for example [database migrations][migrations] and [testing][testing].

[migrations]: https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/migrations-and-transitions/database-migrations-for-dotcom/
[testing]: https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/testing/overview/

Historical documentation is [in GitHubber](https://githubber.com/category/technology/dotcom). The docs
include information about how dotcom developers and code are organized, for example [getting started][start],
[process and philosophy][process], and [tech stack][stack].

[start]: https://githubber.com/category/technology/dotcom#getting-started
[process]: https://githubber.com/category/technology/dotcom#process-philosophy-and-people
[stack]: https://githubber.com/category/technology/dotcom#the-githubcom-tech-stack

## Areas of Responsibility

We use [Areas of Responsibility](https://githubber.com/article/technology/dotcom/areas-of-responsibility)
when thinking about our application.
